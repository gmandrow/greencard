package com.aspirity.greencard;

import android.app.Application;
import android.content.SharedPreferences;

import com.aspirity.greencard.data.db.DatabaseFactory;
import com.aspirity.greencard.data.network.RetrofitService;
import com.aspirity.greencard.data.prefs.DefaultPreferenceDataManager;
import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.presentation.utils.DateUtil;
import com.aspirity.greencard.presentation.utils.DecimalFormatter;

import androidx.appcompat.app.AppCompatDelegate;

public class GreenCardApplication extends Application {
    public final static String PREFERENCES = "cardGreenPrefs";
    private SharedPreferences sharedPreferences;
    private PreferenceDataManager preferenceDataManager;
    private RetrofitService retrofitService;
    private DateUtil dateUtil;
    private DecimalFormatter decimalFormatter;

    @Override
    public void onCreate() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        sharedPreferences = getSharedPreferences(PREFERENCES, MODE_PRIVATE);
        preferenceDataManager = new DefaultPreferenceDataManager(sharedPreferences);
        retrofitService = new RetrofitService(preferenceDataManager);

        dateUtil = new DateUtil();
        decimalFormatter = new DecimalFormatter();

        super.onCreate();
        DatabaseFactory.initDatabase(this);
    }

    public PreferenceDataManager getPreferenceDataManager() {
        return preferenceDataManager;
    }

    public DateUtil getDateUtils() {
        return dateUtil;
    }

    public DecimalFormatter getDecimalFormatter() {
        return decimalFormatter;
    }
}
