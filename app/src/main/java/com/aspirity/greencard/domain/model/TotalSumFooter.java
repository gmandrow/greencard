package com.aspirity.greencard.domain.model;

public class TotalSumFooter {

    private String totalSum;
    private String totalAccruedBonuses;
    private String totalDebitedBonuses;

    public TotalSumFooter(String totalSum, String totalAccruedBonuses, String totalDebitedBonuses) {
        this.totalSum = totalSum;
        this.totalAccruedBonuses = totalAccruedBonuses;
        this.totalDebitedBonuses = totalDebitedBonuses;
    }

    public String getTotalSum() {
        return totalSum;
    }

    public String getTotalAccruedBonuses() {
        return totalAccruedBonuses;
    }

    public String getTotalDebitedBonuses() {
        return totalDebitedBonuses;
    }
}
