package com.aspirity.greencard.domain.model;

public class Product {

    private int positionNumber;
    private Long id;
    private String name;
    private Double count;
    private String price;
    private String totalPrice;
    //    private String sum;
    private String accruedBonuses;
    private String debitedBonuses;

    public Product(
            String name,
            Double count,
            String price,
            String totalPrice,
            String accruedBonuses,
            String debitedBonuses) {
        this.name = name;
        this.count = count;
        this.price = price;
//        this.sum = sum;
        this.totalPrice = totalPrice;
        this.accruedBonuses = accruedBonuses;
        this.debitedBonuses = debitedBonuses;
    }

    public String getName() {
        return name;
    }

    public Double getCount() {
        return count;
    }

    public String getPrice() {
        return price;
    }

//    public String getSum() {
//        return sum;
//    }

    public String getAccruedBonuses() {
        return accruedBonuses;
    }

    public String getDebitedBonuses() {
        return debitedBonuses;
    }

    public int getPositionNumber() {
        return positionNumber;
    }

    public String getTotalPrice() {
        return totalPrice;
    }
}
