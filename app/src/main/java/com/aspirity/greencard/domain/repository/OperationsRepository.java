package com.aspirity.greencard.domain.repository;


import com.aspirity.greencard.domain.model.Chunk;
import com.aspirity.greencard.domain.model.Operation;

import java.util.List;

import io.reactivex.Single;

public interface OperationsRepository {

    Single<Chunk<Operation>> operations(
            Integer page,
            String beginDate,
            String endDate,
            String sort,
            List<Long> vendors);

    Single<Chunk<Operation>> operationsFromDb(int page, int pageSize);

    Single<Operation> operation(String uniqueId);

    Single<Operation> operationForceRefresh(String uniqueId);

    Single<Operation> operationFromDb(String uniqueId);
}
