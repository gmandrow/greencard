package com.aspirity.greencard.domain.model;

import java.util.List;

/**
 * Created by namtarr on 21.12.2017.
 */

public class Chunk<T> {

    private List<T> items;
    private int count;

    public Chunk() {
    }

    public Chunk(List<T> items, int count) {
        this.items = items;
        this.count = count;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
