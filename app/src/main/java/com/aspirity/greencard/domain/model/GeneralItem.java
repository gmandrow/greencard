package com.aspirity.greencard.domain.model;

public class GeneralItem<T> extends ListItem {

    private final int contentType;
    private final T data;

    public GeneralItem(T data, int contentType) {
        this.data = data;
        this.contentType = contentType;
    }

    public T getData() {
        return data;
    }

    @Override
    public int getType() {
        return contentType;
    }
}
