package com.aspirity.greencard.domain.usecase;


import android.content.Context;

import com.aspirity.greencard.domain.model.Card;
import com.aspirity.greencard.domain.model.ValidCard;
import com.aspirity.greencard.domain.model.VirtualCard;
import com.aspirity.greencard.domain.repository.CardsRepository;
import com.aspirity.greencard.domain.usecase.base.UseCase;
import com.aspirity.greencard.presentation.utils.Network;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.rx.RxUtil;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;

import java.util.List;

import io.reactivex.Single;

public class CardsUseCase extends UseCase {

    private final Context context;
    private final SchedulerProvider schedulerProvider;
    private CardsRepository cardsRepository;

    public CardsUseCase(
            Context context,
            SchedulerProvider schedulerProvider,
            CardsRepository cardsRepository) {
        this.context = context;
        this.schedulerProvider = schedulerProvider;
        this.cardsRepository = cardsRepository;
    }

    public Single<ValidCard> validateCard(String cardNumber, String cardCode) {
        if (Network.isConnectedToNetwork(context))
            return cardsRepository.validateCard(cardNumber, cardCode)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Boolean> verifyAttachedCard(Long userId, String smsCode) {
        if (Network.isConnectedToNetwork(context))
            return cardsRepository.verifyAttachedCard(userId, smsCode)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<String> attachCard(Long userId) {
        if (Network.isConnectedToNetwork(context))
            return cardsRepository.attachCard(userId)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Boolean> cardChangeState(String password, Long cardId) {
        if (Network.isConnectedToNetwork(context))
            return cardsRepository.cardChangeState(cardId, password)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<List<Card>> getCards(boolean forceRefresh) {
        if (Network.isConnectedToNetwork(context)) {
            if (forceRefresh)
                return cardsRepository.getCardsForceRefresh()
                        .compose(RxUtil.applySingleSchedulers(schedulerProvider));

            return cardsRepository.getCards()
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        } else
            return Single.error(new NetworkException());
    }

    public Single<List<Card>> getCardsFallback() {
        return cardsRepository.getCardsFromDb()
                .compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }

    public Single<VirtualCard> getVirtualCard() {
        if (Network.isConnectedToNetwork(context))
            return cardsRepository.getVirtualCard()
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }
}
