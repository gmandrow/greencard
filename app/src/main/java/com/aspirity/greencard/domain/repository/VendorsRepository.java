package com.aspirity.greencard.domain.repository;


import com.aspirity.greencard.domain.model.Vendor;

import java.util.List;

import io.reactivex.Single;

public interface VendorsRepository {

    Single<List<Vendor>> vendors();

    Single<List<Vendor>> vendorsForceRefresh();

    Single<List<Vendor>> vendorsFromDb();

    Single<List<Vendor>> vendorsOfPartner(long partnerId);

    Single<List<Vendor>> vendorsOfPartnerForceRefresh(long partnerId);

    Single<List<Vendor>> vendorsOfPartnerFromDb(long partnerId);
}
