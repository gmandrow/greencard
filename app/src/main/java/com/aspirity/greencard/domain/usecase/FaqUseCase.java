package com.aspirity.greencard.domain.usecase;


import android.content.Context;

import com.aspirity.greencard.domain.model.Faq;
import com.aspirity.greencard.domain.repository.FaqRepository;
import com.aspirity.greencard.domain.usecase.base.UseCase;
import com.aspirity.greencard.presentation.utils.Network;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.rx.RxUtil;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;

import java.util.List;

import io.reactivex.Single;

public class FaqUseCase extends UseCase {

    private final Context context;
    private final SchedulerProvider schedulerProvider;
    private FaqRepository faqRepository;

    public FaqUseCase(
            Context context,
            SchedulerProvider schedulerProvider,
            FaqRepository faqRepository) {
        this.context = context;
        this.schedulerProvider = schedulerProvider;
        this.faqRepository = faqRepository;
    }

    public Single<List<Faq>> getFaq() {
        if (Network.isConnectedToNetwork(context)) {
            return faqRepository.faq()
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        } else
            return Single.error(new NetworkException());
    }

    public Single<List<Faq>> getFaqFallback() {
        return faqRepository.faqFromDb()
                .compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }
}
