package com.aspirity.greencard.domain.repository;

import com.aspirity.greencard.domain.model.Chunk;
import com.aspirity.greencard.domain.model.Notification;

import io.reactivex.Single;

public interface NotificationsRepository {
    Single<Chunk<Notification>> notifications(String token);

    Single<Void> notificationsStatus(Long notification_id, String token, String status);
}
