package com.aspirity.greencard.domain.model;


import androidx.annotation.Nullable;

public class AttachedCard {

    @Nullable
    private Boolean isAttached;

    @Nullable
    private String smsSentTo;

    public AttachedCard() {
    }

    public AttachedCard(@Nullable Boolean isAttached, @Nullable String smsSentTo) {
        this.isAttached = isAttached;
        this.smsSentTo = smsSentTo;
    }

    @Nullable
    public Boolean isAttached() {
        return isAttached;
    }

    public void setIsAttached(@Nullable Boolean isAttached) {
        this.isAttached = isAttached;
    }

    @Nullable
    public String getSmsSentTo() {
        return smsSentTo;
    }

    public void setSmsSentTo(@Nullable String smsSentTo) {
        this.smsSentTo = smsSentTo;
    }
}
