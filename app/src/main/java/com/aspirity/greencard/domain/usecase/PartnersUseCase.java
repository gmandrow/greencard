package com.aspirity.greencard.domain.usecase;


import android.content.Context;

import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.domain.repository.PartnerRepository;
import com.aspirity.greencard.domain.usecase.base.UseCase;
import com.aspirity.greencard.presentation.utils.Network;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.rx.RxUtil;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleSource;

public class PartnersUseCase extends UseCase {

    private final Context context;
    private final SchedulerProvider schedulerProvider;
    private PartnerRepository partnerRepository;

    public PartnersUseCase(
            Context context,
            SchedulerProvider schedulerProvider,
            PartnerRepository partnerRepository) {
        this.context = context;
        this.schedulerProvider = schedulerProvider;
        this.partnerRepository = partnerRepository;
    }

    public Single<List<Partner>> getPartners() {
        if (Network.isConnectedToNetwork(context)) {
            return partnerRepository.partners()
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        } else
            return Single.error(new NetworkException());
    }

    public SingleSource<List<Partner>> getPartnersFallback() {
        return partnerRepository.partnersFromDb()
                .compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }

    public Single<Partner> getPartner(Long partnerId) {
        if (Network.isConnectedToNetwork(context)) {
            return partnerRepository.partner(partnerId)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        } else
            return Single.error(new NetworkException());
    }

    public SingleSource<Partner> getPartnerFallback(Long partnerId) {
        return partnerRepository.partnerFromDb(partnerId)
                .compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }
}
