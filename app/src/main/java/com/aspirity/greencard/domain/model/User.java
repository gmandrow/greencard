package com.aspirity.greencard.domain.model;

public class User {

    private Long id;
    private String name;
    private String fullName;
    private String phone;
    private String bonuses;
    private String status;
    private String bonusesForNextStatus;
    private String cardNumber;
    private boolean subscribedToPushOperations;
    private boolean subscribedToPush;
    private boolean subscribedToEmail;
    private boolean subscribedToSms;
    private boolean isActive;


    public User(
            Long id,
            String name,
            String fullName,
            String phone,
            String bonuses,
            String status,
            String bonusesForNextStatus,
            String cardNumber,
            boolean subscribedToPushOperations,
            boolean subscribedToPush,
            boolean subscribedToEmail,
            boolean subscribedToSms,
            boolean isActive) {
        this.id = id;
        this.name = name;
        this.fullName = fullName;
        this.phone = phone;
        this.bonuses = bonuses;
        this.status = status;
        this.bonusesForNextStatus = bonusesForNextStatus;
        this.cardNumber = cardNumber;
        this.subscribedToPushOperations = subscribedToPushOperations;
        this.subscribedToPush = subscribedToPush;
        this.subscribedToEmail = subscribedToEmail;
        this.subscribedToSms = subscribedToSms;
        this.isActive = isActive;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public String getBonuses() {
        return bonuses;
    }

    public void setBonuses(String bonuses) {
        this.bonuses = bonuses;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBonusesForNextStatus() {
        return bonusesForNextStatus;
    }

    public void setBonusesForNextStatus(String bonusesForNextStatus) {
        this.bonusesForNextStatus = bonusesForNextStatus;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public boolean isSubscribedToPush() {
        return subscribedToPush;
    }

    public void setSubscribedToPush(boolean subscribedToPush) {
        this.subscribedToPush = subscribedToPush;
    }

    public boolean isSubscribedToPushOperations() {
        return subscribedToPushOperations;
    }

    public void setSubscribedToPushOperations(boolean subscribedToPushOperations) {
        this.subscribedToPushOperations = subscribedToPushOperations;
    }

    public boolean isSubscribedToEmail() {
        return subscribedToEmail;
    }

    public void setSubscribedToEmail(boolean subscribedToEmail) {
        this.subscribedToEmail = subscribedToEmail;
    }

    public boolean isSubscribedToSms() {
        return subscribedToSms;
    }

    public void setSubscribedToSms(boolean subscribedToSms) {
        this.subscribedToSms = subscribedToSms;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
