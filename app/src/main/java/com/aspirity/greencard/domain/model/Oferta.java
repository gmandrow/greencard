package com.aspirity.greencard.domain.model;


public class Oferta {

    private String title;
    private String subHeader;
    private String content;

    public Oferta() {
    }

    public Oferta(String title, String subHeader, String content) {
        this.title = title;
        this.subHeader = subHeader;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
