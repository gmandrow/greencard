package com.aspirity.greencard.domain.model;

/**
 * Created by namtarr on 29.12.2017.
 */

public class OperationChunk extends Chunk<ListItem> {

    private int chunkSize;

    public int getChunkSize() {
        return chunkSize;
    }

    public void setChunkSize(int chunkSize) {
        this.chunkSize = chunkSize;
    }
}
