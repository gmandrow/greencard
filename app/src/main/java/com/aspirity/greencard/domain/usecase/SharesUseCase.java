package com.aspirity.greencard.domain.usecase;


import android.content.Context;

import com.aspirity.greencard.GreenCardApplication;
import com.aspirity.greencard.domain.model.Chunk;
import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.domain.repository.SharesRepository;
import com.aspirity.greencard.domain.usecase.base.UseCase;
import com.aspirity.greencard.presentation.model.ShareFilterWrapper;
import com.aspirity.greencard.presentation.utils.DateUtil;
import com.aspirity.greencard.presentation.utils.ListUtil;
import com.aspirity.greencard.presentation.utils.Network;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.rx.RxUtil;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;

import java.util.List;

import androidx.annotation.NonNull;
import io.reactivex.Single;

public class SharesUseCase extends UseCase {

    private final Context context;
    private final SchedulerProvider schedulerProvider;
    private final DateUtil dateUtil;
    private SharesRepository sharesRepository;

    public SharesUseCase(Context context, SharesRepository sharesRepository, SchedulerProvider schedulerProvider) {
        this.context = context;
        dateUtil = ((GreenCardApplication) context.getApplicationContext()).getDateUtils();
        this.sharesRepository = sharesRepository;
        this.schedulerProvider = schedulerProvider;
    }

    public Single<Chunk<Share>> getShares(Integer page, @NonNull ShareFilterWrapper filter) {
        if (Network.isConnectedToNetwork(context)) {
            String beginDate = dateUtil.getDateForApi(filter.getDateInterval().getBeginDate());
            String endDate = dateUtil.getDateForApi(filter.getDateInterval().getEndDate());
            String sort = filter.getSortParams().getIdentifier();
            List<Long> vendors = ListUtil.map(filter.getVendors(), obj -> obj.getVendor().getId());
            return sharesRepository.shares(page, beginDate, endDate, sort, vendors)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        } else
            return Single.error(new NetworkException());
    }

    public Single<Chunk<Share>> getShares(Integer page) {
        if (Network.isConnectedToNetwork(context))
            return sharesRepository.sharesForceRefresh(page)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Chunk<Share>> getSharesFallback(Integer page, Integer pageSize) {
        return sharesRepository.sharesFromDb(page, pageSize)
                .compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }

    public Single<Chunk<Share>> getSharesOfPartner(Long partnerId) {
        if (Network.isConnectedToNetwork(context))
            return sharesRepository.sharesOfPartner(partnerId)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Chunk<Share>> getSharesOfPartnerFallback(Long partnerId) {
        return sharesRepository.sharesOfPartnerFromDb(partnerId)
                .compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }


    public Single<Chunk<Share>> getArchivedShares(Integer page) {
        if (Network.isConnectedToNetwork(context))
            return sharesRepository.archivedSharesForceRefresh(page)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Chunk<Share>> getArchivedSharesFallback(Integer page, Integer pageSize) {
        return sharesRepository.archivedSharesFromDb(page, pageSize)
                .compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }

    public Single<Share> getShare(Long shareId, boolean isArchived) {
        if (Network.isConnectedToNetwork(context)) {
            if (isArchived)
                return sharesRepository.archivedShare(shareId)
                        .compose(RxUtil.applySingleSchedulers(schedulerProvider));

            return sharesRepository.share(shareId)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        } else
            return Single.error(new NetworkException());
    }

    public Single<Share> getShareFallback(Long shareId) {
        return sharesRepository.shareFromDb(shareId)
                .compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }
}
