package com.aspirity.greencard.domain.model;

public class Card {

    private Long id;

    private String ownerStatus;

    private String ownerName;

    private String ownerPhone;

    private String number;

    private Boolean isMain;

    private Boolean isLocked;

    public Card(
            Long id,
            String ownerStatus,
            String ownerName,
            String ownerPhone,
            String number,
            Boolean isMain,
            Boolean isLocked) {
        this.id = id;
        this.ownerStatus = ownerStatus;
        this.ownerName = ownerName;
        this.ownerPhone = ownerPhone;
        this.number = number;
        this.isMain = isMain;
        this.isLocked = isLocked;
    }

    public Long getId() {
        return id;
    }

    public String getOwnerStatus() {
        return ownerStatus;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getOwnerPhone() {
        return ownerPhone;
    }

    public Boolean isLocked() {
        return isLocked;
    }

    public void setLocked(Boolean locked) {
        isLocked = locked;
    }

    public String getNumber() {
        return number;
    }

    public Boolean isMain() {
        return isMain;
    }

    public void setMain(Boolean main) {
        isMain = main;
    }
}
