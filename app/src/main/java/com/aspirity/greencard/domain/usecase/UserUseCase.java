package com.aspirity.greencard.domain.usecase;

import android.content.Context;

import com.aspirity.greencard.domain.model.Oferta;
import com.aspirity.greencard.domain.model.User;
import com.aspirity.greencard.domain.repository.UserRepository;
import com.aspirity.greencard.domain.usecase.base.UseCase;
import com.aspirity.greencard.presentation.utils.Network;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.rx.RxUtil;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;

import io.reactivex.Single;

public class UserUseCase extends UseCase {

    private final SchedulerProvider schedulerProvider;
    private final Context context;
    private UserRepository userRepository;

    public UserUseCase(Context context, UserRepository userRepository, SchedulerProvider schedulerProvider) {
        this.userRepository = userRepository;
        this.schedulerProvider = schedulerProvider;
        this.context = context;
    }

    public Single<String> auth(String phone, String password) {
        if (Network.isConnectedToNetwork(context))
            return userRepository.auth(phone, password)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<User> registration(
            String cardNumber,
            String cardCode,
            String firstName,
            String gender,
            String birthday,
            String phone,
            String email,
            Boolean agreement) {

        if (Network.isConnectedToNetwork(context))
            return userRepository
                    .registration(
                            cardNumber,
                            cardCode,
                            firstName,
                            gender,
                            birthday,
                            phone,
                            email,
                            agreement)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<User> searchUserByCard(String cardNumber, String cardCode) {
        if (Network.isConnectedToNetwork(context))
            return userRepository.searchUserByCardNumber(cardNumber, cardCode)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Oferta> getOferta() {
        if (Network.isConnectedToNetwork(context))
            return userRepository.getOferta()
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Oferta> getFamilyAccountRules() {
        if (Network.isConnectedToNetwork(context))
            return userRepository.getFamilyAccountRules()
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<User> getUser(boolean forceRefresh) {
        if (Network.isConnectedToNetwork(context)) {
            if (forceRefresh) {
                return userRepository.getUserForceRefresh()
                        .compose(RxUtil.applySingleSchedulers(schedulerProvider));
            }

            return userRepository.getUser()
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        } else
            return Single.error(new NetworkException());
    }

    public Single<User> getUserFallback() {
        return userRepository.getUserFromDb()
                .compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }

    public Single<User> setMailingSettings(boolean push_operations, boolean push, boolean email, boolean sms) {
        if (Network.isConnectedToNetwork(context))
            return userRepository.updateUserSubscribed(push_operations, push, email, sms)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<User> changePassword(String password) {
        if (Network.isConnectedToNetwork(context))
            return userRepository.changePassword(password)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Boolean> restorePassword(String phone) {
        if (Network.isConnectedToNetwork(context))
            return userRepository.restorePassword(phone)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Boolean> validateSmsCode(String smsCode, String phone) {
        if (Network.isConnectedToNetwork(context))
            return userRepository.validateSmsCode(smsCode, phone)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Boolean> setNewPassword(String phone, String smsCode, String password) {
        if (Network.isConnectedToNetwork(context))
            return userRepository.setNewPassword(phone, smsCode, password)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Boolean> activateUser() {
        if (Network.isConnectedToNetwork(context))
            return userRepository.activateUser()
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Boolean> logOut() {
        return userRepository.logOut().compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }
}
