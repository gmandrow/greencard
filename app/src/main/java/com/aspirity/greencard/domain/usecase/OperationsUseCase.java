package com.aspirity.greencard.domain.usecase;


import android.content.Context;

import com.aspirity.greencard.GreenCardApplication;
import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.Operation;
import com.aspirity.greencard.domain.model.OperationChunk;
import com.aspirity.greencard.domain.model.mapper.FullOperationToListItemMapper;
import com.aspirity.greencard.domain.model.mapper.OperationToListItemMapper;
import com.aspirity.greencard.domain.repository.OperationsRepository;
import com.aspirity.greencard.domain.usecase.base.UseCase;
import com.aspirity.greencard.presentation.model.OperationFilterWrapper;
import com.aspirity.greencard.presentation.utils.DateUtil;
import com.aspirity.greencard.presentation.utils.ListUtil;
import com.aspirity.greencard.presentation.utils.Network;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.rx.RxUtil;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;

import java.util.List;

import androidx.core.util.Pair;
import io.reactivex.Single;

public class OperationsUseCase extends UseCase {

    private final Context context;
    private final SchedulerProvider schedulerProvider;
    private final DateUtil dateUtil;
    private OperationsRepository operationsRepository;

    public OperationsUseCase(
            Context context,
            OperationsRepository operationsRepository,
            SchedulerProvider schedulerProvider) {
        this.context = context;
        dateUtil = ((GreenCardApplication) context.getApplicationContext()).getDateUtils();
        this.operationsRepository = operationsRepository;
        this.schedulerProvider = schedulerProvider;
    }

    public Single<OperationChunk> getOperations(Integer page, OperationFilterWrapper filter) {
        if (Network.isConnectedToNetwork(context)) {
            String beginDate = dateUtil.getDateForApi(filter.getDateInterval().getBeginDate());
            String endDate = dateUtil.getDateForApi(filter.getDateInterval().getEndDate());
            String sort = filter.getSortParams().getIdentifier();
            List<Long> vendors = ListUtil.map(filter.getVendors(), obj -> obj.getVendor().getId());
            return operationsRepository.operations(page, beginDate, endDate, sort, vendors)
                    .map(new OperationToListItemMapper(dateUtil))
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        } else
            return Single.error(new NetworkException());
    }

    public Single<OperationChunk> getOperationsFallback(Integer page, Integer pageSize) {
        return operationsRepository.operationsFromDb(page, pageSize)
                .map(new OperationToListItemMapper(dateUtil))
                .compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }

    public Single<Pair<Operation, List<ListItem>>> getOperation(String uniqueId) {
        if (Network.isConnectedToNetwork(context)) {
            return operationsRepository.operation(uniqueId)
                    .observeOn(schedulerProvider.computation())
                    .flatMap(new FullOperationToListItemMapper())
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        } else
            return Single.error(new NetworkException());
    }

    public Single<Pair<Operation, List<ListItem>>> getOperationFallback(String uniqueId) {
        return operationsRepository.operationFromDb(uniqueId)
                .observeOn(schedulerProvider.computation())
                .flatMap(new FullOperationToListItemMapper())
                .compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }
}
