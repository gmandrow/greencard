package com.aspirity.greencard.domain.model;

public class DateHeader {

    private String date;

    public DateHeader(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
