package com.aspirity.greencard.domain.model;


public class VirtualCard {
    private String number;
    private String hash;

    public VirtualCard(String number, String hash) {
        this.number = number;
        this.hash = hash;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
