package com.aspirity.greencard.domain.model;

public abstract class ListItem {

    public static final int TYPE_HEADER = 0;
    public static final int TYPE_GENERAL = 1;
    public static final int TYPE_FOOTER = 2;

    public static final int TYPE_CONTENT_SHARE = 3;
    public static final int TYPE_CONTENT_MESSAGE = 4;
    public static final int TYPE_FILTERS = 5;

    abstract public int getType();
}
