package com.aspirity.greencard.domain.model;

public class HeaderItem<T> extends ListItem {

    private T header;

    public HeaderItem(T header) {
        this.header = header;
    }

    public T getHeader() {
        return header;
    }

    @Override
    public int getType() {
        return TYPE_HEADER;
    }
}
