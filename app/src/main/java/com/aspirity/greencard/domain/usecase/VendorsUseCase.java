package com.aspirity.greencard.domain.usecase;

import android.content.Context;

import com.aspirity.greencard.domain.model.Vendor;
import com.aspirity.greencard.domain.model.mapper.VendorsToMapMapper;
import com.aspirity.greencard.domain.repository.VendorsRepository;
import com.aspirity.greencard.domain.usecase.base.UseCase;
import com.aspirity.greencard.presentation.model.EmptyMapException;
import com.aspirity.greencard.presentation.utils.Network;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.rx.RxUtil;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.SingleSource;

public class VendorsUseCase extends UseCase {

    private final Context context;
    private final SchedulerProvider schedulerProvider;
    private VendorsRepository vendorsRepository;

    public VendorsUseCase(
            Context context,
            SchedulerProvider schedulerProvider,
            VendorsRepository vendorsRepository) {
        this.context = context;
        this.schedulerProvider = schedulerProvider;
        this.vendorsRepository = vendorsRepository;
    }

    public Single<List<Vendor>> getVendors() {
        if (Network.isConnectedToNetwork(context))
            return vendorsRepository.vendors()
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<List<Vendor>> getVendorsFallback() {
        return vendorsRepository.vendorsFromDb()
                .compose(RxUtil.applySingleSchedulers(schedulerProvider));
    }

    public Single<Map<Vendor, LatLng>> getVendorsOfPartner(Long partnerId) {
        if (Network.isConnectedToNetwork(context))
            return vendorsRepository.vendorsOfPartner(partnerId)
                    .observeOn(schedulerProvider.computation())
                    .map(new VendorsToMapMapper())
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider))
                    .flatMap(vendorLatLngMap -> {
                        if (vendorLatLngMap.isEmpty()) {
                            return Single.error(new EmptyMapException());
                        }
                        return Single.just(vendorLatLngMap);
                    });
        else
            return Single.error(new NetworkException());
    }

    public SingleSource<Map<Vendor, LatLng>> getVendorsOfPartnerFallback(Long partnerId) {
        return vendorsRepository.vendorsOfPartnerFromDb(partnerId)
                .observeOn(schedulerProvider.computation())
                .map(new VendorsToMapMapper())
                .compose(RxUtil.applySingleSchedulers(schedulerProvider))
                .flatMap(vendorLatLngMap -> {
                    if (vendorLatLngMap.isEmpty()) {
                        return Single.error(new EmptyMapException());
                    }
                    return Single.just(vendorLatLngMap);
                });
    }
}
