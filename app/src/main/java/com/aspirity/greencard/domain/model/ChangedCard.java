package com.aspirity.greencard.domain.model;


public class ChangedCard {

    private Boolean isChanged;

    public ChangedCard() {
    }

    public ChangedCard(Boolean isChanged) {
        this.isChanged = isChanged;
    }

    public Boolean isChanged() {
        return isChanged;
    }

    public void setIsChanged(Boolean isChanged) {
        this.isChanged = isChanged;
    }

}
