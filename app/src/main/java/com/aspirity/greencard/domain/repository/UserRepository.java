package com.aspirity.greencard.domain.repository;


import com.aspirity.greencard.domain.model.Oferta;
import com.aspirity.greencard.domain.model.User;

import io.reactivex.Single;

public interface UserRepository {
    Single<User> registration(
            String cardNumber,
            String cardCode,
            String firstName,
            String gender,
            String birthDate,
            String phone,
            String email,
            Boolean agreement);

    /**
     * @param phone    - user phone
     * @param password - user password
     * @return pair with boolean, if true then string == token, if false then string == errorMessage
     */
    Single<String> auth(
            String phone,
            String password);

    Single<Boolean> restorePassword(String phone);

    Single<Boolean> validateSmsCode(String smsCode, String phone);

    Single<Boolean> setNewPassword(String phone, String smsCode, String password);

    Single<User> changePassword(
            String password);

    Single<User> updateUserSubscribed(
            Boolean isSubscribedPushOperations,
            Boolean isSubscribedPush,
            Boolean isSubscribedEmail,
            Boolean isSubscribedSms);

    Single<User> searchUserByCardNumber(String cardNumber, String cardCode);

    Single<User> getUser();

    Single<User> getUserForceRefresh();

    Single<User> getUserFromDb();

    Single<Oferta> getOferta();

    Single<Oferta> getFamilyAccountRules();

    Single<Boolean> activateUser();

    Single<Boolean> logOut();
}
