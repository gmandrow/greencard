package com.aspirity.greencard.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BonusPercent implements Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<BonusPercent> CREATOR = new Parcelable.Creator<BonusPercent>() {
        @Override
        public BonusPercent createFromParcel(Parcel in) {
            return new BonusPercent(in);
        }

        @Override
        public BonusPercent[] newArray(int size) {
            return new BonusPercent[size];
        }
    };
    private Long percents;
    private String status;

    public BonusPercent() {
    }

    public BonusPercent(Long percents, String status) {
        this.percents = percents;
        this.status = status;
    }

    protected BonusPercent(Parcel in) {
        percents = in.readByte() == 0x00 ? null : in.readLong();
        status = in.readString();
    }

    public Long getPercents() {
        return percents;
    }

    public void setPercents(Long percents) {
        this.percents = percents;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (percents == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(percents);
        }
        dest.writeString(status);
    }

}
