package com.aspirity.greencard.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Faq implements Parcelable {
    @SuppressWarnings("unused")
    public static final Creator<Faq> CREATOR = new Creator<Faq>() {
        @Override
        public Faq createFromParcel(Parcel in) {
            return new Faq(in);
        }

        @Override
        public Faq[] newArray(int size) {
            return new Faq[size];
        }
    };
    private final Long id;
    private final String question;
    private final String answer;

    public Faq(Long id, String question, String answer) {
        this.id = id;
        this.question = question;
        this.answer = answer;
    }

    private Faq(Parcel in) {
        id = in.readLong();
        question = in.readString();
        answer = in.readString();
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public Long getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(question);
        dest.writeString(answer);
    }
}
