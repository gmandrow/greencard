package com.aspirity.greencard.domain.model;

import java.util.Locale;

public class Notification {
    private Long id;
    private String category;
    private String status;
    private long date_created;
    private Data data;
    private String rendered_text;

    public Notification(Long id, String category, String status, long date_created, Data data, String rendered_text) {
        this.id = id;
        this.category = category;
        this.status = status;
        this.date_created = date_created;
        this.data = data;
        this.rendered_text = rendered_text;
    }

    public Long getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getStatus() {
        return status;
    }

    public long getDateCreated() {
        return date_created;
    }

    public Data getData() {
        return data;
    }

    public String getRenderedText() {
        return rendered_text;
    }

    public class Data {
        private String bp_rrn;
        private String text;
        private String phone;
        private String image_url;
        private String url;
        private String button_label;
        private boolean is_archived = false;

        public String getBp_rrn() {
            return bp_rrn;
        }

        public String getText() {
            return text;
        }

        public boolean isVisibleButton() {
            return url != null || phone != null;
        }

        public String getButtonLabel() {
            if (button_label == null)
                return "ПОДРОБНЕЕ ";
            else
                return button_label.toUpperCase(Locale.getDefault()) + " ";
        }

        public String getImageUrl() {
            return image_url;
        }

        public String getUrl() {
            return url;
        }

        public String getPhone() {
            return phone;
        }

        public boolean isArchived() {
            return is_archived;
        }
    }
}
