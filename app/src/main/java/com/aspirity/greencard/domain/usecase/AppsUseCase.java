package com.aspirity.greencard.domain.usecase;


import android.content.Context;

import com.aspirity.greencard.domain.model.Apps;
import com.aspirity.greencard.domain.repository.AppsRepository;
import com.aspirity.greencard.domain.usecase.base.UseCase;
import com.aspirity.greencard.presentation.utils.Network;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.rx.RxUtil;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;

import io.reactivex.Single;

public class AppsUseCase extends UseCase {
    private final Context context;
    private final SchedulerProvider schedulerProvider;
    private AppsRepository appsRepository;

    public AppsUseCase(Context context, AppsRepository appsRepository, SchedulerProvider schedulerProvider) {
        this.context = context;
        this.appsRepository = appsRepository;
        this.schedulerProvider = schedulerProvider;
    }

    public Single<Apps> getApps() {
        if (Network.isConnectedToNetwork(context))
            return appsRepository.apps()
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Void> registrationApps(String app_id, String type) {
        if (Network.isConnectedToNetwork(context))
            return appsRepository.registrationApp(app_id, type)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }
}
