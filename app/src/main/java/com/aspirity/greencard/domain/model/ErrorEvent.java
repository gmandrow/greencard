package com.aspirity.greencard.domain.model;


import com.aspirity.greencard.presentation.utils.error.RequestError;

public class ErrorEvent {

    public final String message;
    public final RequestError requestError;

    public ErrorEvent(String message, RequestError requestError) {
        this.message = message;
        this.requestError = requestError;
    }
}
