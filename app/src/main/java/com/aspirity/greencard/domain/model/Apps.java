package com.aspirity.greencard.domain.model;

public class Apps {
    private String android;

    public Apps() {
    }

    public Apps(String android) {
        this.android = android;
    }

    public String getAndroid() {
        return android;
    }
}
