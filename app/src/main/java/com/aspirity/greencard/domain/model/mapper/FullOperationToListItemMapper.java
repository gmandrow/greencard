package com.aspirity.greencard.domain.model.mapper;


import com.aspirity.greencard.domain.model.FooterItem;
import com.aspirity.greencard.domain.model.GeneralItem;
import com.aspirity.greencard.domain.model.HeaderItem;
import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.Operation;
import com.aspirity.greencard.domain.model.TotalSumFooter;
import com.aspirity.greencard.presentation.utils.Constants;

import java.util.LinkedList;
import java.util.List;

import androidx.core.util.Pair;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

/**
 * This mapper needed for wrapping products in ListItem with Header and Footer.
 */
public class FullOperationToListItemMapper implements Function<Operation, Single<Pair<Operation, List<ListItem>>>> {

    private int countProduct;

    @Override
    public Single<Pair<Operation, List<ListItem>>> apply(@NonNull Operation operation) {
        if (operation == null) return Single.just(new Pair<>(null, new LinkedList<>()));

        List<ListItem> items = new LinkedList<>();
        countProduct = 0;

        return Observable.fromIterable(operation.getProducts())
                .map(product -> {
                    items.add(new GeneralItem<>(product, ListItem.TYPE_GENERAL));
                    return items.get(countProduct++);
                })
                .toList()
                .map(listItems -> {
                    listItems.add(Constants.FIRST_ITEM, new HeaderItem<>(null));
                    return listItems;
                })
                .map(listItems -> {
                    listItems.add(new FooterItem<>(
                            new TotalSumFooter(
                                    operation.getTotalPrice(),
                                    operation.getAccruedBonuses(),
                                    operation.getDebitedBonuses())));
                    return new Pair<>(operation, listItems);
                });
    }
}
