package com.aspirity.greencard.domain.repository;


import com.aspirity.greencard.domain.model.Faq;

import java.util.List;

import io.reactivex.Single;

public interface FaqRepository {

    Single<List<Faq>> faq();

    Single<List<Faq>> faqFromDb();

    Single<List<Faq>> faqForceRefresh();
}
