package com.aspirity.greencard.domain.model;

import java.util.List;

public class Operation {

    private Long id;
    private String uniqueId;
    private String accruedBonuses;
    private String address;
    private String bonuses;
    private Long dateOf;
    private String debitedBonuses;
    private Long partnerId;
    private String partnerColor;
    private String partnerLogoSrc;
    private String partnerLogoBlackBgSrc;
    private String partnerName;
    private List<Product> productEntities;
    private String totalPrice;
    private String type;
    private String vendorName;

    public Operation() {
    }

    public Operation(
            Long id,
            String uniqueId,
            String address,
            String bonuses,
            String type,
            Long dateOf,
            String accruedBonuses,
            String debitedBonuses,
            String partnerLogoSrc,
            String partnerLogoBlackBgSrc,
            String partnerName,
            String totalPrice,
            String vendorName,
            Long partnerId) {
        this.bonuses = bonuses;
        this.id = id;
        this.uniqueId = uniqueId;
        this.address = address;
        this.type = type;
        this.dateOf = dateOf;
        this.accruedBonuses = accruedBonuses;
        this.debitedBonuses = debitedBonuses;
        this.partnerLogoSrc = partnerLogoSrc;
        this.partnerLogoBlackBgSrc = partnerLogoBlackBgSrc;
        this.partnerName = partnerName;
        this.totalPrice = totalPrice;
        this.vendorName = vendorName;
        this.partnerId = partnerId;
    }

    public Operation(
            Long id,
            String uniqueId,
            String bonuses,
            Long dateOf,
            String accruedBonuses,
            String debitedBonuses,
            String partnerLogoBlackBgSrc, String partnerName,
            String totalPrice,
            String vendorName) {
        this.bonuses = bonuses;
        this.id = id;
        this.uniqueId = uniqueId;
        this.dateOf = dateOf;
        this.accruedBonuses = accruedBonuses;
        this.debitedBonuses = debitedBonuses;
        this.partnerLogoBlackBgSrc = partnerLogoBlackBgSrc;
        this.partnerName = partnerName;
        this.totalPrice = totalPrice;
        this.vendorName = vendorName;
    }

    public Long getId() {
        return id;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public String getAddress() {
        return address;
    }

    public String getBonuses() {
        return bonuses;
    }

    public String getType() {
        return type.substring(0, 1).toUpperCase() + type.substring(1);
    }

    public Long getDateOf() {
        return dateOf;
    }

    public String getAccruedBonuses() {
        return accruedBonuses;
    }

    public String getDebitedBonuses() {
        return debitedBonuses;
    }

    public String getPartnerLogoSrc() {
        return partnerLogoSrc;
    }

    public String getPartnerLogoBlackBgSrc() {
        return partnerLogoBlackBgSrc;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public String getVendorName() {
        return vendorName == null ? "" : vendorName;
    }

    public List<Product> getProducts() {
        return productEntities;
    }

    public void setProducts(List<Product> products) {
        this.productEntities = products;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }
}
