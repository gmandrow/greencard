package com.aspirity.greencard.domain.model;

import com.aspirity.greencard.presentation.utils.Constants;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class Share {
    private Long id;
    private Boolean isSpecial;
    private String content;
    private Long dateEnd;
    private Long daysInMilliseconds;
    private String imageSrc;
    private String imageHorizontalSrc;
    private String imageVerticalSrc;
    private String partnerColor;
    private Long partnerId;
    private String partnerLogoSrc;
    private String partnerLogoWhiteBgSrc;
    private String title;
    private long[] vendors;

    public Share() {
    }

    public Share(Long id) {
        this.id = id;
    }

    public Share(Long id,
                 Boolean isSpecial,
                 String content,
                 Long dateEnd,
                 String imageSrc,
                 String imageHorizontalSrc,
                 String imageVerticalSrc,
                 String partnerColor,
                 Long partnerId,
                 String partnerLogoSrc,
                 String title,
                 long[] vendors) {
        this.id = id;
        this.isSpecial = isSpecial;
        this.content = content;
        this.dateEnd = dateEnd;
        this.imageHorizontalSrc = imageHorizontalSrc;
        this.imageVerticalSrc = imageVerticalSrc;
        this.daysInMilliseconds = calculateDaysInMilliseconds();
        this.imageSrc = imageSrc;
        this.partnerColor = partnerColor;
        this.partnerId = partnerId;
        this.partnerLogoSrc = partnerLogoSrc;
        this.title = title;
        this.vendors = vendors;
    }

    public Share(
            Long id,
            Boolean isSpecial,
            Long dateEnd,
            String imageSrc,
            String imageHorizontalSrc,
            String imageVerticalSrc,
            String partnerColor,
            Long partnerId,
            String partnerLogoSrc,
            String partnerLogoWhiteBgSrc, String title,
            long[] vendors) {
        this.id = id;
        this.isSpecial = isSpecial;
        this.dateEnd = dateEnd;
        this.imageHorizontalSrc = imageHorizontalSrc;
        this.imageVerticalSrc = imageVerticalSrc;
        this.partnerLogoWhiteBgSrc = partnerLogoWhiteBgSrc;
        this.daysInMilliseconds = calculateDaysInMilliseconds();
        this.imageSrc = imageSrc;
        this.partnerColor = partnerColor;
        this.partnerId = partnerId;
        this.partnerLogoSrc = partnerLogoSrc;
        this.title = title;
        this.vendors = vendors;
    }

    public Boolean isSpecial() {
        return isSpecial;
    }

    public Long getId() {
        return id;
    }

    public Boolean getSpecial() {
        return isSpecial;
    }

    public String getContent() {
        return content;
    }

    public Long getDateEnd() {
        return dateEnd;
    }

    public Long getDateEndInMilliseconds() {
        return dateEnd * Constants.COEFFICIENT_TIMESTAMP_TO_MILISECONDS;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public String getImageHorizontalSrc() {
        return imageHorizontalSrc;
    }

    public String getImageVerticalSrc() {
        return imageVerticalSrc;
    }

    public String getPartnerColor() {
        return partnerColor;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public String getPartnerLogoSrc() {
        return partnerLogoSrc;
    }

    public String getPartnerLogoWhiteBgSrc() {
        return partnerLogoWhiteBgSrc;
    }

    public String getTitle() {
        return title;
    }

    public long[] getVendors() {
        return vendors;
    }

    public Long getDaysInMilliseconds() {
        return daysInMilliseconds;
    }

    private Long calculateDaysInMilliseconds() {
        Calendar expireCalendar = Calendar.getInstance();
        expireCalendar.setTimeInMillis(dateEnd * Constants.COEFFICIENT_TIMESTAMP_TO_MILISECONDS);
        expireCalendar.add(Calendar.DATE, Constants.ONE_DAY);
        long msDiff = expireCalendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
        return TimeUnit.MILLISECONDS.toDays(msDiff);
    }
}
