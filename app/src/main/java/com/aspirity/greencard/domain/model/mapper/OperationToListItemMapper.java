package com.aspirity.greencard.domain.model.mapper;


import com.aspirity.greencard.domain.model.Chunk;
import com.aspirity.greencard.domain.model.GeneralItem;
import com.aspirity.greencard.domain.model.HeaderItem;
import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.Operation;
import com.aspirity.greencard.domain.model.OperationChunk;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.DateUtil;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import io.reactivex.functions.Function;

public class OperationToListItemMapper implements Function<Chunk<Operation>, OperationChunk> {

    private final DateUtil dateUtil;

    public OperationToListItemMapper(DateUtil dateUtil) {
        this.dateUtil = dateUtil;
    }

    @Override
    public OperationChunk apply(Chunk<Operation> operationChunk) {
        List<Operation> operations = operationChunk.getItems();
        List<ListItem> items = new LinkedList<>();

        for (int i = 0; i < operations.size(); i++) {
            Operation operation = operations.get(i);
            String date = dateUtil.getDateString(new Date(Long.valueOf(operation.getDateOf())
                    * Constants.COEFFICIENT_TIMESTAMP_TO_MILISECONDS));

            if (i == 0) {
                items.add(new HeaderItem<>(date));
                items.add(new GeneralItem<>(operation, ListItem.TYPE_GENERAL));
                continue;
            }
            ListItem lastItem = items.get(items.size() - 1);
            if (lastItem.getType() == ListItem.TYPE_GENERAL) {
                Operation lastOperation = ((GeneralItem<Operation>) lastItem).getData();
                String lastDate = dateUtil.getDateString(new Date(Long.valueOf(lastOperation.getDateOf())
                        * Constants.COEFFICIENT_TIMESTAMP_TO_MILISECONDS));
                if (!Objects.equals(lastDate, date)) {
                    items.add(new HeaderItem<>(date));
                    items.add(new GeneralItem<>(operation, ListItem.TYPE_GENERAL));
                    continue;
                }
            }
            items.add(new GeneralItem<>(operation, ListItem.TYPE_GENERAL));
        }
        OperationChunk chunk = new OperationChunk();
        chunk.setItems(items);
        chunk.setCount(operationChunk.getCount());
        chunk.setChunkSize(operations.size());
        return chunk;
    }
}
