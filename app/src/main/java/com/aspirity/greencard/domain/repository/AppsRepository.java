package com.aspirity.greencard.domain.repository;


import com.aspirity.greencard.domain.model.Apps;

import io.reactivex.Single;

public interface AppsRepository {
    Single<Apps> apps();

    Single<Void> registrationApp(String app_id, String type);
}
