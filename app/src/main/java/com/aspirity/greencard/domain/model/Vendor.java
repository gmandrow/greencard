package com.aspirity.greencard.domain.model;


import android.os.Parcel;
import android.os.Parcelable;

public class Vendor implements Parcelable {

    public static final Creator<Vendor> CREATOR = new Creator<Vendor>() {
        @Override
        public Vendor createFromParcel(Parcel source) {
            return new Vendor(source);
        }

        @Override
        public Vendor[] newArray(int size) {
            return new Vendor[size];
        }
    };
    private Long id;
    private String address;
    private String email;
    private String latitude;
    private String longitude;
    private String partnerColor;
    private Long partnerId;
    private String partnerLogoSrc;
    private String name;
    private String partnerName;
    private String phone;
    private Long storeId;

    public Vendor() {
    }

    public Vendor(
            Long id,
            String address,
            String email,
            String latitude,
            String longitude,
            String partnerColor,
            Long partnerId,
            String partnerLogoSrc,
            String name,
            String partnerName,
            String phone,
            Long storeId) {
        this.id = id;
        this.address = address;
        this.email = email;
        this.latitude = latitude;
        this.longitude = longitude;
        this.partnerColor = partnerColor;
        this.partnerId = partnerId;
        this.partnerLogoSrc = partnerLogoSrc;
        this.name = name;
        this.partnerName = partnerName;
        this.phone = phone;
        this.storeId = storeId;
    }

    protected Vendor(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.address = in.readString();
        this.email = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.partnerColor = in.readString();
        this.partnerId = (Long) in.readValue(Long.class.getClassLoader());
        this.partnerLogoSrc = in.readString();
        this.name = in.readString();
        this.partnerName = in.readString();
        this.phone = in.readString();
        this.storeId = (Long) in.readValue(Long.class.getClassLoader());
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPartnerColor() {
        return partnerColor;
    }

    public void setPartnerColor(String partnerColor) {
        this.partnerColor = partnerColor;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerLogoSrc() {
        return partnerLogoSrc;
    }

    public void setPartnerLogoSrc(String partnerLogoSrc) {
        this.partnerLogoSrc = partnerLogoSrc;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vendor vendor = (Vendor) o;

        return id != null ? id.equals(vendor.id) : vendor.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.address);
        dest.writeString(this.email);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
        dest.writeString(this.partnerColor);
        dest.writeValue(this.partnerId);
        dest.writeString(this.partnerLogoSrc);
        dest.writeString(this.name);
        dest.writeString(this.partnerName);
        dest.writeString(this.phone);
        dest.writeValue(this.storeId);
    }
}
