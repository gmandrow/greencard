package com.aspirity.greencard.domain.usecase;


import android.content.Context;

import com.aspirity.greencard.domain.model.Chunk;
import com.aspirity.greencard.domain.model.Notification;
import com.aspirity.greencard.domain.repository.NotificationsRepository;
import com.aspirity.greencard.domain.usecase.base.UseCase;
import com.aspirity.greencard.presentation.utils.Network;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.rx.RxUtil;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;

import io.reactivex.Single;

public class NotificationsUseCase extends UseCase {
    private final Context context;
    private final SchedulerProvider schedulerProvider;
    private NotificationsRepository notificationsRepository;

    public NotificationsUseCase(Context context, NotificationsRepository notificationsRepository, SchedulerProvider schedulerProvider) {
        this.context = context;
        this.notificationsRepository = notificationsRepository;
        this.schedulerProvider = schedulerProvider;
    }

    public Single<Chunk<Notification>> getNotifications(String token) {
        if (Network.isConnectedToNetwork(context))
            return notificationsRepository.notifications(token)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }

    public Single<Void> notificationsStatus(Long notification_id, String token, String status) {
        if (Network.isConnectedToNetwork(context))
            return notificationsRepository.notificationsStatus(notification_id, token, status)
                    .compose(RxUtil.applySingleSchedulers(schedulerProvider));
        else
            return Single.error(new NetworkException());
    }
}
