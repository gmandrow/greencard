package com.aspirity.greencard.domain.model;

import java.util.List;

public class Store {

    private Long id;
    private String address;
    private Long partnerId;
    private String partnerName;
    private List<Vendor> vendors;
    private boolean isCheckedStore;

    public Store() {
    }

    public Store(
            Long id,
            String address,
            Long partnerId,
            String partnerName,
            List<Vendor> vendors,
            Boolean isCheckedStore) {
        this.id = id;
        this.address = address;
        this.partnerId = partnerId;
        this.partnerName = partnerName;
        this.vendors = vendors;
        this.isCheckedStore = isCheckedStore;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public List<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    public boolean isCheckedStore() {
        return isCheckedStore;
    }

    public void setCheckedStore(boolean checkedStore) {
        isCheckedStore = checkedStore;
    }
}
