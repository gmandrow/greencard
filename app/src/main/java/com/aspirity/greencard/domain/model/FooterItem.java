package com.aspirity.greencard.domain.model;

public class FooterItem<T> extends ListItem {

    private T footer;

    public FooterItem(T footer) {
        this.footer = footer;
    }

    public T getFooter() {
        return footer;
    }

    @Override
    public int getType() {
        return TYPE_FOOTER;
    }
}
