package com.aspirity.greencard.domain.repository;


import com.aspirity.greencard.domain.model.Partner;

import java.util.List;

import io.reactivex.Single;

public interface PartnerRepository {

    Single<List<Partner>> partners();

    Single<List<Partner>> partnersForceRefresh();

    Single<List<Partner>> partnersFromDb();

    Single<Partner> partner(long partnerId);

    Single<Partner> partnerForceRefresh(long partnerId);

    Single<Partner> partnerFromDb(long partnerId);
}
