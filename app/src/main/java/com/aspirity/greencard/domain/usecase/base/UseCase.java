package com.aspirity.greencard.domain.usecase.base;


import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.presentation.utils.error.RequestError;

public abstract class UseCase {
    protected ErrorEvent networkError = new ErrorEvent(null, RequestError.NETWORK);
    protected ErrorEvent emptyError = new ErrorEvent(null, RequestError.EMPTY);
}
