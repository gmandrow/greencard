package com.aspirity.greencard.domain.model;


public class ValidCard {

    private Boolean isValid;
    private Boolean isRegistered;

    public ValidCard() {
    }

    public ValidCard(Boolean isValid, Boolean isRegistered) {
        this.isValid = isValid;
        this.isRegistered = isRegistered;
    }

    public Boolean isValid() {
        return isValid;
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public Boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(Boolean registered) {
        isRegistered = registered;
    }

}
