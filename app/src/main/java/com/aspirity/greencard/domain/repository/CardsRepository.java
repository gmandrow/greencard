package com.aspirity.greencard.domain.repository;


import com.aspirity.greencard.domain.model.Card;
import com.aspirity.greencard.domain.model.ValidCard;
import com.aspirity.greencard.domain.model.VirtualCard;

import java.util.List;

import io.reactivex.Single;

public interface CardsRepository {

    Single<ValidCard> validateCard(
            String cardNumber,
            String cardCode);

    Single<Boolean> verifyAttachedCard(
            Long userId,
            String smsCode);

    Single<String> attachCard(
            Long userId);

    Single<Boolean> cardChangeState(
            Long cardId,
            String password);

    Single<List<Card>> getCards();

    Single<List<Card>> getCardsForceRefresh();

    Single<List<Card>> getCardsFromDb();

    Single<VirtualCard> getVirtualCard();
}
