package com.aspirity.greencard.domain.repository;


import com.aspirity.greencard.domain.model.Store;

import java.util.List;

import io.reactivex.Single;

public interface StoresRepository {
    Single<List<Store>> stores(
            int page,
            int pageSize);

    Single<Store> stores(
            long storeId);
}
