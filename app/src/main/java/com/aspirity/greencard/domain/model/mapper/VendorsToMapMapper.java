package com.aspirity.greencard.domain.model.mapper;

import com.aspirity.greencard.domain.model.Vendor;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import timber.log.Timber;

public class VendorsToMapMapper implements Function<List<Vendor>, Map<Vendor, LatLng>> {

    @Override
    public Map<Vendor, LatLng> apply(@NonNull List<Vendor> vendors) {
        if (vendors == null || vendors.isEmpty()) return new HashMap<>();

        Map<Vendor, LatLng> vendorLatLngMap = new HashMap<>();

        for (Vendor vendor : vendors) {
            if (vendor.getLatitude() != null || vendor.getLongitude() != null) {
                vendorLatLngMap.put(vendor, new LatLng(
                        Double.valueOf(vendor.getLatitude()),
                        Double.valueOf(vendor.getLongitude())));
            }
        }

        Timber.d("IN MAPPER Count Vendors: %s", vendorLatLngMap.size());
        return vendorLatLngMap;
    }
}
