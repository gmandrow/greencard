package com.aspirity.greencard.domain.model;

import java.util.List;

import androidx.annotation.Nullable;

public class Partner {

    private Long id;
    private String color;
    private String logoSrc;
    private String logoMapSrc;
    private String logoWhiteBgSrc;
    private String logoBlackBgSrc;
    private String logoDetailSrc;
    private String name;
    private String bonusDescription;
    private String features;
    private List<BonusPercent> bonusPercents;
    private String description;
    private List<String> photos;
    private String videoCode;
    private List<Long> vendors;
    private Integer vendorsCount;
    @Nullable
    private String errorMessage;

    public Partner() {
    }

    public Partner(
            Long id,
            String color,
            String logoSrc,
            String logoMapSrc,
            String logoWhiteBgSrc,
            String logoBlackBgSrc,
            String logoDetailSrc,
            String name,
            String bonusDescription,
            String description,
            List<String> photos,
            String videoCode,
            String features,
            Integer vendorsCount,
            String errorMessage) {
        this.id = id;
        this.color = color;
        this.logoSrc = logoSrc;
        this.logoMapSrc = logoMapSrc;
        this.logoWhiteBgSrc = logoWhiteBgSrc;
        this.logoBlackBgSrc = logoBlackBgSrc;
        this.logoDetailSrc = logoDetailSrc;
        this.name = name;
        this.bonusDescription = bonusDescription;
        this.bonusPercents = bonusPercents;
        this.description = description;
        this.photos = photos;
        this.videoCode = videoCode;
        this.errorMessage = errorMessage;
        this.vendorsCount = vendorsCount;
        this.features = features;
    }

    public Partner(
            Long id,
            String color,
            String logoSrc,
            String logoMapSrc,
            String logoWhiteBgSrc,
            String logoBlackBgSrc,
            String logoDetailSrc,
            String name) {
        this.id = id;
        this.color = color;
        this.logoSrc = logoSrc;
        this.logoMapSrc = logoMapSrc;
        this.logoWhiteBgSrc = logoWhiteBgSrc;
        this.logoBlackBgSrc = logoBlackBgSrc;
        this.logoDetailSrc = logoDetailSrc;
        this.name = name;
    }

    public Partner(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLogoSrc() {
        return logoSrc;
    }

    public void setLogoSrc(String logoSrc) {
        this.logoSrc = logoSrc;
    }

    public String getLogoMapSrc() {
        return logoMapSrc;
    }

    public String getLogoWhiteBgSrc() {
        return logoWhiteBgSrc;
    }

    public String getLogoBlackBgSrc() {
        return logoBlackBgSrc;
    }

    public String getLogoDetailSrc() {
        return logoDetailSrc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBonusDescription() {
        return bonusDescription;
    }

    public void setBonusDescription(String bonusDescription) {
        this.bonusDescription = bonusDescription;
    }

    public List<BonusPercent> getBonusPercents() {
        return bonusPercents;
    }

    public void setBonusPercents(List<BonusPercent> bonusPercents) {
        this.bonusPercents = bonusPercents;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public String getVideoCode() {
        return videoCode;
    }

    public void setVideoCode(String videoCode) {
        this.videoCode = videoCode;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public List<Long> getVendors() {
        return vendors;
    }

    public void setVendors(List<Long> vendors) {
        this.vendors = vendors;
        if (vendors != null) {
            setVendorsCount(vendors.size());
        } else {
            setVendorsCount(0);
        }
    }

    public Integer getVendorsCount() {
        return vendorsCount;
    }

    public void setVendorsCount(Integer vendorsCount) {
        this.vendorsCount = vendorsCount;
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(@Nullable String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
