package com.aspirity.greencard.domain.repository;


import com.aspirity.greencard.domain.model.Chunk;
import com.aspirity.greencard.domain.model.Share;

import java.util.List;

import io.reactivex.Single;

public interface SharesRepository {

    // SHARES

    Single<Chunk<Share>> shares(int page, int pageSize);

    Single<Chunk<Share>> sharesForceRefresh(int page);

    Single<Chunk<Share>> sharesFromDb(int page, int pageSize);


    //FILTERED SHARES

    Single<Chunk<Share>> shares(
            int page,
            String beginDate,
            String endDate,
            String sort,
            List<Long> vendors);


    // SHARES OF PARTNER

    Single<Chunk<Share>> sharesOfPartner(
            Long partnerId);

    Single<Chunk<Share>> sharesOfPartnerFromDb(
            Long partnerId);

    Single<Chunk<Share>> sharesOfPartnerForceRefresh(long partnerId);


    // ARCHIVED SHARES

    Single<Chunk<Share>> archivedShares(int page);

    Single<Chunk<Share>> archivedSharesFromDb(int page, int pageSize);

    Single<Chunk<Share>> archivedSharesForceRefresh(int page);

    Single<Share> archivedShare(long shareId);

    Single<Share> share(
            long shareId);

    Single<Share> shareForceRefresh(
            long shareId);

    Single<Share> shareFromDb(long shareId);
}
