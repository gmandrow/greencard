package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.domain.model.User;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.paginate.Paginate;

import java.util.List;

public interface MainContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showUserData(User user);

        void showCardNumber(String number);

        void showSharesData(List<Share> shares, Integer countShares);

        void showMoreSharesData(List<Share> shares);

        void setHasLoadedAllItems();
    }

    interface Presenter extends MvpPresenter<View> {

        void getContent(boolean forceRefresh);

        void retryLastRequest();

        Paginate.Callbacks getCallbacks();
    }
}
