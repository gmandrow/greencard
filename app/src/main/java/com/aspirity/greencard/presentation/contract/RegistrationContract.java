package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.Oferta;
import com.aspirity.greencard.presentation.activities.RegistrationActivity;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.util.Map;

import androidx.annotation.StringRes;

public interface RegistrationContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showCardNumberError(@StringRes int messageId);

        void showCardCodeError(@StringRes int messageId);

        void showFirstNameError(@StringRes int messageId);

        void showBirthdayError(@StringRes int messageId);

        void showPhoneError(@StringRes int messageId);

        void showAgreementOfertaError(@StringRes int messageId);

        void showEmailError(@StringRes int messageId);

        void hideBirthdayError();

        void hideNameError();

        void hidePhoneError();

        void hideEmailError();

        void hideFieldError();

        void registrationSuccess();

        void allowRegistration();

        void firstStepRegistration();

        void secondStepRegistration();

        void showOferta(Oferta oferta);

        void showFieldErrors(Map<String, String> fieldErrors);
    }

    interface Presenter extends MvpPresenter<View> {

        void registration(
                String cardNumber,
                String cardCode,
                String firstName,
                String birthDate,
                String gender,
                String phone,
                String email,
                Boolean agreement);

        void loadOferta();

        void checkFieldNumberAndCode(String cardNumber, String cardCode);

        void checkStepRegistration(RegistrationActivity.RegistrationStep registrationStep);

        void checkPersonData(
                boolean isAgreementOferta,
                String cardNumber,
                String cardCode,
                String firstName,
                String birthDate,
                String gender,
                String phone,
                String email);

        void checkBirthday(String birthday);

        void checkName(String name);

        void checkPhone(String phone);

        void checkEmail(String email);

        RequestType getLastRequest();
    }
}
