package com.aspirity.greencard.presentation.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BaseActivity;
import com.aspirity.greencard.presentation.contract.ForgetPasswordContract;
import com.aspirity.greencard.presentation.presenter.ForgotPasswordPresenter;
import com.aspirity.greencard.presentation.utils.ViewUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.google.android.material.textfield.TextInputLayout;
import com.jakewharton.rxbinding2.view.RxView;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.res.ResourcesCompat;
import timber.log.Timber;

public class ForgotPasswordActivity extends BaseActivity<ForgetPasswordContract.View, ForgetPasswordContract.Presenter>
        implements ForgetPasswordContract.View {

    private ViewGroup root;
    private TextView logoGreenCardTextView;
    private ForgotPasswordStep step;
    private Button nextStepButton;
    private View wrapperFirstStep;
    private TextInputLayout phoneTil;
    private EditText phoneEditText;
    private MaskedTextChangedListener phoneMaskedListener;
    private String phone = "";
    private View wrapperSecondStep;
    private TextInputLayout verificationCodeTil;
    private EditText verificationCodeEditText;
    private View wrapperThirdStep;
    private TextInputLayout passwordTil;
    private TextInputLayout repeatPasswordTil;
    private EditText passwordEditText;
    private EditText repeatPasswordEditText;

    private Dialog errorDialog;
    private TextView titleErrorTextView;
    private TextView messageTextView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        UserUseCase userUseCase = new UserUseCase(
                getApplicationContext(),
                RepositoryProvider.getUserRepository(),
                new AppSchedulerProvider());
        onCreatePresenter(new ForgotPasswordPresenter(userUseCase), this);
        step = ForgotPasswordStep.CHECK_PHONE;
        initViews();
        initErrorDialog();
    }

    private void initViews() {
        root = findViewById(R.id.root);

        logoGreenCardTextView = findViewById(R.id.text_green_card_logo_auth);
        logoGreenCardTextView.setText(getSpannableStringBuilder(), TextView.BufferType.SPANNABLE);
        logoGreenCardTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_bold.otf"));

        phoneTil = findViewById(R.id.til_phone);
        phoneEditText = findViewById(R.id.edit_phone);
        phoneEditText.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_semibold.otf"));
        phoneEditText.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_DONE) {
                nextStepButton.performClick();
            }
            return false;
        });

        verificationCodeTil = findViewById(R.id.til_verification_code);
        verificationCodeEditText = findViewById(R.id.edit_verification_code);
        verificationCodeEditText.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_semibold.otf"));
        verificationCodeEditText.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_DONE) {
                nextStepButton.performClick();
            }
            return false;
        });

        passwordTil = findViewById(R.id.til_new_password);
        passwordEditText = findViewById(R.id.edit_new_password);
        passwordEditText.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_semibold.otf"));

        repeatPasswordTil = findViewById(R.id.til_repeat_password);
        repeatPasswordEditText = findViewById(R.id.edit_repeat_password);
        repeatPasswordEditText.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_semibold.otf"));
        repeatPasswordEditText.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_DONE) {
                nextStepButton.performClick();
            }
            return false;
        });

        wrapperFirstStep = findViewById(R.id.linear_wrapper_first_step_forgot_pass);
        wrapperSecondStep = findViewById(R.id.linear_wrapper_second_step_forgot_pass);
        wrapperThirdStep = findViewById(R.id.linear_wrapper_third_step_forgot_pass);

        nextStepButton = findViewById(R.id.btn_forget_password);
        getCompositeDisposable().add(RxView.clicks(nextStepButton)
                .subscribe(o -> getPresenter().checkStepForgotPassword(step),
                        throwable -> Timber.w(throwable, throwable.getMessage())));

        View authButton = findViewById(R.id.linear_wrapper_auth);
        getCompositeDisposable().add(RxView.clicks(authButton)
                .subscribe(o -> goToAuthScreen(),
                        throwable -> Timber.w(throwable, throwable.getMessage())));

    }

    private void initErrorDialog() {
        errorDialog = new Dialog(this);
        errorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        errorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        errorDialog.setContentView(R.layout.dialog_error);
        errorDialog.setCanceledOnTouchOutside(false);
        titleErrorTextView = errorDialog.findViewById(R.id.text_name_error);
        messageTextView = errorDialog.findViewById(R.id.text_message_error);
        TextView closeActionTextView = errorDialog.findViewById(R.id.text_close_dialog);
        closeActionTextView.setOnClickListener(view -> errorDialog.dismiss());
        TextView repeatActionTextView = errorDialog.findViewById(R.id.text_action_repeat);
        repeatActionTextView.setOnClickListener(view -> {
            errorDialog.dismiss();
            switch (step) {
                case CHECK_PHONE:
                    getPresenter().checkFieldPhone(phone);
                    break;
                case CHECK_VERIFICATION_CODE:
                    getPresenter().checkFieldVerificationCode(verificationCodeEditText.getText().toString(), phone);
                    break;
                case CHANGE_PASSWORD:
                    getPresenter().checkFieldPasswordAndRepeatPassword(
                            passwordEditText.getText().toString(),
                            repeatPasswordEditText.getText().toString(),
                            phone, verificationCodeEditText.getText().toString());
                    break;
            }
        });
    }

    private void goToAuthScreen() {
        Intent gotoAuthIntent = new Intent(ForgotPasswordActivity.this, AuthActivity.class);
        startActivity(gotoAuthIntent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        phoneMaskedListener = new MaskedTextChangedListener(
                "+{7} ([000]) [000]-[00]-[00]",
                true,
                phoneEditText,
                null,
                (maskFilled, extractedValue) -> phone = extractedValue);
        phoneEditText.addTextChangedListener(phoneMaskedListener);
        phoneEditText.setOnFocusChangeListener(phoneMaskedListener);
    }

    @NonNull
    private SpannableStringBuilder getSpannableStringBuilder() {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString neededText = new SpannableString(
                getResources().getString(R.string.splash_green_card).toUpperCase());
        neededText.setSpan(new ForegroundColorSpan(ResourcesCompat
                .getColor(getResources(), R.color.colorWhite, null)), 5, neededText.length(), 0);
        builder.append(neededText);
        return builder;
    }

    @Override
    public void onBackPressed() {
        getCurrentFocus().clearFocus();
        switch (step) {
            case CHECK_PHONE:
                goToAuthScreen();
                break;
            case CHECK_VERIFICATION_CODE:
                step = ForgotPasswordStep.CHECK_PHONE;
                nextStepButton.setText(getString(R.string.send_code));
                wrapperFirstStep.setVisibility(View.VISIBLE);
                wrapperSecondStep.setVisibility(View.GONE);
                wrapperThirdStep.setVisibility(View.GONE);
                break;
            case CHANGE_PASSWORD:
                allowVerificationCode();
                break;
        }
    }

    @Override
    public void showError(String message, RequestError requestError) {
        switch (requestError) {
            case TIMEOUT:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_timeout);
                errorDialog.show();
                break;
            case CUSTOM:
                titleErrorTextView.setText(R.string.error_title);
                messageTextView.setText(message);
                errorDialog.show();
                break;
            case NETWORK:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_not_connection_network);
                errorDialog.show();
                break;
        }
    }

    @Override
    public void showPhoneError(@StringRes int messageId) {
        phoneTil.setErrorEnabled(true);
        phoneTil.setError(getString(messageId));
    }

    @Override
    public void showVerificationCodeError(@StringRes int messageId) {
        verificationCodeTil.setErrorEnabled(true);
        verificationCodeTil.setError(getString(messageId));
    }

    @Override
    public void showPasswordError(@StringRes int messageId) {
        passwordTil.setErrorEnabled(true);
        passwordTil.setError(getString(messageId));
    }

    @Override
    public void showRepeatPasswordError(@StringRes int messageId) {
        repeatPasswordTil.setErrorEnabled(true);
        repeatPasswordTil.setError(getString(messageId));
    }

    @Override
    public void hideFieldError() {
        if (passwordTil.isErrorEnabled()) passwordTil.setErrorEnabled(false);
        if (repeatPasswordTil.isErrorEnabled()) repeatPasswordTil.setErrorEnabled(false);
    }

    @Override
    public void hideFieldPhoneError() {
        if (phoneTil.isErrorEnabled()) phoneTil.setErrorEnabled(false);
    }

    @Override
    public void hideFieldVerificationError() {
        if (verificationCodeTil.isErrorEnabled()) verificationCodeTil.setErrorEnabled(false);
    }

    @Override
    public void allowVerificationCode() {
        step = ForgotPasswordStep.CHECK_VERIFICATION_CODE;
        nextStepButton.setText(getString(R.string.verify_code));
        wrapperFirstStep.setVisibility(View.GONE);
        wrapperSecondStep.setVisibility(View.VISIBLE);
        wrapperThirdStep.setVisibility(View.GONE);
    }

    @Override
    public void allowPasswordChange() {
        step = ForgotPasswordStep.CHANGE_PASSWORD;
        nextStepButton.setText(getString(R.string.save_new_password));
        wrapperFirstStep.setVisibility(View.GONE);
        wrapperSecondStep.setVisibility(View.GONE);
        wrapperThirdStep.setVisibility(View.VISIBLE);
    }

    @Override
    public void restorePasswordSuccess() {
        Toast.makeText(this, getString(R.string.password_changed_successfully), Toast.LENGTH_SHORT).show();
        goToAuthScreen();
    }

    @Override
    public void firstStepForgotPassword() {
        getPresenter().checkFieldPhone(phone);
    }

    @Override
    public void secondStepForgotPassword() {
        getPresenter().checkFieldVerificationCode(verificationCodeEditText.getText().toString(), phone);
    }

    @Override
    public void thirdStepForgotPassword() {
        getPresenter().checkFieldPasswordAndRepeatPassword(
                passwordEditText.getText().toString(),
                repeatPasswordEditText.getText().toString(),
                phone, verificationCodeEditText.getText().toString());
    }

    @Override
    public void showFieldErrors(Map<String, String> fieldErrors) {
        for (Map.Entry<String, String> entry : fieldErrors.entrySet()) {
            String id = entry.getKey();
            TextInputLayout layout = root.findViewWithTag(id);
            if (layout != null) {
                layout.setErrorEnabled(true);
                layout.setError(entry.getValue());
            }
        }
    }

    @Override
    public void showToastSendCode() {
        ViewUtil.showToast(
                this,
                String.format(
                        Locale.getDefault(),
                        "%s %s",
                        getString(R.string.fragment_family_account_sms_send_on_phone),
                        phoneEditText.getText().toString()));
    }

    public enum ForgotPasswordStep {
        CHECK_PHONE,
        CHECK_VERIFICATION_CODE,
        CHANGE_PASSWORD
    }
}
