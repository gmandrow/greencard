package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.PartnerDescriptionContract;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;

import java.util.List;

public class PartnerDescriptionPresenter extends BasePresenter<PartnerDescriptionContract.View>
        implements PartnerDescriptionContract.Presenter {

    /**
     * KOSTIL'
     * This is method formatting <iframe></iframe> with code video into html page.
     * And set style for iframe because his value of width and height is very big
     *
     * @param videoCode - this is <iframe>with youtube url</iframe>
     * @return html page with youtube video
     */
    private String getUrlVideo(String videoCode) {
        return String.format("%s%s%s", "<html><style type=\"text/css\">\n" +
                "iframe {\n" +
                "    height: auto !important;\n" +
                "    max-width: 100% !important;\n" +
                "    }\n" +
                "body {\n" +
                "    margin: 0px;\n" +
                "    }\n" +
                "</style><body>", videoCode, "</body></html>");
    }

    @Override
    public void checkData(String description, List<String> srcPhotos, String videoCode) {
        if (!isViewAttached()) return;

        getView().showDescription(description);

        if (CommonUtil.checkListOnNullOrEmpty(srcPhotos)) {
            getView().hideGallery();
            getView().hideOnePhoto();
        } else if (srcPhotos.size() == 1) {
            getView().showOnePhoto(srcPhotos.get(Constants.FIRST_ITEM));
            getView().hideGallery();
        } else {
            getView().showGallery(srcPhotos);
            getView().hideOnePhoto();
        }

        if (CommonUtil.checkStringOnNullOrEmpty(videoCode)) getView().hideVideo();
        else getView().showVideo(
                getUrlVideo(videoCode)
        );
    }
}
