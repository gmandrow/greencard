package com.aspirity.greencard.presentation.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.Oferta;
import com.aspirity.greencard.domain.usecase.CardsUseCase;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BaseActivity;
import com.aspirity.greencard.presentation.contract.RegistrationContract;
import com.aspirity.greencard.presentation.presenter.RegistrationPresenter;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.ViewUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.google.android.material.textfield.TextInputLayout;
import com.jakewharton.rxbinding2.view.RxView;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.res.ResourcesCompat;
import timber.log.Timber;

public class RegistrationActivity
        extends BaseActivity<RegistrationContract.View, RegistrationContract.Presenter>
        implements RegistrationContract.View {

    private EditText cardNumberEditText;
    private EditText cardCodeEditText;
    private String cardNumber = "";
    private MaskedTextChangedListener cardNumberMaskedListener;
    private Button registrationButton;
    private TextView logoGreenCardTextView;
    private RegistrationStep step;
    private View wrapperSecondStep;
    private TextInputLayout firstNameTil;
    private EditText firstNameEditText;
    private TextInputLayout birthdayTil;
    private EditText birthdayEditText;
    private TextInputLayout phoneTil;
    private EditText phoneEditText;
    private TextInputLayout emailTil;
    private EditText emailEditText;
    private RadioGroup genderRadioGroup;
    private RadioButton maleRadioButton;
    private RadioButton femaleRadioButton;
    private CheckBox agreementOfertaCheckBox;
    private View showOferta;
    private TextInputLayout cardNumberTil;
    private TextInputLayout cardCodeTil;
    private MaskedTextChangedListener phoneMaskedListener;
    private String phone = "";
    private String birthday = "";
    private MaskedTextChangedListener birthdayMaskedListener;
    private View wrapperFirstStep;
    private Dialog ofertaDialog;
    private TextView titleErrorTextView;
    private TextView messageTextView;
    private Dialog errorDialog;
    private ViewGroup root;
    private View wrapperOferta;
    private HtmlTextView ofertaTextView;
    private TextView ofertaTitleTextView;
    private TextView ofertaSubheaderTextView;
    private Button ofertaButton;
    private TextView ofertaHeader;
    private boolean isOfertaShown = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        step = RegistrationStep.CHECK_CARD;
        UserUseCase userUseCase = new UserUseCase(
                getApplicationContext(),
                RepositoryProvider.getUserRepository(),
                new AppSchedulerProvider());
        CardsUseCase cardsUseCase = new CardsUseCase(
                getApplicationContext(),
                new AppSchedulerProvider(),
                RepositoryProvider.getCardsRepository());
        onCreatePresenter(new RegistrationPresenter(
                getGreenCardApp().getPreferenceDataManager(),
                userUseCase,
                cardsUseCase,
                getGreenCardApp().getDateUtils()), this);
        initErrorDialog();
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getCurrentFocus() != null) {
            getCurrentFocus().clearFocus();
        }
        if (birthdayEditText != null) {
            birthdayEditText.setOnFocusChangeListener((v, hasFocus) -> {
                if (hasFocus) {
                    birthdayEditText.setHint("дд.мм.гггг");
                } else {
                    birthdayEditText.setHint("");
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (errorDialog == null) {
            initErrorDialog();
        }

        cardNumberMaskedListener = new MaskedTextChangedListener(
                "[0000] [0000] [0000] [0000]",
                true,
                cardNumberEditText,
                null,
                (maskFilled, extractedValue) -> {
                    cardNumber = extractedValue;
                });
        cardNumberEditText.addTextChangedListener(cardNumberMaskedListener);
        cardNumberEditText.setOnFocusChangeListener(cardNumberMaskedListener);

        birthdayMaskedListener = new MaskedTextChangedListener(
                "[00]{.}[00]{.}[0000]",
                true,
                birthdayEditText,
                null,
                (maskFilled, extractedValue) -> {
                    birthday = extractedValue;
                });
        birthdayEditText.addTextChangedListener(birthdayMaskedListener);
        birthdayEditText.setOnFocusChangeListener(birthdayMaskedListener);

        phoneMaskedListener = new MaskedTextChangedListener(
                "+{7} ([000]) [000]-[00]-[00]",
                true,
                phoneEditText,
                null,
                (maskFilled, extractedValue) -> {
                    phone = extractedValue;
                });
        phoneEditText.addTextChangedListener(phoneMaskedListener);
        phoneEditText.setOnFocusChangeListener(phoneMaskedListener);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
//        showError(event.message, event.requestError);
    }

    @Override
    protected void onStop() {
        super.onStop();
        phoneEditText.removeTextChangedListener(phoneMaskedListener);
        birthdayEditText.removeTextChangedListener(birthdayMaskedListener);
        if (ofertaDialog != null) {
            ofertaDialog.dismiss();
            ofertaDialog = null;
        }

        if (errorDialog != null) {
            errorDialog.dismiss();
            errorDialog = null;
        }
    }

    private void initViews() {
        root = findViewById(R.id.root);
        initLogo();
        initFirstRegistrationStepViews();
        initSecondRegistrationStepViews();

        View authButton = findViewById(R.id.linear_wrapper_auth);
        getCompositeDisposable().add(RxView.clicks(authButton)
                .subscribe(o -> {
                    goToAuthScreen();
                }, throwable -> Timber.w(throwable, throwable.getMessage())));

        registrationButton = findViewById(R.id.btn_registration);
        getCompositeDisposable().add(RxView.clicks(registrationButton)
                .subscribe(
                        o -> getPresenter().checkStepRegistration(step),
                        throwable -> Timber.w(throwable, throwable.getMessage())));
    }

    private void initLogo() {
        logoGreenCardTextView = findViewById(R.id.text_green_card_logo_auth);
        logoGreenCardTextView.setText(getSpannableStringBuilder(), TextView.BufferType.SPANNABLE);
        logoGreenCardTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_bold.otf"));
    }

    private void initFirstRegistrationStepViews() {
        wrapperFirstStep = findViewById(R.id.linear_wrapper_first_step_reg);
        cardNumberTil = findViewById(R.id.til_card_number);
        cardNumberEditText = findViewById(R.id.edit_card);
        cardCodeTil = findViewById(R.id.til_card_code);
        cardCodeEditText = findViewById(R.id.edit_card_code);
        cardCodeEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                registrationButton.performClick();
            }
            return false;
        });
    }

    private void initSecondRegistrationStepViews() {
        wrapperSecondStep = findViewById(R.id.linear_wrapper_second_step_reg);

        firstNameTil = findViewById(R.id.til_first_name);
        firstNameEditText = findViewById(R.id.edit_first_name);

        firstNameEditText.setFilters(new InputFilter[]{
                (src, start, end, dest, dstart, dend) -> {
                    if (src.equals("")) { // for backspace
                        return src;
                    }
                    if (src.toString().matches("[a-zA-Zа-яА-Я ]+")) {
                        return src;
                    }
                    return "";
                },
                new InputFilter.LengthFilter(50)
        });

        birthdayTil = findViewById(R.id.til_birthday);
        birthdayEditText = findViewById(R.id.edit_birthday);

        phoneTil = findViewById(R.id.til_phone);
        phoneEditText = findViewById(R.id.edit_phone);

        emailTil = findViewById(R.id.til_email);
        emailEditText = findViewById(R.id.edit_email);

        emailEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                registrationButton.performClick();
            }
            return false;
        });

        genderRadioGroup = findViewById(R.id.radio_group_gender);
        maleRadioButton = findViewById(R.id.radio_btn_male);
        femaleRadioButton = findViewById(R.id.radio_btn_female);

        agreementOfertaCheckBox = findViewById(R.id.checkbox_agreement_oferta);
        showOferta = findViewById(R.id.image_oferta);
        showOferta.setOnClickListener(view -> getPresenter().loadOferta());

        wrapperOferta = findViewById(R.id.linear_wrapper_oferta);
        ofertaButton = findViewById(R.id.btn_oferta);
        ofertaTitleTextView = findViewById(R.id.text_title_oferta);
        ofertaSubheaderTextView = findViewById(R.id.text_sub_header_oferta);
        ofertaTextView = findViewById(R.id.text_content_oferta);
        ofertaHeader = findViewById(R.id.text_header_oferta);
    }

    private void initErrorDialog() {
        errorDialog = new Dialog(this);
        errorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        errorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        errorDialog.setContentView(R.layout.dialog_error);
        errorDialog.setCanceledOnTouchOutside(false);
        titleErrorTextView = errorDialog.findViewById(R.id.text_name_error);
        messageTextView = errorDialog.findViewById(R.id.text_message_error);
        TextView closeActionTextView = errorDialog.findViewById(R.id.text_close_dialog);
        closeActionTextView.setOnClickListener(view -> errorDialog.dismiss());
        TextView repeatActionTextView = errorDialog.findViewById(R.id.text_action_repeat);
        repeatActionTextView.setOnClickListener(view -> {
            errorDialog.dismiss();
            RequestType req = getPresenter().getLastRequest();
            switch (req) {
                case VALIDATE_CARD:
                    getPresenter().checkFieldNumberAndCode(
                            cardNumberEditText.getText().toString(),
                            cardCodeEditText.getText().toString()
                    );
                    break;
                case LOAD_OFERTA:
                    getPresenter().loadOferta();
                    break;
                case REGISTRATION:
                    getPresenter().registration(
                            cardNumberEditText.getText().toString(),
                            cardCodeEditText.getText().toString(),
                            firstNameEditText.getText().toString(),
                            birthday,
                            maleRadioButton.isChecked() ? "M" : "F",
                            phone,
                            emailEditText.getText().toString(),
                            agreementOfertaCheckBox.isChecked());
                    break;
            }
        });
    }

    private void goToAuthScreen() {
        Intent gotoAuthIntent = new Intent(RegistrationActivity.this, AuthActivity.class);
        startActivity(gotoAuthIntent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    @NonNull
    private SpannableStringBuilder getSpannableStringBuilder() {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString neededText = new SpannableString(
                getResources().getString(R.string.splash_green_card).toUpperCase());
        neededText.setSpan(new ForegroundColorSpan(ResourcesCompat
                .getColor(getResources(), R.color.colorWhite, null)), 5, neededText.length(), 0);
        builder.append(neededText);
        return builder;
    }

    @Override
    public void onBackPressed() {
        getCurrentFocus().clearFocus();
        if (step == RegistrationStep.PERSON_DATA) {
            if (isOfertaShown) {
                wrapperOferta.setVisibility(View.GONE);
                ofertaButton.setVisibility(View.GONE);
                ofertaHeader.setVisibility(View.GONE);
                isOfertaShown = false;
                registrationButton.setVisibility(View.VISIBLE);
                logoGreenCardTextView.setVisibility(View.VISIBLE);
                findViewById(R.id.text_green_card_logo_g_auth).setVisibility(View.VISIBLE);
            } else {
                step = RegistrationStep.CHECK_CARD;
                registrationButton.setText(R.string.registration);
                cardNumberTil.setEnabled(true);
                cardCodeTil.setEnabled(true);
                hideEmailError();
                hideBirthdayError();
                hideNameError();
                hidePhoneError();
                wrapperSecondStep.setVisibility(View.GONE);
            }
        } else {
            super.onBackPressed();
            finishAffinity();
        }
    }

    @Override
    public void showError(String message, RequestError requestError) {
        switch (requestError) {
            case TIMEOUT:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_timeout);
                errorDialog.show();
                break;
            case CUSTOM:
                titleErrorTextView.setText(R.string.error_title);
                messageTextView.setText(message);
                errorDialog.show();
                break;
            case NETWORK:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_not_connection_network);
                errorDialog.show();
                break;
        }
    }

    @Override
    public void showCardNumberError(@StringRes int messageId) {
        cardNumberTil.setErrorEnabled(true);
        cardNumberTil.setError(getString(messageId));
    }

    @Override
    public void showCardCodeError(@StringRes int messageId) {
        cardCodeTil.setErrorEnabled(true);
        cardCodeTil.setError(getString(messageId));
    }

    @Override
    public void showFirstNameError(@StringRes int messageId) {
        firstNameTil.setErrorEnabled(true);
        firstNameTil.setError(getString(messageId));
    }

    @Override
    public void showBirthdayError(@StringRes int messageId) {
        birthdayTil.setErrorEnabled(true);
        birthdayTil.setError(getString(messageId));
    }

    @Override
    public void showPhoneError(@StringRes int messageId) {
        phoneTil.setErrorEnabled(true);
        phoneTil.setError(getString(messageId));
    }

    @Override
    public void showAgreementOfertaError(@StringRes int messageId) {
        Toast.makeText(this, messageId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmailError(@StringRes int messageId) {
        emailTil.setErrorEnabled(true);
        emailTil.setError(getString(messageId));
    }

    @Override
    public void showFieldErrors(Map<String, String> fieldErrors) {
        for (Map.Entry<String, String> entry : fieldErrors.entrySet()) {
            String id = entry.getKey();
            TextInputLayout layout = root.findViewWithTag(id);
            if (layout != null) {
                layout.setErrorEnabled(true);
                layout.setError(entry.getValue());
            }
        }
    }

    @Override
    public void hideBirthdayError() {
        birthdayTil.setErrorEnabled(false);
    }

    @Override
    public void hideNameError() {
        firstNameTil.setErrorEnabled(false);
    }

    @Override
    public void hidePhoneError() {
        phoneTil.setErrorEnabled(false);
    }

    @Override
    public void hideEmailError() {
        emailTil.setErrorEnabled(false);
    }

    @Override
    public void hideFieldError() {
        cardNumberTil.setErrorEnabled(false);
        cardCodeTil.setErrorEnabled(false);
    }

    @Override
    public void registrationSuccess() {
        ViewUtil.showToast(
                this,
                String.format(
                        Locale.getDefault(),
                        "%s %s",
                        getString(R.string.fragment_family_account_sms_send_on_phone),
                        phoneEditText.getText().toString()));
        goToAuthScreen();
    }

    @Override
    public void allowRegistration() {
        step = RegistrationStep.PERSON_DATA;
        registrationButton.setText(R.string.continue_registration);
        cardNumberTil.setEnabled(false);
        cardCodeTil.setEnabled(false);
        birthdayEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                birthdayEditText.setHint("дд.мм.гггг");
            } else {
                birthdayEditText.setHint("");
            }
        });
        wrapperSecondStep.setVisibility(View.VISIBLE);
    }

    @Override
    public void firstStepRegistration() {
        getPresenter().checkFieldNumberAndCode(
                cardNumber,
                cardCodeEditText.getText().toString());
    }

    @Override
    public void secondStepRegistration() {
        getPresenter().checkPersonData(
                agreementOfertaCheckBox.isChecked(),
                cardNumber,
                cardCodeEditText.getText().toString(),
                firstNameEditText.getText().toString(),
                birthday,
                maleRadioButton.isChecked() ? "M" : "F",
                phone,
                emailEditText.getText().toString());
    }

    @Override
    public void showOferta(Oferta oferta) {
        isOfertaShown = true;
        wrapperOferta.setVisibility(View.VISIBLE);
        ofertaButton.setVisibility(View.VISIBLE);
        ofertaHeader.setVisibility(View.VISIBLE);
        registrationButton.setVisibility(View.GONE);
        logoGreenCardTextView.setVisibility(View.INVISIBLE);
        findViewById(R.id.text_green_card_logo_g_auth).setVisibility(View.INVISIBLE);
        ofertaTitleTextView.setText(oferta.getTitle());
        ofertaSubheaderTextView.setText(oferta.getSubHeader());
        ofertaTextView.setHtml(oferta.getContent());
        ofertaButton.setOnClickListener(view -> {
            wrapperOferta.setVisibility(View.GONE);
            ofertaButton.setVisibility(View.GONE);
            ofertaHeader.setVisibility(View.GONE);
            logoGreenCardTextView.setVisibility(View.VISIBLE);
            findViewById(R.id.text_green_card_logo_g_auth).setVisibility(View.VISIBLE);
            isOfertaShown = false;
            registrationButton.setVisibility(View.VISIBLE);
            agreementOfertaCheckBox.setChecked(true);
        });
        /*
        ofertaDialog = new Dialog(this);
        ofertaDialog.setContentView(R.layout.dialog_oferta);
        TextView title = ofertaDialog.findViewById(R.id.text_title);
        title.setText(oferta.getTitle());
        TextView subHeader = ofertaDialog.findViewById(R.id.text_sub_header);
        subHeader.setText(oferta.getSubHeader());
        HtmlTextView content = ofertaDialog.findViewById(R.id.text_content);
        content.setHtml(oferta.getContent());
        content.setMovementMethod(LinkMovementMethod.getInstance());
        Button close = ofertaDialog.findViewById(R.id.btn_close);
        close.setOnClickListener(view -> ofertaDialog.dismiss());
        ofertaDialog.show();*/
    }

    public enum RegistrationStep {
        CHECK_CARD,
        PERSON_DATA
    }
}
