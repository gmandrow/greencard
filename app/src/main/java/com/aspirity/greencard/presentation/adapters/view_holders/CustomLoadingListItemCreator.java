package com.aspirity.greencard.presentation.adapters.view_holders;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aspirity.greencard.R;
import com.paginate.recycler.LoadingListItemCreator;

import androidx.recyclerview.widget.RecyclerView;

public class CustomLoadingListItemCreator implements LoadingListItemCreator {
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.loading_row, parent, false);
        return new LoadingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // Bind custom loading row if needed
    }

    public static class LoadingViewHolder extends RecyclerView.ViewHolder {

        public LoadingViewHolder(View itemView) {
            super(itemView);
        }
    }
}
