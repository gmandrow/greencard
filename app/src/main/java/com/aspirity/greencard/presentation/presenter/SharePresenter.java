package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.SharesUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.ShareContract;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.net.SocketTimeoutException;

import retrofit2.HttpException;

public class SharePresenter extends BasePresenter<ShareContract.View>
        implements ShareContract.Presenter {

    private final PreferenceDataManager preferenceDataManager;
    private final SharesUseCase sharesUseCase;

    public SharePresenter(
            PreferenceDataManager preferenceDataManager,
            SharesUseCase sharesUseCase) {
        this.preferenceDataManager = preferenceDataManager;
        this.sharesUseCase = sharesUseCase;
    }

    @Override
    public void getShareContent(Long shareId, boolean isArchived) {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(
                sharesUseCase.getShare(shareId, isArchived)
                        .onErrorResumeNext(error -> {
                            handleError(error);
                            return sharesUseCase.getShareFallback(shareId);
                        })
                        .subscribe(share -> {
                            getView().showContentShare(share);
                        }, this::handleError));
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                getView().showError(
                        ErrorParse.parseError(httpException.response()),
                        RequestError.CUSTOM);
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        } else {
            // generic error handling
        }
    }
}
