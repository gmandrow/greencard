package com.aspirity.greencard.presentation.views;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

public class HorizontalScrollBehavior extends CoordinatorLayout.Behavior<View> {

    private int tempOffset = 0;

    public HorizontalScrollBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull View child, @NonNull View directTargetChild, @NonNull View target, int axes, int type) {
        return true;
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof RecyclerView;

    }

    @Override
    public void onNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull View child, @NonNull View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed, int type) {
//        int offset = ((RecyclerView)target).computeVerticalScrollOffset();
//        Timber.d("OFFSET RECYCLER %s", String.valueOf(offset));
//        if (offset >= tempOffset) {
//            child.setTranslationX(offset);
//            tempOffset = offset;
//        } else {
//            child.setTranslationX(offset + 20);
//        }
    }
}
