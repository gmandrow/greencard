package com.aspirity.greencard.presentation.utils;

import android.annotation.SuppressLint;
import android.os.Build;
import android.transition.Slide;
import android.view.Gravity;

import com.aspirity.greencard.R;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class FragmentUtil {

    public static final String FRAGMENT_TAG = "FRAGMENT_TAG";

    private FragmentUtil() {
    }

    public static void setTransitionSlide(Fragment fragment, int gravity) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            Slide slide = new Slide(gravity);
            slide.setDuration(Constants.DURATION_SLIDE_ANIM);
            fragment.setEnterTransition(slide);
            fragment.setExitTransition(slide);
        }
    }

    public static void setTransitionSlide(Fragment fragment, int gravityEnter, int gravityExit) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            Slide slideEnter = new Slide(gravityEnter);
            Slide slideExit = new Slide(gravityExit);
            slideEnter.setDuration(Constants.DURATION_SLIDE_ANIM);
            slideExit.setDuration(Constants.DURATION_SLIDE_ANIM);
            fragment.setEnterTransition(slideEnter);
            fragment.setExitTransition(slideExit);
        }
    }

    @SuppressLint("RtlHardcoded")
    public static void setTransitionSlideForTwoFragment(Fragment fragmentOne, Fragment fragmentTwo) {
        // TODO: 31.10.2017 запилил на RIGHT & LEFT вместо END & START если когда либо будут заходить в страны RTL
        setTransitionSlide(fragmentOne, Gravity.RIGHT, Gravity.LEFT);
        setTransitionSlide(fragmentTwo, Gravity.RIGHT);
    }

    public static void replaceFragment(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, fragment, FRAGMENT_TAG)
                .commit();
    }

    public static void addBackStackByNameFragment(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction()
                .addToBackStack(fragment.getClass().getName().split("\\{")[0])
                .add(R.id.frame_container, fragment)
                .commit();
    }

    public static void addBackStackFragment(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction()
                .addToBackStack(null)
                .replace(R.id.frame_container, fragment)
                .commit();
    }

    public static void addBackStackWithAddFragment(FragmentManager fragmentManager, Fragment fragment) {
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.frame_container);
        if (!currentFragment.getClass().equals(fragment.getClass())) {
            fragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .add(R.id.frame_container, fragment)
                    .commit();
        }
    }
}
