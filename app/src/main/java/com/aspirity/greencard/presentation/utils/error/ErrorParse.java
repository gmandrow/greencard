package com.aspirity.greencard.presentation.utils.error;

import com.aspirity.greencard.presentation.model.ErrorWrapper;
import com.aspirity.greencard.presentation.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import retrofit2.Response;
import timber.log.Timber;

public class ErrorParse {

    public static String parseError(Response<?> response) {

        try {
            String errorBody = response.errorBody().string();
            JSONObject jObjError = new JSONObject(errorBody);
            return jObjError.getString("non_field_errors")
                    .replace("[", "")
                    .replace("]", "")
                    .replace("\"", "");
        } catch (IOException e) {
            Timber.w(e, e.getMessage());
            return Constants.EMPTY_ANSWER;
        } catch (JSONException e) {
            Timber.w(e, e.getMessage());
            return Constants.EMPTY_ANSWER;
        }
    }

    public static ErrorWrapper parseFormErrors(Response<?> response) {

        ErrorWrapper wrapper = new ErrorWrapper();
        try {
            Map<String, String> errors = new HashMap<>();
            String errorBody = response.errorBody().string();
            JSONObject obj = new JSONObject(errorBody);
            Iterator<?> keys = obj.keys();

            while (keys.hasNext()) {
                String key = (String) keys.next();
                String value = obj.getString(key)
                        .replaceAll("[\\[\\]\\\"]", "");
                if (Objects.equals(key, "non_field_errors")) {
                    wrapper.setGeneralError(value);
                } else if (Objects.equals(key, "errors")) {
                    wrapper.setGeneralError(value);
                } else {
                    errors.put(key, value);
                }
            }
            wrapper.setFieldErrors(errors);
            return wrapper;
        } catch (IOException | JSONException e) {
            Timber.w(e, e.getMessage());
            return wrapper;
        }
    }
}
