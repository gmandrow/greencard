package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.R;
import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.CardsUseCase;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.activities.RegistrationActivity;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.RegistrationContract;
import com.aspirity.greencard.presentation.model.ErrorWrapper;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.DateUtil;
import com.aspirity.greencard.presentation.utils.RegExpHelper;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.Locale;

import retrofit2.HttpException;

public class RegistrationPresenter extends BasePresenter<RegistrationContract.View>
        implements RegistrationContract.Presenter {

    private PreferenceDataManager preferenceDataManager;
    private UserUseCase userUseCase;
    private CardsUseCase cardsUseCase;
    private RequestType lastRequest;
    private boolean isValidPersonData;
    private DateUtil dateUtil;

    public RegistrationPresenter(
            PreferenceDataManager preferenceDataManager,
            UserUseCase userUseCase,
            CardsUseCase cardsUseCase,
            DateUtil dateUtil) {
        this.preferenceDataManager = preferenceDataManager;
        this.userUseCase = userUseCase;
        this.cardsUseCase = cardsUseCase;
        this.dateUtil = dateUtil;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                ErrorWrapper wrapper = ErrorParse.parseFormErrors(httpException.response());
                String generalError = wrapper.getGeneralError();
                if (wrapper.getFieldErrors() != null && CommonUtil.checkStringOnNullOrEmpty(generalError)) {
                    getView().showFieldErrors(wrapper.getFieldErrors());
                } else {
                    getView().showError(generalError, RequestError.CUSTOM);
                }
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        }
    }

    @Override
    public void registration(
            String cardNumber,
            String cardCode,
            String firstName,
            String birthDate,
            String gender,
            String phone,
            String email,
            Boolean agreement) {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(userUseCase
                .registration(
                        cardNumber,
                        cardCode,
                        firstName,
                        gender,
                        parseBirthday(birthDate),
                        phone,
                        email,
                        agreement)
                .doOnSubscribe(disposable -> {
                    lastRequest = RequestType.REGISTRATION;
                    getView().showLoading();
                })
                .doOnSuccess(user -> getView().hideLoading())
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(user -> {
                    getView().registrationSuccess();
                }, this::handleError));
    }

    private String parseBirthday(String birthDate) {
        String[] strings = birthDate.split("\\.");
        if (strings.length == 3) {
            return String.format(
                    Locale.getDefault(),
                    "%s-%s-%s",
                    strings[2],
                    strings[1],
                    strings[0]);
        }
        return Constants.EMPTY_ANSWER;
    }

    @Override
    public void checkFieldNumberAndCode(String cardNumber, String cardCode) {
        if (!isViewAttached()) return;

        getView().hideFieldError();

        if (CommonUtil.checkStringOnNullOrEmpty(cardNumber.trim())
                && CommonUtil.checkStringOnNullOrEmpty(cardCode.trim())) {
            getView().showCardNumberError(R.string.error_message_empty_field);
            getView().showCardCodeError(R.string.error_message_empty_field);
            return;
        } else if (CommonUtil.checkStringOnNullOrEmpty(cardNumber.trim()) && cardCode.length() != 4) {
            getView().showCardNumberError(R.string.error_message_empty_field);
            getView().showCardCodeError(R.string.error_message_short_card_code);
            return;
        } else if (CommonUtil.checkStringOnNullOrEmpty(cardCode.trim()) && cardNumber.length() != 16) {
            getView().showCardCodeError(R.string.error_message_empty_field);
            getView().showCardNumberError(R.string.error_message_short_card_number);
            return;
        } else if (CommonUtil.checkStringOnNullOrEmpty(cardNumber.trim())) {
            getView().showCardNumberError(R.string.error_message_empty_field);
            return;
        } else if (CommonUtil.checkStringOnNullOrEmpty(cardCode.trim())) {
            getView().showCardCodeError(R.string.error_message_empty_field);
            return;
        } else if (cardNumber.length() != 16 && cardCode.length() != 4) {
            getView().showCardNumberError(R.string.error_message_short_card_number);
            getView().showCardCodeError(R.string.error_message_short_card_code);
            return;
        } else if (cardNumber.length() != 16) {
            getView().showCardNumberError(R.string.error_message_short_card_number);
            return;
        } else if (cardCode.length() != 4) {
            getView().showCardCodeError(R.string.error_message_short_card_code);
        }

        getCompositeDisposable().add(cardsUseCase.validateCard(cardNumber, cardCode)
                .doOnSubscribe(disposable -> {
                    lastRequest = RequestType.VALIDATE_CARD;
                    getView().showLoading();
                })
                .doOnSuccess(booleanStringPair -> getView().hideLoading())
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(validCard -> getView().allowRegistration(), this::handleError));
    }

    @Override
    public void checkStepRegistration(RegistrationActivity.RegistrationStep registrationStep) {
        if (!isViewAttached()) return;

        switch (registrationStep) {
            case CHECK_CARD:
                getView().firstStepRegistration();
                break;
            case PERSON_DATA:
                getView().secondStepRegistration();
                break;
        }
    }

    @Override
    public void checkPersonData(
            boolean isAgreementOferta,
            String cardNumber,
            String cardCode,
            String firstName,
            String birthDate,
            String gender,
            String phone,
            String email) {
        if (!isViewAttached()) return;

        isValidPersonData = true;
        checkName(firstName);
        checkBirthday(birthDate);
        checkPhone(phone);
        checkEmail(email);
        if (!isAgreementOferta) {
            getView().showAgreementOfertaError(R.string.error_message_agreement_oferta);
            isValidPersonData = false;
        }

        if (isValidPersonData) {
            registration(cardNumber, cardCode, firstName, birthDate, gender, phone, email, true);
        }
    }

    @Override
    public void loadOferta() {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(userUseCase.getOferta()
                .doOnSubscribe(disposable -> {
                    lastRequest = RequestType.LOAD_OFERTA;
                    getView().showLoading();
                })
                .doOnSuccess(booleanStringPair -> getView().hideLoading())
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(oferta -> getView().showOferta(oferta), this::handleError));
    }

    @Override
    public void checkBirthday(String birthday) {
        if (!isViewAttached()) return;

        if (!RegExpHelper.isMatches(RegExpHelper.DATE, birthday)) {
            isValidPersonData = false;
            getView().showBirthdayError(R.string.error_message_wrong_format_date);
            return;
        }

        Date date = dateUtil.getDateFromString(birthday);
        if (date == null) {
            isValidPersonData = false;
            getView().showBirthdayError(R.string.error_message_wrong_format_date);
            return;
        }

        if (date.after(new Date())) {
            isValidPersonData = false;
            getView().showBirthdayError(R.string.error_message_date_future);
            return;
        }

        getView().hideBirthdayError();
    }

    @Override
    public void checkName(String name) {
        if (!isViewAttached()) return;

        if (CommonUtil.checkStringOnNullOrEmpty(name)) {
            getView().showFirstNameError(R.string.error_message_empty_field);
            isValidPersonData = false;
        } else {
            getView().hideNameError();
        }
    }

    @Override
    public void checkPhone(String phone) {
        if (!isViewAttached()) return;

        if (RegExpHelper.isMatches(RegExpHelper.PHONE, phone)) {
            getView().hidePhoneError();
        } else {
            isValidPersonData = false;
            getView().showPhoneError(R.string.error_message_wrong_format_phone);
        }
    }

    @Override
    public void checkEmail(String email) {
        if (!isViewAttached()) return;

        if (CommonUtil.checkStringOnNullOrEmpty(email)) return;

        if (RegExpHelper.isMatches(RegExpHelper.EMAIL, email)) {
            getView().hideEmailError();
        } else {
            isValidPersonData = false;
            getView().showEmailError(R.string.error_message_wrong_format_email);
        }
    }

    @Override
    public RequestType getLastRequest() {
        return lastRequest;
    }
}
