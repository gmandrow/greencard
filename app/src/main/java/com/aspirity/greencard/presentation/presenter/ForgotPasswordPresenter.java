package com.aspirity.greencard.presentation.presenter;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.activities.ForgotPasswordActivity;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.ForgetPasswordContract;
import com.aspirity.greencard.presentation.model.ErrorWrapper;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.net.SocketTimeoutException;

import retrofit2.HttpException;

public class ForgotPasswordPresenter extends BasePresenter<ForgetPasswordContract.View>
        implements ForgetPasswordContract.Presenter {

    private UserUseCase userUseCase;

    public ForgotPasswordPresenter(UserUseCase userUseCase) {
        this.userUseCase = userUseCase;
    }

    @Override
    public void sendVerificationCode(String phone) {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(userUseCase.restorePassword(phone)
                .doOnSubscribe(disposable -> getView().showLoading())
                .doOnSuccess(success -> {
                    getView().hideLoading();
                    getView().showToastSendCode();
                })
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(o -> getView().allowVerificationCode(), this::handleError));
    }

    @Override
    public void verifySmsCode(String phone, String smsCode) {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(userUseCase.validateSmsCode(smsCode, phone)
                .doOnSubscribe(disposable -> getView().showLoading())
                .doOnSuccess(success -> {
                    getView().hideLoading();
                })
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(o -> getView().allowPasswordChange(), this::handleError));
    }

    @Override
    public void setNewPassword(String phone, String smsCode, String password) {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(userUseCase.setNewPassword(phone, smsCode, password)
                .doOnSubscribe(disposable -> getView().showLoading())
                .doOnSuccess(success -> {
                    getView().hideLoading();
                })
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(o -> getView().restorePasswordSuccess(), this::handleError));
    }

    @Override
    public void checkFieldPhone(String phone) {
        if (!isViewAttached()) {
            return;
        }

        getView().hideFieldPhoneError();

        if (!CommonUtil.checkStringOnNullOrEmpty(phone) && phone.length() == 11) {
            sendVerificationCode(phone);
            return;
        }

        if (CommonUtil.checkStringOnNullOrEmpty(phone)) {
            getView().showPhoneError(R.string.error_message_empty_field);
        } else if (phone.length() < 11) {
            getView().showPhoneError(R.string.error_message_wrong_format_phone);
        }
    }

    @Override
    public void checkFieldVerificationCode(String code, String phone) {
        if (!isViewAttached()) {
            return;
        }

        getView().hideFieldVerificationError();

        if (!CommonUtil.checkStringOnNullOrEmpty(code) && code.length() >= 4) {
            verifySmsCode(phone, code);
            return;
        } else if (CommonUtil.checkStringOnNullOrEmpty(code)) {
            getView().showVerificationCodeError(R.string.error_message_empty_field);
        } else {
            getView().showVerificationCodeError(R.string.error_message_short_code);
        }
    }

    @Override
    public void checkFieldPasswordAndRepeatPassword(String password, String repeat,
                                                    String phone, String smsCode) {
        if (!isViewAttached()) {
            return;
        }

        getView().hideFieldError();

        if (!CommonUtil.checkStringOnNullOrEmpty(password)
                && !CommonUtil.checkStringOnNullOrEmpty(repeat) &&
                password.length() >= Constants.MIN_LENGTH_PASSWORD && repeat.equals(password)) {
            setNewPassword(phone, smsCode, password);
            return;
        }

        if (CommonUtil.checkStringOnNullOrEmpty(password)) {
            getView().showPasswordError(R.string.error_message_empty_field);
        } else if (password.length() < Constants.MIN_LENGTH_PASSWORD) {
            getView().showPasswordError(R.string.error_message_short_password);
        }

        if (CommonUtil.checkStringOnNullOrEmpty(repeat)) {
            getView().showRepeatPasswordError(R.string.error_message_empty_field);
        } else if (!repeat.equals(password)) {
            getView().showRepeatPasswordError(R.string.error_message_passwords_not_equal);
        }
    }

    @Override
    public void checkStepForgotPassword(ForgotPasswordActivity.ForgotPasswordStep step) {
        if (!isViewAttached()) {
            return;
        }

        switch (step) {
            case CHECK_PHONE:
                getView().firstStepForgotPassword();
                break;
            case CHECK_VERIFICATION_CODE:
                getView().secondStepForgotPassword();
                break;
            case CHANGE_PASSWORD:
                getView().thirdStepForgotPassword();
                break;
        }
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                ErrorWrapper wrapper = ErrorParse.parseFormErrors(httpException.response());
                String generalError = wrapper.getGeneralError();
                if (wrapper.getFieldErrors() != null && CommonUtil.checkStringOnNullOrEmpty(generalError)) {
                    getView().showFieldErrors(wrapper.getFieldErrors());
                } else {
                    getView().showError(generalError, RequestError.CUSTOM);
                }
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        }
    }
}
