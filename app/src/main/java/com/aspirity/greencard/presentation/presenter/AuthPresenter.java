package com.aspirity.greencard.presentation.presenter;


import android.annotation.SuppressLint;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.AppsUseCase;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.AuthContract;
import com.aspirity.greencard.presentation.model.ErrorWrapper;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.google.firebase.iid.FirebaseInstanceId;

import java.net.SocketTimeoutException;

import retrofit2.HttpException;

public class AuthPresenter extends BasePresenter<AuthContract.View>
        implements AuthContract.Presenter {

    private PreferenceDataManager preferenceDataManager;
    private UserUseCase userUseCase;
    private AppsUseCase appsUseCase;
    private RequestType lastRequest;

    public AuthPresenter(
            PreferenceDataManager preferenceDataManager,
            UserUseCase userUseCase,
            AppsUseCase appsUseCase) {
        this.preferenceDataManager = preferenceDataManager;
        this.userUseCase = userUseCase;
        this.appsUseCase = appsUseCase;
    }

    @SuppressLint("CheckResult")
    @Override
    public void authorization(String phone, String password) {
        if (!isViewAttached()) return;

        userUseCase.auth(phone, password)
                .doOnSubscribe(disposable -> {
                    lastRequest = RequestType.AUTH;
                    getView().showLoading();
                })
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(token -> {
                    if (token != null) {
                        preferenceDataManager.setToken(token);

                        registrationApps();
                        getUser();
                    } else
                        getView().hideLoading();
                }, this::handleError);
    }

    @SuppressLint("CheckResult")
    @Override
    public void registrationApps() {
        appsUseCase.registrationApps(FirebaseInstanceId.getInstance().getToken(), "ANDROID")
                .subscribe(apps -> {
                }, this::handleError);
    }

    @SuppressLint("CheckResult")
    @Override
    public void getUser() {
        if (!isViewAttached()) return;

        userUseCase.getUser(false)
                .doOnSubscribe(disposable -> {
                    lastRequest = RequestType.GET_USER;
                    getView().showLoading();
                })
                .doOnSuccess(booleanStringPair -> getView().hideLoading())
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(user -> {
                    if (user.isActive()) {
                        preferenceDataManager.setActiveUser(true);
                        getView().authSuccess();
                    } else {
                        getView().goToActivation();
                    }
                }, this::handleError);
    }

    @Override
    public void checkFieldPhoneAndPassword(String phone, String password) {
        if (!isViewAttached()) return;

        getView().hideFieldError();

        if (!CommonUtil.checkStringOnNullOrEmpty(phone.trim())
                && !CommonUtil.checkStringOnNullOrEmpty(password.trim()) && phone.length() >= 11) {
            getView().allowAuth();
            return;
        }

        if (CommonUtil.checkStringOnNullOrEmpty(phone)) {
            getView().showPhoneError(R.string.error_message_empty_field);
        } else if (phone.length() < 11) {
            getView().showPhoneError(R.string.error_message_wrong_format_phone);
        }

        if (CommonUtil.checkStringOnNullOrEmpty(password)) {
            getView().showPasswordError();
        }
    }

    @Override
    public RequestType getLastRequest() {
        return lastRequest;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                ErrorWrapper wrapper = ErrorParse.parseFormErrors(httpException.response());
                String generalError = wrapper.getGeneralError();
                if (wrapper.getFieldErrors() != null && CommonUtil.checkStringOnNullOrEmpty(generalError)) {
                    getView().showFieldErrors(wrapper.getFieldErrors());
                } else {
                    getView().showError(generalError, RequestError.CUSTOM);
                }
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        }
    }
}
