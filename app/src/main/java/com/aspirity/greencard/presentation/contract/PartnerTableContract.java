package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.BonusPercent;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;

import java.util.List;

public interface PartnerTableContract {

    interface View extends MvpView {

        void showMemberPercents(String percents);

        void showSilverPercents(String percents);

        void showGoldPercents(String percents);

        void showPlatinumPercents(String percents);
    }

    interface Presenter extends MvpPresenter<PartnerTableContract.View> {

        void checkStatuses(List<BonusPercent> bonusPercents);
    }
}
