package com.aspirity.greencard.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.presentation.adapters.view_holders.SharesViewHolder;
import com.aspirity.greencard.presentation.utils.DateUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ShareAdapter extends RecyclerView.Adapter<SharesViewHolder> {

    private final List<Share> shares;
    private final ShareListener shareListener;
    private final Picasso picasso;
    private final DateUtil dateUtil;
    private final Context context;

    public ShareAdapter(Context context, List<Share> shares, DateUtil dateUtil, ShareListener shareListener) {
        this.shares = shares;
        this.shareListener = shareListener;
        this.dateUtil = dateUtil;
        this.picasso = Picasso.with(context);
        this.context = context;
    }

    @Override
    public SharesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SharesViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.item_shares, parent, false));
    }

    @Override
    public void onBindViewHolder(SharesViewHolder holder, int position) {
        holder.bind(context, shares.get(position), picasso, dateUtil);
        holder.bindListener(shareListener, shares.get(position));
    }

    @Override
    public int getItemCount() {
        return shares.size();
    }

    public void addData(List<Share> shares) {
        this.shares.clear();
        this.shares.addAll(shares);
        notifyDataSetChanged();
    }

    public void addNewItems(List<Share> shares) {
        this.shares.addAll(shares);
        notifyDataSetChanged();
    }

    public interface ShareListener {
        void onClick(Share share, String dateEnd);
    }
}
