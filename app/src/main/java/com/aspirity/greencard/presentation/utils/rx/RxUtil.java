package com.aspirity.greencard.presentation.utils.rx;

import io.reactivex.FlowableTransformer;
import io.reactivex.MaybeTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.SingleTransformer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class RxUtil {

    private RxUtil() {
    }

    public static void unsubscribe(Disposable disposable) {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    public static void unsubscribe(CompositeDisposable compositeDisposable) {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public static <T> ObservableTransformer<T, T> applySchedulers(SchedulerProvider provider) {
        return observable -> observable.subscribeOn(provider.io())
                .observeOn(provider.ui());
    }

    public static <T> MaybeTransformer<T, T> applyMaybeSchedulers(SchedulerProvider provider) {
        return observable -> observable.subscribeOn(provider.io())
                .observeOn(provider.ui());
    }

    public static <T> SingleTransformer<T, T> applySingleSchedulers(SchedulerProvider provider) {
        return observable -> observable.subscribeOn(provider.io())
                .observeOn(provider.ui());
    }

    public static <T> SingleTransformer<T, T> applySingleMainThreadSchedulers(SchedulerProvider provider) {
        return observable -> observable.subscribeOn(provider.ui())
                .observeOn(provider.ui());
    }

    public static <T> FlowableTransformer<T, T> applyFlowableSchedulers(SchedulerProvider provider) {
        return observable -> observable.subscribeOn(provider.io())
                .observeOn(provider.ui());
    }
}
