package com.aspirity.greencard.presentation.fragments;

import android.animation.Animator;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.domain.usecase.SharesUseCase;
import com.aspirity.greencard.presentation.activities.MainActivity;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.ShareContract;
import com.aspirity.greencard.presentation.presenter.SharePresenter;
import com.aspirity.greencard.presentation.utils.Calculating;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.FontUtil;
import com.aspirity.greencard.presentation.utils.ImageAlphaTrasformation;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.aspirity.greencard.presentation.views.CustomFontTextView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import timber.log.Timber;

public class ShareFragment extends BaseFragment<ShareContract.View, ShareContract.Presenter>
        implements ShareContract.View {

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    private GestureDetector detector;
    private BottomSheetBehavior sheetBehavior;
    private HtmlTextView contentShareTextView;
    private TextSwitcher dateShareTextView;
    private TextView titleShareTextView;
    private ImageView fullImageShareImageView;
    private Dialog errorDialog;
    private TextView messageTextView;
    private TextView titleErrorTextView;
    private boolean doShowContent = false;
    private View moreWrapperClickable;
    private View imageShareDarkness;
    private String date = "";

    public static ShareFragment newInstance(Long shareId, String colorPartner) {
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_SHARE_ID, shareId);
        args.putString(Constants.ARG_BACKGROUND_COLOR_PARTNER, colorPartner);
        ShareFragment fragment = new ShareFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ShareFragment newInstance(Long id) {
        Bundle args = new Bundle();
        Log.i("mlg", "id: " + id);
        args.putLong(Constants.ARG_SHARE_ID, id);
        ShareFragment fragment = new ShareFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ShareFragment newInstance(Long id, boolean isArchived) {
        Bundle args = new Bundle();
        Log.i("mlg", "id: " + id);
        args.putLong(Constants.ARG_SHARE_ID, id);
        args.putBoolean(Constants.ARG_SHARE_IS_ARCHIVED, isArchived);
        ShareFragment fragment = new ShareFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ShareFragment newInstance(Share share, String dateEnd, boolean isArchived) {
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_SHARE_ID, share.getId());
        args.putString(Constants.ARG_SHARE_TITLE, share.getTitle());
        args.putString(Constants.ARG_SHARE_DATE_FORMAT, dateEnd);
        args.putString(Constants.ARG_BACKGROUND_COLOR_PARTNER, share.getPartnerColor());
        args.putBoolean(Constants.ARG_SHARE_IS_ARCHIVED, isArchived);
        ShareFragment fragment = new ShareFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        onCreatePresenter(new SharePresenter(
                getGreenCardApp().getPreferenceDataManager(),
                new SharesUseCase(
                        getGreenCardApp().getApplicationContext(),
                        RepositoryProvider.getSharesRepository(),
                        new AppSchedulerProvider())), this);
        View view = inflater.inflate(R.layout.fragment_share, container, false);
        initViews(view);
        initErrorDialog();
        getPresenter().getShareContent(
                getArguments().getLong(Constants.ARG_SHARE_ID),
                getArguments().getBoolean(Constants.ARG_SHARE_IS_ARCHIVED)
        );
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (errorDialog == null)
            initErrorDialog();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) Objects.requireNonNull(getActivity())).setBackButtonColor(null);
    }

    @Override
    public void onStop() {
        String colorPartner = getArguments().getString(Constants.ARG_BACKGROUND_COLOR_PARTNER);
        if (!CommonUtil.checkStringOnNullOrEmpty(colorPartner)
                && !getArguments().getBoolean(Constants.ARG_SHARE_IS_ARCHIVED, false)) {
            ((MainActivity) getActivity()).setBackButtonColor(colorPartner);
        }
        super.onStop();
    }

    private void initViews(View view) {
        fullImageShareImageView = view.findViewById(R.id.image_share_full_image);
        titleShareTextView = view.findViewById(R.id.text_share_title);
        titleShareTextView.setText(getArguments().getString(Constants.ARG_SHARE_TITLE, Constants.EMPTY_ANSWER));
        dateShareTextView = view.findViewById(R.id.text_share_date);
        dateShareTextView.setFactory(() -> {
            CustomFontTextView tv = new CustomFontTextView(getActivity());
            tv.setTextSize(12f);
            tv.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
            tv.setTypeface(FontUtil.getFont("proxima_nova_bold.otf", getActivity()));
            return tv;
        });
        dateShareTextView.setText(getArguments().getString(Constants.ARG_SHARE_DATE_FORMAT, Constants.EMPTY_ANSWER));
        dateShareTextView.setInAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.view_fade_in));
        dateShareTextView.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.view_fade_out));
        moreWrapperClickable = view.findViewById(R.id.linear_wrapper_more);
        moreWrapperClickable.setAlpha(0);
        moreWrapperClickable.setVisibility(View.GONE);
        contentShareTextView = view.findViewById(R.id.web_share_content);

        View bottomSheet = view.findViewById(R.id.bottom_sheet);
        bottomSheet.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        sheetBehavior = BottomSheetBehavior.from(bottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_EXPANDED:
                        dateShareTextView.setText("");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        dateShareTextView.setText(getArguments().getString(Constants.ARG_SHARE_DATE_FORMAT, Constants.EMPTY_ANSWER));
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        dateShareTextView.setText("");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        moreWrapperClickable.setOnClickListener(view1 -> {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        });

        detector = new GestureDetector(getContext(), initOnGestureListener());
        view.setOnTouchListener((view12, motionEvent) -> {
            detector.onTouchEvent(motionEvent);
            return true;
        });
    }

    private void initErrorDialog() {
        errorDialog = new Dialog(getActivity());
        errorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        errorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        errorDialog.setContentView(R.layout.dialog_error);
        errorDialog.setCanceledOnTouchOutside(false);
        titleErrorTextView = errorDialog.findViewById(R.id.text_name_error);
        messageTextView = errorDialog.findViewById(R.id.text_message_error);
        errorDialog.setOnShowListener(dialogInterface -> {
            // TODO: 16.11.2017 малый костыль, так как с каких то щей диалог открывается 2 раза, пока не понял почему
            if (errorDialog.isShowing())
                errorDialog.dismiss();
        });
        TextView closeActionTextView = errorDialog.findViewById(R.id.text_close_dialog);
        closeActionTextView.setOnClickListener(view -> errorDialog.dismiss());
        TextView repeatActionTextView = errorDialog.findViewById(R.id.text_action_repeat);
        repeatActionTextView.setOnClickListener(view -> {
            errorDialog.dismiss();
            getPresenter().getShareContent(getArguments().getLong(Constants.ARG_SHARE_ID), false);
        });
    }

    private GestureDetector.OnGestureListener initOnGestureListener() {
        return new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent motionEvent) {
                return true;
            }

            @Override
            public void onShowPress(MotionEvent motionEvent) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent motionEvent) {
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent motionEvent) {

            }

            @Override
            public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float velocityX, float velocityY) {
                boolean result = false;
                Timber.d("TOUCH IN Y: %s and %s", motionEvent.getY(), motionEvent2.getY());
                float diffY = motionEvent2.getY() - motionEvent.getY();
                float diffX = motionEvent2.getX() - motionEvent.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        getActivity().onBackPressed();
                        result = true;
                    }
                } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    //down to up swipe
                    if (diffY < 0 && motionEvent.getY() >= Calculating.getScreenHeightWithoutThirdPartScreen() && doShowContent) {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                    result = true;
                }

                return result;
            }
        };
    }

    @Override
    public void showError(String message, RequestError requestError) {
        switch (requestError) {
            case TIMEOUT:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_timeout);
                if (!errorDialog.isShowing()) errorDialog.show();
                break;
            case CUSTOM:
                titleErrorTextView.setText(R.string.error_title);
                messageTextView.setText(message);
                if (!errorDialog.isShowing()) errorDialog.show();
                break;
            case NETWORK:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_not_connection_network);
                if (!errorDialog.isShowing()) errorDialog.show();
                break;
        }
    }

    @Override
    public void showContentShare(Share share) {
        ImageAlphaTrasformation alphaTrasformation = new ImageAlphaTrasformation(0.3f);

        Picasso.with(getContext())
                .load(getImageSrc(share))
                .placeholder(android.R.color.transparent)
                .error(android.R.color.transparent)
                .transform(alphaTrasformation)
                .fit()
                .centerCrop()
                .into(fullImageShareImageView);

        String date = getGreenCardApp().getDateUtils().getDateString(new Date(share.getDateEnd() * Constants.COEFFICIENT_TIMESTAMP_TO_MILISECONDS));
        dateShareTextView.setText(String.format(Locale.getDefault(), "%s %s", getString(R.string.before), date));
        titleShareTextView.setText(share.getTitle());
        if (share.getContent() == null) {
            contentShareTextView.setText(R.string.error_message_not_connection_network);
        } else {
            contentShareTextView.setHtml(share.getContent(), new HtmlHttpImageGetter(contentShareTextView, "http://www.green-bonus.ru"));
            contentShareTextView.setMovementMethod(LinkMovementMethod.getInstance());
        }

        moreWrapperClickable.animate()
                .alpha(1)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        moreWrapperClickable.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        doShowContent = true;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
    }

    private String getImageSrc(Share share) {
        String path = share.getImageVerticalSrc();
        if (CommonUtil.checkStringOnNullOrEmpty(path)) {
            path = share.getImageSrc();
        }
        return path;
    }
}
