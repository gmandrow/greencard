package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.Notification;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.paginate.Paginate;

import java.util.List;

public interface NotificationsContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showNotifications(List<Notification> notifications);

        void setHasLoadedAllItems();

        void playStartAnimation();

        void showToast(String toast);
    }

    interface Presenter extends MvpPresenter<NotificationsContract.View> {

        Paginate.Callbacks getCallbacks();

        void getContent();

        void getNotifications();

        void retryLastRequest();

        void notificationsStatus(Long notification_id, String token, String status);
    }
}
