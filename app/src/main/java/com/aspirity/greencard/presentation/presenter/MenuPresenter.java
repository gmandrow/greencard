package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.MenuContract;

import timber.log.Timber;

public class MenuPresenter extends BasePresenter<MenuContract.View>
        implements MenuContract.Presenter {

    private final UserUseCase userUseCase;
    private final PreferenceDataManager preferenceDataManager;

    public MenuPresenter(UserUseCase userUseCase, PreferenceDataManager preferenceDataManager) {
        this.userUseCase = userUseCase;
        this.preferenceDataManager = preferenceDataManager;
    }

    @Override
    public void logOut() {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(userUseCase.logOut()
                .doOnSubscribe(disposable -> getView().showLoading())
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(aBoolean -> {
                    getView().hideLoading();
                    preferenceDataManager.clearAll();
                    getView().successLogOut();
                }, throwable -> {
                    Timber.e(throwable, "LogOut error: %s", throwable.getMessage());
                }));
    }
}
