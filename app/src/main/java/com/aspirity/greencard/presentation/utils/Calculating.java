package com.aspirity.greencard.presentation.utils;

import android.content.res.Resources;
import android.util.DisplayMetrics;

public class Calculating {

    public static final int THREE_PART_OF_SCREEN = 3;

    private Calculating() {
    }

    public static int dpToPx(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float logicalDensity = metrics.density;
        return (int) Math.ceil(dp * logicalDensity);
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static int getThirdPartScreenHeight() {
        int screenHeight = getScreenHeight();
        return screenHeight / THREE_PART_OF_SCREEN;
    }

    public static int getScreenHeightWithoutThirdPartScreen() {
        return getScreenHeight() - getThirdPartScreenHeight();
    }
}
