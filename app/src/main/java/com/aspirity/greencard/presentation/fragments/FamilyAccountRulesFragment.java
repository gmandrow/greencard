package com.aspirity.greencard.presentation.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.Oferta;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.FamilyAccountRulesContract;
import com.aspirity.greencard.presentation.presenter.FamilyAccountRulesPresenter;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class FamilyAccountRulesFragment extends BaseFragment<FamilyAccountRulesContract.View,
        FamilyAccountRulesContract.Presenter> implements FamilyAccountRulesContract.View {

    private TextView titleRulesFamilyAccount;
    private TextView subtitleRulesFamilyAccount;
    private HtmlTextView textRulesFamilyAccount;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onCreatePresenter(new FamilyAccountRulesPresenter(
                getGreenCardApp().getPreferenceDataManager(),
                new UserUseCase(
                        getActivity().getApplicationContext(),
                        RepositoryProvider.getUserRepository(),
                        new AppSchedulerProvider())), this);
        getPresenter().loadRulesFamilyAccount();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rules_family_account, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        titleRulesFamilyAccount = view.findViewById(R.id.text_title_rules_family_account);
        subtitleRulesFamilyAccount = view.findViewById(R.id.text_sub_header_rules_family_account);
        textRulesFamilyAccount = view.findViewById(R.id.text_content_rules_family_account);
        view.findViewById(R.id.btn_close).setOnClickListener(view1 ->
                getActivity().getSupportFragmentManager().popBackStack());
    }

    @Override
    public void showRulesFamilyAccount(Oferta rules) {
        titleRulesFamilyAccount.setText(rules.getTitle());
        subtitleRulesFamilyAccount.setText(rules.getSubHeader());
        textRulesFamilyAccount.setText(rules.getContent());
    }

    @Override
    public void showError(String message, RequestError requestError) {

    }
}
