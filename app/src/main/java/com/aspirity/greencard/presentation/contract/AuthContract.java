package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.util.Map;

import androidx.annotation.StringRes;

public interface AuthContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void authSuccess();

        void showPhoneError(@StringRes int messageId);

        void showPasswordError();

        void hideFieldError();

        void allowAuth();

        void showFieldErrors(Map<String, String> fieldErrors);

        void goToActivation();
    }

    interface Presenter extends MvpPresenter<View> {

        void authorization(String phone, String password);

        void registrationApps();

        void getUser();

        void checkFieldPhoneAndPassword(String phone, String password);

        RequestType getLastRequest();
    }
}
