package com.aspirity.greencard.presentation.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.transition.Slide;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.domain.usecase.PartnersUseCase;
import com.aspirity.greencard.presentation.activities.MainActivity;
import com.aspirity.greencard.presentation.adapters.PartnerInfoPagerAdapter;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.PartnerContract;
import com.aspirity.greencard.presentation.dialogs.ErrorDialog;
import com.aspirity.greencard.presentation.presenter.PartnerPresenter;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.FadeTransformer;
import com.aspirity.greencard.presentation.utils.FragmentUtil;
import com.aspirity.greencard.presentation.utils.ViewUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.viewpager.widget.ViewPager;
import io.reactivex.subjects.BehaviorSubject;


public class PartnerFragment extends BaseFragment<PartnerContract.View, PartnerContract.Presenter>
        implements PartnerContract.View {

    private TabLayout infoTabLayout;
    private ViewPager infoPartnerViewPager;
    private PartnerInfoPagerAdapter partnerInfoPagerAdapter;
    private ImageView logoImageView;
    private View partnerParentView;
    private String logoSrc;
    private String logoSrcForMap;
    private AppBarLayout partnerAppBar;
    private AppBarLayout.OnOffsetChangedListener offsetChangedListener;
    private Toolbar toolbar;
    private View wrapperNavigation;
    private TextView showSharesTextView;
    private TextView showStoresOnMapTextView;
    private ImageView showStoresOnMapImageView;
    private Integer vendorsCount;
    private String partnerColor;

    private BehaviorSubject<Boolean> transitionFinished = BehaviorSubject.create();

    public static PartnerFragment newInstance(Long partnerId, String backgroundColorPartner) {
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_PARTNER_ID, partnerId);
        if (backgroundColorPartner.length() == 7) {
            args.putString(Constants.ARG_BACKGROUND_COLOR_PARTNER, backgroundColorPartner);
        } else {
            args.putString(Constants.ARG_BACKGROUND_COLOR_PARTNER, Constants.DEFAULT_COLOR);
        }
        PartnerFragment partnerFragment = new PartnerFragment();
        partnerFragment.setArguments(args);
        return partnerFragment;
    }

    public static PartnerFragment newInstance(Long partnerId, String backgroundColorPartner, String logoSrc) {
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_PARTNER_ID, partnerId);
        if (backgroundColorPartner.length() == 7) {
            args.putString(Constants.ARG_BACKGROUND_COLOR_PARTNER, backgroundColorPartner);
        } else {
            args.putString(Constants.ARG_BACKGROUND_COLOR_PARTNER, Constants.DEFAULT_COLOR);
        }
        args.putString(Constants.ARG_PARTNER_LOGO, logoSrc);
        PartnerFragment partnerFragment = new PartnerFragment();
        partnerFragment.setArguments(args);
        return partnerFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        SchedulerProvider schedulerProvider = new AppSchedulerProvider();
        PartnersUseCase partnersUseCase = new PartnersUseCase(
                context.getApplicationContext(),
                schedulerProvider,
                RepositoryProvider.getPartnerRepository(schedulerProvider));
        onCreatePresenter(new PartnerPresenter(
                getGreenCardApp().getPreferenceDataManager(),
                partnersUseCase), this);
        partnerInfoPagerAdapter = new PartnerInfoPagerAdapter(getChildFragmentManager(), context.getResources());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Slide transition = (Slide) getEnterTransition();
            if (transition != null) {
                transition.addListener(new Transition.TransitionListener() {
                    @Override
                    public void onTransitionStart(@NonNull Transition transition) {
                    }

                    @Override
                    public void onTransitionEnd(@NonNull Transition transition) {
                        transitionFinished.onNext(true);
                    }

                    @Override
                    public void onTransitionCancel(@NonNull Transition transition) {
                    }

                    @Override
                    public void onTransitionPause(@NonNull Transition transition) {
                    }

                    @Override
                    public void onTransitionResume(@NonNull Transition transition) {
                    }
                });
            }
        } else {
            transitionFinished.onNext(true);
        }
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_partner, container, false);
        getPresenter().getPartner(getArguments().getLong(Constants.ARG_PARTNER_ID));
        initViews(view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //infoPartnerViewPager.setAdapter(null);
        //infoTabLayout.setupWithViewPager(null);
        partnerAppBar.removeOnOffsetChangedListener(offsetChangedListener);
        ((MainActivity) getActivity()).setBackButtonColor(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setBackButtonColor(
                getArguments().getString(Constants.ARG_BACKGROUND_COLOR_PARTNER));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    @Override
    public void showError(String message, RequestError requestError) {
        ErrorDialog errorDialog = new ErrorDialog(getContext());
        getCompositeDisposable().addAll(
                errorDialog.closeAction().subscribe(),
                errorDialog.retryAction().subscribe(obj ->
                        getPresenter().retryLastRequest())
        );
        errorDialog.setErrorDescription(requestError, message);
        errorDialog.show();
    }

    @Override
    public void showPartnerContent(Partner partner) {

        getCompositeDisposable().add(
                transitionFinished.subscribe(finished -> {
                    ViewUtil.animationAlpha(showStoresOnMapTextView, Constants.MAX_ALPHA);
                    ViewUtil.animationAlpha(showStoresOnMapImageView, Constants.MAX_ALPHA);
                    logoSrc = getLogoSrc(partner);
                    logoSrcForMap = getLogoSrcForMap(partner);
                    partnerInfoPagerAdapter.setDataToShow(partner,
                            !CommonUtil.checkListOnNullOrEmpty(partner.getBonusPercents()));
                    ViewUtil.animationAlpha(infoTabLayout, Constants.MAX_ALPHA);
                    ViewUtil.animationAlpha(infoPartnerViewPager, Constants.MAX_ALPHA);
                    Picasso.with(getActivity())
                            .load(logoSrc)
                            .fit()
                            .centerInside()
                            .into(logoImageView);
                    vendorsCount = partner.getVendorsCount();
                    if (partner.getVendorsCount() == 0) {
                        showStoresOnMapImageView.setVisibility(View.GONE);
                        showStoresOnMapTextView.setVisibility(View.GONE);
                    }
                    int c = Color.parseColor(partner.getColor());
                    partnerColor = partner.getColor();
                    partnerParentView.setBackgroundColor(c);
                    ((MainActivity) getActivity()).setBackButtonColor(partner.getColor());

                    showSharesTextView.setOnClickListener(view1 -> {
                        ListSharesOfPartnerFragment fragmentPartner =
                                ListSharesOfPartnerFragment.newInstance(
                                        getArguments().getLong(Constants.ARG_PARTNER_ID),
                                        partnerColor,
                                        logoSrc,
                                        logoSrcForMap,
                                        vendorsCount);
//                      FragmentUtil.setTransitionSlideForTwoFragment(this, fragmentPartner);
                        FragmentUtil.addBackStackWithAddFragment(
                                getActivity().getSupportFragmentManager(),
                                fragmentPartner);
//                      FragmentUtil.addBackStackFragment(getActivity().getSupportFragmentManager(), fragmentPartner);
                    });

                })
        );

//        Picasso.with(getActivity())
//                .load(logoSrc)
//                .fit()
//                .centerInside()
//                .into(logoImageView);
    }

    private String getLogoSrcForMap(Partner partner) {
        String path = partner.getLogoMapSrc();
        if (CommonUtil.checkStringOnNullOrEmpty(path)) {
            path = partner.getLogoSrc();
        }
        return path;
    }

    private String getLogoSrc(Partner partner) {
        String path = partner.getLogoDetailSrc();
        if (CommonUtil.checkStringOnNullOrEmpty(path)) {
            path = partner.getLogoSrc();
        }
        return path;
    }

    private void initViews(View view) {
        partnerAppBar = view.findViewById(R.id.appbar_partner);
        toolbar = view.findViewById(R.id.toolbar_partner);
        wrapperNavigation = view.findViewById(R.id.relative_wrapper_navigation);
        initAppBarListener();

        partnerParentView = view.findViewById(R.id.relative_layout_partner);
        partnerParentView.setBackgroundColor(Color
                .parseColor(getArguments().getString(Constants.ARG_BACKGROUND_COLOR_PARTNER)));
        logoImageView = view.findViewById(R.id.image_partner_logo);

        showSharesTextView = view.findViewById(R.id.text_show_shares);
        showStoresOnMapTextView = view.findViewById(R.id.text_show_stores_on_map);
        showStoresOnMapTextView.setVisibility(View.GONE);
        showStoresOnMapImageView = view.findViewById(R.id.image_show_stores_on_map);
        showStoresOnMapImageView.setImageResource(R.drawable.ic_location);
        showStoresOnMapImageView.setVisibility(View.GONE);
        showStoresOnMapTextView.setOnClickListener(view12 -> {
            goToPartnerMap();
        });
        showStoresOnMapImageView.setOnClickListener(view12 -> {
            goToPartnerMap();
        });

        initTabLayout(view);

        partnerColor = getArguments().getString(Constants.ARG_BACKGROUND_COLOR_PARTNER);

    }

    private void goToPartnerMap() {
        PartnerMapFragment fragmentPartner = PartnerMapFragment.newInstance(
                getArguments().getLong(Constants.ARG_PARTNER_ID),
                logoSrcForMap,
                partnerColor);
        FragmentUtil.setTransitionSlideForTwoFragment(this, fragmentPartner);
        FragmentUtil.addBackStackWithAddFragment(getActivity().getSupportFragmentManager(), fragmentPartner);
    }

    private void initAppBarListener() {
        offsetChangedListener = (appBarLayout, verticalOffset) -> {
            float percentage = ((float) Math.abs(verticalOffset) / appBarLayout.getTotalScrollRange());
            wrapperNavigation.setAlpha(1 - percentage * 2.3f);
        };
        partnerAppBar.addOnOffsetChangedListener(offsetChangedListener);
    }

    private void initTabLayout(View view) {
        infoTabLayout = view.findViewById(R.id.tabs_partner);
        infoTabLayout.setVisibility(View.GONE);
        infoTabLayout.setSelectedTabIndicatorColor(ResourcesCompat.getColor(getResources(), R.color.colorTransparent, null));
        infoPartnerViewPager = view.findViewById(R.id.viewpager_info_partner);
        infoPartnerViewPager.setPageTransformer(false, new FadeTransformer());
        infoPartnerViewPager.setVisibility(View.GONE);
        infoPartnerViewPager.setAdapter(partnerInfoPagerAdapter);
        infoTabLayout.setupWithViewPager(infoPartnerViewPager);
    }
}
