package com.aspirity.greencard.presentation.model;

import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.presentation.utils.ListUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.core.util.Pair;

public class StorePointDataWrapper {


    private List<Pair<Partner, List<SelectableVendor>>> partners;

    public StorePointDataWrapper() {
    }

    public StorePointDataWrapper(List<Partner> partners, List<SelectableVendor> vendors, List<SelectableVendor> alreadySelected) {
        List<SelectableVendor> filteredVendors = ListUtil.filter(vendors,
                vendor -> {
                    String address = vendor.getVendor().getAddress();
                    Long id = vendor.getVendor().getPartnerId();
                    return address != null && !address.isEmpty() && id != null;
                });
        this.partners = new ArrayList<>();
        applySelected(vendors, filteredVendors);
        this.partners.add(new Pair<>(new Partner(-1L), filteredVendors));
        for (Partner p : partners) {
            long partnerId = p.getId();
            List<SelectableVendor> filtered = ListUtil.filter(filteredVendors,
                    obj -> obj.getVendor().getPartnerId() != null
                            && obj.getVendor().getPartnerId() == partnerId);
            if (filtered.size() != 0) {
                this.partners.add(new Pair<>(p, filtered));
            }
        }
    }

    private void applySelected(List<SelectableVendor> vendors, List<SelectableVendor> alreadySelected) {
        ListUtil.apply(alreadySelected, obj -> {
            SelectableVendor vendor = ListUtil.firstOrNull(vendors, obj1 ->
                    Objects.equals(obj.getVendor().getId(), obj1.getVendor().getId()));
            if (vendor != null) {
                vendor.setSelected(obj.isSelected());
            }
        });
    }

    public List<Pair<Partner, List<SelectableVendor>>> getPartners() {
        return partners;
    }
}
