package com.aspirity.greencard.presentation.presenter;


import android.util.Pair;

import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.OperationsUseCase;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.BalanceAndOperationsContract;
import com.aspirity.greencard.presentation.model.OperationFilterWrapper;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.paginate.Paginate;

import java.net.SocketTimeoutException;

import io.reactivex.Single;
import retrofit2.HttpException;
import timber.log.Timber;

public class BalanceAndOperationsPresenter extends BasePresenter<BalanceAndOperationsContract.View>
        implements BalanceAndOperationsContract.Presenter {

    private final PreferenceDataManager preferenceDataManager;
    private final OperationsUseCase operationsUseCase;
    private final UserUseCase userUseCase;

    private OperationFilterWrapper filter = new OperationFilterWrapper();

    private RequestType lastRequest = null;
    private int totalItemsAmount = 0;
    private int currentItemsAmount = 0;
    private int page = 1;
    private boolean loading = false;
    private Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            getMoreOperations();
        }

        @Override
        public boolean isLoading() {
            return loading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            if (currentItemsAmount >= totalItemsAmount) {
                getView().setHasLoadedAllItems();
                return true;
            }
            return false;
        }
    };

    public BalanceAndOperationsPresenter(
            PreferenceDataManager preferenceDataManager,
            OperationsUseCase operationsUseCase,
            UserUseCase userUseCase) {
        this.preferenceDataManager = preferenceDataManager;
        this.operationsUseCase = operationsUseCase;
        this.userUseCase = userUseCase;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                getView().showError(
                        ErrorParse.parseError(httpException.response()),
                        RequestError.CUSTOM);
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        } else {
            // generic error handling
        }
    }

    @Override
    public void getContent(boolean forceRefresh) {
        if (!isViewAttached()) {
            return;
        }

        if (forceRefresh) {
            currentItemsAmount = 0;
            totalItemsAmount = 0;
            page = 1;
        }

        getCompositeDisposable().add(
                Single.zip(
                        userUseCase.getUser(forceRefresh),
                        operationsUseCase.getOperations(page, filter),
                        Pair::new)
                        .doOnSubscribe(disposable -> {
                            loading = true;
                            lastRequest = RequestType.CONTENT;
                            lastRequest.forceRefresh = forceRefresh;
                            if (!forceRefresh) {
                                getView().showLoading();
                            }
                        })
                        .onErrorResumeNext(error -> {
                            handleError(error);
                            return Single.zip(
                                    userUseCase.getUserFallback(),
                                    operationsUseCase.getOperationsFallback(page, 10),
                                    Pair::new);
                        })
                        .subscribe(userOperationsPair -> {
                            totalItemsAmount = userOperationsPair.second.getCount();
                            currentItemsAmount = userOperationsPair.second.getChunkSize();
                            getView().showUserData(userOperationsPair.first);
                            getView().showOperationsData(
                                    userOperationsPair.second.getItems(),
                                    userOperationsPair.second.getCount());
                            getView().playStartAnimation();
                            getView().hideLoading();
                            if (userOperationsPair.second.getChunkSize() != 0) {
                                page++;
                            }
                            loading = false;
                        }, this::handleError));
    }

    @Override
    public void getOperations(boolean restart) {
        if (!isViewAttached()) {
            return;
        }

        if (restart) {
            currentItemsAmount = 0;
            totalItemsAmount = 0;
            page = 1;
        }

        getCompositeDisposable().add(
                operationsUseCase.getOperations(page, filter)
                        .doOnSubscribe(disposable -> {
                            loading = true;
                            lastRequest = RequestType.OPERATION_PAGE;
                            lastRequest.forceRefresh = restart;
                            if (restart) {
                                getView().showLoading();
                            }
                        })
                        .onErrorResumeNext(error -> operationsUseCase.getOperationsFallback(page, 10))
                        .subscribe(operations -> {
                            if (restart) {
                                totalItemsAmount = operations.getCount();
                                getView().showOperationsData(operations.getItems(), operations.getCount());
                                getView().hideLoading();
                            } else {
                                getView().showMoreOperationsData(operations.getItems());
                            }
                            currentItemsAmount += operations.getChunkSize();
                            if (operations.getChunkSize() != 0) {
                                page++;
                            }
                            loading = false;
                        }, this::handleError));
    }

    @Override
    public Paginate.Callbacks getCallbacks() {
        return callbacks;
    }

    @Override
    public OperationFilterWrapper getCurrentFilter() {
        return filter;
    }

    @Override
    public void retryLastRequest() {
        switch (lastRequest) {
            case CONTENT:
                getContent(lastRequest.forceRefresh);
                return;
            case OPERATION_PAGE:
                getOperations(lastRequest.forceRefresh);
        }
    }

    private void getMoreOperations() {
        if (!isViewAttached()) {
            return;
        }

        getCompositeDisposable().add(
                operationsUseCase.getOperations(page, filter)
                        .doOnSubscribe(disposable -> {
                            loading = true;
                            lastRequest = RequestType.OPERATION_PAGE;
                        })
                        .onErrorResumeNext(error -> operationsUseCase.getOperationsFallback(page, 10))
                        .subscribe(operations -> {
                            getView().showMoreOperationsData(operations.getItems());
                            currentItemsAmount += operations.getChunkSize();
                            if (operations.getChunkSize() != 0) {
                                page++;
                            }
                            loading = false;
                        }, throwable -> {
                            loading = false;
                            Timber.w(throwable, throwable.getMessage());
                        }));
    }

    private enum RequestType {
        CONTENT,
        OPERATION_PAGE;

        private boolean forceRefresh;
    }
}
