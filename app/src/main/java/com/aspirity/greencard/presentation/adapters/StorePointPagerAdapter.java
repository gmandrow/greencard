package com.aspirity.greencard.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.presentation.model.SelectableVendor;
import com.aspirity.greencard.presentation.model.StorePointDataWrapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.core.util.Pair;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;


public class StorePointPagerAdapter extends PagerAdapter {

    private List<Pair<Partner, List<SelectableVendor>>> partners = new ArrayList<>();
    private Context context;

    public StorePointPagerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return partners == null ? 0 : partners.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        RecyclerView view = (RecyclerView) inflater.inflate(R.layout.fragment_stores_points, container, false);

        Pair<Partner, List<SelectableVendor>> partner = partners.get(position);

        VendorAdapter adapter = new VendorAdapter(partner.first.getId(), partner.second);
        view.setAdapter(adapter);
        view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        RecyclerView view = (RecyclerView) object;
        VendorAdapter adapter = (VendorAdapter) view.getAdapter();
        adapter.clear();
        view.setAdapter(null);
        container.removeView((View) object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Partner p = partners.get(position).first;
        if (p.getId() == -1) {
            return context.getString(R.string.partner_all);
        }
        return partners.get(position).first.getName();
    }

    public void setData(StorePointDataWrapper data) {
        partners = data.getPartners();
        notifyDataSetChanged();
    }

    public List<SelectableVendor> getVendors() {
        if (partners == null || partners.isEmpty()) {
            return Collections.emptyList();
        }
        return partners.get(0).second;
    }
}
