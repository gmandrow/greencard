package com.aspirity.greencard.presentation.base;

import com.aspirity.greencard.presentation.utils.rx.RxUtil;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    private CompositeDisposable compositeDisposable;
    private WeakReference<V> viewWeakReference;

    @Override
    public void attachView(V view) {
        viewWeakReference = new WeakReference<>(view);
        if (compositeDisposable == null || compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }
    }

    @Override
    public void detachView() {
        RxUtil.unsubscribe(compositeDisposable);
        if (viewWeakReference != null) {
            viewWeakReference.clear();
            viewWeakReference = null;
        }
    }

    protected boolean isViewAttached() {
        return viewWeakReference != null && viewWeakReference.get() != null;
    }

    protected V getView() {
        return viewWeakReference.get();
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }
}
