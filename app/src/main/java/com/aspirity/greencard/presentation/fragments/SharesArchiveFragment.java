package com.aspirity.greencard.presentation.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.domain.usecase.SharesUseCase;
import com.aspirity.greencard.presentation.activities.MainActivity;
import com.aspirity.greencard.presentation.adapters.FilterableAdapterListener;
import com.aspirity.greencard.presentation.adapters.FilterableShareAdapter;
import com.aspirity.greencard.presentation.adapters.view_holders.CustomLoadingListItemCreator;
import com.aspirity.greencard.presentation.adapters.view_holders.SharesViewHolder;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.SharesArchiveContract;
import com.aspirity.greencard.presentation.dialogs.ErrorDialog;
import com.aspirity.greencard.presentation.presenter.SharesArchivePresenter;
import com.aspirity.greencard.presentation.utils.FragmentUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.aspirity.greencard.presentation.views.AppbarSwipeRefreshLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.paginate.Paginate;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SharesArchiveFragment extends BaseFragment<SharesArchiveContract.View, SharesArchiveContract.Presenter>
        implements SharesArchiveContract.View {

    /**
     * RECYCLER RELATED VIEWS
     **/
    private RecyclerView shareArchiveRecycler;
    private FilterableShareAdapter shareArchiveAdapter;
    private FilterableAdapterListener<Share> shareArchiveListener;

    /**
     * OTHER VIEWS
     **/
    private View titleSharesArchiveWrapper;
    private AppbarSwipeRefreshLayout swipeRefreshLayout;

    /**
     * PAGINATION
     **/
    private Paginate paginate;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onCreatePresenter(new SharesArchivePresenter(
                getGreenCardApp().getPreferenceDataManager(),
                new SharesUseCase(
                        getGreenCardApp().getApplicationContext(),
                        RepositoryProvider.getSharesRepository(),
                        new AppSchedulerProvider())), this);
        shareArchiveListener = new FilterableAdapterListener<Share>() {
            @Override
            public void onClick(Share item, String dateEnd) {
                ShareFragment shareFragment = ShareFragment.newInstance(item, dateEnd, true);
                FragmentUtil.setTransitionSlide(shareFragment, Gravity.RIGHT, Gravity.LEFT);
                FragmentUtil.addBackStackWithAddFragment(
                        getActivity().getSupportFragmentManager(),
                        shareFragment);
            }

            @Override
            public void onPartnerLogoClick(Share item) {
                PartnerFragment fragment = PartnerFragment.newInstance(item.getPartnerId(),
                        item.getPartnerColor(), item.getPartnerLogoSrc());
                FragmentUtil.setTransitionSlideForTwoFragment(SharesArchiveFragment.this, fragment);
                FragmentUtil.addBackStackWithAddFragment(
                        getActivity().getSupportFragmentManager(),
                        fragment);
            }
        };
        shareArchiveAdapter = new FilterableShareAdapter(
                context.getApplicationContext(),
                new LinkedList<>(),
                getGreenCardApp().getDateUtils(),
                shareArchiveListener) {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                RecyclerView.ViewHolder holder = super.onCreateViewHolder(parent, viewType);
                if (viewType == ListItem.TYPE_FILTERS) {
                    ShareFilterViewHolder h = (ShareFilterViewHolder) holder;
                    h.actionOpenFilters.setVisibility(View.GONE);
                    h.archiveShares();
                }
                return holder;
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                super.onBindViewHolder(holder, position);
                if (position != 0) {
                    SharesViewHolder h = (SharesViewHolder) holder;
                    h.disableDateViews();
                }
            }
        };
        getPresenter().getContent(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shares_archive, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        titleSharesArchiveWrapper = view.findViewById(R.id.text_title_shares_archive);

        shareArchiveRecycler = view.findViewById(R.id.recycler_shares_archive);
        shareArchiveRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        shareArchiveRecycler.getLayoutManager().setAutoMeasureEnabled(true);
        shareArchiveRecycler.setHasFixedSize(false);
        shareArchiveRecycler.setAdapter(shareArchiveAdapter);
        shareArchiveRecycler.setItemAnimator(null);

        AppBarLayout appbar = view.findViewById(R.id.appbar_shares_archive);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setAppBar(appbar);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(() -> getPresenter().getContent(true));
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) getActivity()).setBackButtonColor(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setBackButtonColor(null);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        shareArchiveRecycler.setAdapter(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    @Override
    public void showError(String message, RequestError requestError) {
        ErrorDialog errorDialog = new ErrorDialog(getContext());
        getCompositeDisposable().addAll(
                errorDialog.closeAction().subscribe(),
                errorDialog.retryAction().subscribe(obj ->
                        getPresenter().retryLastRequest(false))
        );
        errorDialog.setErrorDescription(requestError, message);
        errorDialog.show();
    }

    @Override
    public void showSharesData(List<Share> shares, Integer countShares) {
        shareArchiveAdapter.addData(shares, countShares);
        animation(shareArchiveRecycler, animShow);
        if (paginate == null) {
            paginate = Paginate.with(shareArchiveRecycler, getPresenter().getCallbacks())
                    .setLoadingTriggerThreshold(2)
                    .addLoadingListItem(true)
                    .setLoadingListItemCreator(new CustomLoadingListItemCreator())
                    .build();
        }
    }

    @Override
    public void showMoreSharesData(List<Share> listShares) {
        shareArchiveAdapter.addNewItems(listShares);
    }

    @Override
    public void setHasLoadedAllItems() {
        shareArchiveRecycler.post(() -> paginate.setHasMoreDataToLoad(false));
    }
}
