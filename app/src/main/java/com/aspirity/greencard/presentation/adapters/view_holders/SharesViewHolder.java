package com.aspirity.greencard.presentation.adapters.view_holders;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.presentation.adapters.FilterableAdapterListener;
import com.aspirity.greencard.presentation.adapters.ShareAdapter;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.DateUtil;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.Locale;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;


public class SharesViewHolder extends RecyclerView.ViewHolder {

    private final TextView textEndShareTextView;
    private final TextView daysShareTextView;
    private final TextView dateShareTextView;
    private final TextView specialTextView;
    private final ImageView logoShareImageView;
    private final ImageView imageShareImageView;
    private final TextView titleShareTextView;
    private final ImageButton shareInfoImageButton;

    public SharesViewHolder(View itemView) {
        super(itemView);
        textEndShareTextView = itemView.findViewById(R.id.text_end_of_share);
        daysShareTextView = itemView.findViewById(R.id.text_days_of_share);
        dateShareTextView = itemView.findViewById(R.id.text_share_date);
        specialTextView = itemView.findViewById(R.id.text_special);
        Drawable rightDrawable = AppCompatResources.getDrawable(itemView.getContext(), R.drawable.ic_alarm);
        specialTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, rightDrawable, null);
        logoShareImageView = itemView.findViewById(R.id.image_firm_logo);
        imageShareImageView = itemView.findViewById(R.id.image_share);
        titleShareTextView = itemView.findViewById(R.id.text_share_title);
        shareInfoImageButton = itemView.findViewById(R.id.btn_share_info);
    }

    public <T> void bind(
            Context context,
            final Share item,
            Picasso picasso,
            DateUtil dateUtil) {
        Share share = item;
        if (share.isSpecial()) {
            textEndShareTextView.setVisibility(View.GONE);
            daysShareTextView.setVisibility(View.GONE);
            specialTextView.setVisibility(View.VISIBLE);
        } else {
            textEndShareTextView.setVisibility(View.VISIBLE);
            daysShareTextView.setVisibility(View.VISIBLE);
            daysShareTextView.setText(String.valueOf(share.getDaysInMilliseconds()));
            specialTextView.setVisibility(View.GONE);
        }

        String date = dateUtil.getDateString(new Date(
                share.getDateEnd() * Constants.COEFFICIENT_TIMESTAMP_TO_MILISECONDS));
        dateShareTextView.setText(String.format(
                Locale.getDefault(),
                "%s %s",
                "до",
                date));

        if (share.getPartnerId() == null) {
            picasso.load(R.drawable.ic_green_card_shares)
                    .fit()
                    .centerInside()
                    .into(logoShareImageView);
        } else {
            picasso.load(getPartnerImageSrc(share))
                    .fit()
                    .centerInside()
                    .into(logoShareImageView);
        }
        picasso.cancelRequest(imageShareImageView);
        picasso.load(getImageSrc(share))
                .placeholder(android.R.color.transparent)
                .error(android.R.color.transparent)
                .fit()
                .centerCrop()
                .into(imageShareImageView);
        titleShareTextView.setText(share.getTitle());
    }

    public void bindListener(final FilterableAdapterListener<Share> shareListener, final Share share) {
        itemView.setOnClickListener(view ->
                shareListener.onClick(share, dateShareTextView.getText().toString()));
        shareInfoImageButton.setOnClickListener(view ->
                shareListener.onClick(share, dateShareTextView.getText().toString()));
        logoShareImageView.setOnClickListener(view -> {
            if (share.getPartnerId() != null) {
                shareListener.onPartnerLogoClick(share);
            }
        });
    }

    public void bindListener(final ShareAdapter.ShareListener shareListener, final Share share) {
        itemView.setOnClickListener(view ->
                shareListener.onClick(share, dateShareTextView.getText().toString()));
        shareInfoImageButton.setOnClickListener(view ->
                shareListener.onClick(share, dateShareTextView.getText().toString()));
    }

    public void disableDateViews() {
        textEndShareTextView.setVisibility(View.GONE);
        daysShareTextView.setVisibility(View.GONE);
    }

    private String getImageSrc(Share share) {
        String path = share.getImageHorizontalSrc();
        if (CommonUtil.checkStringOnNullOrEmpty(path)) {
            path = share.getImageSrc();
        }
        return path;
    }

    private String getPartnerImageSrc(Share share) {
        String path = share.getPartnerLogoWhiteBgSrc();
        if (CommonUtil.checkStringOnNullOrEmpty(path)) {
            path = share.getPartnerLogoSrc();
        }
        return path;
    }

}
