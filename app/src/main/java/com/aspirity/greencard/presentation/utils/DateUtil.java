package com.aspirity.greencard.presentation.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

    private final ThreadLocal<SimpleDateFormat> threadLocalSimpleDateFormat =
            new ThreadLocal<SimpleDateFormat>() {
                @Override
                protected SimpleDateFormat initialValue() {
                    return new SimpleDateFormat("dd / MM / yy", Locale.getDefault());
                }
            };

    public String getDateString(Date date) {
        threadLocalSimpleDateFormat.get().applyPattern("dd / MM / yy");
        return threadLocalSimpleDateFormat.get().format(date);
    }

    public String getTimeString(Date date) {
        threadLocalSimpleDateFormat.get().applyPattern("HH:mm");
        return threadLocalSimpleDateFormat.get().format(date);
    }

    public String getDateTimeString(Date date) {
        threadLocalSimpleDateFormat.get().applyPattern("HH:mm   dd / MM / yy");
        return threadLocalSimpleDateFormat.get().format(date);
    }

    public String getFullDateString(Date date) {
        threadLocalSimpleDateFormat.get().applyPattern("dd MMMM yyyy");
        return threadLocalSimpleDateFormat.get().format(date);
    }

    public String getDateForApi(Date date) {
        threadLocalSimpleDateFormat.get().applyPattern("yyyy-MM-dd");
        return threadLocalSimpleDateFormat.get().format(date);
    }

    public Date getDateFromString(String string) {
        threadLocalSimpleDateFormat.get().applyPattern("dd.MM.yyyy");
        try {
            return threadLocalSimpleDateFormat.get().parse(string);
        } catch (ParseException e) {
            return null;
        }
    }
}
