package com.aspirity.greencard.presentation.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;

import com.aspirity.greencard.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Calendar;

import androidx.core.content.res.ResourcesCompat;


public class CurrentDayDecorator implements DayViewDecorator {

    private final Calendar calendar = Calendar.getInstance();
    private final Drawable highlightDrawable;
    private final int color = Color.parseColor("#78d8d8d8");
    private CalendarDay date;

    public CurrentDayDecorator(Context context) {
//        highlightDrawable = new ColorDrawable(color);
        highlightDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.day_circle, null);
        date = CalendarDay.today();
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        day.copyTo(calendar);
        int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
        return date != null && day.equals(date);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setBackgroundDrawable(highlightDrawable);
    }
}
