package com.aspirity.greencard.presentation.adapters.view_holders;

import android.view.View;
import android.widget.ImageView;

import com.aspirity.greencard.R;
import com.squareup.picasso.Picasso;

import androidx.recyclerview.widget.RecyclerView;


public class PhotoViewHolder extends RecyclerView.ViewHolder {
    private final ImageView photoImageView;

    public PhotoViewHolder(View itemView) {
        super(itemView);
        photoImageView = itemView.findViewById(R.id.photo);
    }

    public void bind(final String item, Picasso picasso) {
        picasso.load(item)
                .fit()
                .centerInside()
                .into(photoImageView);
    }
}
