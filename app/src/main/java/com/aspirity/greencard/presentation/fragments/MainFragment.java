package com.aspirity.greencard.presentation.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aspirity.greencard.GreenCardApplication;
import com.aspirity.greencard.R;
import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.domain.model.User;
import com.aspirity.greencard.domain.usecase.AppsUseCase;
import com.aspirity.greencard.domain.usecase.CardsUseCase;
import com.aspirity.greencard.domain.usecase.SharesUseCase;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.adapters.FilterableAdapterListener;
import com.aspirity.greencard.presentation.adapters.FilterableShareAdapter;
import com.aspirity.greencard.presentation.adapters.view_holders.CustomLoadingListItemCreator;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.MainContract;
import com.aspirity.greencard.presentation.dialogs.ErrorDialog;
import com.aspirity.greencard.presentation.presenter.MainPresenter;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.FragmentUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.aspirity.greencard.presentation.views.AppbarSwipeRefreshLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.paginate.Paginate;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainFragment extends BaseFragment<MainContract.View, MainContract.Presenter>
        implements MainContract.View {
    private RecyclerView sharesRecycler;
    private FilterableShareAdapter adapter;
    private Paginate paginate;

    private PreferenceDataManager mPref;
    private View virtualCardWrapper;
    private View wrapperHeader;
    private TextView availableBonusesTextView;
    private TextView statusTextView;
    private TextView payForNextStatusTextView;
    private TextView titleCardNumberTextView;
    private TextView cardNumberTextView;
    private AppbarSwipeRefreshLayout swipeRefreshLayout;

    private boolean isShowingError = false;

    public static MainFragment newInstance(boolean isRestore) {
        MainFragment mainFragment = new MainFragment();
        Bundle args = new Bundle();
        args.putBoolean(Constants.ARG_IS_RESTORE, isRestore);
        mainFragment.setArguments(args);
        return mainFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onCreatePresenter(new MainPresenter(
                new UserUseCase(
                        context.getApplicationContext(),
                        RepositoryProvider.getUserRepository(),
                        new AppSchedulerProvider()),
                new SharesUseCase(
                        context.getApplicationContext(),
                        RepositoryProvider.getSharesRepository(),
                        new AppSchedulerProvider()),
                new AppsUseCase(
                        context.getApplicationContext(),
                        RepositoryProvider.getAppsRepository(),
                        new AppSchedulerProvider()),
                new CardsUseCase(
                        context.getApplicationContext(),
                        new AppSchedulerProvider(),
                        RepositoryProvider.getCardsRepository()),
                ((GreenCardApplication) getActivity().getApplication()).getPreferenceDataManager()), this);

        FilterableAdapterListener<Share> shareListener = new FilterableAdapterListener<Share>() {
            @Override
            public void onClick(Share item, String dateEnd) {
                ShareFragment shareFragment = ShareFragment.newInstance(item, dateEnd, false);
                FragmentUtil.setTransitionSlideForTwoFragment(MainFragment.this, shareFragment);
                FragmentUtil.addBackStackWithAddFragment(
                        Objects.requireNonNull(getActivity()).getSupportFragmentManager(),
                        shareFragment);
            }

            @Override
            public void onPartnerLogoClick(Share item) {
                PartnerFragment fragment = PartnerFragment.newInstance(item.getPartnerId(), item.getPartnerColor(), item.getPartnerLogoWhiteBgSrc());
                FragmentUtil.setTransitionSlideForTwoFragment(MainFragment.this, fragment);
                FragmentUtil.addBackStackWithAddFragment(
                        Objects.requireNonNull(getActivity()).getSupportFragmentManager(),
                        fragment);
            }
        };

        adapter = new FilterableShareAdapter(context.getApplicationContext(),
                new LinkedList<>(),
                getGreenCardApp().getDateUtils(),
                shareListener) {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                RecyclerView.ViewHolder holder = super.onCreateViewHolder(parent, viewType);
                if (viewType == ListItem.TYPE_FILTERS) {
                    ShareFilterViewHolder h = (ShareFilterViewHolder) holder;
                    h.actionOpenFilters.setVisibility(View.GONE);
                }
                return holder;
            }
        };

    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initViews(view);

        getPresenter().getContent(true);
        return view;
    }

    private void initViews(View view) {
        mPref = ((GreenCardApplication) getActivity().getApplication()).getPreferenceDataManager();

        wrapperHeader = view.findViewById(R.id.linear_wrapper_main_info);
        virtualCardWrapper = view.findViewById(R.id.linear_wrapper_virtual_card);
        AppBarLayout appbar = view.findViewById(R.id.appbar_shares);
        appbar.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            wrapperHeader.setTranslationX(-verticalOffset * Constants.COEFFICIENT_TRANSLATION_X);
            virtualCardWrapper.setTranslationY(verticalOffset / Constants.COEFFICIENT_TRANSLATION_Y);
        });
        virtualCardWrapper.setOnClickListener(view1 -> {
            VirtualCardFragment virtualCardFragment = VirtualCardFragment.newInstance();
            FragmentUtil.addBackStackWithAddFragment(
                    Objects.requireNonNull(getActivity()).getSupportFragmentManager(),
                    virtualCardFragment);
        });

        availableBonusesTextView = view.findViewById(R.id.text_available_bonuses);
        statusTextView = view.findViewById(R.id.text_current_status);
        payForNextStatusTextView = view.findViewById(R.id.text_next_status);
        titleCardNumberTextView = view.findViewById(R.id.text_title_card_number);
        cardNumberTextView = view.findViewById(R.id.text_card_number);

        sharesRecycler = view.findViewById(R.id.recycler_shares);
        sharesRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        sharesRecycler.getLayoutManager().setAutoMeasureEnabled(true);
        sharesRecycler.setHasFixedSize(false);
        sharesRecycler.setAdapter(adapter);
        sharesRecycler.setItemAnimator(null);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setAppBar(appbar);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(() -> getPresenter().getContent(true));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (paginate != null)
            paginate.unbind();

        sharesRecycler.setAdapter(null);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    @Override
    public void showError(String message, RequestError requestError) {
        isShowingError = true;

        ErrorDialog errorDialog = new ErrorDialog(Objects.requireNonNull(getContext()));
        getCompositeDisposable().addAll(
                errorDialog.closeAction().subscribe(obj -> isShowingError = false),
                errorDialog.retryAction().subscribe(obj -> {
                    if (requestError == RequestError.NEW_VERSION) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=ru.greenbonus.greencard"));
                        startActivity(intent);
                    } else {
                        getPresenter().retryLastRequest();
                        isShowingError = false;
                    }
                })
        );
        errorDialog.setErrorDescription(requestError, message);
        errorDialog.show();
    }

    @Override
    public void showUserData(User user) {
        String userBonuses = user.getBonuses() == null ?
                "0.00" : getGreenCardApp().getDecimalFormatter().getValueString(user.getBonuses());
        if (userBonuses.startsWith(".")) userBonuses = "0" + userBonuses;

        availableBonusesTextView.setText(userBonuses);
        statusTextView.setText(user.getStatus().toLowerCase(Locale.getDefault()));
        payForNextStatusTextView.setText(user.getBonusesForNextStatus() == null ?
                "0" : getGreenCardApp().getDecimalFormatter().getValueString(user.getBonusesForNextStatus()));
    }

    @Override
    public void showCardNumber(String number) {
        if (number != null && !number.isEmpty()) {
            titleCardNumberTextView.setVisibility(View.VISIBLE);
            cardNumberTextView.setVisibility(View.VISIBLE);

            String card_number = getGreenCardApp().getDecimalFormatter().getNumberByFourNumeralAsString(Long.parseLong(number));
            card_number = card_number.substring(0, card_number.length() / 2) + "\n" + card_number.substring(card_number.length() / 2 + 1);
            cardNumberTextView.setText(card_number);
        } else {
            titleCardNumberTextView.setVisibility(View.GONE);
            cardNumberTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showSharesData(List<Share> shares, Integer countShares) {
        adapter.addData(shares, countShares);
        if (paginate == null)
            paginate = Paginate.with(sharesRecycler, getPresenter().getCallbacks())
                    .setLoadingTriggerThreshold(2)
                    .addLoadingListItem(true)
                    .setLoadingListItemCreator(new CustomLoadingListItemCreator())
                    .build();

        animationShow(wrapperHeader, sharesRecycler);
    }

    @Override
    public void showMoreSharesData(List<Share> shares) {
        adapter.addNewItems(shares);
    }

    @Override
    public void setHasLoadedAllItems() {
        sharesRecycler.post(() -> paginate.setHasMoreDataToLoad(false));
    }
}
