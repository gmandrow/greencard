package com.aspirity.greencard.presentation.base;

public interface MvpPresenter<V> {

    void attachView(V view);

    void detachView();
}
