package com.aspirity.greencard.presentation.utils.error;


public enum RequestError {
    NETWORK,
    EMPTY,
    TIMEOUT,
    CUSTOM,
    UNKNOWN,
    NEW_VERSION,
}
