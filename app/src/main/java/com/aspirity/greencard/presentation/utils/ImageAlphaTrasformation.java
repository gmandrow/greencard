package com.aspirity.greencard.presentation.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.squareup.picasso.Transformation;

public class ImageAlphaTrasformation implements Transformation {

    private int mAlpha;

    public ImageAlphaTrasformation(int alpha) {
        mAlpha = Math.max(0, Math.min(255, alpha));
    }

    public ImageAlphaTrasformation(float alpha) {
        mAlpha = (int) ((Math.max(0, Math.min(1, alpha))) * 255);
    }

    @Override
    public Bitmap transform(Bitmap source) {

        Paint paint = new Paint();
        Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(output);
        canvas.drawBitmap(source, 0, 0, paint);

        paint.setColor(Color.BLACK);
        paint.setAlpha(mAlpha);

        canvas.drawPaint(paint);

        source.recycle();

        return output;
    }

    @Override
    public String key() {
        return "shade:" + mAlpha;
    }
}
