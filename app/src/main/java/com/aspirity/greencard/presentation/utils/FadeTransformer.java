package com.aspirity.greencard.presentation.utils;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

public class FadeTransformer implements ViewPager.PageTransformer {

    @Override
    public void transformPage(@NonNull View page, float position) {
        if (position <= -1 || position >= 1) {
            page.setAlpha(0.0f);
        } else if (position == 0.0f) {
            page.setAlpha(1.0f);
        } else {
            if (position > 0.0f) {
                page.setAlpha(1 - position);
            } else {
                page.setAlpha(1 + position);
            }
        }
    }

}
