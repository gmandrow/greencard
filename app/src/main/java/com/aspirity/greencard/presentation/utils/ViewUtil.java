package com.aspirity.greencard.presentation.utils;

import android.animation.Animator;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

public class ViewUtil {

    private ViewUtil() {

    }

    public static void setTranslationX(int position, View... views) {
        for (View view : views) {
            view.setTranslationX(position * Constants.COEFFICIENT_TRANSLATION_X);
        }
    }

    public static void setTranslationY(int position, View... views) {
        for (View view : views) {
            view.setTranslationY(position / Constants.COEFFICIENT_TRANSLATION_Y);
        }
    }

    public static void setTranslationY(int position, int coefficient, View... views) {
        for (View view : views) {
            view.setTranslationY(position / coefficient);
        }
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void animationAlpha(View view, float alpha) {
        view.animate()
                .alpha(alpha)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        view.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
    }
}
