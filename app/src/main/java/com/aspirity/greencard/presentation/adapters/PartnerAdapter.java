package com.aspirity.greencard.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class PartnerAdapter extends RecyclerView.Adapter<PartnerAdapter.PartnerViewHolder> {

    private final List<Partner> partnerList;
    private final PartnerListener listener;
    private final Picasso picasso;

    public PartnerAdapter(Context context, List<Partner> partnerList, PartnerListener listener) {
        this.partnerList = partnerList;
        this.listener = listener;
        picasso = Picasso.with(context);
    }

    @Override
    public PartnerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PartnerViewHolder(LayoutInflater.
                from(parent.getContext()).inflate(R.layout.item_partner, parent, false));
    }

    @Override
    public void onBindViewHolder(PartnerViewHolder holder, int position) {
        holder.bind(partnerList.get(position), picasso, listener);
    }

    @Override
    public int getItemCount() {
        return partnerList.size();
    }

    public void addData(List<Partner> partners) {
        partnerList.clear();
        partnerList.addAll(partners);
        notifyDataSetChanged();
    }

    public interface PartnerListener {
        void onClick(Partner partner);
    }

    public static class PartnerViewHolder extends RecyclerView.ViewHolder {

        ImageView logoImageView;
        TextView nameTextView;

        public PartnerViewHolder(View itemView) {
            super(itemView);
            logoImageView = itemView.findViewById(R.id.image_partner_logo);
            nameTextView = itemView.findViewById(R.id.text_partner_name);
        }

        void bind(Partner partner, Picasso picasso, PartnerListener listener) {
            picasso.load(getLogoSrc(partner))
                    .fit()
                    .centerInside()
                    .into(logoImageView);
//            logoImageView.setImageResource(partner.getLogoImg());
            nameTextView.setText(partner.getName());
            itemView.setOnClickListener(view -> listener.onClick(partner));
        }

        private String getLogoSrc(Partner partner) {
            String path = partner.getLogoBlackBgSrc();
            if (CommonUtil.checkStringOnNullOrEmpty(path)) {
                path = partner.getLogoSrc();
            }
            return path;
        }
    }
}
