package com.aspirity.greencard.presentation.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.base.BaseActivity;
import com.aspirity.greencard.presentation.fragments.ListNotificationsFragment;
import com.aspirity.greencard.presentation.fragments.MainFragment;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.FragmentUtil;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.DrawableRes;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class MainActivity extends BaseActivity {

    private static final int TIME_DELAY = 2000;
    private static final String KEY_APP_STATE = "KEY_APP_STATE";
    private FloatingActionButton menuButton;
    private FragmentManager fragmentManager;
    private FloatingActionButton backButton;
    private Animatable menuFabAnimatable;
    private long timeMillisBackPressed;
    private DrawerLayout drawerLayout;
    private FrameLayout contentView;
    private AppState appState = AppState.ROOT;
    private boolean menuAllowed = true;
    private Handler closeKeyBoardHandler = new Handler();
    private View.OnClickListener navigationButtonClickListener = view -> {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0, new ResultReceiver(closeKeyBoardHandler) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                super.onReceiveResult(resultCode, resultData);
                getCurrentFocus().clearFocus();
            }
        });
        switch (appState) {
            case MENU:
                if (menuAllowed) {
                    menuAllowed = false;
                    closeMenu();
                    Handler handler = new Handler();
                    handler.postDelayed(() -> menuAllowed = true, 300);
                }
                return;
            case NONROOT:
                navigateBack();
                return;
            case ROOT:
                if (menuAllowed) {
                    menuAllowed = false;
                    openMenu();
                    Handler handler = new Handler();
                    handler.postDelayed(() -> menuAllowed = true, 300);
                }
        }
    };
    private DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            contentView.setTranslationX(drawerView.getWidth() * slideOffset);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
        }

        @Override
        public void onDrawerClosed(View drawerView) {
        }

        @Override
        public void onDrawerStateChanged(int newState) {
        }
    };
    private FragmentManager.OnBackStackChangedListener backStackListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            if (fragmentManager.getBackStackEntryCount() == Constants.ZERO) {
                setAppState(AppState.ROOT);
            } else {
                setAppState(AppState.NONROOT);
            }
        }
    };

    public void setAppState(AppState appState) {
        this.appState = appState;
        switch (appState) {
            case ROOT: {
                menuButton.setVisibility(View.VISIBLE);
                backButton.setVisibility(View.GONE);

                menuButton.setImageResource(R.drawable.ic_menu);
                menuButton.setBackgroundTintList(
                        ColorStateList.valueOf(getCompatColor(R.color.colorAccent)));
                startAnimationMenuIcon(R.drawable.ic_menu_animatable);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                return;
            }
            case MENU: {
                menuButton.setImageResource(R.drawable.ic_menu_cross);
                menuButton.setBackgroundTintList(
                        ColorStateList.valueOf(getCompatColor(R.color.colorWhite)));
                startAnimationMenuIcon(R.drawable.ic_menu_cross_animatable);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
                return;
            }
            case NONROOT: {
                menuButton.setVisibility(View.GONE);
                backButton.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(backStackListener);

        initViews();

        if (savedInstanceState == null) {
            showFragment(MainFragment.newInstance(false));
            setAppState(AppState.ROOT);
        } else {
            setAppState((AppState) savedInstanceState.getSerializable(KEY_APP_STATE));
        }

        if (getIntent().getStringExtra("menuFragment") != null)
            if (getIntent().getStringExtra("menuFragment").equals("notifications"))
                handleNotification();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (menuFabAnimatable != null) {
            menuFabAnimatable.stop();
            menuFabAnimatable = null;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_APP_STATE, appState);
    }

    @Override
    public void onBackPressed() {
        switch (appState) {
            case MENU:
                closeMenu();
                return;
            case NONROOT:
                navigateBack();
                return;
            case ROOT:
                if (timeMillisBackPressed + TIME_DELAY > System.currentTimeMillis()) {
                    super.onBackPressed();
                } else {
                    Toast.makeText(getBaseContext(), R.string.condition_for_exit,
                            Toast.LENGTH_SHORT).show();
                }
                timeMillisBackPressed = System.currentTimeMillis();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (getIntent().getStringExtra("menuFragment") != null)
            if (getIntent().getStringExtra("menuFragment").equals("notifications"))
                handleNotification();
    }

    private void initViews() {
        backButton = findViewById(R.id.btn_back);
        menuButton = findViewById(R.id.fab_menu);
        drawerLayout = findViewById(R.id.drawer_layout);
        contentView = findViewById(R.id.frame_container);

        findViewById(R.id.menu_container).getLayoutParams().width =
                getResources().getDisplayMetrics().widthPixels;

        backButton.setOnClickListener(navigationButtonClickListener);
        menuButton.setOnClickListener(navigationButtonClickListener);

        drawerLayout.addDrawerListener(drawerListener);
        drawerLayout.setDrawerElevation(0F);
        drawerLayout.setScrimColor(Color.TRANSPARENT);
    }

    private void navigateBack() {
        fragmentManager.popBackStack();
    }

    private void openMenu() {
        drawerLayout.openDrawer(GravityCompat.START, true);
        setAppState(AppState.MENU);
    }

    public void closeMenu() {
        drawerLayout.closeDrawer(GravityCompat.START, true);
        setAppState(AppState.ROOT);
    }

    public void showFragment(Fragment fragment) {
        Fragment current = fragmentManager.findFragmentByTag(FragmentUtil.FRAGMENT_TAG);
        if (current == null || current.getClass() != fragment.getClass()) {
            FragmentUtil.replaceFragment(fragmentManager, fragment);
        }
    }

    public void setBackButtonColor(String colorPartner) {
//        if (colorPartner != null) {
//            backButton.setColorFilter(Color.parseColor(colorPartner));
//            backButton.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.back_white_circle, null));
//        } else {
//            backButton.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.colorWhite, null));
//            backButton.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.back_green_circle, null));
//        }
    }

    private void startAnimationMenuIcon(@DrawableRes int idDrawable) {
        menuButton.setImageResource(idDrawable);
        menuFabAnimatable = (Animatable) menuButton.getDrawable();
        menuFabAnimatable.start();
    }

    private void handleNotification() {
        showFragment(ListNotificationsFragment.newInstance(false));
    }

    enum AppState {
        ROOT,
        MENU,
        NONROOT
    }
}