package com.aspirity.greencard.presentation.contract;

import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.model.SelectableVendor;
import com.aspirity.greencard.presentation.model.StorePointDataWrapper;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.util.List;

public interface StorePointFilterContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void loadFilter(StorePointDataWrapper data);
    }

    interface Presenter extends MvpPresenter<View> {

        void getVendors(List<SelectableVendor> alreadySelected);
    }
}
