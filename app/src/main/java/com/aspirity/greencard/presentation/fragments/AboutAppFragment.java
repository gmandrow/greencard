package com.aspirity.greencard.presentation.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.jakewharton.rxbinding2.view.RxView;

import androidx.annotation.Nullable;


public class AboutAppFragment extends BaseFragment {

    public AboutAppFragment() {
    }

    public static AboutAppFragment newInstance() {
        return new AboutAppFragment();
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_about_app, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        TextView versionAppTextView = view.findViewById(R.id.text_version_app);
        versionAppTextView.setText(CommonUtil.getVersion(getActivity().getApplicationContext()));

        View appRatingView = view.findViewById(R.id.view_app_rating);
        getCompositeDisposable().add(RxView.clicks(appRatingView).subscribe(o -> {
            Intent marketAppIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=ru.greenbonus.greencard"));
            marketAppIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            startActivity(marketAppIntent);
        }));
    }

}
