package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.User;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.model.OperationFilterWrapper;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.paginate.Paginate;

import java.util.List;

public interface BalanceAndOperationsContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showUserData(User user);

        void showOperationsData(List<ListItem> operations, Integer countOperations);

        void showMoreOperationsData(List<ListItem> operations);

        void setHasLoadedAllItems();

        void playStartAnimation();
    }

    interface Presenter extends MvpPresenter<BalanceAndOperationsContract.View> {

        Paginate.Callbacks getCallbacks();

        OperationFilterWrapper getCurrentFilter();

        void getContent(boolean forceRefresh);

        void getOperations(boolean restart);

        void retryLastRequest();
    }
}
