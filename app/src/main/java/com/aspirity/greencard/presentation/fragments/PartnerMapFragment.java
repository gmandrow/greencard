package com.aspirity.greencard.presentation.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.Vendor;
import com.aspirity.greencard.domain.usecase.VendorsUseCase;
import com.aspirity.greencard.presentation.activities.MainActivity;
import com.aspirity.greencard.presentation.adapters.ContactInfoPagerAdapter;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.PartnerMapContract;
import com.aspirity.greencard.presentation.dialogs.ErrorDialog;
import com.aspirity.greencard.presentation.model.PicassoMarker;
import com.aspirity.greencard.presentation.presenter.PartnerMapPresenter;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.tabs.TabLayout;
import com.jakewharton.rxbinding2.view.RxView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;


public class PartnerMapFragment
        extends BaseFragment<PartnerMapContract.View, PartnerMapContract.Presenter>
        implements PartnerMapContract.View {

    private GoogleMap map;
    private List<Target> targets;
    private ViewPager contactInfoViewPager;
    private View contactInfoWrapper;
    private View mapWrapper;
    private ContactInfoPagerAdapter contactInfoPagerAdapter;
    private TextView countVendorsTextView;
    private CircularViewPagerHandler circularViewPagerHandler;
    private PublishSubject<Object> mapIsReadySubject = PublishSubject.create();

    private boolean isShowingError = false;

    public static PartnerMapFragment newInstance(Long partnerId, String partnerLogoSrcForMap, String partnerColor) {
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_PARTNER_ID, partnerId);
        args.putString(Constants.ARG_PARTNER_MAP_LOGO, partnerLogoSrcForMap);
        args.putString(Constants.ARG_BACKGROUND_COLOR_PARTNER, partnerColor);
        PartnerMapFragment partnerMapFragment = new PartnerMapFragment();
        partnerMapFragment.setArguments(args);
        return partnerMapFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        VendorsUseCase vendorsUseCase = new VendorsUseCase(
                context.getApplicationContext(),
                new AppSchedulerProvider(),
                RepositoryProvider.getVendorsRepository());
        onCreatePresenter(new PartnerMapPresenter(
                getGreenCardApp().getPreferenceDataManager(),
                vendorsUseCase, mapIsReadySubject.firstOrError()), this);
        contactInfoPagerAdapter = new ContactInfoPagerAdapter(context, new LinkedList<>());
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_partner_map, container, false);
        initViews(view);
        initMap();
        getPresenter().getVendors(getArguments().getLong(Constants.ARG_PARTNER_ID));
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //contactInfoViewPager.setAdapter(null);
        contactInfoViewPager.removeOnPageChangeListener(circularViewPagerHandler);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setBackButtonColor(null);
        animationShow(mapWrapper, contactInfoWrapper);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    @Override
    public void onStop() {
        super.onStop();
        ((MainActivity) getActivity())
                .setBackButtonColor(getArguments().getString(Constants.ARG_BACKGROUND_COLOR_PARTNER));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //contactInfoViewPager.setAdapter(null);
        contactInfoViewPager.removeOnPageChangeListener(circularViewPagerHandler);
        contactInfoPagerAdapter = null;
    }


    private void initViews(View view) {
        countVendorsTextView = view.findViewById(R.id.text_count_vendors);
        mapWrapper = view.findViewById(R.id.linear_wrapper_map);
        contactInfoWrapper = view.findViewById(R.id.linear_wrapper_contact_info_map);
        ((GradientDrawable) contactInfoWrapper.getBackground())
                .setColor(Color.parseColor(getArguments().getString(Constants.ARG_BACKGROUND_COLOR_PARTNER)));
        contactInfoViewPager = view.findViewById(R.id.viewpager_content_store_on_map);
        contactInfoViewPager.setAdapter(contactInfoPagerAdapter);
        TabLayout indicatorTabLayout = view.findViewById(R.id.tabs_indicator);
        indicatorTabLayout.setupWithViewPager(contactInfoViewPager, true);

        ImageView nextPageImageView = view.findViewById(R.id.image_next_page_contact_info_map);
        getCompositeDisposable().add(RxView.clicks(nextPageImageView)
                .subscribe(o -> {
                    if (isLastItem()) {
                        contactInfoViewPager.setCurrentItem(0, true);
                    } else {
                        contactInfoViewPager.setCurrentItem(
                                contactInfoViewPager.getCurrentItem() + 1,
                                true);
                    }
                }, throwable -> Timber.w(throwable, throwable.getMessage())));

        ImageView prevPageImageView = view.findViewById(R.id.image_prev_page_contact_info_map);
        getCompositeDisposable().add(RxView.clicks(prevPageImageView)
                .subscribe(o -> {
                    if (isFirstItem()) {
                        int size = contactInfoPagerAdapter.getCount();
                        contactInfoViewPager.setCurrentItem(size - 1, true);
                    } else {
                        contactInfoViewPager.setCurrentItem(
                                contactInfoViewPager.getCurrentItem() - 1,
                                true);
                    }
                }, throwable -> Timber.w(throwable, throwable.getMessage())));
    }

    private void initMap() {
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(googleMap -> {
            map = googleMap;
            map.setMapStyle(MapStyleOptions
                    .loadRawResourceStyle(getActivity(), R.raw.style_map_json));
            map.setOnMarkerClickListener(marker -> {
                getPresenter().checkPositionOfSelectedMarkerInListOfMarkers(
                        marker.getPosition(),
                        targets);
                return true;
            });
            map.setPadding(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.padding_bottom_partner_map));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(56.012683, 92.850567), 14f));
            mapIsReadySubject.onNext(new Object());
        });
    }

    private void showMarkers(List<LatLng> coordinateStores, String srcIcon) {
        createMarkersList(coordinateStores, srcIcon);

        initOnPageChangeListener();
        contactInfoViewPager.addOnPageChangeListener(circularViewPagerHandler);

        setBigIconForMarker(targets.get(Constants.FIRST_ITEM), srcIcon);

        animateCameraOfMap(Constants.FIRST_ITEM);
        changeVisibilityMarker(true, ((PicassoMarker) targets.get(Constants.FIRST_ITEM)).getMarker());
        getPresenter().checkListOfMarkersForSameCoordinates(targets, Constants.FIRST_ITEM);
    }

    private void animateCameraOfMap(int position) {
        LatLng center = new LatLng(
                ((PicassoMarker) targets.get(position)).getMarker().getPosition().latitude,
                ((PicassoMarker) targets.get(position)).getMarker().getPosition().longitude);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(center, 14f));
    }

    private void createMarkersList(List<LatLng> coordinateStores, String srcIcon) {
        targets = new LinkedList<>();
        for (LatLng coordinateStore : coordinateStores) {
            Target target = new PicassoMarker(map.addMarker(new MarkerOptions()
                    .position(coordinateStore)));
            targets.add(target);
            setSmallIconForMarker(target, srcIcon);
        }
    }

    private void setSmallIconForMarker(Target target, String srcIcon) {
        Picasso.with(getActivity())
                .load(srcIcon)
                .resizeDimen(R.dimen.map_marker_width, R.dimen.map_marker_height)
                .centerInside()
                .into(target);
    }

    private void setBigIconForMarker(Target target, String srcIcon) {
        Picasso.with(getActivity())
                .load(srcIcon)
                .resizeDimen(R.dimen.big_map_marker_width, R.dimen.big_map_marker_height)
                .centerInside()
                .into(target);
    }

    private void initOnPageChangeListener() {
        circularViewPagerHandler = new CircularViewPagerHandler(contactInfoViewPager);
    }

    @Override
    public void showError(String message, RequestError requestError) {
        ErrorDialog errorDialog = new ErrorDialog(getContext());
        getCompositeDisposable().addAll(
                errorDialog.closeAction().subscribe(obj -> {
                    if (targets == null || targets.isEmpty()) {
                        getActivity().onBackPressed();
                    }
                }),
                errorDialog.retryAction().subscribe(obj -> {
                    getPresenter().retryLastRequest();
                })
        );
        errorDialog.setErrorDescription(requestError, message);
        errorDialog.show();
    }

    @Override
    public void showInfoVendors(Set<Vendor> vendors) {
        countVendorsTextView.setText(String.format(
                Locale.getDefault(),
                "%d %s",
                vendors.size(),
                getString(R.string.stores_on_map)));
        contactInfoPagerAdapter.addItems(vendors);
    }

    @Override
    public void showVendorsOnMap(List<LatLng> vendorsLatLng) {
        showMarkers(vendorsLatLng, getArguments().getString(Constants.ARG_PARTNER_MAP_LOGO));
    }

    @Override
    public void showSelectedVendor(int indexVendor) {
        contactInfoViewPager.setCurrentItem(indexVendor, true);
    }

    @Override
    public void changeVisibilityMarker(boolean isVisible, Marker marker) {
        marker.setVisible(isVisible);
    }

    private boolean isFirstItem() {
        return contactInfoViewPager.getCurrentItem() - 1 < 0;
    }

    private boolean isLastItem() {
        return contactInfoViewPager.getCurrentItem() + 1 == contactInfoPagerAdapter.getCount();
    }

    public class CircularViewPagerHandler implements ViewPager.OnPageChangeListener {
        private ViewPager mViewPager;
        private int mCurrentPosition;
        private int mScrollState;

        public CircularViewPagerHandler(final ViewPager viewPager) {
            mViewPager = viewPager;
        }

        @Override
        public void onPageSelected(final int position) {
            getPresenter().checkListOfMarkersForSameCoordinates(targets, position);
            changeVisibilityMarker(true, ((PicassoMarker) targets.get(position)).getMarker());

            setSmallIconForMarker(
                    targets.get(mCurrentPosition),
                    getArguments().getString(Constants.ARG_PARTNER_MAP_LOGO));
            setBigIconForMarker(
                    targets.get(position),
                    getArguments().getString(Constants.ARG_PARTNER_MAP_LOGO));

            animateCameraOfMap(position);

            mCurrentPosition = position;
        }

        @Override
        public void onPageScrollStateChanged(final int state) {
            handleScrollState(state);
            mScrollState = state;
        }

        private void handleScrollState(final int state) {
            if (state == ViewPager.SCROLL_STATE_IDLE) {
                setNextItemIfNeeded();
            }
        }

        private void setNextItemIfNeeded() {
            if (!isScrollStateSettling()) {
                handleSetNextItem();
            }
        }

        private boolean isScrollStateSettling() {
            return mScrollState == ViewPager.SCROLL_STATE_SETTLING;
        }

        private void handleSetNextItem() {
            final int lastPosition = mViewPager.getAdapter().getCount() - 1;
            if (mCurrentPosition == 0) {
                mViewPager.setCurrentItem(lastPosition, false);
            } else if (mCurrentPosition == lastPosition) {
                mViewPager.setCurrentItem(0, false);
            }
        }

        @Override
        public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
        }
    }
}
