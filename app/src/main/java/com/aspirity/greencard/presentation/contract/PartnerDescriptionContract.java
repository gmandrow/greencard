package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;

import java.util.List;

public interface PartnerDescriptionContract {

    interface View extends MvpView {

        void showVideo(String videoUrl);

        void hideVideo();

        void showGallery(List<String> srcPhotos);

        void hideGallery();

        void showOnePhoto(String srcPhoto);

        void hideOnePhoto();

        void showDescription(String description);
    }

    interface Presenter extends MvpPresenter<PartnerDescriptionContract.View> {

        void checkData(String description, List<String> srcPhotos, String videoCode);
    }
}
