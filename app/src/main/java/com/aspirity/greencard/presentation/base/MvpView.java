package com.aspirity.greencard.presentation.base;


import com.aspirity.greencard.GreenCardApplication;

public interface MvpView {

    void showLoading();

    boolean isShowingDialog();

    void hideLoading();

    GreenCardApplication getGreenCardApp();
}
