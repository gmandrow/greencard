package com.aspirity.greencard.presentation.presenter;


import android.util.Log;

import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.OperationsUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.OperationContract;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;

import java.net.SocketTimeoutException;

import retrofit2.HttpException;

public class OperationPresenter extends BasePresenter<OperationContract.View>
        implements OperationContract.Presenter {

    private final PreferenceDataManager preferenceDataManager;
    private final OperationsUseCase operationsUseCase;
    private String uniqueId;

    public OperationPresenter(
            PreferenceDataManager preferenceDataManager,
            OperationsUseCase operationsUseCase) {
        this.preferenceDataManager = preferenceDataManager;
        this.operationsUseCase = operationsUseCase;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                getView().showError(
                        ErrorParse.parseError(httpException.response()),
                        RequestError.CUSTOM);
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        } else
            Log.i("mlg", throwable.toString());
    }

    @Override
    public void getOperation(String uniqueId) {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(
                operationsUseCase.getOperation(uniqueId)
                        .observeOn(new AppSchedulerProvider().ui())
                        .doOnSubscribe(disposable -> {
                            getView().showLoading();
                            this.uniqueId = uniqueId;
                        })
                        .onErrorResumeNext(error -> {
                            getView().hideLoading();
                            handleError(error);
                            Log.i("mlg", error.toString());
                            return operationsUseCase.getOperationFallback(uniqueId);
                        })
                        .subscribe(operationListPair -> {
                            Log.i("mlg", "subscribe");
                            getView().showOperationData(operationListPair.first);
                            getView().showOperationProducts(operationListPair.second);
                            getView().hideLoading();
                        }, this::handleError));
    }

    @Override
    public void retryLastRequest() {
        getOperation(uniqueId);
    }
}
