package com.aspirity.greencard.presentation.adapters;

import android.view.View;
import android.widget.TextView;

/**
 * Created by namtarr on 15.12.2017.
 */

public abstract class FilterableAdapterListener<T> {

    public abstract void onClick(T item, String dateEnd);

    public abstract void onPartnerLogoClick(T item);

    public void openFilters(
            View actionOpenFilters, View filtersWrapper,
            TextView openFiltersTextView,
            TextView sortByTextView,
            TextView periodCalendarTextView,
            TextView storesPointsTextView) {
    }

    public void showFiltersBy(TextView sortByTextView) {
    }

    public void showFilterByPeriod(TextView periodCalendarTextView) {
    }

    public void showFilterByVendors(TextView storesPointsTextView) {
    }
}
