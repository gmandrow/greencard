package com.aspirity.greencard.presentation.utils;

import java.util.ArrayList;
import java.util.List;

public class ListUtil {

    public static <T> List<T> filter(List<T> original, FilterPredicate<T> predicate) {
        if (original == null || predicate == null) return null;
        List<T> list = new ArrayList<>();
        for (T element : original) {
            if (predicate.test(element)) {
                list.add(element);
            }
        }
        return list;
    }

    public static <T, V> List<V> map(List<T> original, MapFunction<T, V> mapper) {
        if (original == null || mapper == null) return null;
        List<V> list = new ArrayList<>();
        for (T element : original) {
            list.add(mapper.map(element));
        }
        return list;
    }

    public static <T> List<T> apply(List<T> list, ApplyFunction<T> function) {
        if (list == null || function == null) return null;
        for (T element : list) {
            function.apply(element);
        }
        return list;
    }

    public static <T> T firstOrNull(List<T> list, FilterPredicate<T> predicate) {
        if (list == null || predicate == null) return null;
        for (T element : list) {
            if (predicate.test(element)) {
                return element;
            }
        }
        return null;
    }

    public interface FilterPredicate<T> {

        boolean test(T obj);
    }

    public interface MapFunction<T, V> {

        V map(T obj);
    }

    public interface ApplyFunction<T> {
        void apply(T obj);
    }
}
