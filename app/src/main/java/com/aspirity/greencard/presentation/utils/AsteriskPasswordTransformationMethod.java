package com.aspirity.greencard.presentation.utils;

import android.text.method.PasswordTransformationMethod;
import android.view.View;


public class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
    @Override
    public CharSequence getTransformation(CharSequence source, View view) {
        return new PasswordCharSequence(source);
    }

    private class PasswordCharSequence implements CharSequence {
        private CharSequence source;

        PasswordCharSequence(CharSequence source) {
            this.source = source; // Store char sequence
        }

        public char charAt(int index) {
            return '*'; // This is the important part
        }

        public int length() {
            return source.length(); // Return default
        }

        public CharSequence subSequence(int start, int end) {
            return source.subSequence(start, end); // Return default
        }
    }
}
