package com.aspirity.greencard.presentation.base;

import android.app.Dialog;
import android.os.Bundle;

import com.aspirity.greencard.GreenCardApplication;
import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.utils.rx.RxUtil;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseActivity<V extends MvpView, P extends MvpPresenter<V>>
        extends AppCompatActivity implements MvpView {

    private P mvpPresenter;
    private CompositeDisposable compositeDisposable;
    private Dialog loadingDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = new CompositeDisposable();
        initLoadingDialog();
    }

    private void initLoadingDialog() {
        loadingDialog = new Dialog(this, R.style.NoTitleDialog);
        loadingDialog.setContentView(R.layout.dialog_loading);
        loadingDialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        loadingDialog.setCancelable(false);
        loadingDialog.setCanceledOnTouchOutside(false);
    }

    public void onCreatePresenter(P mvpPresenter, V view) {
        this.mvpPresenter = mvpPresenter;
        this.mvpPresenter.attachView(view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) loadingDialog.dismiss();
            loadingDialog = null;
        }

        RxUtil.unsubscribe(compositeDisposable);
        if (mvpPresenter != null) {
            mvpPresenter.detachView();
        }
    }

    @Override
    public void showLoading() {
        if (loadingDialog != null) {
            loadingDialog.show();
        } else {
            initLoadingDialog();
            loadingDialog.show();
        }
    }

    @Override
    public boolean isShowingDialog() {
        return loadingDialog != null && loadingDialog.isShowing();
    }

    @Override
    public void hideLoading() {
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    @Override
    public GreenCardApplication getGreenCardApp() {
        return (GreenCardApplication) getApplication();
    }

    public P getPresenter() {
        return mvpPresenter;
    }

    public int getCompatColor(int color) {
        return ResourcesCompat.getColor(getResources(), color, null);
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    public Dialog getLoadingDialog() {
        return loadingDialog;
    }
}
