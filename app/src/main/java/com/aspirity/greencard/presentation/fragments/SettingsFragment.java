package com.aspirity.greencard.presentation.fragments;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.SettingsContract;
import com.aspirity.greencard.presentation.dialogs.ErrorDialog;
import com.aspirity.greencard.presentation.model.SettingsWrapper;
import com.aspirity.greencard.presentation.presenter.SettingsPresenter;
import com.aspirity.greencard.presentation.utils.AsteriskPasswordTransformationMethod;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.aspirity.greencard.presentation.views.AppbarSwipeRefreshLayout;
import com.aspirity.greencard.presentation.views.CustomFontTextView;
import com.google.android.material.textfield.TextInputLayout;
import com.jakewharton.rxbinding2.support.v4.widget.RxNestedScrollView;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.kyleduo.switchbutton.SwitchButton;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

public class SettingsFragment extends BaseFragment<SettingsContract.View, SettingsContract.Presenter>
        implements SettingsContract.View {

    private NestedScrollView settingsNestedScroll;
    private SwitchButton pushOperationsButton;
    private SwitchButton pushButton;
    private SwitchButton emailButton;
    private SwitchButton smsButton;
    private TextInputLayout newPasswordLayout;
    private TextInputLayout confirmPasswordLayout;
    private EditText newPasswordEditText;
    private EditText confirmPasswordEditText;
    private LottieAnimationView settingsLottie;
    private Animator.AnimatorListener settingsAnimatorListener;
    private AppbarSwipeRefreshLayout swipeRefreshLayout;
    private CustomFontTextView savePasswordButton;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onCreatePresenter(new SettingsPresenter(
                new UserUseCase(getGreenCardApp().getApplicationContext(),
                        RepositoryProvider.getUserRepository(),
                        new AppSchedulerProvider())), this);

        getPresenter().getMailingSettings(true);
    }

    @Nullable
    @Override
    public View onCreateView(
            @NotNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        settingsLottie.removeAnimatorListener(settingsAnimatorListener);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    private void initViews(View view) {
        settingsLottie = view.findViewById(R.id.image_settings);
        settingsNestedScroll = view.findViewById(R.id.nested_scroll_settings);
        View titleSettings = view.findViewById(R.id.text_title_settings);
        getCompositeDisposable().add(RxNestedScrollView.scrollChangeEvents(settingsNestedScroll)
                .subscribe(viewScrollChangeEvent -> {
                    settingsLottie.setTranslationX(
                            viewScrollChangeEvent.scrollY() * Constants.COEFFICIENT_TRANSLATION_X);
                    titleSettings.setTranslationY(
                            -viewScrollChangeEvent.scrollY() / Constants.COEFFICIENT_TRANSLATION_Y);
                }, throwable -> Timber.w(throwable)));

        newPasswordEditText = view.findViewById(R.id.edit_input_new_password);
        newPasswordEditText.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        newPasswordLayout = view.findViewById(R.id.til_new_password);
        newPasswordLayout.setTypeface(Typeface.createFromAsset(
                getActivity().getAssets(),
                "fonts/proxima_nova_semibold.otf"
        ));

        confirmPasswordEditText = view.findViewById(R.id.edit_input_confirm_password);
        confirmPasswordEditText.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        confirmPasswordLayout = view.findViewById(R.id.til_confirm_password);
        confirmPasswordLayout.setTypeface(Typeface.createFromAsset(
                getActivity().getAssets(),
                "fonts/proxima_nova_semibold.otf"
        ));

        CustomFontTextView cancelPasswordButton = view.findViewById(R.id.text_action_cancel_password);
        Observable<String> cancelClick = RxView.clicks(cancelPasswordButton).map(o -> "").share();
        getCompositeDisposable().addAll(
                cancelClick.subscribe(RxTextView.text(newPasswordEditText)),
                cancelClick.subscribe(RxTextView.text(confirmPasswordEditText)),
                cancelClick.subscribe(param -> hideFieldError())
        );

        savePasswordButton = view.findViewById(R.id.text_action_save_password);
        getCompositeDisposable().add(
                RxView.clicks(savePasswordButton).subscribe(click -> {
                    getPresenter().savePassword(
                            newPasswordEditText.getText().toString(),
                            confirmPasswordEditText.getText().toString());
                })
        );
        confirmPasswordEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                savePasswordButton.performClick();
            }
            return false;
        });

        pushOperationsButton = view.findViewById(R.id.switch_push2);
        pushButton = view.findViewById(R.id.switch_push);
        emailButton = view.findViewById(R.id.switch_email);
        smsButton = view.findViewById(R.id.switch_sms);

        getCompositeDisposable().add(
                Observable.combineLatest(
                        RxCompoundButton.checkedChanges(pushOperationsButton),
                        RxCompoundButton.checkedChanges(pushButton),
                        RxCompoundButton.checkedChanges(emailButton),
                        RxCompoundButton.checkedChanges(smsButton),
                        SettingsWrapper::new
                )
                        .skip(1)
                        .debounce(1, TimeUnit.SECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(settings -> {
                            getPresenter().setMailingSettings(pushOperationsButton.isChecked(), pushButton.isChecked(), emailButton.isChecked(), smsButton.isChecked());
                        })
        );

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(() -> {
            settingsNestedScroll.setVisibility(View.INVISIBLE);
            getPresenter().getMailingSettings(true);
        });

        getPresenter().getMailingSettings(true);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(String error, RequestError requestError) {
        ErrorDialog errorDialog = new ErrorDialog(getContext());
        getCompositeDisposable().addAll(
                errorDialog.closeAction().subscribe(),
                errorDialog.retryAction().subscribe(obj ->
                        getPresenter().retryLastRequest())
        );
        errorDialog.setErrorDescription(requestError, error);
        errorDialog.show();
    }

    @Override
    public void showFieldErrors(Map<String, String> errors) {
        for (Map.Entry<String, String> entry : errors.entrySet()) {
            String id = entry.getKey();
            TextInputLayout layout = getView().findViewWithTag(id);
            if (layout != null) {
                layout.setErrorEnabled(true);
                layout.setError(entry.getValue());
            }
        }
    }

    @Override
    public void showMailingSettings(boolean push_operations, boolean push, boolean email, boolean sms) {
        pushOperationsButton.setCheckedImmediatelyNoEvent(push_operations);
        pushButton.setCheckedImmediatelyNoEvent(push);
        emailButton.setCheckedImmediatelyNoEvent(email);
        smsButton.setCheckedImmediatelyNoEvent(sms);
    }

    @Override
    public void showNewPasswordError(@StringRes int messageId) {
        newPasswordLayout.setErrorEnabled(true);
        newPasswordLayout.setError(getString(messageId));
    }

    @Override
    public void showConfirmPasswordError(@StringRes int messageId) {
        confirmPasswordLayout.setErrorEnabled(true);
        confirmPasswordLayout.setError(getString(messageId));
    }

    @Override
    public void hideFieldError() {
        newPasswordLayout.setErrorEnabled(false);
        confirmPasswordLayout.setErrorEnabled(false);
    }

    @Override
    public void passwordChanged() {
        newPasswordEditText.setText(Constants.EMPTY_ANSWER);
        confirmPasswordEditText.setText(Constants.EMPTY_ANSWER);
        Toast.makeText(
                getActivity(),
                R.string.password_changed_successfully,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startAnimation() {
        initSettingsLottieAnimator();
        settingsLottie.addAnimatorListener(settingsAnimatorListener);
        settingsLottie.playAnimation();
    }

    private void initSettingsLottieAnimator() {
        settingsAnimatorListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                animation(settingsNestedScroll, animShow);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };
    }
}
