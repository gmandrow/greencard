package com.aspirity.greencard.presentation.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;

import com.aspirity.greencard.R;


public class VerticalTextView extends androidx.appcompat.widget.AppCompatTextView {
    public final static int ORIENTATION_UP_TO_DOWN = 0;
    public final static int ORIENTATION_DOWN_TO_UP = 1;
    public final static int ORIENTATION_LEFT_TO_RIGHT = 2;
    public final static int ORIENTATION_RIGHT_TO_LEFT = 3;

    Rect textBounds = new Rect();
    private int direction;

    public VerticalTextView(Context context) {
        super(context);
    }

    public VerticalTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.verticaltextview);
        direction = a.getInt(R.styleable.verticaltextview_direction, 0);
        a.recycle();

        requestLayout();
        invalidate();

    }

    public void setDirection(int direction) {
        this.direction = direction;

        requestLayout();
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        getPaint().getTextBounds(getText().toString(), 0, getText().length(),
                textBounds);
        if (direction == ORIENTATION_LEFT_TO_RIGHT
                || direction == ORIENTATION_RIGHT_TO_LEFT) {
            setMeasuredDimension(measureHeight(widthMeasureSpec),
                    measureWidth(heightMeasureSpec));
        } else if (direction == ORIENTATION_UP_TO_DOWN
                || direction == ORIENTATION_DOWN_TO_UP) {
            setMeasuredDimension(measureWidth(widthMeasureSpec),
                    measureHeight(heightMeasureSpec));
        }

    }

    private int measureWidth(int measureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = textBounds.height() + getPaddingTop()
                    + getPaddingBottom();
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = textBounds.width() + getPaddingLeft() + getPaddingRight();
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();

        int startX;
        int startY;
        int stopX;
        int stopY;
        Path path = new Path();
        if (direction == ORIENTATION_UP_TO_DOWN) {
            startX = (getWidth() - textBounds.height() >> 1);
            startY = (getHeight() - textBounds.width() >> 1);
            stopX = (getWidth() - textBounds.height() >> 1);
            stopY = (getHeight() + textBounds.width() >> 1);
            path.moveTo(startX, startY);
            path.lineTo(stopX, stopY);
        } else if (direction == ORIENTATION_DOWN_TO_UP) {
            startX = (getWidth() + textBounds.height() >> 1);
            startY = (getHeight() + textBounds.width() >> 1);
            stopX = (getWidth() + textBounds.height() >> 1);
            stopY = (getHeight() - textBounds.width() >> 1);
            path.moveTo(startX, startY);
            path.lineTo(stopX, stopY);
        } else if (direction == ORIENTATION_LEFT_TO_RIGHT) {
            startX = (getWidth() - textBounds.width() >> 1);
            startY = (getHeight() + textBounds.height() >> 1);
            stopX = (getWidth() + textBounds.width() >> 1);
            stopY = (getHeight() + textBounds.height() >> 1);
            path.moveTo(startX, startY);
            path.lineTo(stopX, stopY);
        } else if (direction == ORIENTATION_RIGHT_TO_LEFT) {
            startX = (getWidth() + textBounds.width() >> 1);
            startY = (getHeight() - textBounds.height() >> 1);
            stopX = (getWidth() - textBounds.width() >> 1);
            stopY = (getHeight() - textBounds.height() >> 1);
            path.moveTo(startX, startY);
            path.lineTo(stopX, stopY);
        }

        this.getPaint().setColor(this.getCurrentTextColor());
        canvas.drawTextOnPath(getText().toString(), path, 0, 0, this.getPaint());

        canvas.restore();
    }
}
