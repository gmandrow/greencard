package com.aspirity.greencard.presentation.base;


import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.aspirity.greencard.GreenCardApplication;
import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.utils.rx.RxUtil;

import androidx.fragment.app.DialogFragment;
import io.reactivex.disposables.CompositeDisposable;

public class BaseDialogFragment<V extends MvpView, P extends MvpPresenter<V>>
        extends DialogFragment implements MvpView {

    protected Animation animShowHorizontal;
    protected Animation animHideHorizontal;
    protected Animation animShow;
    protected Animation animHide;
    protected Animation animHideUp;
    private P mvpPresenter;
    private CompositeDisposable compositeDisposable;
    private Dialog loadingDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (compositeDisposable == null || compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }
        initAnimation();
        initLoadingDialog();
    }

    public void onCreatePresenter(P mvpPresenter, V view) {
        this.mvpPresenter = mvpPresenter;
        this.mvpPresenter.attachView(view);
    }

    private void initLoadingDialog() {
        loadingDialog = new Dialog(getActivity(), R.style.NoTitleDialog);
        loadingDialog.setContentView(R.layout.dialog_loading);
        loadingDialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        loadingDialog.setCancelable(false);
        loadingDialog.setCanceledOnTouchOutside(false);
    }

    private void initAnimation() {
        animShowHorizontal = AnimationUtils.loadAnimation(getActivity(), R.anim.view_show_horizontal);
        animHideHorizontal = AnimationUtils.loadAnimation(getActivity(), R.anim.view_hide_horizontal);
        animShow = AnimationUtils.loadAnimation(getActivity(), R.anim.view_show);
        animHide = AnimationUtils.loadAnimation(getActivity(), R.anim.view_hide);
        animHideUp = AnimationUtils.loadAnimation(getActivity(), R.anim.view_hide_up);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) loadingDialog.dismiss();
            loadingDialog = null;
        }

        RxUtil.unsubscribe(compositeDisposable);
        animShowHorizontal.cancel();
        animShowHorizontal.setAnimationListener(null);
        animHideHorizontal.cancel();
        animHideHorizontal.setAnimationListener(null);
        animShow.cancel();
        animShow.setAnimationListener(null);
        animHide.cancel();
        animHide.setAnimationListener(null);
        animHideUp.cancel();
        animHideUp.setAnimationListener(null);
        if (mvpPresenter != null) {
            mvpPresenter.detachView();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showLoading() {
        loadingDialog.show();
    }

    @Override
    public boolean isShowingDialog() {
        return false;
    }

    @Override
    public void hideLoading() {
        loadingDialog.dismiss();
    }

    @Override
    public GreenCardApplication getGreenCardApp() {
        return (GreenCardApplication) getActivity().getApplication();
    }

    public P getPresenter() {
        return mvpPresenter;
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    public void animationHide(View viewOne, View viewTwo) {
        viewOne.startAnimation(animHideHorizontal);
        viewTwo.startAnimation(animHide);
    }

    public void animationHideOnlyHorizontal(View view) {
        view.startAnimation(animHideHorizontal);
    }

    public void animationShow(View viewOne, View viewTwo) {
        animShowHorizontal.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                viewOne.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animShow.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                viewTwo.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        viewOne.startAnimation(animShowHorizontal);
        viewTwo.startAnimation(animShow);
    }

    public void animationShowForSomeView(Animation animation, View... views) {
        for (View view : views) {
            view.startAnimation(animation);
        }
    }

    public void animationShowOnlyVertical(View view) {
        view.startAnimation(animShow);
    }

    public void animationShowOnlyHorizontal(View view) {
        view.startAnimation(animShowHorizontal);
    }

    public void animation(View view, Animation animation) {
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }
}