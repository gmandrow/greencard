package com.aspirity.greencard.presentation.fragments;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Faq;
import com.aspirity.greencard.presentation.activities.MainActivity;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.utils.Constants;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import androidx.annotation.Nullable;


public class AnswerFragment extends BaseFragment {

    private HtmlTextView answerTextView;
    private TextView questionTextView;
    private ScrollView answerScrollView;

    public AnswerFragment() {
    }

    public static AnswerFragment newInstance(Faq faq) {

        Bundle args = new Bundle();
        args.putParcelable(Constants.ARG_QUESTION, faq);
        AnswerFragment fragment = new AnswerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_answer, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setBackButtonColor(null);
        animation(answerScrollView, animShowHorizontal);
        animation(questionTextView, animShowHorizontal);
    }

    private void initViews(View view) {
        answerScrollView = view.findViewById(R.id.scroll_answer);
        Faq faq = getArguments().getParcelable(Constants.ARG_QUESTION);
        questionTextView = view.findViewById(R.id.text_question);
        questionTextView.setText(faq.getQuestion());
        answerTextView = view.findViewById(R.id.text_answer);
        answerTextView.setHtml(faq.getAnswer());
        answerTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

}
