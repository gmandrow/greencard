package com.aspirity.greencard.presentation.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.domain.usecase.SharesUseCase;
import com.aspirity.greencard.presentation.activities.MainActivity;
import com.aspirity.greencard.presentation.adapters.FilterableAdapterListener;
import com.aspirity.greencard.presentation.adapters.FilterableShareAdapter;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.ListSharesContract;
import com.aspirity.greencard.presentation.dialogs.ErrorDialog;
import com.aspirity.greencard.presentation.presenter.ListSharesPresenter;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.FragmentUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class ListSharesOfPartnerFragment
        extends BaseFragment<ListSharesContract.View, ListSharesContract.Presenter>
        implements ListSharesContract.View {

    private FilterableAdapterListener<Share> shareListener;
    private FilterableShareAdapter adapter;
    private RecyclerView sharesOfPartnerRecycler;
    private TextView textSharesOfPartnerTextView;
    private TextView countSharesOfPartnerTextView;
    private TextView showMoreSharesTextView;
    private ImageView logoImageView;

    private int amount = -1;

    public static ListSharesOfPartnerFragment newInstance(
            Long partnerId,
            String backgroundColor,
            String partnerLogoSrc,
            String partnerLogoSrcForMap,
            Integer vendorsCount) {
        Bundle args = new Bundle();
        ListSharesOfPartnerFragment listSharesOfPartnerFragment = new ListSharesOfPartnerFragment();
        args.putLong(Constants.ARG_PARTNER_ID, partnerId);
        args.putString(Constants.ARG_BACKGROUND_COLOR_PARTNER, backgroundColor);
        args.putString(Constants.ARG_PARTNER_LOGO, partnerLogoSrc);
        args.putString(Constants.ARG_PARTNER_MAP_LOGO, partnerLogoSrcForMap);
        args.putInt(Constants.ARG_VENDORS_COUNT, vendorsCount);
        listSharesOfPartnerFragment.setArguments(args);
        return listSharesOfPartnerFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onCreatePresenter(new ListSharesPresenter(
                getGreenCardApp().getPreferenceDataManager(),
                new SharesUseCase(
                        context.getApplicationContext(),
                        RepositoryProvider.getSharesRepository(),
                        new AppSchedulerProvider())), this);

        shareListener = new FilterableAdapterListener<Share>() {
            @Override
            public void onClick(Share item, String dateEnd) {
                ShareFragment shareFragment = ShareFragment.newInstance(
                        item,
                        dateEnd, false);
                FragmentUtil.setTransitionSlide(shareFragment, Gravity.RIGHT);
                FragmentUtil.addBackStackWithAddFragment(
                        getActivity().getSupportFragmentManager(),
                        shareFragment);
            }

            @Override
            public void onPartnerLogoClick(Share item) {
            }
        };
        adapter = new FilterableShareAdapter(getActivity().getApplicationContext(),
                new LinkedList<>(),
                getGreenCardApp().getDateUtils(),
                shareListener) {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                RecyclerView.ViewHolder holder = super.onCreateViewHolder(parent, viewType);
                if (viewType == ListItem.TYPE_FILTERS) {
                    ShareFilterViewHolder h = (ShareFilterViewHolder) holder;
                    h.actionOpenFilters.setVisibility(View.GONE);
                    h.partnerShares();
                }
                return holder;
            }
        };

        getPresenter().getSharesOfPartner(getArguments().getLong(Constants.ARG_PARTNER_ID));
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shares_of_partner, container, false);
        initViews(view);
        if (amount != -1) {
            countSharesOfPartnerTextView.setText(String.valueOf(amount));
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setBackButtonColor(
                getArguments().getString(Constants.ARG_BACKGROUND_COLOR_PARTNER));
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    private void initViews(View view) {
        View sharesOfPartnerParentView = view.findViewById(R.id.coordinator_shares);
        sharesOfPartnerParentView.setBackgroundColor(Color
                .parseColor(getArguments().getString(Constants.ARG_BACKGROUND_COLOR_PARTNER)));

        logoImageView = view.findViewById(R.id.image_partner_logo);
        Picasso.with(getActivity())
                .load(getArguments().getString(Constants.ARG_PARTNER_LOGO))
                .fit()
                .centerInside()
                .into(logoImageView);
//        showMoreSharesTextView = view.findViewById(R.id.text_show_more_shares);
        // TODO: 29.09.2017 пока скрою кнопку показать больше акций, все равно грузим все
//        showMoreSharesTextView.setVisibility(View.GONE);

        TextView showStoresOnMapTextView = view.findViewById(R.id.text_show_stores_on_map);
        ImageView showStoresOnMapImageView = view.findViewById(R.id.image_show_stores_on_map);
        showStoresOnMapImageView.setImageResource(R.drawable.ic_location);
        showStoresOnMapTextView.setOnClickListener(view12 -> {
            goToPartnerMap();
        });
        showStoresOnMapImageView.setOnClickListener(view12 -> {
            goToPartnerMap();
        });
        if (getArguments().getInt(Constants.ARG_VENDORS_COUNT, 0) == 0) {
            showStoresOnMapImageView.setVisibility(View.GONE);
            showStoresOnMapTextView.setVisibility(View.GONE);
        }

        sharesOfPartnerRecycler = view.findViewById(R.id.recycler_shares_of_partner);
        sharesOfPartnerRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        sharesOfPartnerRecycler.setHasFixedSize(false);
        sharesOfPartnerRecycler.setNestedScrollingEnabled(false);
        sharesOfPartnerRecycler.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //sharesOfPartnerRecycler.setAdapter(null);
    }

    private void goToPartnerMap() {
        FragmentUtil.addBackStackWithAddFragment(
                getActivity().getSupportFragmentManager(),
                PartnerMapFragment.newInstance(
                        getArguments().getLong(Constants.ARG_PARTNER_ID),
                        getArguments().getString(Constants.ARG_PARTNER_MAP_LOGO),
                        getArguments().getString(Constants.ARG_BACKGROUND_COLOR_PARTNER)));
    }

    @Override
    public void showError(String message, RequestError requestError) {
        ErrorDialog errorDialog = new ErrorDialog(getContext());
        getCompositeDisposable().addAll(
                errorDialog.closeAction().subscribe(),
                errorDialog.retryAction().subscribe(obj ->
                        getPresenter().retryLastRequest())
        );
        errorDialog.setErrorDescription(requestError, message);
        errorDialog.show();
    }

    @Override
    public void showSharesData(List<Share> shares, Integer countShares) {
        if (adapter != null) {
            adapter.addData(shares, countShares);
            amount = countShares;
            animationShowOnlyVertical(sharesOfPartnerRecycler);
        }
    }

    @Override
    public void showMoreSharesData(List<Share> listShares) {
    }

    @Override
    public void setHasLoadedAllItems() {
    }

    @Override
    public void playStartAnimation() {
    }
}
