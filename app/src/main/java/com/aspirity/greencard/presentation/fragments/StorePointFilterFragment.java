package com.aspirity.greencard.presentation.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.usecase.PartnersUseCase;
import com.aspirity.greencard.domain.usecase.VendorsUseCase;
import com.aspirity.greencard.presentation.adapters.StorePointPagerAdapter;
import com.aspirity.greencard.presentation.base.BaseDialogFragment;
import com.aspirity.greencard.presentation.contract.StorePointFilterContract;
import com.aspirity.greencard.presentation.model.SelectableVendor;
import com.aspirity.greencard.presentation.model.StorePointDataWrapper;
import com.aspirity.greencard.presentation.presenter.StorePointFilterPresenter;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


public class StorePointFilterFragment extends BaseDialogFragment<StorePointFilterContract.View, StorePointFilterContract.Presenter>
        implements StorePointFilterContract.View {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FloatingActionButton saveButton;
    private Animation showFab;
    private AppBarLayout storesAppBar;
    private View textTitle;
    private StorePointPagerAdapter pagerAdapter;
    private PublishSubject<List<SelectableVendor>> saveButtonPressedSubject = PublishSubject.create();
    private AppBarLayout.OnOffsetChangedListener offsetChangedListener =
            (appBarLayout, verticalOffset) -> textTitle
                    .setTranslationX(-verticalOffset * Constants.COEFFICIENT_TRANSLATION_FAST_X);

    public static StorePointFilterFragment newInstance(List<SelectableVendor> selectedVendors) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(
                Constants.ARG_SELECTED_VENDORS,
                selectedVendors == null ? null : new ArrayList<>(selectedVendors));
        StorePointFilterFragment fragment = new StorePointFilterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()) {
            @Override
            public void onBackPressed() {
                dismiss();
            }
        };
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        saveButton.startAnimation(showFab);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_sort_stores_points, container, false);
        setCancelable(false);

        SchedulerProvider schedulerProvider = new AppSchedulerProvider();
        onCreatePresenter(new StorePointFilterPresenter(
                        new PartnersUseCase(getActivity(),
                                schedulerProvider,
                                RepositoryProvider.getPartnerRepository(schedulerProvider)),
                        new VendorsUseCase(getActivity(),
                                schedulerProvider,
                                RepositoryProvider.getVendorsRepository())),
                this);

        initViews(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().getVendors(getArguments().getParcelableArrayList(Constants.ARG_SELECTED_VENDORS));
    }

    private void initViews(View view) {
        textTitle = view.findViewById(R.id.text_title_sort_stores_points);
        storesAppBar = view.findViewById(R.id.appbar_stores);
        storesAppBar.addOnOffsetChangedListener(offsetChangedListener);
        tabLayout = view.findViewById(R.id.tabs_partners_dialog_sort);
        viewPager = view.findViewById(R.id.viewpager_store_dialog_sort);
        pagerAdapter = new StorePointPagerAdapter(getActivity());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(ContextCompat.getColor(getContext(), R.color.colorWhiteAlpha),
                ContextCompat.getColor(getContext(), R.color.colorWhite));

        FloatingActionButton backFab =
                view.findViewById(R.id.fab_back_filter_dialog);
        backFab.setOnClickListener(view1 -> dismiss());

        saveButton = view.findViewById(R.id.fab_done_filter_dialog);
        saveButton.setOnClickListener(button -> {
            saveButtonPressedSubject.onNext(pagerAdapter.getVendors());
            saveButtonPressedSubject.onComplete();
            dismiss();
        });
        showFab = AnimationUtils.loadAnimation(getContext(), R.anim.view_show);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        storesAppBar.removeOnOffsetChangedListener(offsetChangedListener);
    }

    @Override
    public void showError(String message, RequestError requestError) {

    }

    @Override
    public void loadFilter(StorePointDataWrapper data) {
        pagerAdapter.setData(data);
    }

    public Observable<List<SelectableVendor>> getSaveButtonObservable() {
        return saveButtonPressedSubject
                .flatMap(Observable::fromIterable)
                .filter(SelectableVendor::isSelected)
                .toList()
                .toObservable();
    }
}
