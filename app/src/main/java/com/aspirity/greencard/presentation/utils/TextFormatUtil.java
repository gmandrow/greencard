package com.aspirity.greencard.presentation.utils;

import android.content.Context;

import com.aspirity.greencard.GreenCardApplication;
import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.model.DateInterval;
import com.aspirity.greencard.presentation.model.OperationSortParams;
import com.aspirity.greencard.presentation.model.SelectableVendor;
import com.aspirity.greencard.presentation.model.ShareSortParams;

import java.util.List;

public class TextFormatUtil {

    private TextFormatUtil() {

    }

    public static String formatShareSortParams(Context context, ShareSortParams sortParams) {
        String[] sortCases = context.getResources().getStringArray(R.array.shares_sort_by_items);
        return sortCases[sortParams.getIntValue()];
    }

    public static String formatOperationSortParams(Context context, OperationSortParams sortParams) {
        String[] sortCases = context.getResources().getStringArray(R.array.operations_sort_by_items);
        return sortCases[sortParams.getIntValue()];
    }

    public static String formatDateIntervalText(Context context, DateInterval interval) {
        DateUtil dateUtil = ((GreenCardApplication) context.getApplicationContext()).getDateUtils();
        String begin = dateUtil.getFullDateString(interval.getBeginDate());
        String end = dateUtil.getFullDateString(interval.getEndDate());
        return String.format("%s - %s", begin, end);
    }

    public static String formatStorePointsText(Context context, List<SelectableVendor> vendors) {
        int amount = vendors == null ? 0 : vendors.size();
        if (amount == 0) {
            return context.getString(R.string.text_filter_all_store_points);
        }
        return context
                .getResources()
                .getQuantityString(R.plurals.text_filter_store_points_amount,
                        amount,
                        amount);
    }

    public static String formatPhone(String value) {
        // TODO: 08.12.2017 Сойдет только для федеральных номеров России, если будут расширяться, заменить решение
        return String.format(
                "+%s %s %s %s %s",
                value.substring(0, 1),
                value.substring(1, 4),
                value.substring(4, 7),
                value.substring(7, 9),
                value.substring(9, value.length()));
    }
}
