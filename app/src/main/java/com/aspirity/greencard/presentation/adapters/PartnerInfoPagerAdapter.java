package com.aspirity.greencard.presentation.adapters;

import android.content.res.Resources;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.presentation.fragments.PartnerBonusDescriptionFragment;
import com.aspirity.greencard.presentation.fragments.PartnerDescriptionFragment;
import com.aspirity.greencard.presentation.fragments.PartnerDescriptionUpdateable;
import com.aspirity.greencard.presentation.fragments.PartnerTableBonusFragment;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * This adapter needed for viewPager on partner info.
 * Give adapter list of PartnerInfo.
 */

public class PartnerInfoPagerAdapter extends FragmentPagerAdapter {

    private final FragmentManager fragmentManager;
    private SparseArray<String> tags = new SparseArray<>();

    private Resources resources;

    private Partner partner = null;
    private boolean shouldShowBonuses = false;

    public PartnerInfoPagerAdapter(FragmentManager fm, Resources resources) {
        super(fm);
        this.fragmentManager = fm;
        this.resources = resources;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (partner == null)
                    return PartnerDescriptionFragment.newInstance(
                            "",
                            new ArrayList<>(),
                            "");
                return PartnerDescriptionFragment.newInstance(
                        partner.getDescription(),
                        partner.getPhotos(),
                        partner.getVideoCode());
            case 1:
                if (partner == null)
                    return PartnerBonusDescriptionFragment.newInstance("");
                return PartnerBonusDescriptionFragment.newInstance(partner.getFeatures());
            case 2:
                if (partner == null)
                    return PartnerBonusDescriptionFragment.newInstance("");
                return PartnerBonusDescriptionFragment.newInstance(partner.getBonusDescription());
            case 3:
                if (partner == null)
                    return PartnerTableBonusFragment.newInstance(new ArrayList<>());
                return PartnerTableBonusFragment.newInstance(partner.getBonusPercents());
            default:
                return null;
        }
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        if (object instanceof Fragment) {
            Fragment fragment = (Fragment) object;
            tags.put(position, fragment.getTag());
        }
        return object;
    }

    @Override
    public int getCount() {
        return 3 + (shouldShowBonuses ? 1 : 0);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return resources.getString(R.string.fragment_partner_tab_description);
            case 1:
                return resources.getString(R.string.fragment_partner_tab_advantages);
            case 2:
                return resources.getString(R.string.fragment_partner_tab_benefits);
            case 3:
                return resources.getString(R.string.fragment_partner_tab_table_of_bonuses);
            default:
                return null;
        }
    }

    public void setDataToShow(Partner partner, boolean shouldShowBonuses) {
        this.partner = partner;
        this.shouldShowBonuses = shouldShowBonuses;
        notifyDataSetChanged();
        for (int i = 0; i < tags.size(); i++) {
            Fragment fragment = fragmentManager.findFragmentByTag(tags.get(i));
            if (fragment instanceof PartnerDescriptionUpdateable) {
                PartnerDescriptionUpdateable updateable = (PartnerDescriptionUpdateable) fragment;
                updateable.updateDescription(partner);
            }
        }
    }
}
