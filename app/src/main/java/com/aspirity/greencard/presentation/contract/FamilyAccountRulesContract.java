package com.aspirity.greencard.presentation.contract;

import com.aspirity.greencard.domain.model.Oferta;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.RequestError;

public interface FamilyAccountRulesContract {

    interface View extends MvpView {

        void showRulesFamilyAccount(Oferta rules);

        void showError(String message, RequestError requestError);

    }

    interface Presenter extends MvpPresenter<View> {

        void loadRulesFamilyAccount();

        RequestType getLastRequest();

    }

}
