package com.aspirity.greencard.presentation.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.model.SelectableVendor;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class VendorAdapter extends RecyclerView.Adapter<VendorAdapter.VendorViewHolder> {

    private final long partnerId;
    private final List<SelectableVendor> vendorList;

    public VendorAdapter(long partnerId, List<SelectableVendor> vendorList) {
        this.partnerId = partnerId;
        this.vendorList = vendorList;
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public VendorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VendorViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_point_store, parent, false), partnerId);
    }

    @Override
    public void onBindViewHolder(VendorViewHolder holder, int position) {
        holder.bind(vendorList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return vendorList == null ? 0 : vendorList.size();
    }

    public void clear() {
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVendorEvent(SelectableVendor.VendorEvent event) {
        if (event.getPartnerId() != partnerId) {
            notifyDataSetChanged();
        }
    }

    public static class VendorViewHolder extends RecyclerView.ViewHolder {
        private final long partnerId;
        CheckBox vendorCheckBox;
        TextView titleTextView;
        TextView subtitleTextView;

        public VendorViewHolder(View itemView, long partnerId) {
            super(itemView);
            vendorCheckBox = itemView.findViewById(R.id.checkbox_store);
            titleTextView = itemView.findViewById(R.id.text_store_name);
            subtitleTextView = itemView.findViewById(R.id.text_store_address);
            if (partnerId != -1) {
                subtitleTextView.setVisibility(View.GONE);
            }
            this.partnerId = partnerId;
        }

        void bind(SelectableVendor vendor, int position) {
            vendorCheckBox.setOnCheckedChangeListener(null);
            vendorCheckBox.setChecked(vendor.isSelected());
            if (partnerId == -1) {
                titleTextView.setText(vendor.getVendor().getName());
                subtitleTextView.setText(vendor.getVendor().getAddress());
            } else {
                titleTextView.setText(vendor.getVendor().getAddress());
            }
            itemView.setOnClickListener(view ->
                    vendorCheckBox.setChecked(!vendorCheckBox.isChecked()));
            vendorCheckBox.setOnCheckedChangeListener((compoundButton, b) -> {
                vendor.setSelected(vendorCheckBox.isChecked());
                EventBus.getDefault().post(new SelectableVendor.VendorEvent(partnerId));
            });
        }
    }
}
