package com.aspirity.greencard.presentation.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.Operation;
import com.aspirity.greencard.domain.model.User;
import com.aspirity.greencard.domain.usecase.OperationsUseCase;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.activities.MainActivity;
import com.aspirity.greencard.presentation.adapters.BalanceAndOperationsAdapter;
import com.aspirity.greencard.presentation.adapters.FilterableAdapterListener;
import com.aspirity.greencard.presentation.adapters.view_holders.CustomLoadingListItemCreator;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.BalanceAndOperationsContract;
import com.aspirity.greencard.presentation.dialogs.ErrorDialog;
import com.aspirity.greencard.presentation.dialogs.OperationSortingDialog;
import com.aspirity.greencard.presentation.dialogs.PeriodFilterDialog;
import com.aspirity.greencard.presentation.model.DateInterval;
import com.aspirity.greencard.presentation.model.OperationSortParams;
import com.aspirity.greencard.presentation.model.SelectableVendor;
import com.aspirity.greencard.presentation.presenter.BalanceAndOperationsPresenter;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.ExpandCollapseAnimation;
import com.aspirity.greencard.presentation.utils.FragmentUtil;
import com.aspirity.greencard.presentation.utils.TextFormatUtil;
import com.aspirity.greencard.presentation.utils.ViewUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;
import com.aspirity.greencard.presentation.views.AppbarSwipeRefreshLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.paginate.Paginate;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;

public class BalanceAndOperationsFragment
        extends BaseFragment<BalanceAndOperationsContract.View, BalanceAndOperationsContract.Presenter>
        implements BalanceAndOperationsContract.View {

    /**
     * RECYCLER RELATED VIEWS
     **/
    private RecyclerView operationsRecycler;
    private BalanceAndOperationsAdapter adapter;
    private FilterableAdapterListener<Operation> listener;

    /**
     * OTHER VIEWS
     **/
    private View wrapperMainInfo;
    private TextView availableBonusesTextView;
    private TextView statusTextView;
    private TextView payForNextStatusTextView;
    private AppBarLayout operationsAppBarLayout;
    private AppBarLayout.OnOffsetChangedListener offsetChangedListener;
    private View titleBalanceAndOperations;
    private AppbarSwipeRefreshLayout swipeRefreshLayout;

    /**
     * PAGINATION
     **/
    private Paginate paginate;
    private View actionOpenFilters;
    private Animation.AnimationListener openFilterAnimationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            actionOpenFilters.setEnabled(true);
            animation.setAnimationListener(null);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    public static BalanceAndOperationsFragment newInstance(boolean isRestore) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.ARG_IS_RESTORE, isRestore);
        BalanceAndOperationsFragment fragment = new BalanceAndOperationsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        SchedulerProvider schedulerProvider = new AppSchedulerProvider();
        OperationsUseCase operationsUseCase = new OperationsUseCase(
                context.getApplicationContext(),
                RepositoryProvider.getOperationsRepository(schedulerProvider),
                schedulerProvider);
        UserUseCase userUseCase = new UserUseCase(
                context.getApplicationContext(),
                RepositoryProvider.getUserRepository(),
                schedulerProvider);
        onCreatePresenter(new BalanceAndOperationsPresenter(
                getGreenCardApp().getPreferenceDataManager(),
                operationsUseCase,
                userUseCase), this);
        listener = new FilterableAdapterListener<Operation>() {
            @Override
            public void onClick(Operation operation, String dateEnd) {
                OperationFragment operationFragment =
                        OperationFragment.newInstance(operation.getUniqueId());
                FragmentUtil.setTransitionSlideForTwoFragment(
                        BalanceAndOperationsFragment.this,
                        operationFragment);
                FragmentUtil.addBackStackWithAddFragment(
                        getActivity().getSupportFragmentManager(),
                        operationFragment);
            }

            @Override
            public void onPartnerLogoClick(Operation item) {
            }

            @Override
            public void openFilters(
                    View actionOpenFilters,
                    View filtersWrapper,
                    TextView openFiltersTextView,
                    TextView sortByTextView,
                    TextView periodCalendarTextView,
                    TextView storesPointsTextView) {
                BalanceAndOperationsFragment.this.actionOpenFilters = actionOpenFilters;
                ExpandCollapseAnimation animation;
                if (filtersWrapper.getVisibility() == View.GONE) {
                    actionOpenFilters.setEnabled(false);
                    sortByTextView.setText(TextFormatUtil
                            .formatOperationSortParams(getActivity(),
                                    getPresenter().getCurrentFilter().getSortParams()));
                    periodCalendarTextView.setText(TextFormatUtil
                            .formatDateIntervalText(getActivity(),
                                    getPresenter().getCurrentFilter().getDateInterval()));
                    //storesPointsTextView.setText(TextFormatUtil
                    //.formatStorePointsText(getActivity(),
                    //getPresenter().getCurrentFilter().getVendors()));

                    animation = new ExpandCollapseAnimation(
                            filtersWrapper,
                            Constants.DURATION_EXPAND_COLLAPSE_ANIM,
                            0,
                            getActivity());
                    openFiltersTextView.setText(R.string.action_close_filter);
                    animation.setAnimationListener(openFilterAnimationListener);
                } else {
                    actionOpenFilters.setEnabled(false);
                    animation = new ExpandCollapseAnimation(
                            filtersWrapper,
                            Constants.DURATION_EXPAND_COLLAPSE_ANIM,
                            1,
                            getActivity());
                    openFiltersTextView.setText(R.string.fragment_balance_and_operations_filter);
                    animation.setAnimationListener(openFilterAnimationListener);
                }
                filtersWrapper.startAnimation(animation);
            }

            @Override
            public void showFiltersBy(TextView sortByTextView) {
                showDialogSortBy(sortByTextView);
            }

            @Override
            public void showFilterByPeriod(TextView periodCalendarTextView) {
                showDialogPeriodCalendar(periodCalendarTextView);
            }

            @Override
            public void showFilterByVendors(TextView storesPointsTextView) {
                showDialogStoresPoints(storesPointsTextView);
            }
        };
        adapter = new BalanceAndOperationsAdapter(
                context.getApplicationContext(),
                new LinkedList<>(),
                listener);

        getPresenter().getContent(true);
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_balance_and_operations, container, false);

        initViews(view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (paginate != null) {
            paginate.unbind();
        }
        operationsRecycler.setAdapter(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setBackButtonColor(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    private void initViews(View view) {
        operationsAppBarLayout = view.findViewById(R.id.appbar_operations);
        wrapperMainInfo = view.findViewById(R.id.linear_wrapper_main_info);
        titleBalanceAndOperations = view.findViewById(R.id.text_title_balance_and_operations);

        initAppBarListener();

        availableBonusesTextView = view.findViewById(R.id.text_available_bonuses);
        statusTextView = view.findViewById(R.id.text_current_status);
        payForNextStatusTextView = view.findViewById(R.id.text_next_status);

        operationsRecycler = view.findViewById(R.id.recycler_operations);
        operationsRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        operationsRecycler.setAdapter(adapter);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setAppBar(operationsAppBarLayout);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(() -> getPresenter().getContent(true));
    }

    private void initAppBarListener() {
        offsetChangedListener = (appBarLayout, verticalOffset) -> {
            ViewUtil.setTranslationX(-verticalOffset * 2, wrapperMainInfo);
            ViewUtil.setTranslationY(verticalOffset, 1, titleBalanceAndOperations);
        };
        operationsAppBarLayout.addOnOffsetChangedListener(offsetChangedListener);
    }

    /**
     * Open dialog with filter by different parameters
     */
    private void showDialogSortBy(TextView sortByTextView) {
        final OperationSortingDialog dialog = new OperationSortingDialog(getActivity(),
                getPresenter().getCurrentFilter().getSortParams());
        Observable<OperationSortParams> save =
                dialog.getSaveButtonObservable()
                        .doOnNext(object -> dialog.dismiss())
                        .share();
        getCompositeDisposable().addAll(
                dialog.getCancelButtonObservable().subscribe(object -> dialog.dismiss()),
                save.subscribe(sorting -> getPresenter().getCurrentFilter().setSortParams(sorting)),
                save.map(sorting -> TextFormatUtil.formatOperationSortParams(getActivity(), sorting))
                        .subscribe(RxTextView.text(sortByTextView)),
                save.subscribe(sorting -> {
                    getPresenter().getOperations(true);
                })
        );
        dialog.show();
    }

    /**
     * Open dialog with filter by Calendar date
     */
    private void showDialogPeriodCalendar(TextView periodCalendarTextView) {
        final PeriodFilterDialog dialog = new PeriodFilterDialog(getActivity(),
                getPresenter().getCurrentFilter().getDateInterval());
        Observable<DateInterval> save =
                dialog.getSaveButtonObservable()
                        .doOnNext(object -> dialog.dismiss())
                        .share();
        getCompositeDisposable().addAll(dialog.getInnerDisposables());
        getCompositeDisposable().addAll(
                dialog.getCancelButtonObservable().subscribe(object -> dialog.dismiss()),
                save.subscribe(interval -> getPresenter().getCurrentFilter().setDateInterval(interval)),
                save.map(interval -> TextFormatUtil.formatDateIntervalText(getActivity(), interval))
                        .subscribe(RxTextView.text(periodCalendarTextView)),
                save.subscribe(sorting -> {
                    getPresenter().getOperations(true);
                })
        );
        dialog.show();
    }

    /**
     * Open dialogFragment (StorePointFilterFragment) with filter by stores points
     */
    private void showDialogStoresPoints(TextView storesPointsTextView) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag(Constants.DIALOG_TAG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        StorePointFilterFragment dialogFragment = StorePointFilterFragment.newInstance(
                getPresenter().getCurrentFilter().getVendors());
        Observable<List<SelectableVendor>> save =
                dialogFragment.getSaveButtonObservable()
                        .share();
        getCompositeDisposable().addAll(
                save.subscribe(vendors -> getPresenter().getCurrentFilter().setVendors(vendors)),
                save.map(vendors -> TextFormatUtil.formatStorePointsText(getActivity(), vendors))
                        .subscribe(RxTextView.text(storesPointsTextView)),
                save.subscribe(sorting -> {
                    getPresenter().getOperations(true);
                })
        );
        dialogFragment.show(ft, Constants.DIALOG_TAG);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(String message, RequestError requestError) {
        ErrorDialog errorDialog = new ErrorDialog(getContext());
        getCompositeDisposable().addAll(
                errorDialog.closeAction().subscribe(obj -> hideLoading()),
                errorDialog.retryAction().subscribe(obj -> getPresenter().retryLastRequest())
        );
        errorDialog.setErrorDescription(requestError, message);
        errorDialog.show();
    }

    @Override
    public void showUserData(User user) {
        String userBonuses = user.getBonuses() == null ?
                "0.00" : getGreenCardApp().getDecimalFormatter().getValueString(user.getBonuses());
        if (userBonuses.startsWith(".")) userBonuses = "0" + userBonuses;

        availableBonusesTextView.setText(userBonuses);
        statusTextView.setText(user.getStatus().toLowerCase(Locale.getDefault()));
        payForNextStatusTextView.setText(user.getBonusesForNextStatus() == null ?
                "0" : getGreenCardApp().getDecimalFormatter().getValueString(user.getBonusesForNextStatus()));
    }

    @Override
    public void showOperationsData(List<ListItem> operations, Integer countOperations) {
        adapter.addData(operations);
        if (paginate == null && !operations.isEmpty()) {
            paginate = Paginate.with(operationsRecycler, getPresenter().getCallbacks())
                    .setLoadingTriggerThreshold(2)
                    .addLoadingListItem(true)
                    .setLoadingListItemCreator(new CustomLoadingListItemCreator())
                    .build();
        }
    }

    @Override
    public void showMoreOperationsData(List<ListItem> operations) {
        adapter.addNewItems(operations);
    }

    @Override
    public void setHasLoadedAllItems() {
        operationsRecycler.post(() -> paginate.setHasMoreDataToLoad(false));
    }

    @Override
    public void playStartAnimation() {
        animationShow(wrapperMainInfo, operationsRecycler);
    }
}
