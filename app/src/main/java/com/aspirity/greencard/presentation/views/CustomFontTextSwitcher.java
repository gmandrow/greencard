package com.aspirity.greencard.presentation.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextSwitcher;

import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.utils.FontUtil;

public class CustomFontTextSwitcher extends TextSwitcher {

    private AttributeSet attrs;

    public CustomFontTextSwitcher(Context context) {
        super(context);
    }

    public CustomFontTextSwitcher(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.attrs = attrs;
        FontUtil.setCustomFont(this, context, attrs,
                R.styleable.com_aspirity_greencard_views_CustomFontTextView,
                R.styleable.com_aspirity_greencard_views_CustomFontTextView_myFont);
    }

    public AttributeSet getAttrs() {
        return attrs;
    }
}
