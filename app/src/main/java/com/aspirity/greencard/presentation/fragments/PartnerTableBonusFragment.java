package com.aspirity.greencard.presentation.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.BonusPercent;
import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.PartnerTableContract;
import com.aspirity.greencard.presentation.presenter.PartnerTablePresenter;
import com.aspirity.greencard.presentation.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;


public class PartnerTableBonusFragment
        extends BaseFragment<PartnerTableContract.View, PartnerTableContract.Presenter>
        implements PartnerTableContract.View, PartnerDescriptionUpdateable {

    private TextView memberPercentTextView;
    private TextView silverPercentTextView;
    private TextView goldPercentTextView;
    private TextView platinumPercentTextView;

    public static PartnerTableBonusFragment newInstance(List<BonusPercent> bonusPercent) {
        ArrayList<BonusPercent> bonusPercentArrayList = new ArrayList<>();
        bonusPercentArrayList.addAll(bonusPercent);
        Bundle args = new Bundle();
        args.putParcelableArrayList(Constants.ARG_BONUSES_TABLE_PARTNER, bonusPercentArrayList);
        PartnerTableBonusFragment fragment = new PartnerTableBonusFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_partner_info_table, container, false);
        onCreatePresenter(new PartnerTablePresenter(), this);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        memberPercentTextView = view.findViewById(R.id.text_member_percent_bonuses);
        silverPercentTextView = view.findViewById(R.id.text_silver_percent_bonuses);
        goldPercentTextView = view.findViewById(R.id.text_gold_percent_bonuses);
        platinumPercentTextView = view.findViewById(R.id.text_platinum_percent_bonuses);
        getPresenter().checkStatuses(getArguments()
                .getParcelableArrayList(Constants.ARG_BONUSES_TABLE_PARTNER));
    }

    @Override
    public void showMemberPercents(String percents) {
        memberPercentTextView.setText(percents);
    }

    @Override
    public void showSilverPercents(String percents) {
        silverPercentTextView.setText(percents);
    }

    @Override
    public void showGoldPercents(String percents) {
        goldPercentTextView.setText(percents);
    }

    @Override
    public void showPlatinumPercents(String percents) {
        platinumPercentTextView.setText(percents);
    }

    @Override
    public void updateDescription(Partner partner) {
        ArrayList<BonusPercent> bonusPercentArrayList = new ArrayList<>();
        bonusPercentArrayList.addAll(partner.getBonusPercents());
        Bundle args = new Bundle();
        args.putParcelableArrayList(Constants.ARG_BONUSES_TABLE_PARTNER, bonusPercentArrayList);
        setArguments(args);
        if (getView() != null) {
            getPresenter().checkStatuses(partner.getBonusPercents());
        }
    }
}
