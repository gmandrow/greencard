package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.error.RequestError;

public interface PartnerContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showPartnerContent(Partner partner);
    }

    interface Presenter extends MvpPresenter<PartnerContract.View> {

        void getPartner(Long partnerId);

        void retryLastRequest();
    }
}
