package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.model.Vendor;
import com.aspirity.greencard.domain.usecase.VendorsUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.PartnerMapContract;
import com.aspirity.greencard.presentation.model.EmptyMapException;
import com.aspirity.greencard.presentation.model.PicassoMarker;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Target;

import java.net.SocketTimeoutException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.HttpException;

public class PartnerMapPresenter extends BasePresenter<PartnerMapContract.View>
        implements PartnerMapContract.Presenter {

    private final PreferenceDataManager preferenceDataManager;
    private final VendorsUseCase vendorsUseCase;
    private final Single<Object> mapDidLoad;
    private long partnerId;

    public PartnerMapPresenter(
            PreferenceDataManager preferenceDataManager,
            VendorsUseCase vendorsUseCase, Single<Object> mapDidLoad) {
        this.preferenceDataManager = preferenceDataManager;
        this.vendorsUseCase = vendorsUseCase;
        this.mapDidLoad = mapDidLoad;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                getView().showError(
                        ErrorParse.parseError(httpException.response()),
                        RequestError.CUSTOM);
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        } else if (throwable instanceof EmptyMapException) {
            getView().showError(null, RequestError.EMPTY);
        } else {
            // generic error handling
        }
    }

    private void handleSuccess(Map<Vendor, LatLng> vendorLatLngMap) {
        if (!isViewAttached()) {
            return;
        }

        if (vendorLatLngMap == null || vendorLatLngMap.isEmpty()) {
            getView().hideLoading();
            return;
        }

        LinkedList<LatLng> linkedList = new LinkedList<>();
        linkedList.addAll(vendorLatLngMap.values());

        getView().showInfoVendors(vendorLatLngMap.keySet());
        getView().showVendorsOnMap(linkedList);
        getView().hideLoading();
    }

    @Override
    public void getVendors(Long partnerId) {
        if (!isViewAttached()) {
            return;
        }

        getCompositeDisposable().add(
                Single.zip(vendorsUseCase.getVendorsOfPartner(partnerId), mapDidLoad, (vendors, map) -> vendors)
                        .doOnSubscribe(disposable -> {
                            getView().showLoading();
                            this.partnerId = partnerId;
                        })
                        .onErrorResumeNext(error -> {
                            getView().hideLoading();
                            handleError(error);
                            if (error instanceof EmptyMapException) {
                                return Single.error(error);
                            } else {
                                return vendorsUseCase.getVendorsOfPartnerFallback(partnerId);
                            }
                        })
                        .subscribe(this::handleSuccess, error -> {
                        }));
    }

    @Override
    public void retryLastRequest() {
        getVendors(partnerId);
    }

    @Override
    public void checkPositionOfSelectedMarkerInListOfMarkers(LatLng position, List<Target> targets) {
        if (!isViewAttached()) {
            return;
        }

        final int[] indexMarkerInList = {-1};
        getCompositeDisposable().add(Observable.fromIterable(targets)
                .map(target -> (PicassoMarker) target)
                .filter(picassoMarker -> {
                    ++indexMarkerInList[Constants.FIRST_ITEM];
                    return picassoMarker.getMarker().getPosition().equals(position);
                })
                .firstOrError()
                .subscribe(picassoMarker -> {
                    getView().showSelectedVendor(indexMarkerInList[Constants.FIRST_ITEM]);
                }, Throwable::printStackTrace));
    }

    @Override
    public void checkListOfMarkersForSameCoordinates(List<Target> targets, int index) {
        if (!isViewAttached()) {
            return;
        }

        Target selectedTarget = targets.get(index);
        LatLng selectedPosition = ((PicassoMarker) selectedTarget).getMarker().getPosition();
        double selectedLatitude = selectedPosition.latitude;
        double selectedLongitude = selectedPosition.longitude;
        getCompositeDisposable().add(Observable.fromIterable(targets)
                .filter(target -> !target.equals(selectedTarget))
                .map(target -> (PicassoMarker) target)
                .subscribe(picassoMarker -> {
                    checkSelectedPositionWithAnotherPositionForProximity(
                            selectedLatitude,
                            selectedLongitude,
                            picassoMarker);
                }, Throwable::printStackTrace));
    }

    private void checkSelectedPositionWithAnotherPositionForProximity(
            double selectedLatitude,
            double selectedLongitude,
            PicassoMarker picassoMarker) {
        Marker marker = picassoMarker.getMarker();
        double latitude = marker.getPosition().latitude;
        double longitude = marker.getPosition().longitude;
        if (Math.abs(selectedLatitude - latitude) <= Constants.MAX_DIFF_BETWEEN_POSITIONS
                || Math.abs(selectedLongitude - longitude) <= Constants.MAX_DIFF_BETWEEN_POSITIONS) {
            getView().changeVisibilityMarker(false, marker);
        } else {
            getView().changeVisibilityMarker(true, marker);
        }
    }
}
