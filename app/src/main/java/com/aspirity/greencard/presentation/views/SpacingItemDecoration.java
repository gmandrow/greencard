package com.aspirity.greencard.presentation.views;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by namtarr on 15.12.2017.
 */

public class SpacingItemDecoration extends RecyclerView.ItemDecoration {

    private int fromItem;
    private int spacing;

    public SpacingItemDecoration(int fromItem, int spacing) {
        this.fromItem = fromItem;
        this.spacing = spacing;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (position < fromItem) return;

        outRect.left = spacing;
        outRect.right = spacing;
    }
}
