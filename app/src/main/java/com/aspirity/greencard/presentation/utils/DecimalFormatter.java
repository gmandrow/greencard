package com.aspirity.greencard.presentation.utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import static com.aspirity.greencard.presentation.utils.Constants.DEFAULT_VALUE_BNS;
import static com.aspirity.greencard.presentation.utils.Constants.MILLION;
import static com.aspirity.greencard.presentation.utils.Constants.ZERO_ANSWER;

public class DecimalFormatter {

    private final ThreadLocal<DecimalFormat> threadLocalDecimalFormat =
            new ThreadLocal<DecimalFormat>() {
                @Override
                protected DecimalFormat initialValue() {
                    DecimalFormat decimalFormat = new DecimalFormat("###,###.00");
                    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                    symbols.setGroupingSeparator(' ');
                    symbols.setDecimalSeparator('.');
                    decimalFormat.setDecimalFormatSymbols(symbols);
                    return decimalFormat;
                }
            };

    private final ThreadLocal<DecimalFormat> threadLocalMillionNumbersFormat =
            new ThreadLocal<DecimalFormat>() {
                @Override
                protected DecimalFormat initialValue() {
                    DecimalFormat decimalFormat = new DecimalFormat("###,###.000М");
                    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                    symbols.setGroupingSeparator(' ');
                    symbols.setDecimalSeparator('.');
                    decimalFormat.setDecimalFormatSymbols(symbols);
                    decimalFormat.setRoundingMode(RoundingMode.DOWN);
                    return decimalFormat;
                }
            };

    private final ThreadLocal<DecimalFormat> threadLocalWholeNumberFormat =
            new ThreadLocal<DecimalFormat>() {
                @Override
                protected DecimalFormat initialValue() {
                    DecimalFormat decimalFormat = new DecimalFormat("###,###");
                    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                    symbols.setGroupingSeparator(' ');
                    decimalFormat.setDecimalFormatSymbols(symbols);
                    decimalFormat.setRoundingMode(RoundingMode.DOWN);
                    return decimalFormat;
                }
            };

    private final ThreadLocal<DecimalFormat> threadLocalWholeNumberByFourNumeralFormat =
            new ThreadLocal<DecimalFormat>() {
                @Override
                protected DecimalFormat initialValue() {
                    DecimalFormat decimalFormat = new DecimalFormat("####,####");
                    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                    symbols.setGroupingSeparator(' ');
                    decimalFormat.setDecimalFormatSymbols(symbols);
                    return decimalFormat;
                }
            };

    public <Value> String getValueString(Value value) {
        if (value instanceof String) {
            String valueString = ((String) value);
            if (valueString.isEmpty() || valueString.equals(DEFAULT_VALUE_BNS) || valueString.equals(ZERO_ANSWER)) {
                return DEFAULT_VALUE_BNS;
            }

            return checkNumberOnMillion(valueString);
        } else {
            return threadLocalWholeNumberFormat.get().format(value);
        }
    }

    public <Value> String getNumberByFourNumeralAsString(Value value) {
        return threadLocalWholeNumberByFourNumeralFormat.get().format(value);
    }

    private String checkNumberOnMillion(String value) {
        String wholeNumber = value.split("\\.")[0];
        if (wholeNumber.contains("-") && wholeNumber.length() < 8) return checkNumberOnWhole(value);
        if (wholeNumber.length() < 7) return checkNumberOnWhole(value);
        Double millions = Double.valueOf(wholeNumber) / MILLION;
        return threadLocalMillionNumbersFormat.get().format(millions);
    }

    private String checkNumberOnWhole(String value) {
        double valueDouble = Double.valueOf(value);
        if (valueDouble >= 1000 || valueDouble <= -1000) {
            return threadLocalWholeNumberFormat.get().format(valueDouble);
        } else {
            return threadLocalDecimalFormat.get().format(valueDouble);
        }
    }
}
