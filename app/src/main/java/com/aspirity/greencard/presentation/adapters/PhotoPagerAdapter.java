package com.aspirity.greencard.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aspirity.greencard.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.viewpager.widget.PagerAdapter;

/**
 * This adapter needed for viewPager on partner screen in description section.
 * Give adapter list of string (path photo).
 */

public class PhotoPagerAdapter extends PagerAdapter {

    private final List<String> srcPhotos;
    private final Picasso picasso;
    private Context context;
    private LayoutInflater layoutInflater;

    public PhotoPagerAdapter(Context context, List<String> srcPhotos) {
        this.context = context;
        picasso = Picasso.with(context);
        layoutInflater = LayoutInflater.from(this.context);
        this.srcPhotos = srcPhotos;
    }

    @Override
    public int getCount() {
        return srcPhotos.size();
    }

    // Returns true if a particular object (page) is from a particular page
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    // This method should create the page for the given position passed to it as an argument.
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.item_photo, container, false);

        ImageView photoImageView = itemView.findViewById(R.id.photo);
        picasso.load(srcPhotos.get(position))
                .fit()
                .centerInside()
                .into(photoImageView);

        container.addView(itemView);
        return itemView;
    }

    // Removes the page from the container for the given position.
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
