package com.aspirity.greencard.presentation.views;

import android.content.Context;
import android.util.AttributeSet;

import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.utils.FontUtil;

import androidx.appcompat.widget.AppCompatTextView;


public class CustomFontTextView extends AppCompatTextView {
    public CustomFontTextView(Context context) {
        super(context);
    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        FontUtil.setCustomFont(this, context, attrs,
                R.styleable.com_aspirity_greencard_views_CustomFontTextView,
                R.styleable.com_aspirity_greencard_views_CustomFontTextView_myFont);
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        FontUtil.setCustomFont(this, context, attrs,
                R.styleable.com_aspirity_greencard_views_CustomFontTextView,
                R.styleable.com_aspirity_greencard_views_CustomFontTextView_myFont);
    }
}
