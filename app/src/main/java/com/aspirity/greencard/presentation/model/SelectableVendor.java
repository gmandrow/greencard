package com.aspirity.greencard.presentation.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.aspirity.greencard.domain.model.Vendor;

public class SelectableVendor implements Parcelable {

    public static final Parcelable.Creator<SelectableVendor> CREATOR = new Parcelable.Creator<SelectableVendor>() {
        @Override
        public SelectableVendor createFromParcel(Parcel source) {
            return new SelectableVendor(source);
        }

        @Override
        public SelectableVendor[] newArray(int size) {
            return new SelectableVendor[size];
        }
    };
    private Vendor vendor;
    private boolean isSelected;

    public SelectableVendor() {
    }

    public SelectableVendor(Vendor vendor, boolean isSelected) {
        this.vendor = vendor;
        this.isSelected = isSelected;
    }

    protected SelectableVendor(Parcel in) {
        this.vendor = in.readParcelable(Vendor.class.getClassLoader());
        this.isSelected = in.readByte() != 0;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SelectableVendor that = (SelectableVendor) o;

        return vendor.equals(that.vendor);
    }

    @Override
    public int hashCode() {
        return vendor.hashCode();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.vendor, flags);
        dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
    }

    public static class VendorEvent {
        private final long partnerId;

        public VendorEvent(long partnerId) {
            this.partnerId = partnerId;
        }

        public long getPartnerId() {
            return partnerId;
        }
    }
}
