package com.aspirity.greencard.presentation.fragments;

import android.animation.Animator;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.domain.usecase.SharesUseCase;
import com.aspirity.greencard.presentation.adapters.FilterableAdapterListener;
import com.aspirity.greencard.presentation.adapters.FilterableShareAdapter;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.ListSharesContract;
import com.aspirity.greencard.presentation.dialogs.ErrorDialog;
import com.aspirity.greencard.presentation.dialogs.PeriodFilterDialog;
import com.aspirity.greencard.presentation.dialogs.ShareSortingDialog;
import com.aspirity.greencard.presentation.model.DateInterval;
import com.aspirity.greencard.presentation.model.SelectableVendor;
import com.aspirity.greencard.presentation.model.ShareSortParams;
import com.aspirity.greencard.presentation.presenter.ListSharesPresenter;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.ExpandCollapseAnimation;
import com.aspirity.greencard.presentation.utils.FragmentUtil;
import com.aspirity.greencard.presentation.utils.TextFormatUtil;
import com.aspirity.greencard.presentation.utils.ViewUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.google.android.material.appbar.AppBarLayout;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.paginate.Paginate;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;

public class ListSharesFragment
        extends BaseFragment<ListSharesContract.View, ListSharesContract.Presenter>
        implements ListSharesContract.View {

    private List<Share> shares = new LinkedList<>();

    /**
     * RECYCLER RELATED VIEWS
     **/
    private RecyclerView sharesRecycler;
    private FilterableAdapterListener<Share> shareListener;
    private FilterableShareAdapter adapter;

    /**
     * OTHER VIEWS
     **/
    private LottieAnimationView imagePercent;
    private Animator.AnimatorListener percentAnimationListener;
    private AppBarLayout appBarLayout;
    private AppBarLayout.OnOffsetChangedListener offsetChangedListener;
    private View actionOpenFilters;
    private View archiveSharesWrapper;
    private View titleSharesWrapper;

    /**
     * PAGINATION
     **/
    private Paginate paginate;
    private Animation.AnimationListener openFilterAnimationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            actionOpenFilters.setEnabled(true);
            animation.setAnimationListener(null);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    public static ListSharesFragment newInstance(/*some args*/) {
        return new ListSharesFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onCreatePresenter(new ListSharesPresenter(
                getGreenCardApp().getPreferenceDataManager(),
                new SharesUseCase(
                        getGreenCardApp().getApplicationContext(),
                        RepositoryProvider.getSharesRepository(),
                        new AppSchedulerProvider())), this);

        shareListener = new FilterableAdapterListener<Share>() {
            @Override
            public void onClick(Share item, String dateEnd) {
                ShareFragment shareFragment = ShareFragment.newInstance(item, dateEnd, false);
                /*FragmentUtil.setTransitionSlideForTwoFragment(ListSharesFragment.this,
                        shareFragment);*/
                FragmentUtil.setTransitionSlide(shareFragment, Gravity.RIGHT, Gravity.LEFT);
                FragmentUtil.addBackStackWithAddFragment(
                        getActivity().getSupportFragmentManager(),
                        shareFragment);
            }

            @Override
            public void onPartnerLogoClick(Share item) {
                PartnerFragment fragment = PartnerFragment.newInstance(item.getPartnerId(), item.getPartnerColor(), item.getPartnerLogoSrc());
                FragmentUtil.setTransitionSlideForTwoFragment(ListSharesFragment.this, fragment);
                FragmentUtil.addBackStackWithAddFragment(
                        getActivity().getSupportFragmentManager(),
                        fragment);
            }

            @Override
            public void openFilters(
                    View actionOpenFilters,
                    View filtersWrapper,
                    TextView openFiltersTextView,
                    TextView sortByTextView,
                    TextView periodCalendarTextView,
                    TextView storesPointsTextView) {
                ListSharesFragment.this.actionOpenFilters = actionOpenFilters;
                ExpandCollapseAnimation animation;
                if (filtersWrapper.getVisibility() == View.GONE) {
                    actionOpenFilters.setEnabled(false);
                    sortByTextView.setText(TextFormatUtil
                            .formatShareSortParams(getActivity(), getPresenter().getCurrentFilter().getSortParams()));
                    periodCalendarTextView.setText(TextFormatUtil
                            .formatDateIntervalText(getActivity(), getPresenter().getCurrentFilter().getDateInterval()));
                    //storesPointsTextView.setText(TextFormatUtil
                    //.formatStorePointsText(getActivity(), getPresenter().getCurrentFilter().getVendors()));

                    animation = new ExpandCollapseAnimation(
                            filtersWrapper,
                            Constants.DURATION_EXPAND_COLLAPSE_ANIM,
                            0,
                            getActivity());
                    openFiltersTextView.setText(R.string.action_close_filter);
                    animation.setAnimationListener(openFilterAnimationListener);
                } else {
                    actionOpenFilters.setEnabled(false);
                    animation = new ExpandCollapseAnimation(
                            filtersWrapper,
                            Constants.DURATION_EXPAND_COLLAPSE_ANIM,
                            1,
                            getActivity());
                    openFiltersTextView.setText(R.string.fragment_balance_and_operations_filter);
                    animation.setAnimationListener(openFilterAnimationListener);
                }
                filtersWrapper.startAnimation(animation);
            }

            @Override
            public void showFiltersBy(TextView sortByTextView) {
                showDialogSortBy(sortByTextView);
            }

            @Override
            public void showFilterByPeriod(TextView periodCalendarTextView) {
                showDialogPeriodCalendar(periodCalendarTextView);
            }

            @Override
            public void showFilterByVendors(TextView storesPointsTextView) {
                showDialogStoresPoints(storesPointsTextView);
            }
        };

        adapter = new FilterableShareAdapter(
                context.getApplicationContext(),
                new LinkedList<>(),
                getGreenCardApp().getDateUtils(),
                shareListener);

        getPresenter().getContent();
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_shares, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (paginate != null) {
            paginate.unbind();
        }
        //sharesRecycler.setAdapter(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    private void initViews(View view) {
        appBarLayout = view.findViewById(R.id.appbar_shares);
        archiveSharesWrapper = view.findViewById(R.id.linear_wrapper_archive_shares);
        archiveSharesWrapper.setOnClickListener(view1 -> {
            SharesArchiveFragment fragment = new SharesArchiveFragment();
            FragmentUtil.setTransitionSlide(fragment, Gravity.RIGHT, Gravity.LEFT);
            FragmentUtil.addBackStackWithAddFragment(
                    getActivity().getSupportFragmentManager(),
                    fragment);
        });
        titleSharesWrapper = view.findViewById(R.id.text_title_shares);
        imagePercent = view.findViewById(R.id.image_percent_shares);

        initAppBarListener();

        sharesRecycler = view.findViewById(R.id.recycler_shares);
        sharesRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        sharesRecycler.getLayoutManager().setAutoMeasureEnabled(true);
        sharesRecycler.setHasFixedSize(false);
        sharesRecycler.setAdapter(adapter);
        sharesRecycler.setItemAnimator(null);
    }

    private void initAppBarListener() {
        offsetChangedListener = (appBarLayout, verticalOffset) -> {
            ViewUtil.setTranslationX(-verticalOffset, imagePercent);
        };
        appBarLayout.addOnOffsetChangedListener(offsetChangedListener);
    }

    /**
     * Open dialog with filter by different parameters
     */
    private void showDialogSortBy(TextView sortByTextView) {
        final ShareSortingDialog dialog = new ShareSortingDialog(getActivity(), getPresenter().getCurrentFilter().getSortParams());
        Observable<ShareSortParams> save =
                dialog.getSaveButtonObservable()
                        .doOnNext(object -> dialog.dismiss())
                        .share();
        getCompositeDisposable().addAll(
                dialog.getCancelButtonObservable().subscribe(object -> dialog.dismiss()),
                save.subscribe(sorting -> getPresenter().getCurrentFilter().setSortParams(sorting)),
                save.map(sorting -> TextFormatUtil.formatShareSortParams(getActivity(), sorting))
                        .subscribe(RxTextView.text(sortByTextView)),
                save.subscribe(sorting -> {
                    getPresenter().getShares(true);
                })
        );
        dialog.show();
    }

    /**
     * Open dialog with filter by Calendar date
     */
    private void showDialogPeriodCalendar(TextView periodCalendarTextView) {
        final PeriodFilterDialog dialog = new PeriodFilterDialog(getActivity(), getPresenter().getCurrentFilter().getDateInterval());
        Observable<DateInterval> save =
                dialog.getSaveButtonObservable()
                        .doOnNext(object -> dialog.dismiss())
                        .share();
        getCompositeDisposable().addAll(dialog.getInnerDisposables());
        getCompositeDisposable().addAll(
                dialog.getCancelButtonObservable().subscribe(object -> dialog.dismiss()),
                save.subscribe(interval -> getPresenter().getCurrentFilter().setDateInterval(interval)),
                save.map(interval -> TextFormatUtil.formatDateIntervalText(getActivity(), interval))
                        .subscribe(RxTextView.text(periodCalendarTextView)),
                save.subscribe(sorting -> {
                    getPresenter().getShares(true);
                })
        );
        dialog.show();
    }

    /**
     * Open dialogFragment (StorePointFilterFragment) with filter by stores points
     */
    private void showDialogStoresPoints(TextView storesPointsTextView) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag(Constants.DIALOG_TAG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        StorePointFilterFragment dialogFragment = StorePointFilterFragment.newInstance(getPresenter().getCurrentFilter().getVendors());
        Observable<List<SelectableVendor>> save =
                dialogFragment.getSaveButtonObservable()
                        .share();
        getCompositeDisposable().addAll(
                save.subscribe(vendors -> getPresenter().getCurrentFilter().setVendors(vendors)),
                save.map(vendors -> TextFormatUtil.formatStorePointsText(getActivity(), vendors))
                        .subscribe(RxTextView.text(storesPointsTextView)),
                save.subscribe(sorting -> {
                    getPresenter().getShares(true);
                })
        );
        dialogFragment.show(ft, Constants.DIALOG_TAG);
    }


    @Override
    public void showError(String message, RequestError requestError) {
        ErrorDialog errorDialog = new ErrorDialog(getContext());
        getCompositeDisposable().addAll(
                errorDialog.closeAction().subscribe(),
                errorDialog.retryAction().subscribe(obj ->
                        getPresenter().retryLastRequest())
        );
        errorDialog.setErrorDescription(requestError, message);
        errorDialog.show();
    }

    @Override
    public void showSharesData(List<Share> shares, Integer countShares) {
        adapter.addData(shares, countShares);
        /*if (paginate == null) {
            paginate = Paginate.with(sharesRecycler, getPresenter().getCallbacks())
                    .setLoadingTriggerThreshold(2)
                    .addLoadingListItem(true)
                    .setLoadingListItemCreator(new CustomLoadingListItemCreator())
                    .build();
        }*/
    }

    @Override
    public void showMoreSharesData(List<Share> listShares) {
        adapter.addNewItems(listShares);
    }

    @Override
    public void setHasLoadedAllItems() {
        //sharesRecycler.post(() -> paginate.setHasMoreDataToLoad(false));
    }

    @Override
    public void playStartAnimation() {
        initPercentAnimationListener();
        imagePercent.addAnimatorListener(percentAnimationListener);
        imagePercent.playAnimation();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        imagePercent.removeAnimatorListener(percentAnimationListener);
    }

    private void initPercentAnimationListener() {
        percentAnimationListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                animation(sharesRecycler, animShow);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };
    }
}
