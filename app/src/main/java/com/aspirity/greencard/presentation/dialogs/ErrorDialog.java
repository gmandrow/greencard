package com.aspirity.greencard.presentation.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.jakewharton.rxbinding2.view.RxView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.reactivex.Observable;

/**
 * Created by namtarr on 20.12.2017.
 */

public class ErrorDialog extends Dialog {

    private TextView titleTextView;
    private TextView messageTextView;
    private TextView closeTextView;
    private TextView retryTextView;

    public ErrorDialog(@NonNull Context context) {
        super(context);
        init();
    }

    public ErrorDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    protected ErrorDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init();
    }

    private void init() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_error);
        setCanceledOnTouchOutside(false);

        titleTextView = findViewById(R.id.text_name_error);
        messageTextView = findViewById(R.id.text_message_error);
        closeTextView = findViewById(R.id.text_close_dialog);
        retryTextView = findViewById(R.id.text_action_repeat);
    }

    public Observable<Object> closeAction() {
        return RxView
                .clicks(closeTextView)
                .doOnNext(obj -> dismiss());
    }

    public Observable<Object> retryAction() {
        return RxView
                .clicks(retryTextView)
                .doOnNext(obj -> dismiss());
    }

    public void setErrorDescription(RequestError requestError, String message) {
        switch (requestError) {
            case TIMEOUT:
                titleTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_timeout);
                break;
            case CUSTOM:
                titleTextView.setText(R.string.error_title);
                messageTextView.setText(message);
                break;
            case EMPTY:
                titleTextView.setText(R.string.error_title_empty);
                messageTextView.setText(R.string.error_message_empty);
                break;
            case NETWORK:
                titleTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_not_connection_network);
                break;
            case NEW_VERSION:
                titleTextView.setText(R.string.error_title_new_version);
                messageTextView.setText(R.string.error_message_new_version);
                retryTextView.setText(R.string.dialog_error_btn_repeat_new_version);
                break;
        }
    }
}
