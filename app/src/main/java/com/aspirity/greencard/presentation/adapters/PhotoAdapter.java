package com.aspirity.greencard.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.adapters.view_holders.PhotoViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoViewHolder> {

    private final List<String> photosSrc;
    private final Picasso picasso;

    public PhotoAdapter(Context context, List<String> photosSrc) {
        this.photosSrc = photosSrc;
        this.picasso = Picasso.with(context);
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhotoViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.item_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        holder.bind(photosSrc.get(position), picasso);
    }

    @Override
    public int getItemCount() {
        return photosSrc.size();
    }

    public void addData(List<String> photosSrc) {
        this.photosSrc.clear();
        this.photosSrc.addAll(photosSrc);
        notifyDataSetChanged();
    }
}
