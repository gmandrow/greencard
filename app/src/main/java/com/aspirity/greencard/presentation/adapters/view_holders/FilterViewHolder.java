package com.aspirity.greencard.presentation.adapters.view_holders;

import android.view.View;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.adapters.FilterableAdapterListener;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by namtarr on 15.12.2017.
 */

public class FilterViewHolder extends RecyclerView.ViewHolder {

    public View actionOpenFilters;
    TextView openFiltersTextView;
    View actionShowFiltersBy;
    View actionShowFiltersByPeriod;
    //View actionShowFiltersByVendors;
    View filtersWrapper;
    TextView sortByTextView;
    TextView periodCalendarTextView;
    TextView storesPointsTextView;

    public FilterViewHolder(View itemView) {
        super(itemView);
        actionOpenFilters = itemView.findViewById(R.id.linear_wrapper_open_filter);
        openFiltersTextView = itemView.findViewById(R.id.text_open_filters);
        actionShowFiltersBy = itemView.findViewById(R.id.linear_wrapper_action_show_sort_by);
        actionShowFiltersByPeriod = itemView.findViewById(R.id.linear_wrapper_action_show_sort_period);
        //actionShowFiltersByVendors = itemView.findViewById(R.id.linear_wrapper_action_show_sort_stores);
        filtersWrapper = itemView.findViewById(R.id.linear_wrapper_filters);
        sortByTextView = itemView.findViewById(R.id.text_sort_by);
        periodCalendarTextView = itemView.findViewById(R.id.text_period_calendar);
        //storesPointsTextView = itemView.findViewById(R.id.text_stores_points);
    }

    public void bind(FilterableAdapterListener listener) {
        actionOpenFilters.setOnClickListener(view -> listener.openFilters(
                view,
                filtersWrapper,
                openFiltersTextView,
                sortByTextView,
                periodCalendarTextView,
                storesPointsTextView));
        actionShowFiltersBy.setOnClickListener(view ->
                listener.showFiltersBy(sortByTextView));
        actionShowFiltersByPeriod.setOnClickListener(view ->
                listener.showFilterByPeriod(periodCalendarTextView));
        //actionShowFiltersByVendors.setOnClickListener(view ->
        //listener.showFilterByVendors(storesPointsTextView));
    }
}