package com.aspirity.greencard.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Notification;
import com.aspirity.greencard.presentation.adapters.view_holders.NotificationsViewHolder;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsViewHolder> {

    private final List<Notification> notifications;
    private final NotificationListener notificationListener;
    private final Context context;

    public NotificationsAdapter(Context context, List<Notification> notifications, NotificationListener notificationListener) {
        this.context = context;
        this.notifications = notifications;
        this.notificationListener = notificationListener;
    }

    @Override
    public NotificationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationsViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.item_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(NotificationsViewHolder holder, int position) {
        holder.bind(context, notifications.get(position));
        holder.bindListener(notificationListener, notifications.get(position));
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public void addData(List<Notification> items) {
        this.notifications.clear();
        this.notifications.addAll(items);
        notifyDataSetChanged();
    }

    public interface NotificationListener {
        void onClick(Notification notification);
    }
}
