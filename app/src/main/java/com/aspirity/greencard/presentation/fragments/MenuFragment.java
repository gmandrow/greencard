package com.aspirity.greencard.presentation.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.activities.AuthActivity;
import com.aspirity.greencard.presentation.activities.MainActivity;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.MenuContract;
import com.aspirity.greencard.presentation.presenter.MenuPresenter;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;

import androidx.annotation.Nullable;


public class MenuFragment extends BaseFragment<MenuContract.View, MenuContract.Presenter>
        implements MenuContract.View {

    private ListView menuListView;
    private ArrayAdapter<String> adapter;

    public static MenuFragment newInstance() {
        return new MenuFragment();
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        UserUseCase userUseCase = new UserUseCase(
                getGreenCardApp().getApplicationContext(),
                RepositoryProvider.getUserRepository(),
                new AppSchedulerProvider());
        onCreatePresenter(new MenuPresenter(userUseCase, getGreenCardApp().getPreferenceDataManager()), this);
        initViews(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (adapter == null && menuListView != null) {
            adapter = new ArrayAdapter<>(
                    getActivity(),
                    R.layout.item_menu,
                    getResources().getStringArray(R.array.menu_items));
            menuListView.setAdapter(adapter);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (menuListView != null) {
            menuListView.setAdapter(null);
            adapter = null;
        }
    }

    private void initViews(View view) {
        menuListView = view.findViewById(R.id.list_menu);
        menuListView.setOnItemClickListener((adapterView, view1, i, l) -> {
            switch (i) {
                /*case 0:
                    ((MainActivity) getActivity()).closeNavigation(VirtualCardFragment.newInstance());
                    break;*/
                case 0:
                    ((MainActivity) getActivity()).showFragment(MainFragment.newInstance(false));
                    break;
                case 1:
                    ((MainActivity) getActivity()).showFragment(ListSharesFragment.newInstance());
                    break;
                case 2:
                    ((MainActivity) getActivity()).showFragment(ListPartnersFragment.newInstance(false));
                    break;
                case 3:
                    ((MainActivity) getActivity()).showFragment(BalanceAndOperationsFragment.newInstance(false));
                    break;
                case 4:
                    ((MainActivity) getActivity()).showFragment(ListNotificationsFragment.newInstance(false));
                    break;
                case 5:
                    ((MainActivity) getActivity()).showFragment(SettingsFragment.newInstance());
                    break;
                case 6:
                    ((MainActivity) getActivity()).showFragment(FaqFragment.newInstance());
                    break;
                case 7:
                    ((MainActivity) getActivity()).showFragment(OperationsWithCardFragment.newInstance());
                    break;
                case 8:
                    ((MainActivity) getActivity()).showFragment(InformationFragment.newInstance());
                    break;
                /*case 8:
                    ((MainActivity) getActivity()).showFragment(AboutAppFragment.newInstance());
                    break;*/
                case 9:
                    getPresenter().logOut();
                    break;
            }
            ((MainActivity) getActivity()).closeMenu();
        });
    }

    @Override
    public void successLogOut() {
        startActivity(new Intent(getActivity(), AuthActivity.class));
        getActivity().finish();
    }
}
