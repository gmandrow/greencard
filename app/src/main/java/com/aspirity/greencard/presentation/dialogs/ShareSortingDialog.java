package com.aspirity.greencard.presentation.dialogs;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.model.ShareSortParams;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

public class ShareSortingDialog extends BaseFilterDialog<ShareSortParams> {

    private ListView sortByListView;

    public ShareSortingDialog(@NonNull Context context, ShareSortParams initial) {
        super(context, R.layout.dialog_sort_by, initial);
    }

    @Override
    protected void init(Context context) {
        super.init(context);
        sortByListView = findViewById(R.id.list_sort_by);

        sortByListView.setAdapter(new SortingArrayAdapter(
                context,
                R.layout.item_sort_by,
                context.getResources().getStringArray(R.array.shares_sort_by_items)
        ));
        sortByListView.setOnItemClickListener((adapterView, view, i, l) -> {
            filterValue = ShareSortParams.fromInt(i);
            ((ArrayAdapter) adapterView.getAdapter()).notifyDataSetChanged();
        });
        setOnDismissListener(dialogInterface -> sortByListView.setAdapter(null));
    }

    private final class SortingArrayAdapter extends ArrayAdapter<String> {

        public SortingArrayAdapter(Context context, int resource, String[] objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            view.setTextColor(ContextCompat.getColor(parent.getContext(), Objects.equals(position, filterValue.getIntValue())
                    ? R.color.colorWhite
                    : R.color.colorWhiteAlpha));
            return view;
        }
    }
}
