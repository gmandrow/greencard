package com.aspirity.greencard.presentation.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.Card;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.usecase.CardsUseCase;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.activities.MainActivity;
import com.aspirity.greencard.presentation.adapters.CardPagerAdapter;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.FamilyAccountContract;
import com.aspirity.greencard.presentation.dialogs.ErrorDialog;
import com.aspirity.greencard.presentation.presenter.FamilyAccountPresenter;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.FontUtil;
import com.aspirity.greencard.presentation.utils.ViewUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.aspirity.greencard.presentation.views.AppbarSwipeRefreshLayout;
import com.aspirity.greencard.presentation.views.CustomFontTextView;
import com.google.android.material.textfield.TextInputLayout;
import com.jakewharton.rxbinding2.support.v4.view.RxViewPager;
import com.jakewharton.rxbinding2.support.v4.widget.RxNestedScrollView;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.viewpager.widget.ViewPager;
import me.relex.circleindicator.CircleIndicator;
import timber.log.Timber;

public class FamilyAccountFragment
        extends BaseFragment<FamilyAccountContract.View, FamilyAccountContract.Presenter>
        implements FamilyAccountContract.View {

    private List<Card> cards = new LinkedList<>();
    private CardPagerAdapter adapter;
    private ViewPager familyCardsPager;
    private CircleIndicator indicator;
    private TextSwitcher mainCardTextView;
    private NestedScrollView familyAccountNestedScrollView;
    private TextView getVerificationCodeTextView;
    private TextInputLayout cardNumberTil;
    private EditText cardNumberEditText;
    private String cardNumber = "";
    private TextInputLayout cardCodeTil;
    private EditText cardCodeEditText;
    private TextInputLayout ownerFullNameTil;
    private EditText ownerFullNameEditText;
    private TextInputLayout phoneTil;
    private EditText phoneEditText;
    private TextInputLayout verificationCodeTil;
    private EditText verificationCodeEditText;
    private TextView actionAddCardTextView;
    private LinearLayout addNewCardWrapper;
    private View addNewCardAction;
    private View rulesFamilyAccount;
    private AppbarSwipeRefreshLayout swipeRefreshLayout;

    public static FamilyAccountFragment newInstance() {
        return new FamilyAccountFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        CardsUseCase cardsUseCase = new CardsUseCase(
                Objects.requireNonNull(getContext()).getApplicationContext(),
                new AppSchedulerProvider(),
                RepositoryProvider.getCardsRepository());
        UserUseCase userUseCase = new UserUseCase(
                getContext().getApplicationContext(),
                RepositoryProvider.getUserRepository(),
                new AppSchedulerProvider());
        onCreatePresenter(new FamilyAccountPresenter(cardsUseCase, userUseCase), this);
        adapter = new CardPagerAdapter(getContext(), cards);

        getPresenter().getCards(true);
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_family_account, container, false);

        initViews(view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        /*Bitmap b = Bitmap.createBitmap(familyCardsPager.getWidth(), familyCardsPager.getHeight(), Bitmap.Config.RGB_565);
        Canvas c = new Canvas(b);
        familyCardsPager.draw(c);
        BitmapDrawable bd = new BitmapDrawable(getActivity().getResources(), b);
        familyCardsPager.setBackground(bd);
        familyCardsPager.setAdapter(null);*/
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) Objects.requireNonNull(getActivity())).setBackButtonColor(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    private void initViews(View view) {
        familyAccountNestedScrollView = view.findViewById(R.id.nested_scroll_family_account);
        addNewCardAction = view.findViewById(R.id.linear_wrapper_action_add_new_card);
        addNewCardWrapper = view.findViewById(R.id.linear_wrapper_add_new_card);
        View titleFamilyAccount = view.findViewById(R.id.text_title);
        rulesFamilyAccount = view.findViewById(R.id.linear_wrapper_rules_family_account);

        View rulesFamilyAccountButton = view.findViewById(R.id.btn_rules_family_account);
        rulesFamilyAccountButton.setOnClickListener(v -> {
            Toast.makeText(getActivity(), "В разработке", Toast.LENGTH_SHORT).show();
            /*FamilyAccountRulesFragment fragment = new FamilyAccountRulesFragment();
            FragmentUtil.setTransitionSlide(fragment, Gravity.RIGHT, Gravity.LEFT);
            FragmentUtil.addBackStackWithAddFragment(
                    getActivity().getSupportFragmentManager(),
                    fragment);*/
        });

        TextView cancelActionTextView = view.findViewById(R.id.text_action_cancel);
        getCompositeDisposable().add(RxView.clicks(cancelActionTextView)
                .subscribe(view1 -> {
                    animationWithoutListener(addNewCardAction, animShowHorizontal);
                    addNewCardAction.setVisibility(View.VISIBLE);
                    animationWithoutListener(addNewCardWrapper, animHide);
                    addNewCardWrapper.setVisibility(View.GONE);
                }, throwable -> Timber.w(throwable, throwable.getMessage())));

        mainCardTextView = view.findViewById(R.id.text_main_or_additional_card);

        initPager(view);

        initInputFields(view);

        getVerificationCodeTextView = view.findViewById(R.id.text_action_send_verification_code);
        getCompositeDisposable().add(RxView.clicks(getVerificationCodeTextView)
                .debounce(200, TimeUnit.MILLISECONDS)
                .subscribeOn(new AppSchedulerProvider().ui())
                .observeOn(new AppSchedulerProvider().ui())
                .subscribe(o -> getPresenter().onClickGetVerificationCode(
                        cardNumberEditText.getText().toString(),
                        cardCodeEditText.getText().toString()), throwable -> Timber.d(throwable, throwable.getMessage())));

        actionAddCardTextView = view.findViewById(R.id.text_action_add_card);
        getPresenter().onClickVerifyAttachedCard(RxView.clicks(actionAddCardTextView));

        //animation views when scroll up/down
        getCompositeDisposable().add(RxNestedScrollView.scrollChangeEvents(familyAccountNestedScrollView)
                .subscribe(viewScrollChangeEvent -> {
                    ViewUtil.setTranslationY(
                            -viewScrollChangeEvent.scrollY(),
                            Constants.WITHOUT_COEFFICIENT_TRANSLATION_Y,
                            mainCardTextView,
                            familyCardsPager,
                            indicator);
                    ViewUtil.setTranslationY(
                            -viewScrollChangeEvent.scrollY() * 5,
                            titleFamilyAccount,
                            rulesFamilyAccount,
                            rulesFamilyAccountButton);
                }, throwable -> Timber.w(throwable, throwable.getMessage())));

        getCompositeDisposable().add(RxView.clicks(addNewCardAction)
                .subscribe(o -> {
                    if (addNewCardWrapper.getVisibility() == View.GONE) {
                        addNewCardWrapper.setVisibility(View.VISIBLE);
                        animationWithoutListener(addNewCardWrapper, animShow);
                        animationWithoutListener(addNewCardAction, animHideHorizontal);
                        addNewCardAction.setVisibility(View.GONE);

                        familyAccountNestedScrollView.post(() ->
                                familyAccountNestedScrollView.smoothScrollTo(
                                        0,
                                        addNewCardWrapper.getBottom() / 3));
                    }
                }, throwable -> Timber.w(throwable, throwable.getMessage())));

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(() -> getPresenter().getCards(true));
    }

    private void initInputFields(View view) {
        cardNumberTil = view.findViewById(R.id.til_card_number);
        cardNumberEditText = view.findViewById(R.id.edit_input_card_number);
        MaskedTextChangedListener cardNumberMaskedListener = new MaskedTextChangedListener(
                "[0000] [0000] [0000] [0000]",
                true,
                cardNumberEditText,
                null,
                (maskFilled, extractedValue) -> cardNumber = extractedValue);
        cardNumberEditText.addTextChangedListener(cardNumberMaskedListener);
        cardNumberEditText.setOnFocusChangeListener(cardNumberMaskedListener);

        cardCodeTil = view.findViewById(R.id.til_card_code);
        cardCodeEditText = view.findViewById(R.id.edit_input_card_code);
        ownerFullNameTil = view.findViewById(R.id.til_owner_full_name);
        ownerFullNameEditText = view.findViewById(R.id.edit_input_name_owner_card);
        phoneTil = view.findViewById(R.id.til_phone);
        phoneEditText = view.findViewById(R.id.edit_input_phone_number);
        verificationCodeTil = view.findViewById(R.id.til_verification_code);
        verificationCodeEditText = view.findViewById(R.id.edit_input_verification_code);

        verificationCodeEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                actionAddCardTextView.performClick();
            }
            return false;
        });

        getCompositeDisposable().add(RxTextView.afterTextChangeEvents(cardNumberEditText)
                .skipInitialValue()
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribeOn(new AppSchedulerProvider().ui())
                .observeOn(new AppSchedulerProvider().ui())
                .subscribe(textViewAfterTextChangeEvent -> getPresenter().checkCard(
                        cardNumber,
                        cardCodeEditText.getText().toString()), throwable -> Timber.e(throwable, "OnError input cardNumber: %s", throwable.getMessage())));

        getCompositeDisposable().add(RxTextView.afterTextChangeEvents(cardCodeEditText)
                .skipInitialValue()
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribeOn(new AppSchedulerProvider().ui())
                .observeOn(new AppSchedulerProvider().ui())
                .subscribe(textViewAfterTextChangeEvent -> getPresenter().checkCard(
                        cardNumber,
                        cardCodeEditText.getText().toString()), throwable -> Timber.e(throwable, "OnError input cardCode: %s", throwable.getMessage())));
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initPager(View view) {
        familyCardsPager = view.findViewById(R.id.viewpager_family_cards);
        indicator = view.findViewById(R.id.indicator_viewpager);
        familyCardsPager.setClipToPadding(false);
        familyCardsPager.setPageMargin(getResources().getDimensionPixelSize(
                R.dimen.margin_fragment_family_account_family_card));
        familyCardsPager.setPadding(
                getResources().getDimensionPixelSize(
                        R.dimen.padding_fragment_family_account_family_card),
                0,
                getResources().getDimensionPixelSize(
                        R.dimen.padding_fragment_family_account_family_card),
                0);
        familyCardsPager.setAdapter(adapter);

        familyCardsPager.setPageTransformer(false, (page, position) -> {
            if (familyCardsPager.getCurrentItem() == 0) {
                page.setTranslationX(-getResources().getDimensionPixelSize(
                        R.dimen.padding_fragment_family_account_family_card));
            } else if (familyCardsPager.getCurrentItem() == adapter.getCount() - 1) {
                page.setTranslationX(getResources().getDimensionPixelSize(
                        R.dimen.padding_fragment_family_account_family_last_card));
            } else {
                page.setTranslationX(0);
            }
        });

        mainCardTextView.setFactory(() -> {
            CustomFontTextView tv = new CustomFontTextView(getActivity());
            tv.setTextColor(Objects.requireNonNull(getActivity()).getResources().getColor(R.color.colorWhite));
            tv.setTypeface(FontUtil.getFont("proxima_nova_semibold.otf", getActivity()));
            return tv;
        });
        mainCardTextView.setInAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.view_fade_in));
        mainCardTextView.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.view_fade_out));

        getCompositeDisposable().add(RxViewPager.pageSelections(familyCardsPager)
                .subscribe(integer -> {
                    if (integer == 0)
                        mainCardTextView.setText(getString(R.string.fragment_family_account_main_card));
                    else
                        mainCardTextView.setText(getString(R.string.fragment_family_account_additional_card));

                }, throwable -> Timber.w(throwable, throwable.getMessage())));

        familyCardsPager.setOnTouchListener((view1, motionEvent) -> {
            swipeRefreshLayout.setEnabled(motionEvent.getAction() == MotionEvent.ACTION_UP);
            return false;
        });

        indicator.setViewPager(familyCardsPager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void hideErrors() {
        cardNumberTil.setErrorEnabled(false);
        cardCodeTil.setErrorEnabled(false);
        ownerFullNameTil.setErrorEnabled(false);
        phoneTil.setErrorEnabled(false);
        verificationCodeTil.setErrorEnabled(false);
    }

    @Override
    public void showFieldErrors(Map<String, String> fieldErrors) {
        for (Map.Entry<String, String> entry : fieldErrors.entrySet()) {
            String tag = entry.getKey();
            TextInputLayout layout = Objects.requireNonNull(getView()).findViewWithTag(tag);
            if (layout != null) {
                layout.setErrorEnabled(true);
                layout.setError(entry.getValue());
            }
        }
    }

    @Override
    public void showError(String message, RequestError requestError) {
        ErrorDialog errorDialog = new ErrorDialog(Objects.requireNonNull(getContext()));
        getCompositeDisposable().addAll(
                errorDialog.closeAction().subscribe(obj -> hideLoading()),
                errorDialog.retryAction().subscribe(obj ->
                        getPresenter().retryLastRequest())
        );
        errorDialog.setErrorDescription(requestError, message);
        errorDialog.show();
    }

    @Override
    public void showCardNumberError() {
        cardNumberTil.setErrorEnabled(true);
        cardNumberTil.setError(getString(R.string.error_message_empty_field));
    }

    @Override
    public void showCardCodeError() {
        cardCodeTil.setErrorEnabled(true);
        cardCodeTil.setError(getString(R.string.error_message_empty_field));
    }

    @Override
    public void showCards(List<Card> cards) {
        adapter.addItems(cards);
        mainCardTextView.setVisibility(View.VISIBLE);
        animationShowForSomeView(animShowHorizontal, mainCardTextView, familyCardsPager, indicator);
        animation(familyAccountNestedScrollView, animShow);
    }

    @Override
    public void showUserData(String fullName, String phone) {
        ownerFullNameEditText.setText(fullName);
        phoneEditText.setText(phone);
        getVerificationCodeTextView.setEnabled(true);
    }

    @Override
    public void showToastSendCode(String phone) {
        ViewUtil.showToast(
                getContext(),
                String.format(
                        Locale.getDefault(),
                        "%s %s",
                        getString(R.string.fragment_family_account_sms_send_on_phone),
                        phoneEditText.getText().toString()));
    }

    @Override
    public void showSmsCodeError() {
        verificationCodeTil.setErrorEnabled(true);
        verificationCodeTil.setError(getString(R.string.error_message_empty_field));
    }

    @Override
    public void showSuccess() {
        ViewUtil.showToast(
                getContext(),
                getString(R.string.fragment_family_account_attached_success));
        cardNumberEditText.setText(Constants.EMPTY_ANSWER);
        cardCodeEditText.setText(Constants.EMPTY_ANSWER);
        ownerFullNameEditText.setText(Constants.EMPTY_ANSWER);
        phoneEditText.setText(Constants.EMPTY_ANSWER);
        verificationCodeEditText.setText(Constants.EMPTY_ANSWER);
        animationWithoutListener(addNewCardAction, animShowHorizontal);
        addNewCardAction.setVisibility(View.VISIBLE);
        animationWithoutListener(addNewCardWrapper, animHide);
        addNewCardWrapper.setVisibility(View.GONE);
    }

    @Override
    public void showCardNumberLengthError() {
        cardNumberTil.setErrorEnabled(true);
        cardNumberTil.setError(getString(R.string.error_message_wrong_number_card));
    }

    @Override
    public void showCardCodeLengthError() {
        cardCodeTil.setErrorEnabled(true);
        cardCodeTil.setError(getString(R.string.error_message_wrong_card_code));
    }

    @Override
    public void clickAddCard() {
        getPresenter().verifyAttachedCard(verificationCodeEditText.getText().toString());
    }
}
