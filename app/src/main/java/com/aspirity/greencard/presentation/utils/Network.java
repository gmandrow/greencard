package com.aspirity.greencard.presentation.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Network {

    private Network() {
    }

    public static boolean isConnectedToNetwork(Context context) {
        NetworkInfo info = ((ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info == null || !info.isConnectedOrConnecting()) {
            return false;
        } else if (info.isRoaming()) {
            return true;
        }
        return true;
    }
}
