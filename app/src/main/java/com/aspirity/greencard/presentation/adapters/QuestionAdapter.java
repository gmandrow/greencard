package com.aspirity.greencard.presentation.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Faq;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder> {

    private final List<Faq> questions;
    private final QuestionListener listener;

    public QuestionAdapter(List<Faq> questions, QuestionListener listener) {
        this.questions = questions;
        this.listener = listener;
    }

    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new QuestionViewHolder(LayoutInflater
                .from(parent.getContext()).inflate(R.layout.item_question, parent, false));
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {
        holder.bind(questions.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    public void addData(List<Faq> faqList) {
        questions.clear();
        questions.addAll(faqList);
        notifyDataSetChanged();
    }

    public interface QuestionListener {
        void onClick(Faq question);
    }

    public static class QuestionViewHolder extends RecyclerView.ViewHolder {
        TextView questionTitleTextView;

        public QuestionViewHolder(View itemView) {
            super(itemView);
            questionTitleTextView = itemView.findViewById(R.id.text_question);
        }

        void bind(Faq faq, QuestionListener listener) {
            questionTitleTextView.setText(faq.getQuestion());
            itemView.setOnClickListener(view -> listener.onClick(faq));
        }
    }
}
