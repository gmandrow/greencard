package com.aspirity.greencard.presentation.fragments;

import android.animation.Animator;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.usecase.CardsUseCase;
import com.aspirity.greencard.presentation.activities.MainActivity;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.OperationsWithCardContract;
import com.aspirity.greencard.presentation.presenter.OperationsWithCardPresenter;
import com.aspirity.greencard.presentation.utils.AsteriskPasswordTransformationMethod;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.FragmentUtil;
import com.aspirity.greencard.presentation.utils.ViewUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.google.android.material.textfield.TextInputLayout;
import com.jakewharton.rxbinding2.support.v4.widget.RxNestedScrollView;
import com.jakewharton.rxbinding2.view.RxView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Map;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import timber.log.Timber;


public class OperationsWithCardFragment
        extends BaseFragment<OperationsWithCardContract.View, OperationsWithCardContract.Presenter>
        implements OperationsWithCardContract.View {

    private NestedScrollView operationsWithCardNestedScroll;
    private EditText passwordBlockCardEditText;
    private TextInputLayout passwordBlockCardTil;
    private Dialog errorDialog;
    private TextView titleErrorTextView;
    private TextView messageTextView;
    private TextView currentCardNumberTextView;
    private TextView actionBlockCardTextView;
    private LottieAnimationView cardAnimationView;
    private Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {
            cardAnimationView.setVisibility(View.VISIBLE);
            Handler h = new Handler();
            h.postDelayed(() -> animation(operationsWithCardNestedScroll, animShow), 500);
        }

        @Override
        public void onAnimationEnd(Animator animation) {

        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };

    public static OperationsWithCardFragment newInstance(/*some args*/) {
        return new OperationsWithCardFragment();
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_operations_with_card, container, false);
        CardsUseCase cardsUseCase = new CardsUseCase(
                getActivity().getApplicationContext(),
                new AppSchedulerProvider(),
                RepositoryProvider.getCardsRepository());
        onCreatePresenter(new OperationsWithCardPresenter(cardsUseCase), this);
        initViews(view);
        initErrorDialog();
        getPresenter().getCards();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (errorDialog == null) {
            initErrorDialog();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setBackButtonColor(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (errorDialog != null) {
            errorDialog.dismiss();
            errorDialog = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cardAnimationView.removeAnimatorListener(animatorListener);
    }

    private void initViews(View view) {
        cardAnimationView = view.findViewById(R.id.image_card);
        View titleOperationsWithCard = view.findViewById(R.id.text_title_operations_with_card);
        operationsWithCardNestedScroll = view.findViewById(R.id.nested_scroll_operations_with_card);
        getCompositeDisposable().add(RxNestedScrollView.scrollChangeEvents(operationsWithCardNestedScroll)
                .subscribe(viewScrollChangeEvent -> {
                    ViewUtil.setTranslationY(-viewScrollChangeEvent.scrollY(), titleOperationsWithCard);
                    ViewUtil.setTranslationX(viewScrollChangeEvent.scrollY(), cardAnimationView);
                }, throwable -> Timber.w(throwable, throwable.getMessage())));

        currentCardNumberTextView = view.findViewById(R.id.text_number_of_current_card);

        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/proxima_nova_regular.otf");
        passwordBlockCardTil = view.findViewById(R.id.til_password_block_card);
        passwordBlockCardEditText = view.findViewById(R.id.edit_input_password_block_card);
        passwordBlockCardEditText.setTypeface(type);
        passwordBlockCardEditText.setTransformationMethod(new AsteriskPasswordTransformationMethod());

//        EditText newCardNumberEditText =
//                view.findViewById(R.id.edit_input_new_card_number);
//        newCardNumberEditText.setTypeface(type);
//
//        EditText verificationCodeEditText =
//                view.findViewById(R.id.edit_input_verification_code);
//        verificationCodeEditText.setTypeface(type);

//        EditText passwordReplaceCardEditText =
//                view.findViewById(R.id.edit_input_password_replace_card);
//        passwordReplaceCardEditText.setTypeface(type);
//        passwordReplaceCardEditText.setTransformationMethod(new AsteriskPasswordTransformationMethod());

        actionBlockCardTextView = view.findViewById(R.id.text_action_block_card);
        actionBlockCardTextView.setOnClickListener(view12 -> {
            getPresenter().checkData(
                    passwordBlockCardEditText.getText().toString(),
                    currentCardNumberTextView.getText().toString());
        });
        passwordBlockCardEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                actionBlockCardTextView.performClick();
            }
            return false;
        });

//        ImageView questionBlockCardImageView = view.findViewById(R.id.image_question_block_card);
//        questionBlockCardImageView.setOnClickListener(view1 -> {
//            Dialog dialog = createDialog(R.layout.dialog_question);
//            dialog.show();
//        });
//        ImageView questionReplaceCardImageView = view.findViewById(R.id.image_question_replace_card);
//        questionReplaceCardImageView.setOnClickListener(view12 -> {
//            Dialog dialog = createDialog(R.layout.dialog_question);
//            dialog.show();
//        });

        View goToManagingFamilyAccountWrapper =
                view.findViewById(R.id.relative_wrapper_managing_family_account);
        goToManagingFamilyAccountWrapper.setOnClickListener(view13 -> {
            FamilyAccountFragment familyAccountFragment = FamilyAccountFragment.newInstance();
            FragmentUtil.setTransitionSlideForTwoFragment(this, familyAccountFragment);
            FragmentUtil.addBackStackWithAddFragment(
                    getActivity().getSupportFragmentManager(),
                    familyAccountFragment);
        });
    }

    private void initErrorDialog() {
        errorDialog = new Dialog(getActivity());
        errorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        errorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        errorDialog.setContentView(R.layout.dialog_error);
        errorDialog.setCanceledOnTouchOutside(false);
        titleErrorTextView = errorDialog.findViewById(R.id.text_name_error);
        messageTextView = errorDialog.findViewById(R.id.text_message_error);
        TextView closeActionTextView = errorDialog.findViewById(R.id.text_close_dialog);
        closeActionTextView.setOnClickListener(view -> errorDialog.dismiss());
        TextView repeatActionTextView = errorDialog.findViewById(R.id.text_action_repeat);
        repeatActionTextView.setOnClickListener(view -> {
            errorDialog.dismiss();
            switch (getPresenter().getLastRequest()) {
                case BLOCK_CARD:
                    getPresenter().checkData(
                            passwordBlockCardEditText.getText().toString(),
                            currentCardNumberTextView.getText().toString());
                    break;
                case GET_CARDS:
                    getPresenter().getCards();
                    break;
            }
        });
    }

    public Dialog createDialog(@LayoutRes int idLayoutDialog) {
        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(idLayoutDialog);
        View actionClose = dialog.findViewById(R.id.text_action_close);
        getCompositeDisposable().add(RxView.clicks(actionClose)
                .subscribe(o -> dialog.dismiss(),
                        throwable -> Timber.w(throwable, throwable.getMessage())));
        return dialog;
    }

    @Override
    public void showPasswordError() {
        passwordBlockCardTil.setErrorEnabled(true);
        passwordBlockCardTil.setError(getString(R.string.error_message_empty_field));
    }

    @Override
    public void hideErrors() {
        passwordBlockCardTil.setErrorEnabled(false);
    }

    @Override
    public void showFieldErrors(Map<String, String> fieldErrors) {
        for (Map.Entry<String, String> entry : fieldErrors.entrySet()) {
            String tag = entry.getKey();
            TextInputLayout layout = getView().findViewWithTag(tag);
            if (layout != null) {
                layout.setErrorEnabled(true);
                layout.setError(entry.getValue());
            }
        }
    }

    @Override
    public void showError(String message, RequestError requestError) {
        switch (requestError) {
            case TIMEOUT:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_timeout);
                errorDialog.show();
                break;
            case CUSTOM:
                titleErrorTextView.setText(R.string.error_title);
                messageTextView.setText(message);
                errorDialog.show();
                break;
            case NETWORK:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_not_connection_network);
                errorDialog.show();
                break;
        }
    }

    @Override
    public void showMainCard(String mainCardNumber) {
        currentCardNumberTextView.setText(mainCardNumber);
        cardAnimationView.addAnimatorListener(animatorListener);
        cardAnimationView.playAnimation();
    }

    @Override
    public void updateOperationUnlock() {
        actionBlockCardTextView.setText(getString(R.string.fragment_operations_with_card_action_unlock));
        passwordBlockCardEditText.setText(Constants.EMPTY_ANSWER);
    }

    @Override
    public void updateOperationLock() {
        actionBlockCardTextView.setText(getString(R.string.fragment_operations_with_card_action_lock));
        passwordBlockCardEditText.setText(Constants.EMPTY_ANSWER);
    }

    @Override
    public void successOperationLock() {
        Toast.makeText(
                getActivity(),
                R.string.fragment_operations_with_card_success_lock,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void successOperationUnlock() {
        Toast.makeText(
                getActivity(),
                R.string.fragment_operations_with_card_success_unlock,
                Toast.LENGTH_SHORT).show();
    }
}
