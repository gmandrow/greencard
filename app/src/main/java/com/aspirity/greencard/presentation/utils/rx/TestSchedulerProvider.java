package com.aspirity.greencard.presentation.utils.rx;


import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class TestSchedulerProvider implements SchedulerProvider {

    public Scheduler ui() {
        return Schedulers.trampoline();
    }

    public Scheduler computation() {
        return Schedulers.trampoline();
    }

    public Scheduler trampoline() {
        return Schedulers.trampoline();
    }

    public Scheduler newThread() {
        return Schedulers.trampoline();
    }

    public Scheduler io() {
        return Schedulers.trampoline();
    }
}
