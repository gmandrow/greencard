package com.aspirity.greencard.presentation.model;

import java.util.List;

public class FilterWrapper {

    protected DateInterval dateInterval = new DateInterval();
    protected List<SelectableVendor> vendors;

    public FilterWrapper() {
    }

    public FilterWrapper(DateInterval dateInterval, List<SelectableVendor> vendors) {
        this.dateInterval = dateInterval;
        this.vendors = vendors;
    }

    public DateInterval getDateInterval() {
        return dateInterval;
    }

    public void setDateInterval(DateInterval dateInterval) {
        this.dateInterval = dateInterval;
    }

    public List<SelectableVendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<SelectableVendor> vendors) {
        this.vendors = vendors;
    }
}
