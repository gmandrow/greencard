package com.aspirity.greencard.presentation.model;

import java.util.Map;

public class ErrorWrapper {

    private String generalError;
    private Map<String, String> fieldErrors;

    public ErrorWrapper() {
    }

    public ErrorWrapper(String generalError, Map<String, String> fieldErrors) {
        this.generalError = generalError;
        this.fieldErrors = fieldErrors;
    }

    public String getGeneralError() {
        return generalError;
    }

    public void setGeneralError(String generalError) {
        this.generalError = generalError;
    }

    public Map<String, String> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(Map<String, String> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }
}
