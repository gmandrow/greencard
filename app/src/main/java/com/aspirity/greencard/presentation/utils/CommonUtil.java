package com.aspirity.greencard.presentation.utils;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;

import java.util.List;

import timber.log.Timber;

public class CommonUtil {

    private CommonUtil() {
    }

    public static String getVersion(Context context) {

        String version = "1.0";
        try {
            version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            Timber.e(e, "Problem in commonUtil: %s", e.getMessage());
        }

        return version;
    }

    public static boolean checkStringOnNullOrEmpty(String value) {
        return value == null || value.isEmpty();
    }

    public static <T> boolean checkListOnNullOrEmpty(List<T> value) {
        return value == null || value.isEmpty();
    }

    public static Spanned getTextFromHtml(String html) {
        if (html == null) {
            return getTextFromHtml("");
        }

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT);
        } else {
            return Html.fromHtml(html);
        }
    }
}
