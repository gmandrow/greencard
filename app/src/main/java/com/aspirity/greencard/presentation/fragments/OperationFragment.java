package com.aspirity.greencard.presentation.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.Operation;
import com.aspirity.greencard.domain.usecase.OperationsUseCase;
import com.aspirity.greencard.presentation.adapters.ProductAdapter;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.OperationContract;
import com.aspirity.greencard.presentation.dialogs.ErrorDialog;
import com.aspirity.greencard.presentation.presenter.OperationPresenter;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.FragmentUtil;
import com.aspirity.greencard.presentation.utils.ViewUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;
import com.google.android.material.appbar.AppBarLayout;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class OperationFragment
        extends BaseFragment<OperationContract.View, OperationContract.Presenter>
        implements OperationContract.View {

    private ProductAdapter adapter;
    private RecyclerView productsRecycler;

    private View wrapperMainInfo;
    private ImageView partnerLogo;
    private TextView addressOfBuyingTextView;
    private TextView dateTimeOfBuyingTextView;
    private TextView typeOfOperationTextView;
    private TextView numberOfOperationTextView;

    private AppBarLayout appBarLayout;
    private AppBarLayout.OnOffsetChangedListener onOffsetChangedListener;
    private View titleView;

    public static OperationFragment newInstance(String uniqueId) {
        Log.i("mlg", "uniqueId: " + uniqueId);
        Bundle args = new Bundle();
        args.putString(Constants.ARG_OPERATION_UNIQUE_ID, uniqueId);
        OperationFragment operationFragment = new OperationFragment();
        operationFragment.setArguments(args);
        return operationFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        SchedulerProvider schedulerProvider = new AppSchedulerProvider();
        OperationsUseCase operationsUseCase = new OperationsUseCase(
                context.getApplicationContext(),
                RepositoryProvider.getOperationsRepository(schedulerProvider),
                schedulerProvider);
        onCreatePresenter(new OperationPresenter(
                getGreenCardApp().getPreferenceDataManager(),
                operationsUseCase), this);
        adapter = new ProductAdapter(new LinkedList<>(), getActivity().getResources());
        getPresenter().getOperation(getArguments().getString(Constants.ARG_OPERATION_UNIQUE_ID));
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_operation, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //productsRecycler.setAdapter(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    private void initViews(View view) {
        titleView = view.findViewById(R.id.text_title_operation);
        wrapperMainInfo = view.findViewById(R.id.relative_wrapper_main_info);
        appBarLayout = view.findViewById(R.id.appbar_operation);

        initAppBarListener();

        partnerLogo = view.findViewById(R.id.image_firm_logo);

        addressOfBuyingTextView = view.findViewById(R.id.text_address_of_buying);
        dateTimeOfBuyingTextView = view.findViewById(R.id.text_time_of_buying);
        typeOfOperationTextView = view.findViewById(R.id.text_type_of_operation);
        numberOfOperationTextView = view.findViewById(R.id.text_number_of_operation);

        productsRecycler = view.findViewById(R.id.recycler_products);
        productsRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        productsRecycler.setAdapter(adapter);
    }

    private void initAppBarListener() {
        onOffsetChangedListener = (appBarLayout, verticalOffset) -> {
            ViewUtil.setTranslationX(-verticalOffset, wrapperMainInfo);
            ViewUtil.setTranslationY(verticalOffset, 1, titleView);
        };
        appBarLayout.addOnOffsetChangedListener(onOffsetChangedListener);
    }

    @Override
    public void showError(String message, RequestError requestError) {
        ErrorDialog errorDialog = new ErrorDialog(getContext());
        getCompositeDisposable().addAll(
                errorDialog.closeAction().subscribe(),
                errorDialog.retryAction().subscribe(obj ->
                        getPresenter().retryLastRequest())
        );
        errorDialog.setErrorDescription(requestError, message);
        errorDialog.show();
    }

    @Override
    public void showOperationData(Operation operation) {
        String dateTime = getGreenCardApp().getDateUtils()
                .getDateTimeString(new Date(Long.valueOf(operation.getDateOf())
                        * Constants.COEFFICIENT_TIMESTAMP_TO_MILISECONDS));
        if (CommonUtil.checkStringOnNullOrEmpty(getPartnerLogoSrc(operation))) {
            Picasso.with(getActivity().getApplicationContext())
                    .load(R.drawable.ic_g_logo_light_green)
                    .fit()
                    .centerInside()
                    .into(partnerLogo);
        } else {
            Picasso.with(getActivity().getApplicationContext())
                    .load(getPartnerLogoSrc(operation))
                    .fit()
                    .centerInside()
                    .into(partnerLogo);
            partnerLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PartnerFragment fragment = PartnerFragment.newInstance(operation.getPartnerId(),
                            "", operation.getPartnerLogoSrc());
                    FragmentUtil.setTransitionSlideForTwoFragment(OperationFragment.this, fragment);
                    FragmentUtil.addBackStackWithAddFragment(
                            getActivity().getSupportFragmentManager(),
                            fragment);
                }
            });
        }
        addressOfBuyingTextView.setText(operation.getAddress());
        dateTimeOfBuyingTextView.setText(dateTime);
        typeOfOperationTextView.setText(operation.getType());
        numberOfOperationTextView.setText(operation.getUniqueId());
    }

    private String getPartnerLogoSrc(Operation operation) {
        String path = operation.getPartnerLogoBlackBgSrc();
        if (CommonUtil.checkStringOnNullOrEmpty(path)) {
            path = operation.getPartnerLogoSrc();
        }
        return path;
    }

    @Override
    public void showOperationProducts(List<ListItem> products) {
        if (adapter != null) {
            adapter.addData(products);
            animationShow(wrapperMainInfo, productsRecycler);
        }
    }
}
