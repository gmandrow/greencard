package com.aspirity.greencard.presentation.model;


public enum OperationSortParams {
    DATE_ASC(0, "date_of"),
    DATE_DESC(1, "-date_of"),
    BALANCE_ASC(2, "bonuses"),
    BALANCE_DESC(3, "-bonuses"),
    SUM_ASC(4, "total_price"),
    SUM_DESC(5, "-total_price");

    private int intValue;
    private String identifier;

    OperationSortParams(int value, String identifier) {
        this.intValue = value;
        this.identifier = identifier;
    }

    public static OperationSortParams fromInt(int i) {
        for (OperationSortParams sort : OperationSortParams.values()) {
            if (sort.getIntValue() == i) {
                return sort;
            }
        }
        return null;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public String getIdentifier() {
        return identifier;
    }
}
