package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.FaqUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.FaqContract;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.net.SocketTimeoutException;

import retrofit2.HttpException;

public class FaqPresenter extends BasePresenter<FaqContract.View>
        implements FaqContract.Presenter {

    private PreferenceDataManager preferenceDataManager;
    private FaqUseCase faqUseCase;

    public FaqPresenter(
            PreferenceDataManager preferenceDataManager,
            FaqUseCase faqUseCase) {
        this.preferenceDataManager = preferenceDataManager;
        this.faqUseCase = faqUseCase;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                getView().showError(
                        ErrorParse.parseError(httpException.response()),
                        RequestError.CUSTOM);
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        }
    }

    @Override
    public void getFaq() {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(
                faqUseCase
                        .getFaq()
                        .onErrorResumeNext(error -> {
                            handleError(error);
                            return faqUseCase.getFaqFallback();
                        })
                        .doOnSubscribe(disposable ->
                                getView().showLoading())
                        .subscribe(faqList -> {
                            getView().showFaq(faqList);
                            getView().hideLoading();
                        }, this::handleError));
    }
}
