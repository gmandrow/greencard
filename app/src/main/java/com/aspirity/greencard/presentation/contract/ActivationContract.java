package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.Oferta;
import com.aspirity.greencard.presentation.activities.ActivationActivity;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.util.Map;

import androidx.annotation.StringRes;

public interface ActivationContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showPasswordError(@StringRes int messageId);

        void showConfirmPasswordError();

        void showFieldErrors(Map<String, String> fieldErrors);

        void hideFieldError();

        void acceptedOferta();

        void activationSuccess();

        void allowActivation();

        void passwordStepActivation();

        void showOferta(Oferta oferta);

        void goToAuthScreen();
    }

    interface Presenter extends MvpPresenter<View> {

        void activation(String password);

        void loadOferta();

        void checkPasswords(String password, String confirmPassword);

        void checkStep(ActivationActivity.ActivationStep step);

        RequestType getLastRequest();

        void onBackPressed();
    }
}
