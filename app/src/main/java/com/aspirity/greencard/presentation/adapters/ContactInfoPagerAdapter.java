package com.aspirity.greencard.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Vendor;
import com.aspirity.greencard.presentation.utils.CommonUtil;

import java.util.List;
import java.util.Set;

import androidx.viewpager.widget.PagerAdapter;

/**
 * This adapter needed for viewPager on partners map.
 * Give adapter list of Store.
 */

public class ContactInfoPagerAdapter extends PagerAdapter {

    private final List<Vendor> vendors;
    private Context context;
    private LayoutInflater layoutInflater;

    public ContactInfoPagerAdapter(Context context, List<Vendor> vendors) {
        this.context = context;
        layoutInflater = LayoutInflater.from(this.context);
        this.vendors = vendors;
    }

    @Override
    public int getCount() {
        return vendors.size();
    }

    // Returns true if a particular object (page) is from a particular page
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    // This method should create the page for the given position passed to it as an argument.
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.item_contact_info, container, false);

        TextView titleTextView = itemView.findViewById(R.id.text_store_title_map);
        titleTextView.setText(vendors.get(position).getName());

        TextView addressTitleTextView = itemView.findViewById(R.id.text_title_store_address_map);
        TextView addressTextView = itemView.findViewById(R.id.text_store_address_map);
        String address = vendors.get(position).getAddress();
        if (CommonUtil.checkStringOnNullOrEmpty(address)) {
            addressTitleTextView.setVisibility(View.GONE);
            addressTextView.setVisibility(View.GONE);
        } else {
            addressTitleTextView.setVisibility(View.VISIBLE);
            addressTextView.setVisibility(View.VISIBLE);
            addressTextView.setText(address);
        }

        TextView phoneTitleTextView = itemView.findViewById(R.id.text_title_store_phone_map);
        TextView phoneTextView = itemView.findViewById(R.id.text_store_phone_map);
        String phone = vendors.get(position).getPhone();
        if (CommonUtil.checkStringOnNullOrEmpty(phone)) {
            phoneTextView.setVisibility(View.GONE);
            phoneTitleTextView.setVisibility(View.GONE);
        } else {
            phoneTextView.setVisibility(View.VISIBLE);
            phoneTitleTextView.setVisibility(View.VISIBLE);
            phoneTextView.setText(phone);
        }

        TextView emailTitleTextView = itemView.findViewById(R.id.text_title_store_email_map);
        TextView emailTextView = itemView.findViewById(R.id.text_store_email_map);
        String email = vendors.get(position).getEmail();
        if (CommonUtil.checkStringOnNullOrEmpty(email)) {
            emailTextView.setVisibility(View.GONE);
            emailTitleTextView.setVisibility(View.GONE);
        } else {
            emailTextView.setVisibility(View.VISIBLE);
            emailTitleTextView.setVisibility(View.VISIBLE);
            emailTextView.setText(email);
        }

        container.addView(itemView);
        return itemView;
    }

    public void addItems(Set<Vendor> vendors) {
        this.vendors.clear();
        this.vendors.addAll(vendors);
        notifyDataSetChanged();
    }

    // Removes the page from the container for the given position.
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
