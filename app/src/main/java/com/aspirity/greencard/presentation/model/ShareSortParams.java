package com.aspirity.greencard.presentation.model;

public enum ShareSortParams {
    DATE_ASC(0, "date_start"),
    DATE_DESC(1, "-date_start");

    private int intValue;
    private String identifier;

    ShareSortParams(int value, String identifier) {
        this.intValue = value;
        this.identifier = identifier;
    }

    public static ShareSortParams fromInt(int i) {
        for (ShareSortParams sort : ShareSortParams.values()) {
            if (sort.getIntValue() == i) {
                return sort;
            }
        }
        return null;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public String getIdentifier() {
        return identifier;
    }
}
