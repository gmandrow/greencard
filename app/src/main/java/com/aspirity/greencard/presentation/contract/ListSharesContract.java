package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.model.ShareFilterWrapper;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.paginate.Paginate;

import java.util.List;

public interface ListSharesContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showSharesData(List<Share> shares, Integer countShares);

        void showMoreSharesData(List<Share> listShares);

        void setHasLoadedAllItems();

        void playStartAnimation();
    }

    interface Presenter extends MvpPresenter<ListSharesContract.View> {

        Paginate.Callbacks getCallbacks();

        ShareFilterWrapper getCurrentFilter();

        void getShares(boolean restart);

        void getSharesOfPartner(Long partnerId);

        void retryLastRequest();

        void getContent();
    }
}
