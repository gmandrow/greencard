package com.aspirity.greencard.presentation.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.widget.AppCompatTextView;


public class CustomFontTabLayout extends TabLayout {

    private Typeface typeface;

    public CustomFontTabLayout(Context context) {
        super(context);
    }

    public CustomFontTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomFontTabLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private void initTypeface() {
        typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/proxima_nova_semibold.otf");
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        final ViewGroup tabStrip = (ViewGroup) getChildAt(0);
        final int tabCount = tabStrip.getChildCount();
        ViewGroup tabView;
        int tabChildCount;
        View tabViewChild;

        for (int i = 0; i < tabCount; i++) {
            tabView = (ViewGroup) tabStrip.getChildAt(i);
            tabChildCount = tabView.getChildCount();
            if (i == tabCount - 1) {
                tabView.setPadding(
                        getContext().getResources().getDimensionPixelOffset(R.dimen.margin_left_recycler_shares),
                        0, 0, 0);
            }
            for (int j = 0; j < tabChildCount; j++) {
                tabViewChild = tabView.getChildAt(j);
                if (tabViewChild instanceof AppCompatTextView) {
                    if (typeface == null) {
                        initTypeface();
                    }
                    TextView textView = ((TextView) tabViewChild);
                    textView.setTypeface(typeface);
                    textView.setAllCaps(false);
                }
            }
        }
    }
}
