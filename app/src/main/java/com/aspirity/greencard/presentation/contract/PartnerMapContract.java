package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.Vendor;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Target;

import java.util.List;
import java.util.Set;

public interface PartnerMapContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showInfoVendors(Set<Vendor> vendors);

        void showVendorsOnMap(List<LatLng> vendorsLatLng);

        void showSelectedVendor(int indexVendor);

        void changeVisibilityMarker(boolean isVisible, Marker marker);
    }

    interface Presenter extends MvpPresenter<PartnerMapContract.View> {

        void getVendors(Long partnerId);

        void checkPositionOfSelectedMarkerInListOfMarkers(LatLng position, List<Target> targets);

        void retryLastRequest();

        void checkListOfMarkersForSameCoordinates(List<Target> targets, int index);
    }
}
