package com.aspirity.greencard.presentation.utils;


public enum RequestType {
    SETTINGS,
    PASSWORD,
    ACTIVATION,
    REGISTRATION,
    VALIDATE_CARD,
    AUTH,
    GET_USER,
    BLOCK_CARD,
    GET_CARDS,
    SEARCH_USER_BY_CARD,
    ATTACH_CARD,
    VERIFY_ATTACHED_CARD,
    LOAD_OFERTA,
    LOAD_RULES_FAMILY_ACCOUNT
}
