package com.aspirity.greencard.presentation.dialogs;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.aspirity.greencard.GreenCardApplication;
import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.model.DateInterval;
import com.aspirity.greencard.presentation.utils.CurrentDayDecorator;
import com.aspirity.greencard.presentation.utils.DateUtil;
import com.aspirity.greencard.presentation.utils.ExpandCollapseAnimation;
import com.jakewharton.rxbinding2.view.RxView;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import timber.log.Timber;

public class PeriodFilterDialog extends BaseFilterDialog<DateInterval> {

    private NestedScrollView calendarNestedScroll;
    private MaterialCalendarView beginDateCalendarView;
    private MaterialCalendarView endDateCalendarView;
    private View beginDateCalendarContainer;
    private View endDateCalendarContainer;
    private TextView beginDateText;
    private TextView endDateText;

    private CurrentDayDecorator dayDecorator;
    private DateUtil dateUtil;

    private boolean isCollapsedBeginCalendar;
    private boolean isCollapsedEndCalendar;

    public PeriodFilterDialog(@NonNull Context context, DateInterval interval) {
        super(context, R.layout.dialog_sort_period_calendar, interval);
    }

    @Override
    protected void init(Context context) {
        super.init(context);

        dateUtil = ((GreenCardApplication) context.getApplicationContext()).getDateUtils();

        isCollapsedBeginCalendar = false;
        isCollapsedEndCalendar = false;

        calendarNestedScroll = findViewById(R.id.nested_scroll_calendar);
        beginDateCalendarContainer = findViewById(R.id.frame_container_calendar_view_begin);
        endDateCalendarContainer = findViewById(R.id.frame_container_calendar_view_end);
        endDateCalendarView = findViewById(R.id.calendar_view_end_date);
        beginDateCalendarView = findViewById(R.id.calendar_view_begin_date);
        beginDateText = findViewById(R.id.text_begin_date);
        endDateText = findViewById(R.id.text_end_date);

        dayDecorator = new CurrentDayDecorator(context.getApplicationContext());
        endDateCalendarView.addDecorator(dayDecorator);
        beginDateCalendarView.addDecorator(dayDecorator);

        beginDateCalendarView.setOnDateChangedListener((widget, date, selected) -> {
            Date d = date.getDate();
            setBeginDate(d);
            filterValue.setBeginDate(d);
        });
        endDateCalendarView.setOnDateChangedListener((widget, date, selected) -> {
            Date d = date.getDate();
            setEndDate(d);
            filterValue.setEndDate(d);
        });

        beginDateCalendarView.setSelectedDate(filterValue.getBeginDate());
        beginDateCalendarView.setCurrentDate(filterValue.getBeginDate());
        setBeginDate(filterValue.getBeginDate());

        endDateCalendarView.setSelectedDate(filterValue.getEndDate());
        endDateCalendarView.setCurrentDate(filterValue.getEndDate());
        setEndDate(filterValue.getEndDate());

        TextView changeBeginDateTextView = findViewById(R.id.text_begin_date_change);
        disposables.add(RxView.clicks(changeBeginDateTextView)
                .doOnNext(o -> isCollapsedBeginCalendar = !isCollapsedBeginCalendar)
                .subscribe(
                        o -> {
                            if (isCollapsedBeginCalendar) {
                                ExpandCollapseAnimation.expand(beginDateCalendarContainer);
                                changeBeginDateTextView.setText(R.string.action_hide_change_date_dialog_sort_period_calendar);
                                calendarNestedScroll.post(() ->
                                        calendarNestedScroll.smoothScrollTo(
                                                0,
                                                beginDateCalendarContainer.getBaseline()));
                            } else {
                                ExpandCollapseAnimation.collapse(beginDateCalendarContainer);
                                changeBeginDateTextView.setText(R.string.action_change_date_dialog_sort_period_calendar);
                            }
                        },
                        throwable -> Timber.w(throwable, throwable.getMessage())));

        TextView changeEndDateTextView = findViewById(R.id.text_end_date_change);
        disposables.add(RxView.clicks(changeEndDateTextView)
                .doOnNext(o -> isCollapsedEndCalendar = !isCollapsedEndCalendar)
                .subscribe(
                        o -> {
                            if (isCollapsedEndCalendar) {
                                ExpandCollapseAnimation.expand(endDateCalendarContainer);
                                changeEndDateTextView.setText(R.string.action_hide_change_date_dialog_sort_period_calendar);
                                calendarNestedScroll.post(() ->
                                        calendarNestedScroll.smoothScrollTo(
                                                0,
                                                endDateCalendarContainer.getBottom()));
                            } else {
                                ExpandCollapseAnimation.collapse(endDateCalendarContainer);
                                changeEndDateTextView.setText(R.string.action_change_date_dialog_sort_period_calendar);
                            }
                        },
                        throwable -> Timber.w(throwable, throwable.getMessage())));

        setOnDismissListener(dialog -> {
            endDateCalendarView.removeDecorator(dayDecorator);
            beginDateCalendarView.removeDecorator(dayDecorator);
            dayDecorator = null;
        });
    }

    private void setBeginDate(Date date) {
        beginDateText.setText(dateUtil.getFullDateString(date));
        endDateCalendarView.state().edit().setMinimumDate(date).commit();
    }

    private void setEndDate(Date date) {
        endDateText.setText(dateUtil.getFullDateString(date));
        beginDateCalendarView.state().edit().setMaximumDate(date).commit();
    }
}
