package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.Card;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

public interface FamilyAccountContract {

    interface View extends MvpView {

        void hideErrors();

        void showFieldErrors(Map<String, String> fieldErrors);

        void showError(String message, RequestError requestError);

        void showCardNumberError();

        void showCardCodeError();

        void showCards(List<Card> cards);

        void showUserData(String fullName, String phone);

        void showToastSendCode(String s);

        void showSmsCodeError();

        void showSuccess();

        void showCardNumberLengthError();

        void showCardCodeLengthError();

        void clickAddCard();
    }

    interface Presenter extends MvpPresenter<View> {

        void getCards(boolean forceRefresh);

        void checkCard(String cardNumber, String cardCode);

        void searchUserByCard(String cardNumber, String cardCode);

        void getVerificationCode();

        void onClickGetVerificationCode(String cardNumber, String cardCode);

        void verifyAttachedCard(String smsCode);

        void retryLastRequest();

        void onClickVerifyAttachedCard(Observable<Object> clicks);
    }
}
