package com.aspirity.greencard.presentation.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.usecase.AppsUseCase;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BaseActivity;
import com.aspirity.greencard.presentation.contract.AuthContract;
import com.aspirity.greencard.presentation.presenter.AuthPresenter;
import com.aspirity.greencard.presentation.utils.AsteriskPasswordTransformationMethod;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.google.android.material.textfield.TextInputLayout;
import com.jakewharton.rxbinding2.view.RxView;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.res.ResourcesCompat;
import timber.log.Timber;

public class AuthActivity extends BaseActivity<AuthContract.View, AuthContract.Presenter>
        implements AuthContract.View {

    private TextInputLayout phoneTil;
    private TextInputLayout passwordTil;
    private EditText phoneEditText;
    private EditText passwordEditText;
    private Button enterButton;
    private TextView logoGreenCardTextView;
    private Dialog errorDialog;
    private TextView titleErrorTextView;
    private TextView messageTextView;
    private MaskedTextChangedListener phoneMaskedListener;
    private String phone = "";
    private ViewGroup root;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onCreatePresenter(new AuthPresenter(
                getGreenCardApp().getPreferenceDataManager(),
                new UserUseCase(
                        getApplicationContext(),
                        RepositoryProvider.getUserRepository(),
                        new AppSchedulerProvider()),
                new AppsUseCase(
                        getApplicationContext(),
                        RepositoryProvider.getAppsRepository(),
                        new AppSchedulerProvider())), this);
        setContentView(R.layout.activity_auth);
        initViews();
        initErrorDialog();
    }

    private void initViews() {
        root = findViewById(R.id.root);

        logoGreenCardTextView = findViewById(R.id.text_green_card_logo_auth);
        logoGreenCardTextView.setText(getSpannableStringBuilder(), TextView.BufferType.SPANNABLE);
        logoGreenCardTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_bold.otf"));

        View onlineRegistrationButton = findViewById(R.id.linear_wrapper_online_reg);
        getCompositeDisposable().add(RxView.clicks(onlineRegistrationButton)
                .subscribe(o -> {
                    Intent gotoRegistrationIntent = new Intent(AuthActivity.this, RegistrationActivity.class);
                    startActivity(gotoRegistrationIntent);
                    AuthActivity.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    finish();
                }, throwable -> Timber.w(throwable, throwable.getMessage())));

        TextView forgotPasswordTextView = findViewById(R.id.text_forgot_password);
        getCompositeDisposable().add(RxView.clicks(forgotPasswordTextView)
                .subscribe(o -> {
                    //Toast.makeText(AuthActivity.this, "В разработке", Toast.LENGTH_SHORT).show();
                    Intent gotoForgotPasswordIntent = new Intent(AuthActivity.this, ForgotPasswordActivity.class);
                    startActivity(gotoForgotPasswordIntent);
                    AuthActivity.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    finish();
                }));

        phoneTil = findViewById(R.id.til_phone);
        phoneEditText = findViewById(R.id.edit_phone);
        phoneEditText.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_semibold.otf"));

        passwordTil = findViewById(R.id.til_password);
        passwordEditText = findViewById(R.id.edit_password);
        passwordEditText.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_semibold.otf"));

        enterButton = findViewById(R.id.btn_registration);
        getCompositeDisposable().add(RxView.clicks(enterButton)
                .subscribe(
                        o -> getPresenter().checkFieldPhoneAndPassword(
                                phone,
                                passwordEditText.getText().toString()),
                        throwable -> Timber.w(
                                throwable,
                                "Error when click auth button: %s",
                                throwable.getMessage())));

        passwordEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                enterButton.performClick();
            }
            return false;
        });
    }

    @NonNull
    private SpannableStringBuilder getSpannableStringBuilder() {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString neededText = new SpannableString(
                getResources().getString(R.string.splash_green_card).toUpperCase());
        neededText.setSpan(new ForegroundColorSpan(ResourcesCompat
                .getColor(getResources(), R.color.colorWhite, null)), 5, neededText.length(), 0);
        builder.append(neededText);
        return builder;
    }

    private void initErrorDialog() {
        errorDialog = new Dialog(this);
        errorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        errorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        errorDialog.setContentView(R.layout.dialog_error);
        errorDialog.setCanceledOnTouchOutside(false);
        titleErrorTextView = errorDialog.findViewById(R.id.text_name_error);
        messageTextView = errorDialog.findViewById(R.id.text_message_error);
        TextView closeActionTextView = errorDialog.findViewById(R.id.text_close_dialog);
        closeActionTextView.setOnClickListener(view -> errorDialog.dismiss());
        TextView repeatActionTextView = errorDialog.findViewById(R.id.text_action_repeat);
        repeatActionTextView.setOnClickListener(view -> {
            errorDialog.dismiss();
            switch (getPresenter().getLastRequest()) {
                case AUTH:
                    getPresenter().checkFieldPhoneAndPassword(
                            phone,
                            passwordEditText.getText().toString());
                    break;
                case GET_USER:
                    getPresenter().getUser();
                    break;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        phoneMaskedListener = new MaskedTextChangedListener(
                "+{7} ([000]) [000]-[00]-[00]",
                true,
                phoneEditText,
                null,
                (maskFilled, extractedValue) -> phone = extractedValue);
        phoneEditText.addTextChangedListener(phoneMaskedListener);
        phoneEditText.setOnFocusChangeListener(phoneMaskedListener);

        passwordEditText.setTransformationMethod(new AsteriskPasswordTransformationMethod());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
//        showError(event.message, event.requestError);
    }

    @Override
    protected void onStop() {
        super.onStop();
        phoneEditText.removeTextChangedListener(phoneMaskedListener);
        passwordEditText.setTransformationMethod(null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (errorDialog != null) {
            if (errorDialog.isShowing()) errorDialog.dismiss();
            errorDialog = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        getCurrentFocus().clearFocus();
        finishAffinity();
    }

    @Override
    public void showError(String message, RequestError requestError) {
        switch (requestError) {
            case TIMEOUT:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_timeout);
                errorDialog.show();
                break;
            case CUSTOM:
                titleErrorTextView.setText(R.string.error_title_auth);
                messageTextView.setText(message);
                errorDialog.show();
                break;
            case NETWORK:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_not_connection_network);
                errorDialog.show();
                break;
        }
    }

    @Override
    public void authSuccess() {
        Intent gotoMainScreenIntent = new Intent(AuthActivity.this, MainActivity.class);
        startActivity(gotoMainScreenIntent);
        finish();
    }

    @Override
    public void goToActivation() {
        Intent gotoMainScreenIntent = new Intent(AuthActivity.this, ActivationActivity.class);
        startActivity(gotoMainScreenIntent);
        finish();
    }

    @Override
    public void showPhoneError(@StringRes int messageId) {
        phoneTil.setErrorEnabled(true);
        phoneTil.setError(getString(messageId));
    }

    @Override
    public void showFieldErrors(Map<String, String> fieldErrors) {
        for (Map.Entry<String, String> entry : fieldErrors.entrySet()) {
            String id = entry.getKey();
            TextInputLayout layout = root.findViewWithTag(id);
            if (layout != null) {
                layout.setErrorEnabled(true);
                layout.setError(entry.getValue());
            }
        }
    }

    @Override
    public void showPasswordError() {
        passwordTil.setErrorEnabled(true);
        passwordTil.setError(getString(R.string.error_message_empty_field));
    }

    @Override
    public void hideFieldError() {
        if (phoneTil.isErrorEnabled()) phoneTil.setErrorEnabled(false);
        if (passwordTil.isErrorEnabled()) passwordTil.setErrorEnabled(false);
    }

    @Override
    public void allowAuth() {
        getPresenter().authorization(
                phone,
                passwordEditText.getText().toString());
    }
}
