package com.aspirity.greencard.presentation.contract;

import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.paginate.Paginate;

import java.util.List;

public interface SharesArchiveContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showSharesData(List<Share> shares, Integer countShares);

        void showMoreSharesData(List<Share> listShares);

        void setHasLoadedAllItems();

    }

    interface Presenter extends MvpPresenter<View> {

        Paginate.Callbacks getCallbacks();

        void retryLastRequest(boolean forceRefresh);

        void getContent(boolean forceRefresh);

        void getMoreShares();

    }

}
