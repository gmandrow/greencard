package com.aspirity.greencard.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspirity.greencard.GreenCardApplication;
import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Card;
import com.aspirity.greencard.presentation.utils.TextFormatUtil;

import java.util.List;

import androidx.core.content.res.ResourcesCompat;
import androidx.viewpager.widget.PagerAdapter;

/**
 * This adapter needed for viewPager on family account screen.
 * Give adapter list of Cards.
 */

public class CardPagerAdapter extends PagerAdapter {

    private final List<Card> cards;
    private Context context;
    private LayoutInflater layoutInflater;

    public CardPagerAdapter(Context context, List<Card> cards) {
        this.context = context;
        layoutInflater = LayoutInflater.from(this.context);
        this.cards = cards;
    }

    @Override
    public int getCount() {
        return cards.size();
    }

    // Returns true if a particular object (page) is from a particular page
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    // This method should create the page for the given position passed to it as an argument.
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.item_card, container, false);

        Card card = cards.get(position);
        TextView nameTextView = itemView.findViewById(R.id.text_name_owner_card);
        nameTextView.setText(card.getOwnerName());
        TextView phoneTextView = itemView.findViewById(R.id.text_phone_number_owner_card);
        phoneTextView.setText(TextFormatUtil.formatPhone(card.getOwnerPhone()));
        TextView numberTextView = itemView.findViewById(R.id.text_card_number);
        numberTextView.setText(((GreenCardApplication) context.getApplicationContext())
                .getDecimalFormatter()
                .getNumberByFourNumeralAsString(Long.parseLong(card.getNumber())));
        ImageView logoGImageView = itemView.findViewById(R.id.image_logo_g);
        TextView statusOwnerCardTextView = itemView.findViewById(R.id.text_status_owner_card);
        statusOwnerCardTextView.setText(card.getOwnerStatus().toLowerCase());
        View wrapperCard = itemView.findViewById(R.id.relative_wrapper_family_card);
        if (card.isMain()) {
            wrapperCard.setBackground(ResourcesCompat.getDrawable(
                    context.getResources(),
                    R.drawable.card_background_green,
                    null));
            nameTextView.setTextColor(
                    ResourcesCompat.getColor(context.getResources(), R.color.colorWhite, null));
            phoneTextView.setTextColor(
                    ResourcesCompat.getColor(context.getResources(), R.color.colorWhite, null));
            numberTextView.setTextColor(
                    ResourcesCompat.getColor(context.getResources(), R.color.colorWhite, null));
            statusOwnerCardTextView.setTextColor(
                    ResourcesCompat.getColor(context.getResources(), R.color.colorWhite, null));
            logoGImageView.setImageResource(R.drawable.ic_g_logo_white);
        } else {
            wrapperCard.setBackground(ResourcesCompat.getDrawable(
                    context.getResources(),
                    R.drawable.card_background,
                    null));
            nameTextView.setTextColor(
                    ResourcesCompat.getColor(context.getResources(), R.color.colorDimGray, null));
            phoneTextView.setTextColor(
                    ResourcesCompat.getColor(context.getResources(), R.color.colorDimGray, null));
            numberTextView.setTextColor(
                    ResourcesCompat.getColor(context.getResources(), R.color.colorDimGray, null));
            statusOwnerCardTextView.setTextColor(
                    ResourcesCompat.getColor(context.getResources(), R.color.colorDimGray, null));
            logoGImageView.setImageResource(R.drawable.ic_g_logo_green);
        }

        container.addView(itemView);
        return itemView;
    }

    // Removes the page from the container for the given position.
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void addItem(Card card) {
        cards.add(card);
        notifyDataSetChanged();
    }

    public void addItems(List<Card> cards) {
        this.cards.clear();

        for (Card card : cards)
            this.cards.add(0, card);

        notifyDataSetChanged();
    }
}
