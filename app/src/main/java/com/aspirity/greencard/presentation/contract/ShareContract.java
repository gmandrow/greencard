package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.error.RequestError;

public interface ShareContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showContentShare(Share share);
    }

    interface Presenter extends MvpPresenter<ShareContract.View> {

        void getShareContent(Long shareId, boolean isArchived);
    }
}
