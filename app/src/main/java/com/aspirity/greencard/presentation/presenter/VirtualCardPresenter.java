package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.model.VirtualCard;
import com.aspirity.greencard.domain.usecase.CardsUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.VirtualCardContract;
import com.aspirity.greencard.presentation.model.ErrorWrapper;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.net.SocketTimeoutException;

import retrofit2.HttpException;

public class VirtualCardPresenter extends BasePresenter<VirtualCardContract.View>
        implements VirtualCardContract.Presenter {

    private PreferenceDataManager preferenceDataManager;
    private CardsUseCase cardsUseCase;
    private RequestType lastRequest;

    public VirtualCardPresenter(
            PreferenceDataManager preferenceDataManager,
            CardsUseCase cardsUseCase) {
        this.preferenceDataManager = preferenceDataManager;
        this.cardsUseCase = cardsUseCase;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                ErrorWrapper wrapper = ErrorParse.parseFormErrors(httpException.response());
                String generalError = wrapper.getGeneralError();
                getView().showError(generalError, RequestError.CUSTOM);
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        }
    }

    @Override
    public void getVirtualCard() {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(cardsUseCase.getVirtualCard()
                .doOnError(throwable -> {
                    String number = preferenceDataManager.getVirtualCardNumber();
                    String hash = preferenceDataManager.getVirtualCardHash();
                    getView().showVirtualCard(new VirtualCard(number, hash));
                })
                .subscribe(virtual_card -> {
                    preferenceDataManager.setVirtualCardNumber(virtual_card.getNumber());
                    preferenceDataManager.setVirtualCardHash(virtual_card.getHash());
                    getView().showVirtualCard(virtual_card);
                }, this::handleError));
    }

    @Override
    public RequestType getLastRequest() {
        return lastRequest;
    }
}
