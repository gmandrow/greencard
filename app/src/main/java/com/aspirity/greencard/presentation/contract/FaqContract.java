package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.Faq;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.util.List;

public interface FaqContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showFaq(List<Faq> faqList);
    }

    interface Presenter extends MvpPresenter<FaqContract.View> {

        void getFaq();
    }
}
