package com.aspirity.greencard.presentation.utils.rx;


import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AppSchedulerProvider implements SchedulerProvider {

    public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }

    public Scheduler computation() {
        return Schedulers.computation();
    }

    public Scheduler trampoline() {
        return Schedulers.trampoline();
    }

    public Scheduler newThread() {
        return Schedulers.newThread();
    }

    public Scheduler io() {
        return Schedulers.io();
    }
}
