package com.aspirity.greencard.presentation.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.Oferta;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BaseActivity;
import com.aspirity.greencard.presentation.contract.ActivationContract;
import com.aspirity.greencard.presentation.presenter.ActivationPresenter;
import com.aspirity.greencard.presentation.utils.AsteriskPasswordTransformationMethod;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.google.android.material.textfield.TextInputLayout;
import com.jakewharton.rxbinding2.view.RxView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.res.ResourcesCompat;
import timber.log.Timber;

public class ActivationActivity
        extends BaseActivity<ActivationContract.View, ActivationContract.Presenter>
        implements ActivationContract.View {

    private ActivationStep step;
    private TextInputLayout passwordTil;
    private TextInputLayout confirmPasswordTil;
    private EditText passwordEditText;
    private EditText confirmPasswordEditText;
    private Button activationButton;
    private Dialog errorDialog;
    private TextView titleErrorTextView;
    private TextView messageTextView;
    private View wrapperPasswords;
    private View wrapperOferta;
    private HtmlTextView contentOfertaTextView;
    private TextView subHeaderOfertaTextView;
    private TextView titleOfertaTextView;
    private ViewGroup root;
    private TextView logoGreenCardTextView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onCreatePresenter(new ActivationPresenter(
                getGreenCardApp().getPreferenceDataManager(),
                new UserUseCase(
                        getApplicationContext(),
                        RepositoryProvider.getUserRepository(),
                        new AppSchedulerProvider())), this);
        setContentView(R.layout.activity_activation);
        initViews();
        initErrorDialog();
        getPresenter().loadOferta();
    }

    private void initViews() {
        root = findViewById(R.id.root);

        logoGreenCardTextView = findViewById(R.id.text_green_card_logo_auth);
        logoGreenCardTextView.setText(getSpannableStringBuilder(), TextView.BufferType.SPANNABLE);
        logoGreenCardTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_bold.otf"));

        wrapperPasswords = findViewById(R.id.linear_wrapper_activation);
        passwordTil = findViewById(R.id.til_password);
        passwordEditText = findViewById(R.id.edit_password);
        passwordEditText.setTypeface(
                Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_semibold.otf"));

        confirmPasswordTil = findViewById(R.id.til_confirm_password);
        confirmPasswordEditText = findViewById(R.id.edit_confirm_password);
        confirmPasswordEditText.setTypeface(
                Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_semibold.otf"));

        wrapperOferta = findViewById(R.id.linear_wrapper_oferta);
        titleOfertaTextView = findViewById(R.id.text_title_oferta);
        subHeaderOfertaTextView = findViewById(R.id.text_sub_header_oferta);
        contentOfertaTextView = findViewById(R.id.text_content_oferta);

        activationButton = findViewById(R.id.btn_activation);
        getCompositeDisposable().add(RxView.clicks(activationButton)
                .subscribe(
                        o -> getPresenter().checkStep(step),
                        throwable -> Timber.w(
                                throwable,
                                "Error when click auth button: %s",
                                throwable.getMessage())));
    }

    private void initErrorDialog() {
        errorDialog = new Dialog(this);
        errorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        errorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        errorDialog.setContentView(R.layout.dialog_error);
        errorDialog.setCanceledOnTouchOutside(false);
        titleErrorTextView = errorDialog.findViewById(R.id.text_name_error);
        messageTextView = errorDialog.findViewById(R.id.text_message_error);
        TextView closeActionTextView = errorDialog.findViewById(R.id.text_close_dialog);
        closeActionTextView.setOnClickListener(view -> errorDialog.dismiss());
        TextView repeatActionTextView = errorDialog.findViewById(R.id.text_action_repeat);
        repeatActionTextView.setOnClickListener(view -> {
            errorDialog.dismiss();
            RequestType req = getPresenter().getLastRequest();
            switch (req) {
                case LOAD_OFERTA:
                    getPresenter().loadOferta();
                    break;
                case ACTIVATION:
                    getPresenter().activation(passwordEditText.getText().toString());
                    break;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        passwordEditText.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        confirmPasswordEditText.setTransformationMethod(new AsteriskPasswordTransformationMethod());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
//        showError(event.message, event.requestError);
    }

    @Override
    protected void onStop() {
        super.onStop();
        passwordEditText.setTransformationMethod(null);
        confirmPasswordEditText.setTransformationMethod(null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (errorDialog != null) {
            if (errorDialog.isShowing()) errorDialog.dismiss();
            errorDialog = null;
        }
    }

    @Override
    public void onBackPressed() {
        if (step == ActivationStep.PASSWORD_STEP) {
            step = ActivationStep.OFERTA_STEP;
            activationButton.setText(R.string.activation_accept);
            wrapperOferta.setVisibility(View.VISIBLE);
            wrapperPasswords.setVisibility(View.GONE);
        } else {
            getPresenter().onBackPressed();
        }
    }

    @Override
    public void showError(String message, RequestError requestError) {
        switch (requestError) {
            case TIMEOUT:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_timeout);
                errorDialog.show();
                break;
            case CUSTOM:
                titleErrorTextView.setText(R.string.error_title_auth);
                messageTextView.setText(message);
                errorDialog.show();
                break;
            case NETWORK:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_not_connection_network);
                errorDialog.show();
                break;
        }
    }

    @Override
    public void showPasswordError(@StringRes int messageId) {
        passwordTil.setErrorEnabled(true);
        passwordTil.setError(getString(messageId));
    }

    @Override
    public void showConfirmPasswordError() {
        confirmPasswordTil.setErrorEnabled(true);
        confirmPasswordTil.setError(getString(R.string.error_message_passwords_not_equal));
    }

    @Override
    public void showFieldErrors(Map<String, String> fieldErrors) {
        for (Map.Entry<String, String> entry : fieldErrors.entrySet()) {
            String tag = entry.getKey();
            TextInputLayout layout = root.findViewWithTag(tag);
            if (layout != null) {
                layout.setErrorEnabled(true);
                layout.setError(entry.getValue());
            }
        }
    }

    @Override
    public void hideFieldError() {
        passwordTil.setErrorEnabled(false);
        confirmPasswordTil.setErrorEnabled(false);
    }

    @Override
    public void acceptedOferta() {
        step = ActivationStep.PASSWORD_STEP;
        activationButton.setText(R.string.activate);
        wrapperOferta.setVisibility(View.GONE);
        wrapperPasswords.setVisibility(View.VISIBLE);
    }

    @Override
    public void activationSuccess() {
        Intent gotoMainScreenIntent = new Intent(ActivationActivity.this, MainActivity.class);
        startActivity(gotoMainScreenIntent);
        finish();
    }

    @Override
    public void allowActivation() {
        getPresenter().activation(passwordEditText.getText().toString());
    }

    @Override
    public void passwordStepActivation() {
        getPresenter().checkPasswords(
                passwordEditText.getText().toString(),
                confirmPasswordEditText.getText().toString());
    }

    @Override
    public void showOferta(Oferta oferta) {
        step = ActivationStep.OFERTA_STEP;
        titleOfertaTextView.setText(oferta.getTitle());
        subHeaderOfertaTextView.setText(oferta.getSubHeader());
        contentOfertaTextView.setHtml(oferta.getContent());
        contentOfertaTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void goToAuthScreen() {
        startActivity(new Intent(this, AuthActivity.class));
        finishAffinity();
    }

    @NonNull
    private SpannableStringBuilder getSpannableStringBuilder() {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString neededText = new SpannableString(
                getResources().getString(R.string.splash_green_card).toUpperCase());
        neededText.setSpan(new ForegroundColorSpan(ResourcesCompat
                .getColor(getResources(), R.color.colorWhite, null)), 5, neededText.length(), 0);
        builder.append(neededText);
        return builder;
    }

    public enum ActivationStep {
        OFERTA_STEP,
        PASSWORD_STEP
    }
}
