package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.util.Map;

public interface OperationsWithCardContract {

    interface View extends MvpView {

        void showPasswordError();

        void hideErrors();

        void showFieldErrors(Map<String, String> fieldErrors);

        void showError(String message, RequestError requestError);

        void showMainCard(String mainCardNumber);

        void updateOperationUnlock();

        void updateOperationLock();

        void successOperationLock();

        void successOperationUnlock();
    }

    interface Presenter extends MvpPresenter<View> {

        void checkData(String password, String cardNumber);

        void getCards();

        RequestType getLastRequest();
    }
}
