package com.aspirity.greencard.presentation.adapters.view_holders;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Notification;
import com.aspirity.greencard.presentation.adapters.NotificationsAdapter;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.recyclerview.widget.RecyclerView;


public class NotificationsViewHolder extends RecyclerView.ViewHolder {
    private final SimpleDateFormat FORMAT_DATE = new SimpleDateFormat("dd / MM / yy", Locale.getDefault());
    private final SimpleDateFormat FORMAT_TIME = new SimpleDateFormat("HH:mm", Locale.getDefault());

    private final TextView date;
    private final TextView title;
    private final TextView btn_label;
    private final ImageView image;
    private final TextView time;
    private final LinearLayout btn;
    private final ImageView state;
    private final View type;

    public NotificationsViewHolder(View itemView) {
        super(itemView);
        date = itemView.findViewById(R.id.notification_item_date);
        title = itemView.findViewById(R.id.text_title);
        btn_label = itemView.findViewById(R.id.text_action);
        image = itemView.findViewById(R.id.notifications_item_image);
        time = itemView.findViewById(R.id.text_time);
        btn = itemView.findViewById(R.id.notifications_item_button);
        state = itemView.findViewById(R.id.notifications_item_state);
        type = itemView.findViewById(R.id.view_notification_type);
    }

    @SuppressLint("SimpleDateFormat")
    public void bind(Context context, final Notification notification) {
        date.setText(FORMAT_DATE.format(new Date(notification.getDateCreated() * 1000)));
        time.setText(FORMAT_TIME.format(new Date(notification.getDateCreated() * 1000)));
        if (!notification.getCategory().equals("OPERATIONS"))
            btn.setVisibility(notification.getData().isVisibleButton() ? View.VISIBLE : View.GONE);
        else
            btn.setVisibility(View.VISIBLE);
        btn_label.setText(notification.getData().getButtonLabel());
        title.setText(notification.getRenderedText());

        if (notification.getStatus().equals("SENT")) {
            state.setVisibility(View.VISIBLE);
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.view_alpha);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    state.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            state.startAnimation(animation);
        } else
            state.setVisibility(View.GONE);

        if (notification.getData().getImageUrl() != null) {
            image.setVisibility(View.VISIBLE);
            Picasso.with(context)
                    .load(notification.getData().getImageUrl())
                    .placeholder(android.R.color.transparent)
                    .error(android.R.color.transparent)
                    .fit()
                    .centerCrop()
                    .into(image);
        } else
            image.setVisibility(View.GONE);

        switch (notification.getCategory()) {
            case "AD":
                type.setBackgroundColor(0xFFE71111);
                break;
            case "SERVICE":
                type.setBackgroundColor(0xFFF7D413);
                break;
            case "OPERATIONS":
                type.setBackgroundColor(0xFF5EB400);
                break;
        }
    }

    public void bindListener(final NotificationsAdapter.NotificationListener notificationListener, final Notification notification) {
        itemView.setOnClickListener(view -> notificationListener.onClick(notification));
    }
}
