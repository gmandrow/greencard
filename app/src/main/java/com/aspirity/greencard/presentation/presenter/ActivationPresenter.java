package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.R;
import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.activities.ActivationActivity;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.ActivationContract;
import com.aspirity.greencard.presentation.model.ErrorWrapper;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.net.SocketTimeoutException;
import java.util.Objects;

import androidx.core.util.Pair;
import io.reactivex.Single;
import retrofit2.HttpException;
import timber.log.Timber;

public class ActivationPresenter extends BasePresenter<ActivationContract.View>
        implements ActivationContract.Presenter {

    private PreferenceDataManager preferenceDataManager;
    private UserUseCase userUseCase;
    private RequestType lastRequest;

    public ActivationPresenter(
            PreferenceDataManager preferenceDataManager,
            UserUseCase userUseCase) {
        this.preferenceDataManager = preferenceDataManager;
        this.userUseCase = userUseCase;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                ErrorWrapper wrapper = ErrorParse.parseFormErrors(httpException.response());
                String generalError = wrapper.getGeneralError();
                if (CommonUtil.checkStringOnNullOrEmpty(generalError)) {
                    getView().showFieldErrors(wrapper.getFieldErrors());
                } else {
                    getView().showError(generalError, RequestError.CUSTOM);
                }
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        }
    }

    @Override
    public void activation(String password) {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(Single.zip(
                userUseCase.changePassword(password),
                userUseCase.activateUser(),
                Pair::new)
                .doOnSubscribe(disposable -> {
                    lastRequest = RequestType.ACTIVATION;
                    getView().showLoading();
                })
                .doOnSuccess(booleanStringPair -> getView().hideLoading())
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(booleanUserPair -> {
                    preferenceDataManager.setActiveUser(true);
                    getView().activationSuccess();
                }, this::handleError));
    }

    @Override
    public void loadOferta() {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(userUseCase.getOferta()
                .doOnSubscribe(disposable -> {
                    lastRequest = RequestType.LOAD_OFERTA;
                    getView().showLoading();
                })
                .doOnSuccess(booleanStringPair -> getView().hideLoading())
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(oferta -> getView().showOferta(oferta), this::handleError));
    }

    @Override
    public void checkPasswords(String password, String confirmPassword) {
        if (!isViewAttached()) return;

        getView().hideFieldError();
        boolean shouldAllow = true;

        if (CommonUtil.checkStringOnNullOrEmpty(password)) {
            shouldAllow = false;
            getView().showPasswordError(R.string.error_message_empty_field);
        } else if (password.length() < 6) {
            shouldAllow = false;
            getView().showPasswordError(R.string.error_message_short_password);
        }

        if (!Objects.equals(password, confirmPassword)) {
            shouldAllow = false;
            getView().showConfirmPasswordError();
        }

        if (shouldAllow) {
            getView().allowActivation();
        }
    }

    @Override
    public void checkStep(ActivationActivity.ActivationStep step) {
        if (!isViewAttached()) return;

        switch (step) {
            case OFERTA_STEP:
                getView().acceptedOferta();
                break;
            case PASSWORD_STEP:
                getView().passwordStepActivation();
                break;
        }
    }

    @Override
    public RequestType getLastRequest() {
        return lastRequest;
    }

    @Override
    public void onBackPressed() {
        if (!isViewAttached()) {
            return;
        }

        getCompositeDisposable().add(userUseCase.logOut()
                .subscribe(success -> {
                    preferenceDataManager.clearAll();
                    getView().goToAuthScreen();
                }, throwable -> {
                    Timber.e(throwable, "Back from Activation error: %s", throwable.getMessage());
                }));
    }
}
