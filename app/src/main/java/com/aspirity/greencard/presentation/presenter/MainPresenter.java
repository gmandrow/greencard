package com.aspirity.greencard.presentation.presenter;


import android.annotation.SuppressLint;
import android.util.Pair;

import com.aspirity.greencard.BuildConfig;
import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.model.Card;
import com.aspirity.greencard.domain.usecase.AppsUseCase;
import com.aspirity.greencard.domain.usecase.CardsUseCase;
import com.aspirity.greencard.domain.usecase.SharesUseCase;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.MainContract;
import com.aspirity.greencard.presentation.model.ShareFilterWrapper;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.paginate.Paginate;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.reactivex.Single;
import retrofit2.HttpException;
import timber.log.Timber;

public class MainPresenter extends BasePresenter<MainContract.View>
        implements MainContract.Presenter {

    private CardsUseCase cardUseCase;
    private UserUseCase userUseCase;
    private SharesUseCase sharesUseCase;
    private AppsUseCase appsUseCase;
    private PreferenceDataManager preferenceDataManager;

    private RequestType lastRequest = null;
    private String newVersion;
    private int totalItemsAmount = 0;
    private int currentItemsAmount = 0;
    private Card mainCard;
    private int page = 1;
    private boolean loading = false;

    private Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            getMoreShares();
        }

        @Override
        public boolean isLoading() {
            return loading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            if (currentItemsAmount >= totalItemsAmount) {
                getView().setHasLoadedAllItems();
                return true;
            }
            return false;
        }
    };

    public MainPresenter(UserUseCase userUseCase, SharesUseCase sharesUseCase, AppsUseCase appsUseCase, CardsUseCase cardUseCase,
                         PreferenceDataManager preferenceDataManager) {
        this.userUseCase = userUseCase;
        this.sharesUseCase = sharesUseCase;
        this.appsUseCase = appsUseCase;
        this.cardUseCase = cardUseCase;
        this.preferenceDataManager = preferenceDataManager;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached())
            return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();

            if (statusCode == Constants.STATUS_BAD_REQUEST)
                getView().showError(
                        ErrorParse.parseError(httpException.response()),
                        RequestError.CUSTOM);
        } else if (throwable instanceof SocketTimeoutException)
            getView().showError(null, RequestError.TIMEOUT);
        else if (throwable instanceof NetworkException)
            getView().showError(null, RequestError.NETWORK);
    }

    private void handleApps() {
        if (!isViewAttached())
            return;

        getView().showError(null, RequestError.NEW_VERSION);
    }

    @SuppressLint({"SimpleDateFormat", "CheckResult"})
    @Override
    public void getContent(boolean forceRefresh) {
        if (!isViewAttached())
            return;

        if (forceRefresh) {
            currentItemsAmount = 0;
            totalItemsAmount = 0;
            page = 1;
        }

        getCompositeDisposable().add(Single.zip(
                userUseCase.getUser(forceRefresh),
                sharesUseCase.getShares(page),
                Pair::new)
                .doOnSubscribe(disposable -> {
                    loading = true;
                    lastRequest = RequestType.CONTENT;

                    if (!forceRefresh)
                        getView().showLoading();
                })
                .onErrorResumeNext(error -> {
                    getView().hideLoading();
                    handleError(error);
                    return Single.zip(userUseCase.getUserFallback(), sharesUseCase.getSharesFallback(page, 30), Pair::new);
                })
                .subscribe(userSharesPair -> {
                    totalItemsAmount = userSharesPair.second.getCount();
                    currentItemsAmount = userSharesPair.second.getItems().size();
                    getView().showUserData(userSharesPair.first);
                    getView().showSharesData(userSharesPair.second.getItems(), userSharesPair.second.getCount());
                    getView().hideLoading();
                    if (!userSharesPair.second.getItems().isEmpty()) {
                        page++;
                    }
                    loading = false;
                }, this::handleError));

        getCompositeDisposable().add(cardUseCase.getCards(true)
                .doOnSuccess(aBoolean -> getView().hideLoading())
                .subscribe(cards -> {
                    mainCard = getMainCard(cards);

                    getView().showCardNumber(mainCard.getNumber());
                }, this::handleError));

        if (!preferenceDataManager.getCheckUpdate().equals(new SimpleDateFormat("yyyy-MM-dd").format(new Date())))
            getCompositeDisposable().add(
                    appsUseCase.getApps()
                            .subscribe(apps -> {
                                newVersion = apps.getAndroid();

                                if (!newVersion.equals(BuildConfig.VERSION_NAME))
                                    handleApps();

                                preferenceDataManager.setCheckUpdate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                            }));

        getCompositeDisposable().add(
                appsUseCase.registrationApps(FirebaseInstanceId.getInstance().getToken(), "ANDROID")
                        .subscribe(apps -> {
                        }, this::handleError));
    }

    @Override
    public void retryLastRequest() {
        switch (lastRequest) {
            case CONTENT:
                getContent(false);
                return;
            case SHARE_PAGE:
                getMoreShares();
        }
    }

    @Override
    public Paginate.Callbacks getCallbacks() {
        return callbacks;
    }

    private Card getMainCard(List<Card> cards) {
        for (Card card : cards) {
            if (card.isMain()) return card;
        }
        return null;
    }

    private void getMoreShares() {
        if (!isViewAttached()) {
            return;
        }

        getCompositeDisposable().add(
                sharesUseCase.getShares(page, new ShareFilterWrapper())
                        .doOnSubscribe(disposable -> {
                            loading = true;
                            lastRequest = RequestType.SHARE_PAGE;
                        })
                        .onErrorResumeNext(error -> sharesUseCase.getSharesFallback(page, 30))
                        .subscribe(listPair -> {
                            getView().showMoreSharesData(listPair.getItems());
                            currentItemsAmount += listPair.getItems().size();
                            if (!listPair.getItems().isEmpty()) {
                                page++;
                            }
                            loading = false;
                        }, throwable -> {
                            loading = false;
                            Timber.w(throwable, throwable.getMessage());
                        }));
    }

    private enum RequestType {
        CONTENT,
        SHARE_PAGE
    }
}
