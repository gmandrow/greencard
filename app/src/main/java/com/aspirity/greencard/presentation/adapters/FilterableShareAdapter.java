package com.aspirity.greencard.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.presentation.adapters.view_holders.FilterViewHolder;
import com.aspirity.greencard.presentation.adapters.view_holders.SharesViewHolder;
import com.aspirity.greencard.presentation.utils.DateUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by namtarr on 15.12.2017.
 */

public class FilterableShareAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<Share> shares;
    private final FilterableAdapterListener<Share> listener;
    private final Picasso picasso;
    private final DateUtil dateUtil;
    private final Context context;
    private int overallAmount = 0;
    private boolean isDataLoaded = false;

    public FilterableShareAdapter(Context context, List<Share> shares, DateUtil dateUtil, FilterableAdapterListener<Share> shareListener) {
        this.shares = shares;
        this.listener = shareListener;
        this.dateUtil = dateUtil;
        this.picasso = Picasso.with(context);
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return ListItem.TYPE_FILTERS;
            default:
                return ListItem.TYPE_GENERAL;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case ListItem.TYPE_FILTERS:
                itemView = LayoutInflater
                        .from(parent.getContext()).inflate(R.layout.item_header_shares, parent, false);
                ShareFilterViewHolder holder = new ShareFilterViewHolder(itemView);
                holder.sharesCount = shares.size();
                return holder;
            default:
                itemView = LayoutInflater
                        .from(parent.getContext()).inflate(R.layout.item_shares, parent, false);
                return new SharesViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);

        if (type == ListItem.TYPE_FILTERS) {
            ShareFilterViewHolder filtersViewHolder = (ShareFilterViewHolder) holder;
            if (filtersViewHolder.isPartner) {
                filtersViewHolder.sharesCount = shares.size();
                filtersViewHolder.loaded = isDataLoaded;
                filtersViewHolder.partnerShares();
            }
            filtersViewHolder.bind(listener, overallAmount);

        } else {
            SharesViewHolder viewHolder = (SharesViewHolder) holder;
            viewHolder.bind(context, shares.get(position - 1), picasso, dateUtil);
            viewHolder.bindListener(listener, shares.get(position - 1));
        }
    }

    @Override
    public int getItemCount() {
        return shares == null || shares.isEmpty() ? 1 : shares.size() + 1;
    }

    public void addData(List<Share> shares, int overallAmount) {
        this.shares.clear();
        this.shares.addAll(shares);
        this.overallAmount = overallAmount;
        dataLoaded();
        notifyDataSetChanged();
    }

    public void addNewItems(List<Share> shares) {
        int oldSize = this.shares.size();
        int chunkSize = shares.size();
        this.shares.addAll(shares);
        dataLoaded();
        notifyItemRangeInserted(oldSize, chunkSize);
    }

    public void dataLoaded() {
        isDataLoaded = true;
    }

    public static class ShareFilterViewHolder extends FilterViewHolder {

        TextView amountView, newSharesTextView, partnerSharesTextView;
        boolean isPartner = false, loaded = false, isArchive = false;
        int sharesCount;

        public ShareFilterViewHolder(View itemView) {
            super(itemView);
            amountView = itemView.findViewById(R.id.text_count_shares);
            newSharesTextView = itemView.findViewById(R.id.text_new_shares);
            partnerSharesTextView = itemView.findViewById(R.id.text_shares_of_partner);
        }

        public void bind(FilterableAdapterListener listener, int amount) {
            super.bind(listener);
            amountView.setText(String.valueOf(amount));
        }

        public void partnerShares() {
            isPartner = true;
            newSharesTextView.setVisibility(View.GONE);
            if (loaded) {
                partnerSharesTextView.setVisibility(View.VISIBLE);
                amountView.setVisibility(View.VISIBLE);
            } else {
                amountView.setVisibility(View.INVISIBLE);
            }
        }

        public void archiveShares() {
            isArchive = true;
            newSharesTextView.setText("Истекшие \nакции");
        }
    }

}
