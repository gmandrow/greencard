package com.aspirity.greencard.presentation.contract;

import com.aspirity.greencard.presentation.activities.ForgotPasswordActivity;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.util.Map;

import androidx.annotation.StringRes;

public interface ForgetPasswordContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showPhoneError(@StringRes int messageId);

        void showVerificationCodeError(@StringRes int messageId);

        void showPasswordError(@StringRes int messageId);

        void showRepeatPasswordError(@StringRes int messageId);

        void hideFieldError();

        void hideFieldPhoneError();

        void hideFieldVerificationError();

        void allowVerificationCode();

        void allowPasswordChange();

        void firstStepForgotPassword();

        void secondStepForgotPassword();

        void thirdStepForgotPassword();

        void restorePasswordSuccess();

        void showFieldErrors(Map<String, String> fieldErrors);

        void showToastSendCode();
    }

    interface Presenter extends MvpPresenter<View> {

        void sendVerificationCode(String phone);

        void verifySmsCode(String phone, String smsCode);

        void setNewPassword(String phone, String smsCode, String password);

        void checkFieldPhone(String phone);

        void checkFieldVerificationCode(String code, String phone);

        void checkFieldPasswordAndRepeatPassword(String password, String repeat, String phone, String smsCode);

        void checkStepForgotPassword(ForgotPasswordActivity.ForgotPasswordStep step);

    }
}
