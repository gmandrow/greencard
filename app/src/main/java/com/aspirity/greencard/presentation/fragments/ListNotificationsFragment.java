package com.aspirity.greencard.presentation.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.Notification;
import com.aspirity.greencard.domain.usecase.NotificationsUseCase;
import com.aspirity.greencard.presentation.activities.MainActivity;
import com.aspirity.greencard.presentation.adapters.NotificationsAdapter;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.NotificationsContract;
import com.aspirity.greencard.presentation.dialogs.ErrorDialog;
import com.aspirity.greencard.presentation.presenter.NotificationsPresenter;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.FragmentUtil;
import com.aspirity.greencard.presentation.utils.ViewUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.jakewharton.rxbinding2.support.v4.widget.RxNestedScrollView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import timber.log.Timber;

public class ListNotificationsFragment
        extends BaseFragment<NotificationsContract.View, NotificationsContract.Presenter>
        implements NotificationsContract.View, NotificationsAdapter.NotificationListener {
    /**
     * RECYCLER RELATED VIEWS
     **/
    private NestedScrollView nestedScrollView;
    private RecyclerView notificationsRecycler;
    private NotificationsAdapter adapter;

    public static ListNotificationsFragment newInstance(boolean isRestore) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.ARG_IS_RESTORE, isRestore);
        ListNotificationsFragment fragment = new ListNotificationsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        SchedulerProvider schedulerProvider = new AppSchedulerProvider();
        NotificationsUseCase notificationsUseCase = new NotificationsUseCase(
                context.getApplicationContext(),
                RepositoryProvider.getnotificationsRepository(schedulerProvider),
                schedulerProvider);
        onCreatePresenter(new NotificationsPresenter(notificationsUseCase), this);
        adapter = new NotificationsAdapter(
                context.getApplicationContext(),
                new LinkedList<>(),
                this);

        getPresenter().getContent();
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        notificationsRecycler.setAdapter(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setBackButtonColor(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    private void initViews(View view) {
        View title = view.findViewById(R.id.text_title_notifications);
        nestedScrollView = view.findViewById(R.id.nested_scroll_notifications);
        getCompositeDisposable().add(RxNestedScrollView.scrollChangeEvents(nestedScrollView)
                .subscribe(viewScrollChangeEvent -> {
                    ViewUtil.setTranslationY(-viewScrollChangeEvent.scrollY() * 2, title);
                }, throwable -> Timber.w(throwable, throwable.getMessage())));

        notificationsRecycler = view.findViewById(R.id.recycler_notifications);
        notificationsRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        notificationsRecycler.setAdapter(adapter);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
    }

    @Override
    public void showError(String message, RequestError requestError) {
        ErrorDialog errorDialog = new ErrorDialog(getContext());
        getCompositeDisposable().addAll(
                errorDialog.closeAction().subscribe(obj -> hideLoading()),
                errorDialog.retryAction().subscribe(obj -> getPresenter().retryLastRequest())
        );
        errorDialog.setErrorDescription(requestError, message);
        errorDialog.show();
    }

    @Override
    public void showNotifications(List<Notification> notifications) {
        adapter.addData(notifications);
        animation(nestedScrollView, animShow);
    }

    @Override
    public void setHasLoadedAllItems() {
    }

    @Override
    public void playStartAnimation() {
    }

    @Override
    public void showToast(String toast) {
        Toast.makeText(getContext(), toast, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(Notification notification) {
        if (notification.getStatus().equals("SENT") || notification.getStatus().equals("READ"))
            getPresenter().notificationsStatus(notification.getId(), FirebaseInstanceId.getInstance().getToken(), "REDIRECTED");

        String url = notification.getData().getUrl();
        String phone = notification.getData().getPhone();

        if (url != null) {
            if (validateUrl(url))
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(notification.getData().getUrl())));
            else if (url.contains("shares")) {
                Long id = Long.valueOf(url.substring(url.lastIndexOf('/') + 1));
                ShareFragment shareFragment = ShareFragment.newInstance(id, notification.getData().isArchived());
                FragmentUtil.setTransitionSlide(shareFragment, Gravity.END, Gravity.START);
                FragmentUtil.addBackStackWithAddFragment(getActivity().getSupportFragmentManager(), shareFragment);
            } else if (url.contains("operations")) {
                String uniqueId = url.substring(url.lastIndexOf('/') + 1);
                OperationFragment operationFragment = OperationFragment.newInstance(uniqueId);
                FragmentUtil.setTransitionSlide(operationFragment, Gravity.END, Gravity.START);
                FragmentUtil.addBackStackWithAddFragment(getActivity().getSupportFragmentManager(), operationFragment);
            }
        } else if (phone != null)
            startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone)));
        else if (notification.getCategory().equals("OPERATIONS")) {
            String uniqueId = notification.getData().getBp_rrn();
            OperationFragment operationFragment = OperationFragment.newInstance(uniqueId);
            FragmentUtil.setTransitionSlide(operationFragment, Gravity.END, Gravity.START);
            FragmentUtil.addBackStackWithAddFragment(getActivity().getSupportFragmentManager(), operationFragment);
        }
    }

    public boolean validateUrl(String address) {
        return android.util.Patterns.WEB_URL.matcher(address).matches();
    }
}
