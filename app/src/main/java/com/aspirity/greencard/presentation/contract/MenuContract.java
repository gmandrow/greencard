package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;

public interface MenuContract {

    interface View extends MvpView {

        void successLogOut();
    }

    interface Presenter extends MvpPresenter<View> {

        void logOut();
    }
}
