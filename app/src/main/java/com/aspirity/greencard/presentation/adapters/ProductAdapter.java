package com.aspirity.greencard.presentation.adapters;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.FooterItem;
import com.aspirity.greencard.domain.model.GeneralItem;
import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.Product;
import com.aspirity.greencard.domain.model.TotalSumFooter;

import java.text.NumberFormat;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


public class ProductAdapter extends RecyclerView.Adapter {

    private final List<ListItem> items;

    private final Resources resources;

    public ProductAdapter(List<ListItem> items, Resources resources) {
        this.items = items;
        this.resources = resources;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case ListItem.TYPE_HEADER:
                itemView = LayoutInflater
                        .from(parent.getContext()).inflate(R.layout.item_header_product, parent, false);
                return new HeaderViewHolder(itemView);
            case ListItem.TYPE_GENERAL:
                itemView = LayoutInflater
                        .from(parent.getContext()).inflate(R.layout.item_product, parent, false);
                return new ContentViewHolder(itemView);
            default:
                itemView = LayoutInflater
                        .from(parent.getContext()).inflate(R.layout.item_footer_product, parent, false);
                return new FooterViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);
        switch (type) {
            case ListItem.TYPE_GENERAL:
                ContentViewHolder contentViewHolder = (ContentViewHolder) holder;
                contentViewHolder.bind((GeneralItem<Product>) items.get(position), getPrevOfLastItem(), resources);
                break;
            case ListItem.TYPE_FOOTER:
                FooterViewHolder footerViewHolder = (FooterViewHolder) holder;
                footerViewHolder.bind((FooterItem<TotalSumFooter>) items.get(position), resources);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    public int getPrevOfLastItem() {
        return items.size() - 2;
    }

    public void addData(List<ListItem> products) {
        items.clear();
        items.addAll(products);
        notifyDataSetChanged();
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {

        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        TextView totalSumTextView;
        TextView totalAccruedBonusesTextView;
        TextView totalDebitedBonusesTextView;

        public FooterViewHolder(View itemView) {
            super(itemView);

            totalSumTextView = itemView.findViewById(R.id.text_total_sum);
            totalAccruedBonusesTextView =
                    itemView.findViewById(R.id.text_total_accrued_bonuses);
            totalDebitedBonusesTextView =
                    itemView.findViewById(R.id.text_total_debited_bonuses);
        }

        void bind(FooterItem<TotalSumFooter> totalSumFooter, Resources resources) {
            totalSumTextView.setText(totalSumFooter.getFooter().getTotalSum());
            totalAccruedBonusesTextView.setText(resources.getString(R.string.bonuses,
                    totalSumFooter.getFooter().getTotalAccruedBonuses()));
            totalDebitedBonusesTextView.setText(resources.getString(R.string.bonuses,
                    totalSumFooter.getFooter().getTotalDebitedBonuses()));
        }
    }

    public static class ContentViewHolder extends RecyclerView.ViewHolder {

        TextView nameProductTextView;
        TextView countProductTextView;
        TextView priceProductTextView;
        TextView sumProductTextView;
        TextView accruedBonusesTextView;
        TextView debitedBonusesTextView;
        View dividerBottom;
        NumberFormat f = NumberFormat.getInstance();

        public ContentViewHolder(View itemView) {
            super(itemView);

            nameProductTextView = itemView.findViewById(R.id.text_product_name);
            countProductTextView = itemView.findViewById(R.id.text_product_count);
            priceProductTextView = itemView.findViewById(R.id.text_price);
            sumProductTextView = itemView.findViewById(R.id.text_sum);
            accruedBonusesTextView = itemView.findViewById(R.id.text_accrued_bonuses);
            debitedBonusesTextView = itemView.findViewById(R.id.text_debited_bonuses);
            dividerBottom = itemView.findViewById(R.id.divider_bottom);

            f.setMinimumFractionDigits(0);
        }

        void bind(GeneralItem<Product> product, int prevOfLastItem, Resources resources) {
            nameProductTextView.setText(product.getData().getName());
            countProductTextView.setText(f.format(product.getData().getCount()));
            priceProductTextView.setText(product.getData().getPrice());
            sumProductTextView.setText(product.getData().getTotalPrice());
            accruedBonusesTextView.setText(resources.getString(R.string.bonuses, product.getData().getAccruedBonuses()));
            debitedBonusesTextView.setText(resources.getString(R.string.bonuses, product.getData().getDebitedBonuses()));
            if (getAdapterPosition() == prevOfLastItem) {
                dividerBottom.setVisibility(View.INVISIBLE);
            } else {
                dividerBottom.setVisibility(View.VISIBLE);
            }
        }
    }
}
