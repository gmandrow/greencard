package com.aspirity.greencard.presentation.dialogs;

import android.app.Dialog;
import android.content.Context;

import com.aspirity.greencard.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jakewharton.rxbinding2.view.RxView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public abstract class BaseFilterDialog<T> extends Dialog {

    protected T filterValue;
    protected List<Disposable> disposables = new ArrayList<>();

    private FloatingActionButton saveButton;
    private FloatingActionButton cancelButton;

    public BaseFilterDialog(@NonNull Context context, @LayoutRes int layoutId, T initial) {
        super(context, R.style.FullScreenDialog);
        filterValue = initial;
        setContentView(layoutId);
        //setCancelable(false);
        setCanceledOnTouchOutside(false);
        init(context);
    }

    protected void init(Context context) {
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        saveButton = findViewById(R.id.fab_done_filter_dialog);
        cancelButton = findViewById(R.id.fab_back_filter_dialog);

        //setOnCancelListener(dialogInterface -> dialogInterface.dismiss());
    }

    public Observable<T> getSaveButtonObservable() {
        return RxView.clicks(saveButton).map(o -> filterValue);
    }

    public Observable<Object> getCancelButtonObservable() {
        return RxView.clicks(cancelButton);
    }

    public Disposable[] getInnerDisposables() {
        return disposables.toArray(new Disposable[disposables.size()]);
    }
}
