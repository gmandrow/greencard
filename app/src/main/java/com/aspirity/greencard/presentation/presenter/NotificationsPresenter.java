package com.aspirity.greencard.presentation.presenter;


import android.annotation.SuppressLint;

import com.aspirity.greencard.domain.model.Notification;
import com.aspirity.greencard.domain.usecase.NotificationsUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.NotificationsContract;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.paginate.Paginate;

import java.net.SocketTimeoutException;

import retrofit2.HttpException;

public class NotificationsPresenter extends BasePresenter<NotificationsContract.View>
        implements NotificationsContract.Presenter {

    private final NotificationsUseCase notificationsUseCase;

    private RequestType lastRequest = null;
    private boolean loading = false;

    private Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            getNotifications();
        }

        @Override
        public boolean isLoading() {
            return loading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            return false;
        }
    };

    public NotificationsPresenter(NotificationsUseCase notificationsUseCase) {
        this.notificationsUseCase = notificationsUseCase;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                getView().showError(
                        ErrorParse.parseError(httpException.response()),
                        RequestError.CUSTOM);
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        } else {
            // generic error handling
        }
    }

    @Override
    public void getContent() {
        if (!isViewAttached()) {
            return;
        }

        getCompositeDisposable().add(
                notificationsUseCase.getNotifications(FirebaseInstanceId.getInstance().getToken())
                        .doOnSubscribe(disposable -> {
                            loading = true;
                            lastRequest = RequestType.CONTENT;
                            getView().showLoading();
                        })
                        .onErrorResumeNext(error -> {
                            getView().hideLoading();
                            handleError(error);
                            return notificationsUseCase.getNotifications(FirebaseInstanceId.getInstance().getToken());
                        })
                        .subscribe(notifications -> {
                            getView().showNotifications(notifications.getItems());
                            getView().playStartAnimation();
                            getView().hideLoading();
                            loading = false;

                            for (Notification notification : notifications.getItems())
                                if (notification.getStatus().equals("SENT"))
                                    notificationsStatus(notification.getId(), FirebaseInstanceId.getInstance().getToken(), "READ");
                        }, this::handleError));
    }

    @Override
    public void getNotifications() {
        if (!isViewAttached()) {
            return;
        }

        getCompositeDisposable().add(
                notificationsUseCase.getNotifications(FirebaseInstanceId.getInstance().getToken())
                        .doOnSubscribe(disposable -> {
                            loading = true;
                            lastRequest = RequestType.CONTENT;
                        })
                        .subscribe(notifications -> {
                            getView().showNotifications(notifications.getItems());
                            loading = false;
                        }, this::handleError));
    }


    @SuppressLint("CheckResult")
    @Override
    public void notificationsStatus(Long notification_id, String token, String status) {
        notificationsUseCase.notificationsStatus(notification_id, token, status)
                .subscribe(apps -> {
                }, this::handleError);
    }

    @Override
    public Paginate.Callbacks getCallbacks() {
        return callbacks;
    }

    @Override
    public void retryLastRequest() {
        switch (lastRequest) {
            case CONTENT:
                getContent();
                return;
            case OPERATION_PAGE:
                getNotifications();
        }
    }

    private enum RequestType {
        CONTENT,
        OPERATION_PAGE;

        private boolean forceRefresh;
    }
}
