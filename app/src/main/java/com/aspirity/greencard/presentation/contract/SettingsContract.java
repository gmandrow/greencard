package com.aspirity.greencard.presentation.contract;

import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.util.Map;

import androidx.annotation.StringRes;

public interface SettingsContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showFieldErrors(Map<String, String> errors);

        void showMailingSettings(boolean push_operations, boolean push, boolean email, boolean sms);

        void showNewPasswordError(@StringRes int messageId);

        void showConfirmPasswordError(@StringRes int messageId);

        void hideFieldError();

        void passwordChanged();

        void startAnimation();
    }

    interface Presenter extends MvpPresenter<View> {

        void getMailingSettings(boolean forceRefresh);

        void setMailingSettings(boolean push_operations, boolean push, boolean email, boolean sms);

        void savePassword(String newPassword, String confirmPassword);

        void retryLastRequest();
    }
}
