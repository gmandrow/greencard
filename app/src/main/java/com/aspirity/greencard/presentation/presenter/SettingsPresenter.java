package com.aspirity.greencard.presentation.presenter;

import android.widget.Toast;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.SettingsContract;
import com.aspirity.greencard.presentation.model.ErrorWrapper;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.net.SocketTimeoutException;
import java.util.Objects;

import retrofit2.HttpException;

public class SettingsPresenter extends BasePresenter<SettingsContract.View>
        implements SettingsContract.Presenter {

    private UserUseCase userUseCase;
    private RequestType lastRequest = null;

    public SettingsPresenter(UserUseCase userUseCase) {
        this.userUseCase = userUseCase;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                ErrorWrapper wrapper = ErrorParse.parseFormErrors(httpException.response());
                String generalError = wrapper.getGeneralError();
                if (CommonUtil.checkStringOnNullOrEmpty(generalError)) {
                    getView().showFieldErrors(wrapper.getFieldErrors());
                } else {
                    getView().showError(generalError, RequestError.CUSTOM);
                }
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        }
    }

    @Override
    public void getMailingSettings(boolean forceRefresh) {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(
                userUseCase.getUser(forceRefresh)
                        .doOnSubscribe(disposable -> {
                            lastRequest = RequestType.GET_SETTINGS;
                            lastRequest.forceRefresh = forceRefresh;
                            if (!forceRefresh) {
                                getView().showLoading();
                            }
                        })
                        .onErrorResumeNext(error -> {
                            getView().hideLoading();
                            handleError(error);
                            return userUseCase.getUserFallback();
                        })
                        .subscribe(user -> {
                            getView().showMailingSettings(
                                    user.isSubscribedToPushOperations(),
                                    user.isSubscribedToPush(),
                                    user.isSubscribedToEmail(),
                                    user.isSubscribedToSms());
                            getView().hideLoading();
                            getView().startAnimation();
                        }, this::handleError));
    }

    @Override
    public void setMailingSettings(boolean push_operations, boolean push, boolean email, boolean sms) {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(
                userUseCase.setMailingSettings(push_operations, push, email, sms)
                        .doOnSubscribe(disposable -> {
                            lastRequest = RequestType.SET_SETTINGS;
                            lastRequest.push_operations = push_operations;
                            lastRequest.push = push;
                            lastRequest.email = email;
                            lastRequest.sms = sms;
                        })
                        .onErrorResumeNext(error -> {
                            handleError(error);
                            return userUseCase.getUserFallback();
                        })
                        .subscribe(user -> {
                            Toast.makeText(getView().getGreenCardApp().getApplicationContext(),
                                    "Настройки сохранены", Toast.LENGTH_SHORT).show();
                            getView().showMailingSettings(
                                    user.isSubscribedToPushOperations(),
                                    user.isSubscribedToPush(),
                                    user.isSubscribedToEmail(),
                                    user.isSubscribedToSms()
                            );
                        }, this::handleError));
    }

    @Override
    public void savePassword(String newPassword, String confirmPassword) {
        if (!isViewAttached()) return;

        getView().hideFieldError();

        if (!CommonUtil.checkStringOnNullOrEmpty(newPassword.trim()) &&
                Objects.equals(newPassword.trim(), confirmPassword.trim())
                && newPassword.length() >= 6 && confirmPassword.length() >= 6) {
            getCompositeDisposable().add(
                    userUseCase
                            .changePassword(newPassword)
                            .doOnSubscribe(disposable -> {
                                lastRequest = RequestType.PASSWORD;
                                lastRequest.password = newPassword;
                                lastRequest.confirm = confirmPassword;
                                getView().showLoading();
                                getView().hideFieldError();
                            })
                            .doOnError(disposable -> getView().hideLoading())
                            .subscribe(user -> {
                                getView().hideLoading();
                                getView().passwordChanged();
                            }, this::handleError)
            );
            return;
        } else {
            if (CommonUtil.checkStringOnNullOrEmpty(newPassword.trim())) {
                getView().showNewPasswordError(R.string.error_message_empty_field);
            }
            if (newPassword.length() < 6) {
                getView().showNewPasswordError(R.string.error_message_short_password);
            }
            if (confirmPassword.length() < 6) {
                getView().showConfirmPasswordError(R.string.error_message_short_password);
            }
            if (!Objects.equals(newPassword.trim(), confirmPassword.trim())) {
                getView().showConfirmPasswordError(R.string.error_message_passwords_not_equal);
            }
        }
    }

    @Override
    public void retryLastRequest() {
        switch (lastRequest) {
            case PASSWORD:
                savePassword(lastRequest.password, lastRequest.confirm);
                return;
            case SET_SETTINGS:
                setMailingSettings(lastRequest.push_operations, lastRequest.push, lastRequest.email, lastRequest.sms);
                return;
            case GET_SETTINGS:
                getMailingSettings(lastRequest.forceRefresh);
        }
    }

    private enum RequestType {
        PASSWORD,
        SET_SETTINGS,
        GET_SETTINGS;

        private boolean forceRefresh;

        private String password;
        private String confirm;

        private boolean push_operations;
        private boolean push;
        private boolean email;
        private boolean sms;
    }
}
