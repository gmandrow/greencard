package com.aspirity.greencard.presentation.model;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ShareFilterWrapper extends FilterWrapper {

    private ShareSortParams sortParams = ShareSortParams.DATE_DESC;

    public ShareFilterWrapper() {
        super();

        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, 1);
        Date endTime = c.getTime();

        c.add(Calendar.YEAR, -2);
        Date beginTime = c.getTime();
        dateInterval = new DateInterval(beginTime, endTime);
    }

    public ShareFilterWrapper(ShareSortParams sortParams, DateInterval dateInterval, List<SelectableVendor> vendors) {
        super(dateInterval, vendors);
        this.sortParams = sortParams;
    }

    public ShareSortParams getSortParams() {
        return sortParams;
    }

    public void setSortParams(ShareSortParams sortParams) {
        this.sortParams = sortParams;
    }
}
