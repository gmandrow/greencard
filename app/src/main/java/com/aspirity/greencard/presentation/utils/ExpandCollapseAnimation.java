package com.aspirity.greencard.presentation.utils;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import java.lang.ref.WeakReference;


public class ExpandCollapseAnimation extends Animation {
    private WeakReference<View> viewWeakReference;
    //    private View mAnimatedView;
    private int mEndHeight;
    private int mType;

    public ExpandCollapseAnimation(View view, int duration, int type, Activity activity) {
        setDuration(duration);
        viewWeakReference = new WeakReference<>(view);
//        mAnimatedView = view;

        setHeightForWrapContent(new WeakReference<>(activity).get(), viewWeakReference.get());

        mEndHeight = viewWeakReference.get().getLayoutParams().height;

        mType = type;
        if (mType == 0) {
            viewWeakReference.get().getLayoutParams().height = 0;
            viewWeakReference.get().setVisibility(View.VISIBLE);
        }
    }

    public static void setHeightForWrapContent(Activity activity, View view) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int screenWidth = metrics.widthPixels;

        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(screenWidth, View.MeasureSpec.EXACTLY);

        view.measure(widthMeasureSpec, heightMeasureSpec);
        int height = view.getMeasuredHeight();
        view.getLayoutParams().height = height;
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(200/*(int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density)*/);
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(200/*(int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density)*/);
        v.startAnimation(a);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        if (interpolatedTime < 1.0f) {
            if (mType == 0) {
                viewWeakReference.get().getLayoutParams().height = (int) (mEndHeight * interpolatedTime);
            } else {
                viewWeakReference.get().getLayoutParams().height = mEndHeight - (int) (mEndHeight * interpolatedTime);
            }
            viewWeakReference.get().requestLayout();
        } else {
            if (mType == 0) {
                viewWeakReference.get().getLayoutParams().height = mEndHeight;
                viewWeakReference.get().requestLayout();
            } else {
                viewWeakReference.get().getLayoutParams().height = 0;
                viewWeakReference.get().setVisibility(View.GONE);
                viewWeakReference.get().requestLayout();
                viewWeakReference.get().getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;     // Return to wrap
            }
        }
    }
}
