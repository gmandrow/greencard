package com.aspirity.greencard.presentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aspirity.greencard.GreenCardApplication;
import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.GeneralItem;
import com.aspirity.greencard.domain.model.HeaderItem;
import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.Operation;
import com.aspirity.greencard.presentation.adapters.view_holders.FilterViewHolder;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.DateUtil;

import java.util.Date;
import java.util.List;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

public class BalanceAndOperationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<ListItem> items;
    private final Context context;
    private final FilterableAdapterListener<Operation> listener;

    public BalanceAndOperationsAdapter(Context context, List<ListItem> items, FilterableAdapterListener<Operation> listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case ListItem.TYPE_HEADER:
                itemView = LayoutInflater
                        .from(parent.getContext()).inflate(R.layout.item_date, parent, false);
                return new HeaderViewHolder(itemView);
            case ListItem.TYPE_FILTERS:
                itemView = LayoutInflater
                        .from(parent.getContext()).inflate(R.layout.item_header_operations, parent, false);
                return new FilterViewHolder(itemView);
            default:
                itemView = LayoutInflater
                        .from(parent.getContext()).inflate(R.layout.item_operation, parent, false);
                return new ContentViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);

        if (type == ListItem.TYPE_FILTERS) {
            FilterViewHolder filtersViewHolder = (FilterViewHolder) holder;
            filtersViewHolder.bind(listener);
            return;
        }

        if (type == ListItem.TYPE_HEADER) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.bind(
                    (HeaderItem<String>) items.get(position - 1),
                    ((GreenCardApplication) context).getDateUtils());
        } else {
            ContentViewHolder contentViewHolder = (ContentViewHolder) holder;
            contentViewHolder.bind(
                    context,
                    (GeneralItem<Operation>) items.get(position - 1),
                    listener,
                    ((GreenCardApplication) context).getDateUtils());
        }
    }

    @Override
    public int getItemCount() {
        return items == null || items.isEmpty() ? 1 : items.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return ListItem.TYPE_FILTERS;
            default:
                return items.get(position - 1).getType();
        }
    }

    public void addData(List<ListItem> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void addNewItems(List<ListItem> operations) {
        this.items.addAll(operations);
        notifyDataSetChanged();
    }

    public static class ContentViewHolder extends RecyclerView.ViewHolder {

        TextView placeOfBuyingTextView;
        TextView bonusesTextView;
        TextView sumTextView;
        TextView timeOfBuyingTextView;


        public ContentViewHolder(View itemView) {
            super(itemView);
            placeOfBuyingTextView = itemView.findViewById(R.id.text_place_of_buying);
            bonusesTextView = itemView.findViewById(R.id.text_bonuses);
            sumTextView = itemView.findViewById(R.id.text_sum);
            timeOfBuyingTextView = itemView.findViewById(R.id.text_time_of_buying);
        }

        void bind(Context context, GeneralItem<Operation> item,
                  FilterableAdapterListener<Operation> listener, DateUtil dateUtils) {
            placeOfBuyingTextView.setText(item.getData().getVendorName().split(",")[0]);
            String time = dateUtils.getTimeString(new Date(
                    Long.valueOf(item.getData().getDateOf()) * Constants.COEFFICIENT_TIMESTAMP_TO_MILISECONDS));
            timeOfBuyingTextView.setText(time);
            String bonuses = item.getData().getBonuses();
            if (bonuses.contains("-")) {
                bonusesTextView.setTextColor(ResourcesCompat.
                        getColor(context.getResources(), R.color.colorRed, null));
                String b = bonuses.substring(1);
                if (b.length() > 6) {
                    b = b.substring(0, b.length() - 6) + " " + b.substring(b.length() - 6);
                }
                String minus = "- " + b;
                bonusesTextView.setText(minus);
            } else {
                if (bonuses.length() > 6) {
                    bonuses = bonuses.substring(0, bonuses.length() - 6) + " " + bonuses.substring(bonuses.length() - 6);
                }
                bonusesTextView.setTextColor(ResourcesCompat.
                        getColor(context.getResources(), R.color.colorGreen, null));
                bonusesTextView.setText(context.getResources().getString(R.string.bonuses_plus, " " + bonuses));
            }

            String sum = item.getData().getTotalPrice();
            if (sum.length() > 6) {
                sum = sum.substring(0, sum.length() - 6) + " " + sum.substring(sum.length() - 6);
            }
            sumTextView.setText(sum);

            itemView.setOnClickListener(view -> listener.onClick(item.getData(), Constants.EMPTY_ANSWER));
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView dateOfBuyingTextView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            dateOfBuyingTextView = itemView.findViewById(R.id.text_date_of_buying);
        }

        void bind(HeaderItem<String> item, DateUtil dateUtils) {
            dateOfBuyingTextView.setText(item.getHeader());
        }
    }
}
