package com.aspirity.greencard.presentation.fragments;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.presentation.adapters.PhotoAdapter;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.PartnerDescriptionContract;
import com.aspirity.greencard.presentation.presenter.PartnerDescriptionPresenter;
import com.aspirity.greencard.presentation.utils.Constants;
import com.squareup.picasso.Picasso;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class PartnerDescriptionFragment
        extends BaseFragment<PartnerDescriptionContract.View, PartnerDescriptionContract.Presenter>
        implements PartnerDescriptionContract.View, PartnerDescriptionUpdateable {

    private RecyclerView galleryRecyclerView;
    private List<String> photoSrc = new LinkedList<>();
    private HtmlTextView descriptionTextView;
    private WebView videoWebView;
    private PhotoAdapter photoAdapter;
    private ImageView onePhotoImageView;

    public static PartnerDescriptionFragment newInstance(
            String descriptionPartner,
            List<String> srcPhotos,
            String videoCode) {
        ArrayList<String> strings = new ArrayList<>();
        strings.addAll(srcPhotos);
        Bundle args = new Bundle();
        args.putString(Constants.ARG_DESCRIPTION_PARTNER, descriptionPartner);
        args.putString(Constants.ARG_VIDEO_CODE_PARTNER, videoCode);
        args.putStringArrayList(Constants.ARG_SRC_PHOTOS_PARTNER, strings);
        PartnerDescriptionFragment fragment = new PartnerDescriptionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_partner_description, container, false);
        onCreatePresenter(new PartnerDescriptionPresenter(), this);
        initViews(view);
        getPresenter().checkData(
                getArguments().getString(Constants.ARG_DESCRIPTION_PARTNER),
                getArguments().getStringArrayList(Constants.ARG_SRC_PHOTOS_PARTNER),
                getArguments().getString(Constants.ARG_VIDEO_CODE_PARTNER));
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (photoAdapter == null && galleryRecyclerView.getVisibility() == View.VISIBLE) {
            photoAdapter = new PhotoAdapter(getActivity(), photoSrc);
            galleryRecyclerView.setAdapter(photoAdapter);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (galleryRecyclerView.getVisibility() == View.VISIBLE) {
            //galleryRecyclerView.setAdapter(null);
            photoAdapter = null;
        }
    }

    private void initViews(View view) {
        onePhotoImageView = view.findViewById(R.id.image_one_photo);

        galleryRecyclerView = view.findViewById(R.id.recycler_gallery_photos);
        galleryRecyclerView.setLayoutManager(new LinearLayoutManager(
                getContext(),
                LinearLayoutManager.HORIZONTAL,
                false));
        galleryRecyclerView.setNestedScrollingEnabled(false);
        galleryRecyclerView.setHasFixedSize(true);

        descriptionTextView = view.findViewById(R.id.text_description);
        videoWebView = view.findViewById(R.id.web_video);
        videoWebView.setWebViewClient(new WebViewClient());
        WebSettings settings = videoWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);
    }

    @Override
    public void showVideo(String videoUrl) {
        videoWebView.setVisibility(View.VISIBLE);
        videoWebView.loadData(videoUrl, "text/html", null);
        videoWebView.onResume();
    }

    @Override
    public void hideVideo() {
        videoWebView.stopLoading();
        videoWebView.onPause();
        videoWebView.setVisibility(View.GONE);
    }

    @Override
    public void showGallery(List<String> srcPhotos) {
        galleryRecyclerView.setVisibility(View.VISIBLE);
        if (photoAdapter == null) {
            photoAdapter = new PhotoAdapter(getActivity(), photoSrc);
            galleryRecyclerView.setAdapter(photoAdapter);
        }
        photoAdapter.addData(srcPhotos);
    }

    @Override
    public void hideGallery() {
        galleryRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void showOnePhoto(String srcPhoto) {
        onePhotoImageView.setVisibility(View.VISIBLE);
        Picasso.with(getActivity())
                .load(srcPhoto)
                .fit()
                .centerInside()
                .into(onePhotoImageView);
    }

    @Override
    public void hideOnePhoto() {
        onePhotoImageView.setVisibility(View.GONE);
    }

    @Override
    public void showDescription(String description) {
        if (description != null) {
            descriptionTextView.setHtml(description);
            descriptionTextView.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    @Override
    public void updateDescription(Partner partner) {
        ArrayList<String> strings = new ArrayList<>();
        strings.addAll(partner.getPhotos());
        Bundle args = new Bundle();
        args.putString(Constants.ARG_DESCRIPTION_PARTNER, partner.getDescription());
        args.putString(Constants.ARG_VIDEO_CODE_PARTNER, partner.getVideoCode());
        args.putStringArrayList(Constants.ARG_SRC_PHOTOS_PARTNER, strings);
        setArguments(args);
        if (getView() != null) {
            getPresenter().checkData(
                    partner.getDescription(),
                    partner.getPhotos(),
                    partner.getVideoCode());
        }
    }
}
