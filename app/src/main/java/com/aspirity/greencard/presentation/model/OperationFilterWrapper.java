package com.aspirity.greencard.presentation.model;


import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class OperationFilterWrapper extends FilterWrapper {

    private OperationSortParams sortParams = OperationSortParams.DATE_DESC;

    public OperationFilterWrapper() {
        super();

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        Date endTime = c.getTime();

        c.set(2015, 8, 15);
        Date beginTime = c.getTime();
        dateInterval = new DateInterval(beginTime, endTime);
    }

    public OperationFilterWrapper(DateInterval dateInterval, List<SelectableVendor> vendors, OperationSortParams sortParams) {
        super(dateInterval, vendors);
        this.sortParams = sortParams;
    }

    public OperationSortParams getSortParams() {
        return sortParams;
    }

    public void setSortParams(OperationSortParams sortParams) {
        this.sortParams = sortParams;
    }
}
