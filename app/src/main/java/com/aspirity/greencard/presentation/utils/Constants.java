package com.aspirity.greencard.presentation.utils;

public abstract class Constants {
    public static final int DURATION_SLIDE_ANIM = 300;
    public static final int DURATION_EXPAND_COLLAPSE_ANIM = 200;
    public static final int COEFFICIENT_TRANSLATION_X = 2;
    public static final int COEFFICIENT_TRANSLATION_FAST_X = 6;
    public static final int COEFFICIENT_TRANSLATION_MEDIUM_Y = 6;
    public static final int COEFFICIENT_TRANSLATION_Y = 12;
    public static final int WITHOUT_COEFFICIENT_TRANSLATION_Y = 1;
    public static final int COEFFICIENT_TIMESTAMP_TO_MILISECONDS = 1000;
    public static final int FIRST_ITEM = 0;
    public static final int ZERO = 0;
    public static final int ONE_DAY = 1;
    public static final int DEBOUNCE = 100;
    public static final float MAX_ALPHA = 1;

    public static final int MILLION = 1000000;

    public static final String TYPE_FILTER_SORT_BY = "sort_by";
    public static final String TYPE_FILTER_SORT_PERIOD = "sort_period";
    public static final String TYPE_FILTER_SORT_STORES = "sort_stores";
    public static final String DIALOG_TAG = "dialog";

    /*for fragments arguments*/
    public static final String ARG_QUESTION = "question";
    public static final String ARG_SHARE_ID = "share_id";
    public static final String ARG_SHARE_TITLE = "share_title";
    public static final String ARG_SHARE_DATE_FORMAT = "share_date";
    public static final String ARG_OPERATION_ID = "operation_id";
    public static final String ARG_OPERATION_UNIQUE_ID = "unique_id";
    public static final String ARG_PARTNER_ID = "partner_id";
    public static final String ARG_PARTNER_LOGO = "partner_logo";
    public static final String ARG_PARTNER_MAP_LOGO = "partner_map_logo";
    public static final String ARG_BACKGROUND_COLOR_PARTNER = "background_color_partner";
    public static final String ARG_DESCRIPTION_PARTNER = "description_partner";
    public static final String ARG_VIDEO_CODE_PARTNER = "video_code";
    public static final String ARG_SRC_PHOTOS_PARTNER = "src_photos";
    public static final String ARG_BONUSES_DESCRIPTION_PARTNER = "bonuses_description";
    public static final String ARG_BONUSES_TABLE_PARTNER = "bonuses_table";
    public static final String ARG_SELECTED_VENDORS = "selected_vendors";
    public static final String ARG_IS_RESTORE = "is_restore";
    public static final String ARG_VENDORS_COUNT = "vendors_count";
    public static final String ARG_SHARE_IS_ARCHIVED = "is_archived";

    /* Notification data */
    public static final String EXTRA_SHARE_ID = "share_id";

    public static final Integer PAGE_SIZE = 4;
    public static final Integer INITIAL_PAGE = 1;

    /*for primaryKey constants*/
    public static final String PRIMARY_KEY_ID = "id";

    /*for error api*/
    public static final String ERROR_AUTH = "Unable to log in with provided credentials";
    public static final int STATUS_BAD_REQUEST = 400;
    public static final int STATUS_UNAUTHORIZED = 401;

    public static final String EMPTY_ANSWER = "";
    public static final String ZERO_ANSWER = "0";

    public static final String DEFAULT_VALUE_BNS = "0.00";

    public static final String DEFAULT_COLOR = "#000000";

    public static final String MILLION_SYMBOL = "М";

    //test
    public static final int POSITION_DESIRED_ELEMENT = 4;

    public static final double MAX_DIFF_BETWEEN_POSITIONS = 0.001;

    public static final int MIN_LENGTH_PASSWORD = 6;
}
