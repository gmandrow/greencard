package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.PartnersUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.ListPartnersContract;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.net.SocketTimeoutException;

import retrofit2.HttpException;

public class ListPartnersPresenter extends BasePresenter<ListPartnersContract.View>
        implements ListPartnersContract.Presenter {

    private PreferenceDataManager preferenceDataManager;
    private PartnersUseCase partnersUseCase;

    public ListPartnersPresenter(
            PreferenceDataManager preferenceDataManager,
            PartnersUseCase partnersUseCase) {
        this.preferenceDataManager = preferenceDataManager;
        this.partnersUseCase = partnersUseCase;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                getView().showError(
                        ErrorParse.parseError(httpException.response()),
                        RequestError.CUSTOM);
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        }
    }

    @Override
    public void getPartners() {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(partnersUseCase.getPartners()
                .doOnSubscribe(d -> getView().showLoading())
                .onErrorResumeNext(error -> {
                    getView().hideLoading();
                    handleError(error);
                    return partnersUseCase.getPartnersFallback();
                })
                .subscribe(partnerList -> {
                    getView().showPartnersData(partnerList);
                    getView().hideLoading();
                }, this::handleError));
    }

    @Override
    public void retryLastRequest() {
        getPartners();
    }
}
