package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.util.List;

public interface ListPartnersContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showPartnersData(List<Partner> partners);
    }

    interface Presenter extends MvpPresenter<ListPartnersContract.View> {

        void getPartners();

        void retryLastRequest();
    }
}
