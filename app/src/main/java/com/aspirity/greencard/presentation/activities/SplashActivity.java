package com.aspirity.greencard.presentation.activities;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aspirity.greencard.GreenCardApplication;
import com.aspirity.greencard.R;
import com.aspirity.greencard.data.db.DatabaseFactory;
import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.google.firebase.iid.FirebaseInstanceId;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import io.reactivex.Single;

public class SplashActivity extends AppCompatActivity {

    public static final int FULL_PROGRESS = 100;
    private static final Handler progressSplashHandler = new Handler();
    private ProgressBar splashProgressBar;
    private int progress;
    private final Runnable changingProgressRunnable = new Runnable() {
        @Override
        public void run() {
            changeProgressMock();
            if (splashProgressBar.getProgress() == FULL_PROGRESS) {
                progressSplashHandler.removeCallbacks(changingProgressRunnable);
                Intent intent = new Intent(SplashActivity.this, AuthActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Pair<View, String> p1 = Pair.create(findViewById(R.id.text_green_card_logo_g_auth), "logo_g");
                    Pair<View, String> p2 = Pair.create(findViewById(R.id.text_green_card_logo_auth), "logo_green_card");
                    ActivityOptions options = ActivityOptions.
                            makeSceneTransitionAnimation(SplashActivity.this, p1, p2);
                    startActivity(intent, options.toBundle());
                } else {
                    startActivity(intent);
                }
                // TODO: 06.12.2017 поставил finish, так как убрал noHistory в manifest,
                // так как из за него при клике на иконку, даже если свернуто приложение, оно запускается заного.
                // ИЗ ЗА FINISH не работает анимация что ниже находится, т.е. FADE эффект, тут короче надо выбирать из двух зол.
                finish();
                SplashActivity.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }
    };
    private TextView peopleWantsMoreTextView;
    private PreferenceDataManager preferenceDataManager;

    @SuppressLint("CheckResult")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.SplashTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        preferenceDataManager = ((GreenCardApplication) getApplication()).getPreferenceDataManager();

        if (preferenceDataManager.getClear()) {
            Single.just(true).doOnSubscribe(t -> DatabaseFactory.getInstance().deleteAll());
            preferenceDataManager.setClear(false);
        }

//        copyPushToken();

        Log.i("mlg", "Token: " + preferenceDataManager.getToken());

        if (!preferenceDataManager.getToken().isEmpty() && preferenceDataManager.isActiveUser()) {
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            intent.putExtra("menuFragment", getIntent().getStringExtra("menuFragment"));
            startActivity(intent);
            finish();
        }
        progress = 0;

        initViews();
    }

    private void copyPushToken() {
        ClipboardManager mClipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        String token = FirebaseInstanceId.getInstance().getToken();

        if (mClipboardManager != null) {
            ClipData mClipData = ClipData.newPlainText("Token", token);
            mClipboardManager.setPrimaryClip(mClipData);
            Toast.makeText(this, "Token скопирован в буфер обмена", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        changeProgressMock();
    }

    @Override
    protected void onPause() {
        super.onPause();
        progressSplashHandler.removeCallbacks(changingProgressRunnable);
    }

    private void initViews() {
        splashProgressBar = findViewById(R.id.progress_splash);
        splashProgressBar.getProgressDrawable().setColorFilter(
                ResourcesCompat.getColor(getResources(), R.color.colorYellow, getTheme()),
                android.graphics.PorterDuff.Mode.SRC_IN);

        peopleWantsMoreTextView = findViewById(R.id.text_green_card_wants_more);
        peopleWantsMoreTextView.setText(getSpannableStringBuilder(), TextView.BufferType.SPANNABLE);
        peopleWantsMoreTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/proxima_nova_bold.otf"));
    }

    @NonNull
    private SpannableStringBuilder getSpannableStringBuilder() {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString neededText = new SpannableString(
                getResources().getString(R.string.splash_for_people_wants_more));
        neededText.setSpan(new ForegroundColorSpan(ResourcesCompat
                .getColor(getResources(), R.color.colorYellow, null)), 21, 26, 0);
        builder.append(neededText);
        return builder;
    }

    private void changeProgressMock() {
        splashProgressBar.setProgress(progress++);
        if (progressSplashHandler != null) {
            progressSplashHandler.postDelayed(changingProgressRunnable, 20);
        }
    }
}
