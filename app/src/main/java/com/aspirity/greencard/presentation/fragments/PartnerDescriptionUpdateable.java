package com.aspirity.greencard.presentation.fragments;

import com.aspirity.greencard.domain.model.Partner;

/**
 * Created by namtarr on 27.12.2017.
 */

public interface PartnerDescriptionUpdateable {

    void updateDescription(Partner partner);
}
