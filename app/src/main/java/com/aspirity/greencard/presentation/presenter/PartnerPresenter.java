package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.PartnersUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.PartnerContract;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.net.SocketTimeoutException;

import retrofit2.HttpException;

public class PartnerPresenter extends BasePresenter<PartnerContract.View>
        implements PartnerContract.Presenter {

    private final PreferenceDataManager preferenceDataManager;
    private final PartnersUseCase partnersUseCase;

    private long partnerId;

    public PartnerPresenter(
            PreferenceDataManager preferenceDataManager,
            PartnersUseCase partnersUseCase) {
        this.preferenceDataManager = preferenceDataManager;
        this.partnersUseCase = partnersUseCase;
    }

    @Override
    public void getPartner(Long partnerId) {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(partnersUseCase.getPartner(partnerId)
                .doOnSubscribe(disposable -> {
                    this.partnerId = partnerId;
                })
                .onErrorResumeNext(error -> {
                    handleError(error);
                    return partnersUseCase.getPartnerFallback(partnerId);
                })
                .subscribe(partner -> getView().showPartnerContent(partner), this::handleError));
    }

    @Override
    public void retryLastRequest() {
        getPartner(partnerId);
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                getView().showError(
                        ErrorParse.parseError(httpException.response()),
                        RequestError.CUSTOM);
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        } else {
            // generic error handling
        }
    }
}
