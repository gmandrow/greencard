package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.domain.model.BonusPercent;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.PartnerTableContract;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class PartnerTablePresenter extends BasePresenter<PartnerTableContract.View>
        implements PartnerTableContract.Presenter {

    @Override
    public void checkStatuses(List<BonusPercent> bonusPercents) {
        if (!isViewAttached()) return;
        //пока такой костыль, позже надо придумать заполнение таблицы полностью вместе со статусами
        //здесь мы берем из расчета что бонусы всегда идут по возрастающей, так как это логично.
        Collections.sort(bonusPercents, (o1, o2) -> (int) (o1.getPercents() - o2.getPercents()));
        getView().showMemberPercents(getStringPercents(bonusPercents.get(0).getPercents()));
        getView().showSilverPercents(getStringPercents(bonusPercents.get(1).getPercents()));
        getView().showGoldPercents(getStringPercents(bonusPercents.get(2).getPercents()));
        getView().showPlatinumPercents(getStringPercents(bonusPercents.get(3).getPercents()));
    }

    private String getStringPercents(Long percents) {
        return String.format(Locale.getDefault(), "%d%s", percents, "%");
    }
}
