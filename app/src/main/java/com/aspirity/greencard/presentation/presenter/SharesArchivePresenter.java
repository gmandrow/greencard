package com.aspirity.greencard.presentation.presenter;

import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.SharesUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.SharesArchiveContract;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.paginate.Paginate;

import java.net.SocketTimeoutException;

import retrofit2.HttpException;
import timber.log.Timber;

public class SharesArchivePresenter extends BasePresenter<SharesArchiveContract.View>
        implements SharesArchiveContract.Presenter {

    private PreferenceDataManager preferenceDataManager;
    private SharesUseCase sharesUseCase;

    private int totalItemsAmount = 0;
    private int currentItemsAmount = 0;
    private int page = 1;
    private boolean loading = false;

    private Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            getMoreShares();
        }

        @Override
        public boolean isLoading() {
            return loading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            if (currentItemsAmount >= totalItemsAmount) {
                getView().setHasLoadedAllItems();
                return true;
            }
            return false;
        }
    };

    public SharesArchivePresenter(
            PreferenceDataManager preferenceDataManager,
            SharesUseCase sharesUseCase) {
        this.preferenceDataManager = preferenceDataManager;
        this.sharesUseCase = sharesUseCase;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                getView().showError(
                        ErrorParse.parseError(httpException.response()),
                        RequestError.CUSTOM);
            }
        } else if (throwable instanceof SocketTimeoutException)
            getView().showError(null, RequestError.TIMEOUT);
        else if (throwable instanceof NetworkException)
            getView().showError(null, RequestError.NETWORK);
    }

    @Override
    public Paginate.Callbacks getCallbacks() {
        return callbacks;
    }

    @Override
    public void retryLastRequest(boolean forceRefresh) {
        getContent(forceRefresh);
    }

    @Override
    public void getContent(boolean forceRefresh) {
        if (!isViewAttached()) {
            return;
        }

        if (forceRefresh) {
            currentItemsAmount = 0;
            totalItemsAmount = 0;
            page = 1;
        }

        getCompositeDisposable().add(
                sharesUseCase.getArchivedShares(page)
                        .doOnSubscribe(disposable -> {
                            loading = true;

                            if (!forceRefresh)
                                getView().showLoading();
                        })
                        .onErrorResumeNext(error -> {
                            getView().hideLoading();
                            handleError(error);
                            return sharesUseCase.getArchivedSharesFallback(page, 30);
                        })
                        .subscribe(listPair -> {
                            totalItemsAmount = listPair.getCount();
                            if (forceRefresh) currentItemsAmount = listPair.getItems().size();
                            else currentItemsAmount += listPair.getItems().size();
                            getView().showSharesData(listPair.getItems(), listPair.getCount());
                            getView().hideLoading();
                            if (!listPair.getItems().isEmpty()) {
                                page++;
                            }
                            loading = false;
                        }, this::handleError)
        );
    }

    @Override
    public void getMoreShares() {
        if (!isViewAttached()) {
            return;
        }

        getCompositeDisposable().add(
                sharesUseCase.getArchivedShares(page)
                        .doOnSubscribe(disposable -> loading = true)
                        .onErrorResumeNext(error -> sharesUseCase.getArchivedSharesFallback(page, 30))
                        .subscribe(listPair -> {
                            getView().showMoreSharesData(listPair.getItems());
                            currentItemsAmount += listPair.getItems().size();

                            if (!listPair.getItems().isEmpty())
                                page++;

                            loading = false;
                        }, throwable -> {
                            loading = false;
                            Timber.w(throwable, throwable.getMessage());
                        })
        );
    }
}
