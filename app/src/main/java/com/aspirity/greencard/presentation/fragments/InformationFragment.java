package com.aspirity.greencard.presentation.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.usecase.FaqUseCase;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.presenter.FaqPresenter;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;

import androidx.annotation.Nullable;


public class InformationFragment extends BaseFragment {
    private WebView mWeb;

    public InformationFragment() {
    }

    public static InformationFragment newInstance() {
        return new InformationFragment();
    }


    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_information, container, false);
        onCreatePresenter(new FaqPresenter(
                        getGreenCardApp().getPreferenceDataManager(),
                        new FaqUseCase(
                                getGreenCardApp().getApplicationContext(),
                                new AppSchedulerProvider(),
                                RepositoryProvider.getFaqRepository())),
                this);
        initViews(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initViews(View view) {
        mWeb = view.findViewById(R.id.information_web);

        mWeb.setWebChromeClient(new WebChromeClient());
        mWeb.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        WebSettings mWebSettings = mWeb.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        mWebSettings.setLoadWithOverviewMode(true);
        mWebSettings.setUseWideViewPort(true);

        mWeb.loadUrl("http://green-bonus.ru/help/oferta/");
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
