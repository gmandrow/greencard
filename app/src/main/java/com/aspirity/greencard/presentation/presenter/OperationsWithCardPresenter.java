package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.domain.model.Card;
import com.aspirity.greencard.domain.usecase.CardsUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.OperationsWithCardContract;
import com.aspirity.greencard.presentation.model.ErrorWrapper;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.net.SocketTimeoutException;
import java.util.List;

import retrofit2.HttpException;

public class OperationsWithCardPresenter extends BasePresenter<OperationsWithCardContract.View>
        implements OperationsWithCardContract.Presenter {

    private RequestType lastRequest;
    private CardsUseCase cardsUseCase;
    private Card mainCard;
    private Long mainCardId;

    public OperationsWithCardPresenter(CardsUseCase cardsUseCase) {
        this.cardsUseCase = cardsUseCase;
    }

    @Override
    public void getCards() {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(cardsUseCase
                .getCards(false)
                .doOnSubscribe(disposable -> {
                    lastRequest = RequestType.GET_CARDS;
                    getView().showLoading();
                })
                .doOnSuccess(aBoolean -> getView().hideLoading())
                .onErrorResumeNext(error -> {
                    getView().hideLoading();
                    handleError(error);
                    return cardsUseCase.getCardsFallback();
                })
                .subscribe(cards -> {
                    mainCard = getMainCard(cards);

                    if (mainCard == null) return;

                    mainCardId = mainCard.getId();
                    getView().showMainCard(getView().getGreenCardApp()
                            .getDecimalFormatter()
                            .getNumberByFourNumeralAsString(Long.parseLong(mainCard.getNumber())));
                    if (mainCard.isLocked()) {
                        getView().updateOperationUnlock();
                    } else {
                        getView().updateOperationLock();
                    }
                }, this::handleError));
    }

    @Override
    public void checkData(String password, String cardNumber) {
        if (!isViewAttached()) return;

        if (CommonUtil.checkStringOnNullOrEmpty(password)) {
            getView().showPasswordError();
            return;
        }

        if (mainCardId == null) return;

        getCompositeDisposable().add(cardsUseCase
                .cardChangeState(password, mainCardId)
                .doOnSubscribe(disposable -> {
                    lastRequest = RequestType.BLOCK_CARD;
                    getView().hideErrors();
                    getView().showLoading();
                })
                .doOnSuccess(aBoolean -> getView().hideLoading())
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(aBoolean -> {
                    if (mainCard == null) return;

                    mainCard.setLocked(!mainCard.isLocked());
                    if (mainCard.isLocked()) {
                        getView().updateOperationUnlock();
                        getView().successOperationLock();
                    } else {
                        getView().updateOperationLock();
                        getView().successOperationUnlock();
                    }
                }, this::handleError));
    }

    @Override
    public RequestType getLastRequest() {
        return lastRequest;
    }

    private Card getMainCard(List<Card> cards) {
        for (Card card : cards) {
            if (card.isMain()) return card;
        }
        return null;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                ErrorWrapper wrapper = ErrorParse.parseFormErrors(httpException.response());
                String generalError = wrapper.getGeneralError();
                if (CommonUtil.checkStringOnNullOrEmpty(generalError)) {
                    getView().showFieldErrors(wrapper.getFieldErrors());
                } else {
                    getView().showError(generalError, RequestError.CUSTOM);
                }
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        }
    }
}
