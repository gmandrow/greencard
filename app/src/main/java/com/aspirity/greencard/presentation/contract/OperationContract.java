package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.ListItem;
import com.aspirity.greencard.domain.model.Operation;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.util.List;

public interface OperationContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showOperationData(Operation operation);

        void showOperationProducts(List<ListItem> products);
    }

    interface Presenter extends MvpPresenter<OperationContract.View> {

        void getOperation(String uniqueId);

        void retryLastRequest();
    }
}
