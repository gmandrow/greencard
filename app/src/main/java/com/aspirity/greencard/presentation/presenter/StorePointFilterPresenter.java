package com.aspirity.greencard.presentation.presenter;

import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.domain.usecase.PartnersUseCase;
import com.aspirity.greencard.domain.usecase.VendorsUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.StorePointFilterContract;
import com.aspirity.greencard.presentation.model.SelectableVendor;
import com.aspirity.greencard.presentation.model.StorePointDataWrapper;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.HttpException;

public class StorePointFilterPresenter extends BasePresenter<StorePointFilterContract.View>
        implements StorePointFilterContract.Presenter {

    private PartnersUseCase partnersUseCase;
    private VendorsUseCase vendorsUseCase;

    public StorePointFilterPresenter(PartnersUseCase partnersUseCase, VendorsUseCase vendorsUseCase) {
        this.partnersUseCase = partnersUseCase;
        this.vendorsUseCase = vendorsUseCase;
    }

    private void handleError(Throwable throwable) {
        throwable.printStackTrace();
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                String error = ErrorParse.parseError(httpException.response());
                getView().showError(error, RequestError.CUSTOM);
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        }
    }

    @Override
    public void getVendors(List<SelectableVendor> alreadySelected) {
        if (!isViewAttached()) return;

        Observable<List<Partner>> partners = partnersUseCase
                .getPartners()
                .toObservable();

        Observable<List<SelectableVendor>> vendors = vendorsUseCase
                .getVendors()
                .toObservable()
                .onErrorResumeNext(error -> {
                    handleError(error);
                    return vendorsUseCase.getVendorsFallback().toObservable();
                })
                .flatMap(Observable::fromIterable)
                .map(vendor -> new SelectableVendor(vendor, false))
                .toList()
                .toObservable();

        Observable<List<SelectableVendor>> alreadySelectedObservable =
                alreadySelected != null ?
                        Observable.just(alreadySelected) : Observable.just(new ArrayList<>());

        Observable<StorePointDataWrapper> dataWrapper =
                Observable.combineLatest(partners, vendors, alreadySelectedObservable, StorePointDataWrapper::new)
                        .doOnSubscribe(disposable -> getView().showLoading())
                        .doOnError(disposable -> getView().hideLoading());

        getCompositeDisposable().add(
                dataWrapper.subscribe(data -> {
                    getView().hideLoading();
                    getView().loadFilter(data);
                }, this::handleError)
        );
    }
}
