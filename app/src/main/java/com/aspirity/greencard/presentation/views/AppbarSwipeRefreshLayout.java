package com.aspirity.greencard.presentation.views;

/**
 * Created by namtarr on 29.12.2017.
 */

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.material.appbar.AppBarLayout;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class AppbarSwipeRefreshLayout extends SwipeRefreshLayout implements AppBarLayout.OnOffsetChangedListener {

    private AppBarLayout appBar;

    public AppbarSwipeRefreshLayout(Context context) {
        super(context);
    }

    public AppbarSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setAppBar(AppBarLayout appBar) {
        this.appBar = appBar;
        if (appBar != null) {
            appBar.addOnOffsetChangedListener(this);
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        setEnabled(verticalOffset == 0);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (appBar != null) {
            appBar.removeOnOffsetChangedListener(this);
            appBar = null;
        }
    }
}