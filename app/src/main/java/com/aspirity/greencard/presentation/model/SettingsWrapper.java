package com.aspirity.greencard.presentation.model;

public class SettingsWrapper {

    private boolean email;
    private boolean push;
    private boolean push2;
    private boolean sms;

    public SettingsWrapper() {
    }

    public SettingsWrapper(boolean email, boolean push, boolean push2, boolean sms) {
        this.email = email;
        this.push = push;
        this.push2 = push2;
        this.sms = sms;
    }

    public boolean emailSetting() {
        return email;
    }

    public void setEmail(boolean email) {
        this.email = email;
    }

    public boolean pushSetting() {
        return push;
    }

    public void setPush(boolean push) {
        this.push = push;
    }

    public boolean push2Setting() {
        return push2;
    }

    public void setPush2(boolean push2) {
        this.push2 = push2;
    }

    public boolean smsSetting() {
        return sms;
    }

    public void setSms(boolean sms) {
        this.sms = sms;
    }
}
