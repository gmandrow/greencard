package com.aspirity.greencard.presentation.utils;


import java.util.regex.Pattern;

public enum RegExpHelper {
    PHONE("^[+]?[0-9]{11,13}$"),

    EMAIL(".+@.+\\.+.+"),

    DATE("(0[1-9]|[12][0-9]|3[01])\\.(0[1-9]|1[012])\\.(19|20)\\d\\d");

    private final Pattern pattern;

    RegExpHelper(String regexp) {
        this.pattern = Pattern.compile(regexp);
    }

    public static boolean isMatches(RegExpHelper helper, CharSequence value) {
        return helper.getPattern().matcher(value).matches();
    }

    public Pattern getPattern() {
        return pattern;
    }
}
