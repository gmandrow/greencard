package com.aspirity.greencard.presentation.views;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;

import androidx.core.widget.NestedScrollView;


public class ReboundNestedScrollView extends NestedScrollView {
    private static final float MOVE_DELAY = 0.4f;// Drag coefficient when pulling out screen
    private static final int ANIM_TIME = 500;// Resilience time
    private static final double FLING = 1.3;//fling  coefficient

    private View childView;
    private boolean isMoved;

    private Rect originalRect = new Rect();

    private float startY;

    public ReboundNestedScrollView(Context context, AttributeSet attrs,
                                   int defStyle) {
        super(context, attrs, defStyle);
    }

    public ReboundNestedScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ReboundNestedScrollView(Context context) {
        super(context);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (getChildCount() > 0) {
            childView = getChildAt(0);
        }
    }

    @Override
    public void fling(int velocityY) {
        super.fling((int) (velocityY / FLING));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

        if (childView == null)
            return;

        originalRect.set(childView.getLeft(), childView.getTop(),
                childView.getRight(), childView.getBottom());
    }

    /**
     * In touch events ,  Handle pull and drop logic
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (childView == null) {
            return super.dispatchTouchEvent(ev);
        }

        int action = ev.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                startY = ev.getY();
                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (!isMoved)
                    break;

                TranslateAnimation anim = new TranslateAnimation(0, 0,
                        childView.getTop(), originalRect.top);
                anim.setDuration(ANIM_TIME);

                childView.startAnimation(anim);
                //  Set flag back to false
                isMoved = false;
                resetViewLayout();
                break;
            case MotionEvent.ACTION_MOVE:

                float nowY = ev.getY();
                int deltaY = (int) (nowY - startY);
                int offset = (int) (deltaY * MOVE_DELAY);
                childView.layout(originalRect.left, originalRect.top + offset,
                        originalRect.right, originalRect.bottom + offset);

                isMoved = true;
                break;
            default:
                break;
        }

        return super.dispatchTouchEvent(ev);
    }

    public void resetViewLayout() {
        childView.layout(originalRect.left, originalRect.top,
                originalRect.right, originalRect.bottom);
    }

}
