package com.aspirity.greencard.presentation.presenter;

import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.FamilyAccountRulesContract;
import com.aspirity.greencard.presentation.model.ErrorWrapper;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;

import java.net.SocketTimeoutException;

import retrofit2.HttpException;

public class FamilyAccountRulesPresenter extends BasePresenter<FamilyAccountRulesContract.View>
        implements FamilyAccountRulesContract.Presenter {

    private PreferenceDataManager preferenceDataManager;
    private UserUseCase userUseCase;
    private RequestType lastRequest;

    public FamilyAccountRulesPresenter(
            PreferenceDataManager preferenceDataManager,
            UserUseCase userUseCase) {
        this.preferenceDataManager = preferenceDataManager;
        this.userUseCase = userUseCase;
    }

    @Override
    public void loadRulesFamilyAccount() {
        if (!isViewAttached()) return;

        getCompositeDisposable().add(userUseCase.getFamilyAccountRules()
                .doOnSubscribe(disposable -> {
                    lastRequest = RequestType.LOAD_RULES_FAMILY_ACCOUNT;
                    getView().showLoading();
                })
                .doOnSuccess(booleanStringPair -> getView().hideLoading())
                .doOnError(throwable -> getView().hideLoading())
                .subscribe(oferta -> getView().showRulesFamilyAccount(oferta), this::handleError));
    }

    @Override
    public RequestType getLastRequest() {
        return lastRequest;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                ErrorWrapper wrapper = ErrorParse.parseFormErrors(httpException.response());
                String generalError = wrapper.getGeneralError();
                if (CommonUtil.checkStringOnNullOrEmpty(generalError)) {
                } else {
                    getView().showError(generalError, RequestError.CUSTOM);
                }
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        }
    }

}
