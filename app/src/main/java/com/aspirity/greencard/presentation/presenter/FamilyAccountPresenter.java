package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.domain.usecase.CardsUseCase;
import com.aspirity.greencard.domain.usecase.UserUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.FamilyAccountContract;
import com.aspirity.greencard.presentation.model.ErrorWrapper;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;

import java.net.SocketTimeoutException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import retrofit2.HttpException;

public class FamilyAccountPresenter extends BasePresenter<FamilyAccountContract.View>
        implements FamilyAccountContract.Presenter {

    private final UserUseCase userUseCase;
    private RequestType lastRequest;
    private Long userId;
    private CardsUseCase cardsUseCase;
    private boolean isValid;

    public FamilyAccountPresenter(CardsUseCase cardsUseCase, UserUseCase userUseCase) {
        this.cardsUseCase = cardsUseCase;
        this.userUseCase = userUseCase;
    }

    @Override
    public void getCards(boolean forceRefresh) {
        if (!isViewAttached())
            return;

        getCompositeDisposable().add(
                cardsUseCase
                        .getCards(forceRefresh)
                        .doOnSubscribe(disposable -> {
                            lastRequest = RequestType.GET_CARDS;
                            lastRequest.forceRefresh = forceRefresh;

                            if (!forceRefresh)
                                getView().showLoading();
                        })
                        .onErrorResumeNext(error -> {
                            handleError(error);
                            return cardsUseCase.getCardsFallback();
                        })
                        .doOnSuccess(aBoolean -> getView().hideLoading())
                        .subscribe(cards -> getView().showCards(cards), this::handleError));
    }

    @Override
    public void checkCard(String cardNumber, String cardCode) {
        if (!isViewAttached())
            return;

        isValid = true;
        checkCardData(cardNumber, cardCode);

        if (isValid)
            searchUserByCard(cardNumber, cardCode);
    }

    private void checkCardData(String cardNumber, String cardCode) {
        if (CommonUtil.checkStringOnNullOrEmpty(cardNumber)) {
            getView().showCardNumberError();
            isValid = false;
        }

        if (!CommonUtil.checkStringOnNullOrEmpty(cardNumber) && cardNumber.length() < 15) {
            getView().showCardNumberLengthError();
            isValid = false;
        }

        if (CommonUtil.checkStringOnNullOrEmpty(cardCode)) {
            getView().showCardCodeError();
            isValid = false;
        }

        if (!CommonUtil.checkStringOnNullOrEmpty(cardCode) && cardCode.length() < 4) {
            getView().showCardCodeLengthError();
            isValid = false;
        }
    }

    @Override
    public void searchUserByCard(String cardNumber, String cardCode) {
        if (!isViewAttached())
            return;

        getCompositeDisposable().add(userUseCase.searchUserByCard(cardNumber, cardCode)
                .doOnSubscribe(disposable -> {
                    lastRequest = RequestType.SEARCH_USER_BY_CARD;
                    lastRequest.cardNumber = cardNumber;
                    lastRequest.cardCode = cardCode;
                    getView().hideErrors();
                    if (!getView().isShowingDialog()) getView().showLoading();
                })
                .doOnError(throwable -> getView().hideLoading())
                .doOnSuccess(user -> getView().hideLoading())
                .subscribe(user -> {
                    userId = user.getId();
                    getView().showUserData(user.getFullName(), checkPhone(user.getPhone()));
                }, this::handleError));
    }

    @Override
    public void getVerificationCode() {
        if (!isViewAttached())
            return;

        if (userId == null)
            return;

        getCompositeDisposable().add(cardsUseCase.attachCard(userId)
                .doOnSubscribe(disposable -> {
                    lastRequest = RequestType.ATTACH_CARD;
                    getView().hideErrors();
                    if (!getView().isShowingDialog()) getView().showLoading();
                })
                .doOnError(throwable -> getView().hideLoading())
                .doOnSuccess(s -> getView().hideLoading())
                .subscribe(s -> getView().showToastSendCode(s), this::handleError));
    }

    @Override
    public void onClickGetVerificationCode(String cardNumber, String cardCode) {
        if (!isViewAttached())
            return;

        checkCardData(cardNumber, cardCode);

        if (isValid)
            getVerificationCode();
    }

    @Override
    public void verifyAttachedCard(String smsCode) {
        if (!isViewAttached())
            return;

        boolean isValid = true;

        if (CommonUtil.checkStringOnNullOrEmpty(smsCode)) {
            getView().showSmsCodeError();
            isValid = false;
        }

        if (userId == null)
            return;

        if (isValid)
            getCompositeDisposable().add(cardsUseCase.verifyAttachedCard(userId, smsCode)
                    .doOnSubscribe(disposable -> {
                        lastRequest = RequestType.VERIFY_ATTACHED_CARD;
                        lastRequest.verificationCode = smsCode;
                        getView().hideErrors();
                        if (!getView().isShowingDialog()) getView().showLoading();
                    })
                    .doOnError(throwable -> getView().hideLoading())
                    .subscribe(aBoolean -> {
                        getCards(false);
                        getView().showSuccess();
                    }, this::handleError));
    }

    @Override
    public void onClickVerifyAttachedCard(Observable<Object> clicks) {
        if (!isViewAttached())
            return;

        getCompositeDisposable().add(clicks.debounce(200, TimeUnit.MILLISECONDS)
                .subscribeOn(new AppSchedulerProvider().ui())
                .observeOn(new AppSchedulerProvider().ui())
                .subscribe(o -> getView().clickAddCard(), this::handleError));
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached())
            return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                ErrorWrapper wrapper = ErrorParse.parseFormErrors(httpException.response());
                String generalError = wrapper.getGeneralError();
                if (CommonUtil.checkStringOnNullOrEmpty(generalError)) {
                    getView().showFieldErrors(wrapper.getFieldErrors());
                } else {
                    getView().showError(generalError, RequestError.CUSTOM);
                }
            }
        } else if (throwable instanceof SocketTimeoutException)
            getView().showError(null, RequestError.TIMEOUT);
        else if (throwable instanceof NetworkException)
            getView().showError(null, RequestError.NETWORK);
    }

    private String checkPhone(String phone) {
        if (phone.contains("+"))
            return phone;
        else
            return String.format(Locale.getDefault(), "%s%s", "+", phone);
    }

    @Override
    public void retryLastRequest() {
        switch (lastRequest) {
            case GET_CARDS:
                getCards(lastRequest.forceRefresh);
                break;
            case SEARCH_USER_BY_CARD:
                checkCard(lastRequest.cardNumber, lastRequest.cardCode);
                break;
            case ATTACH_CARD:
                getVerificationCode();
                break;
            case VERIFY_ATTACHED_CARD:
                verifyAttachedCard(lastRequest.verificationCode);
                break;
        }
    }

    private enum RequestType {
        GET_CARDS,
        SEARCH_USER_BY_CARD,
        ATTACH_CARD,
        VERIFY_ATTACHED_CARD;

        private boolean forceRefresh;

        private String cardNumber;
        private String cardCode;
        private String verificationCode;
    }
}
