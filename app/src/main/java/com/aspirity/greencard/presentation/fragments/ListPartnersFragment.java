package com.aspirity.greencard.presentation.fragments;

import android.animation.Animator;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.domain.usecase.PartnersUseCase;
import com.aspirity.greencard.presentation.adapters.PartnerAdapter;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.ListPartnersContract;
import com.aspirity.greencard.presentation.dialogs.ErrorDialog;
import com.aspirity.greencard.presentation.presenter.ListPartnersPresenter;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.FragmentUtil;
import com.aspirity.greencard.presentation.utils.ViewUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;
import com.aspirity.greencard.presentation.views.SimpleDividerItemDecoration;
import com.google.android.material.appbar.AppBarLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


public class ListPartnersFragment
        extends BaseFragment<ListPartnersContract.View, ListPartnersContract.Presenter>
        implements ListPartnersContract.View {

    private RecyclerView partnersRecycler;
    private AppBarLayout appbar;
    private AppBarLayout.OnOffsetChangedListener offsetChangedListener;
    private View loadingProgress;
    private LottieAnimationView plusImage;
    private PartnerAdapter adapter;
    private PartnerAdapter.PartnerListener listener;
    private TextView listPartnersTitle;

    private PublishSubject<Boolean> playedAnimation = PublishSubject.create();
    private PublishSubject<Boolean> showLoading = PublishSubject.create();

    public static ListPartnersFragment newInstance(boolean isRestore) {
        Bundle args = new Bundle();
        args.putBoolean(Constants.ARG_IS_RESTORE, isRestore);
        ListPartnersFragment listPartnersFragment = new ListPartnersFragment();
        listPartnersFragment.setArguments(args);
        return listPartnersFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        SchedulerProvider schedulerProvider = new AppSchedulerProvider();
        onCreatePresenter(new ListPartnersPresenter(
                        getGreenCardApp().getPreferenceDataManager(),
                        new PartnersUseCase(
                                getGreenCardApp().getApplicationContext(),
                                schedulerProvider,
                                RepositoryProvider.getPartnerRepository(schedulerProvider))),
                this);
        listener = partner -> {
            PartnerFragment fragmentPartner = PartnerFragment.newInstance(
                    partner.getId(),
                    partner.getColor(),
                    partner.getLogoDetailSrc());
            FragmentUtil.setTransitionSlideForTwoFragment(ListPartnersFragment.this, fragmentPartner);
            FragmentUtil.addBackStackWithAddFragment(getActivity().getSupportFragmentManager(), fragmentPartner);
        };
        adapter = new PartnerAdapter(
                context.getApplicationContext(),
                new LinkedList<>(),
                listener);

        Observable<Boolean> playedAnimationObservable = playedAnimation.toFlowable(BackpressureStrategy.LATEST).toObservable();
        Observable<Boolean> showLoadingObservable = showLoading.toFlowable(BackpressureStrategy.LATEST).toObservable();
        Observable<Boolean> showLoadingAfterAnimation = Observable
                .combineLatest(playedAnimationObservable, showLoadingObservable, (played, show) -> show);
        getCompositeDisposable().add(
                showLoadingAfterAnimation.subscribe(show -> loadingProgress.setVisibility(show ? View.VISIBLE : View.INVISIBLE))
        );

        getPresenter().getPartners();
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_partner, container, false);
        initViews(view);
        plusImage.playAnimation();
        plusImage.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                playedAnimation.onNext(true);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //partnersRecycler.setAdapter(null);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    @Override
    public void showLoading() {
        showLoading.onNext(true);
    }

    @Override
    public void hideLoading() {
        showLoading.onNext(false);
    }

    private void initViews(View view) {
        listPartnersTitle = view.findViewById(R.id.text_list_partner_title);
        plusImage = view.findViewById(R.id.image_plus);
        loadingProgress = view.findViewById(R.id.loading_progress);
        appbar = view.findViewById(R.id.appbar_partners);

        initAppBarListener();

        partnersRecycler = view.findViewById(R.id.recycler_partners);
        partnersRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        partnersRecycler.addItemDecoration(new SimpleDividerItemDecoration(
                getContext(),
                R.drawable.divider_partners_list));
        partnersRecycler.setAdapter(adapter);
    }

    private void initAppBarListener() {
        offsetChangedListener = (appBarLayout, verticalOffset) -> {
            ViewUtil.setTranslationX(-verticalOffset * 2, plusImage);
            ViewUtil.setTranslationY(verticalOffset, 1, listPartnersTitle);
        };
        appbar.addOnOffsetChangedListener(offsetChangedListener);
    }

    @Override
    public void showError(String message, RequestError requestError) {
        // TODO: 12.09.2017 пока описываю это в каждом фрагменте,
        // если в будущем не нужно будет менять данный метод, то перенесу его в Base.

        ErrorDialog errorDialog = new ErrorDialog(getContext());
        getCompositeDisposable().addAll(
                errorDialog.closeAction().subscribe(),
                errorDialog.retryAction().subscribe(obj ->
                        getPresenter().retryLastRequest())
        );
        errorDialog.setErrorDescription(requestError, message);
        errorDialog.show();
    }

    @Override
    public void showPartnersData(List<Partner> partners) {
        adapter.addData(partners);
        animationShowOnlyVertical(partnersRecycler);
    }
}
