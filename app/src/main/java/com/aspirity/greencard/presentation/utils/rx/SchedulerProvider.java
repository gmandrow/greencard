package com.aspirity.greencard.presentation.utils.rx;


import io.reactivex.Scheduler;

public interface SchedulerProvider {

    Scheduler computation();

    Scheduler io();

    Scheduler newThread();

    Scheduler trampoline();

    Scheduler ui();
}
