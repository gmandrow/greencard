package com.aspirity.greencard.presentation.fragments;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aspirity.greencard.R;
import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.utils.CommonUtil;
import com.aspirity.greencard.presentation.utils.Constants;

import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import androidx.annotation.Nullable;


public class PartnerBonusDescriptionFragment extends BaseFragment
        implements PartnerDescriptionUpdateable {

    private HtmlTextView description;

    public static PartnerBonusDescriptionFragment newInstance(String bonusesDescription) {
        Bundle args = new Bundle();
        args.putString(Constants.ARG_BONUSES_DESCRIPTION_PARTNER, bonusesDescription);
        PartnerBonusDescriptionFragment fragment = new PartnerBonusDescriptionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_partner_bonus_description, container, false);
        description = view.findViewById(R.id.text_description);
        String content = getArguments().getString(Constants.ARG_BONUSES_DESCRIPTION_PARTNER);
        if (!CommonUtil.checkStringOnNullOrEmpty(content)) {
            description.setHtml(content, new HtmlHttpImageGetter(description, "http://www.green-bonus.ru"));
            description.setMovementMethod(LinkMovementMethod.getInstance());
        }
        return view;
    }

    @Override
    public void updateDescription(Partner partner) {
        Bundle newArguments = new Bundle();
        newArguments.putString(Constants.ARG_BONUSES_DESCRIPTION_PARTNER, partner.getBonusDescription());
        setArguments(newArguments);
        if (description != null) {
            String content = partner.getBonusDescription();
            if (!CommonUtil.checkStringOnNullOrEmpty(content)) {
                description.setHtml(content, new HtmlHttpImageGetter(description, "http://www.green-bonus.ru"));
                description.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }
}
