package com.aspirity.greencard.presentation.fragments;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.aspirity.greencard.GreenCardApplication;
import com.aspirity.greencard.R;
import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.VirtualCard;
import com.aspirity.greencard.domain.usecase.CardsUseCase;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.VirtualCardContract;
import com.aspirity.greencard.presentation.presenter.VirtualCardPresenter;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.aspirity.greencard.presentation.views.CustomFontTextView;

import androidx.annotation.Nullable;


public class VirtualCardFragment extends BaseFragment<VirtualCardContract.View, VirtualCardContract.Presenter>
        implements VirtualCardContract.View {
    private CustomFontTextView barcode;
    private CustomFontTextView barcode2;
    private CustomFontTextView number;
    private ObjectAnimator animation;

    private float last_brightness;

    private Dialog errorDialog;
    private TextView messageTextView;
    private TextView titleErrorTextView;

    private PreferenceDataManager mPref;

    public static VirtualCardFragment newInstance() {
        return new VirtualCardFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onCreatePresenter(new VirtualCardPresenter(
                ((GreenCardApplication) getActivity().getApplication()).getPreferenceDataManager(),
                new CardsUseCase(context.getApplicationContext(),
                        new AppSchedulerProvider(),
                        RepositoryProvider.getCardsRepository())), this);
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_virtual_card, container, false);
        initViews(view);
        initErrorDialog();
        getPresenter().getVirtualCard();
        mPref = ((GreenCardApplication) getActivity().getApplication()).getPreferenceDataManager();
        getBrightness();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        last_brightness = mPref.getLastBrightness();
        animationShowForSomeView(animShow);
        setBrightness(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (errorDialog == null)
            initErrorDialog();
    }

    private void initViews(View view) {
        barcode = view.findViewById(R.id.image_virtual_card);
        barcode2 = view.findViewById(R.id.image_virtual_card2);
        number = view.findViewById(R.id.virtual_card_number);

        PreferenceDataManager mPref = ((GreenCardApplication) getActivity().getApplication()).getPreferenceDataManager();
        String number = mPref.getVirtualCardNumber();
        String hash = mPref.getVirtualCardHash();

        if (!number.isEmpty() && !hash.isEmpty())
            showVirtualCard(new VirtualCard(number, hash));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        setBrightness(false);
        animation = null;
    }

    private void initErrorDialog() {
        errorDialog = new Dialog(getActivity());
        errorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        errorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        errorDialog.setContentView(R.layout.dialog_error);
        errorDialog.setCanceledOnTouchOutside(false);
        titleErrorTextView = errorDialog.findViewById(R.id.text_name_error);
        messageTextView = errorDialog.findViewById(R.id.text_message_error);
        errorDialog.setOnShowListener(dialogInterface -> {
            // TODO: 16.11.2017 малый костыль, так как с каких то щей диалог открывается 2 раза, пока не понял почему
            if (errorDialog.isShowing())
                errorDialog.dismiss();
        });
        TextView closeActionTextView = errorDialog.findViewById(R.id.text_close_dialog);
        closeActionTextView.setOnClickListener(view -> errorDialog.dismiss());
        TextView repeatActionTextView = errorDialog.findViewById(R.id.text_action_repeat);
        repeatActionTextView.setOnClickListener(view -> {
            errorDialog.dismiss();
            getPresenter().getVirtualCard();
        });
    }

    @Override
    public void showError(String message, RequestError requestError) {
        switch (requestError) {
            case TIMEOUT:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_timeout);
                if (!errorDialog.isShowing()) errorDialog.show();
                break;
            case CUSTOM:
                titleErrorTextView.setText(R.string.error_title);
                messageTextView.setText(message);
                if (!errorDialog.isShowing()) errorDialog.show();
                break;
            case NETWORK:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_not_connection_network);
                if (!errorDialog.isShowing()) errorDialog.show();
                break;
        }
    }

    @Override
    public void showVirtualCard(VirtualCard virtualCard) {
        barcode.setText(virtualCard.getHash());
        barcode2.setText(virtualCard.getHash());
        number.setText(getGreenCardApp().getDecimalFormatter().getNumberByFourNumeralAsString(Long.parseLong(virtualCard.getNumber())));
    }

    private void getBrightness() {
        if (getActivity() != null) {
            WindowManager.LayoutParams lp = getActivity().getWindow().getAttributes();
            last_brightness = lp.screenBrightness;
            mPref.setLastBrightness(last_brightness);
        }
    }

    private void setBrightness(boolean max) {
        if (getActivity() != null) {
            WindowManager.LayoutParams lp = getActivity().getWindow().getAttributes();
            lp.screenBrightness = max ? WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL : last_brightness;
            getActivity().getWindow().setAttributes(lp);
        }
    }
}
