package com.aspirity.greencard.presentation.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.aspirity.greencard.R;
import com.aspirity.greencard.data.repository.RepositoryProvider;
import com.aspirity.greencard.domain.model.ErrorEvent;
import com.aspirity.greencard.domain.model.Faq;
import com.aspirity.greencard.domain.usecase.FaqUseCase;
import com.aspirity.greencard.presentation.adapters.QuestionAdapter;
import com.aspirity.greencard.presentation.base.BaseFragment;
import com.aspirity.greencard.presentation.contract.FaqContract;
import com.aspirity.greencard.presentation.presenter.FaqPresenter;
import com.aspirity.greencard.presentation.utils.FragmentUtil;
import com.aspirity.greencard.presentation.utils.ViewUtil;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;
import com.jakewharton.rxbinding2.support.v4.widget.RxNestedScrollView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import timber.log.Timber;


public class FaqFragment extends BaseFragment<FaqContract.View, FaqContract.Presenter>
        implements FaqContract.View {

    private NestedScrollView questionsNestedScrollView;
    private View titleQuestions;
    private List<Faq> questions = new LinkedList<>();
    private QuestionAdapter.QuestionListener listener;
    private QuestionAdapter adapter;
    private RecyclerView questionsRecyclerView;
    private Dialog errorDialog;
    private TextView titleErrorTextView;
    private TextView messageTextView;

    public FaqFragment() {
    }

    public static FaqFragment newInstance() {
        return new FaqFragment();
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_faq, container, false);
        onCreatePresenter(new FaqPresenter(
                        getGreenCardApp().getPreferenceDataManager(),
                        new FaqUseCase(
                                getGreenCardApp().getApplicationContext(),
                                new AppSchedulerProvider(),
                                RepositoryProvider.getFaqRepository())),
                this);
        initViews(view);
        createAdapterAndListener();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        createAdapterAndListener();

        if (errorDialog == null) {
            initErrorDialog();
        }

        getPresenter().getFaq();
    }

    @Override
    public void onResume() {
        super.onResume();
        //((MainActivity) getActivity()).changeNavigationButton((false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorEvent(ErrorEvent event) {
        showError(event.message, event.requestError);
    }

    private void initViews(View view) {
        View titleFaq = view.findViewById(R.id.text_title_faq);
        titleQuestions = view.findViewById(R.id.text_title_questions);
        questionsNestedScrollView = view.findViewById(R.id.nested_scroll_faq);
        getCompositeDisposable().add(RxNestedScrollView.scrollChangeEvents(questionsNestedScrollView)
                .subscribe(viewScrollChangeEvent -> {
                    ViewUtil.setTranslationX(-viewScrollChangeEvent.scrollY(), titleQuestions);
                    ViewUtil.setTranslationY(-viewScrollChangeEvent.scrollY() * 2, titleFaq);
                }, throwable -> Timber.w(throwable, throwable.getMessage())));

        questionsRecyclerView = view.findViewById(R.id.recycler_questions);
        questionsRecyclerView.setNestedScrollingEnabled(false);
        questionsRecyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity().getApplicationContext()));
    }

    private void initErrorDialog() {
        errorDialog = new Dialog(getActivity());
        errorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        errorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        errorDialog.setContentView(R.layout.dialog_error);
        errorDialog.setCanceledOnTouchOutside(false);
        titleErrorTextView = errorDialog.findViewById(R.id.text_name_error);
        messageTextView = errorDialog.findViewById(R.id.text_message_error);
        TextView closeActionTextView = errorDialog.findViewById(R.id.text_close_dialog);
        closeActionTextView.setOnClickListener(view -> {
            errorDialog.dismiss();
            hideLoading();
        });
        TextView repeatActionTextView = errorDialog.findViewById(R.id.text_action_repeat);
        repeatActionTextView.setOnClickListener(view -> {
            errorDialog.dismiss();
            getPresenter().getFaq();
        });
    }

    @Override
    public void onStop() {
        super.onStop();
//        questionsNestedScrollView.scrollTo(0, 0);
        //questionsRecyclerView.setAdapter(null);
        adapter = null;
        listener = null;
    }

    @Override
    public void showError(String message, RequestError requestError) {
        switch (requestError) {
            case TIMEOUT:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_timeout);
                if (!errorDialog.isShowing()) errorDialog.show();
                break;
            case CUSTOM:
                titleErrorTextView.setText(R.string.error_title);
                messageTextView.setText(message);
                if (!errorDialog.isShowing()) errorDialog.show();
                break;
            case NETWORK:
                titleErrorTextView.setText(R.string.error_title_network);
                messageTextView.setText(R.string.error_message_not_connection_network);
                if (!errorDialog.isShowing()) errorDialog.show();
                break;
        }
    }

    @Override
    public void showFaq(List<Faq> faqList) {
        if (adapter != null) {
            titleQuestions.setVisibility(View.VISIBLE);
            adapter.addData(faqList);
            animation(titleQuestions, animShowHorizontal);
            animation(questionsNestedScrollView, animShow);
        }
    }

    private void createAdapterAndListener() {
        if (listener == null || adapter == null) {
            listener = question -> {
                AnswerFragment answerFragment = AnswerFragment.newInstance(question);
                FragmentUtil.setTransitionSlideForTwoFragment(this, answerFragment);
                FragmentUtil.addBackStackWithAddFragment(
                        getActivity().getSupportFragmentManager(),
                        answerFragment);
            };
            adapter = new QuestionAdapter(
                    questions,
                    listener);
            questionsRecyclerView.setAdapter(adapter);
        }
    }
}
