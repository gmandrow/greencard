package com.aspirity.greencard.presentation.presenter;


import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.aspirity.greencard.domain.usecase.SharesUseCase;
import com.aspirity.greencard.presentation.base.BasePresenter;
import com.aspirity.greencard.presentation.contract.ListSharesContract;
import com.aspirity.greencard.presentation.model.ShareFilterWrapper;
import com.aspirity.greencard.presentation.utils.Constants;
import com.aspirity.greencard.presentation.utils.error.ErrorParse;
import com.aspirity.greencard.presentation.utils.error.NetworkException;
import com.aspirity.greencard.presentation.utils.error.RequestError;
import com.paginate.Paginate;

import java.net.SocketTimeoutException;

import retrofit2.HttpException;
import timber.log.Timber;

public class ListSharesPresenter extends BasePresenter<ListSharesContract.View>
        implements ListSharesContract.Presenter {

    private PreferenceDataManager preferenceDataManager;
    private SharesUseCase sharesUseCase;

    private ShareFilterWrapper filter = new ShareFilterWrapper();

    private RequestType lastRequest = null;
    private int totalItemsAmount = 0;
    private int currentItemsAmount = 0;
    private int page = 1;
    private boolean loading = false;

    private Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            getMoreShares();
        }

        @Override
        public boolean isLoading() {
            return loading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            if (currentItemsAmount >= totalItemsAmount) {
                getView().setHasLoadedAllItems();
                return true;
            }
            return false;
        }
    };

    public ListSharesPresenter(
            PreferenceDataManager preferenceDataManager,
            SharesUseCase sharesUseCase) {
        this.preferenceDataManager = preferenceDataManager;
        this.sharesUseCase = sharesUseCase;
    }

    private void handleError(Throwable throwable) {
        if (!isViewAttached()) return;

        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int statusCode = httpException.code();
            if (statusCode == Constants.STATUS_BAD_REQUEST) {
                getView().showError(
                        ErrorParse.parseError(httpException.response()),
                        RequestError.CUSTOM);
            }
        } else if (throwable instanceof SocketTimeoutException) {
            getView().showError(null, RequestError.TIMEOUT);
        } else if (throwable instanceof NetworkException) {
            getView().showError(null, RequestError.NETWORK);
        } else {
            // generic error handling
        }
    }

    @Override
    public void getContent() {
        if (!isViewAttached()) {
            return;
        }

        getCompositeDisposable().add(
                sharesUseCase.getShares(page, filter)
                        .doOnSubscribe(disposable -> {
                            loading = true;
                            lastRequest = RequestType.FILTERED_SHARES;
                            getView().showLoading();
                        })
                        .onErrorResumeNext(error -> {
                            getView().hideLoading();
                            handleError(error);
                            return sharesUseCase.getSharesFallback(page, 30);
                        })
                        .subscribe(listPair -> {
                            totalItemsAmount = listPair.getCount();
                            currentItemsAmount += listPair.getItems().size();
                            getView().showSharesData(listPair.getItems(), listPair.getCount());
                            getView().playStartAnimation();
                            getView().hideLoading();
                            if (!listPair.getItems().isEmpty()) {
                                page++;
                            }
                            loading = false;
                        }, this::handleError)
        );
    }

    @Override
    public void getShares(boolean restart) {
        if (!isViewAttached()) {
            return;
        }

        if (restart) {
            currentItemsAmount = 0;
            totalItemsAmount = 0;
            page = 1;
        }

        getCompositeDisposable().add(
                sharesUseCase.getShares(page, filter)
                        .doOnSubscribe(disposable -> {
                            loading = true;
                            lastRequest = RequestType.FILTERED_SHARES;
                            if (restart) {
                                getView().showLoading();
                            }
                        })
                        .onErrorResumeNext(error -> sharesUseCase.getSharesFallback(page, 30))
                        .subscribe(listPair -> {
                            if (restart) {
                                totalItemsAmount = listPair.getCount();
                                getView().showSharesData(listPair.getItems(), listPair.getCount());
                                getView().hideLoading();
                            } else {
                                getView().showMoreSharesData(listPair.getItems());
                            }

                            currentItemsAmount += listPair.getItems().size();
                            if (!listPair.getItems().isEmpty()) {
                                page++;
                            }
                            loading = false;
                        }, this::handleError)
        );
    }

    @Override
    public void getSharesOfPartner(Long partnerId) {
        if (!isViewAttached()) {
            return;
        }

        getCompositeDisposable().add(sharesUseCase.getSharesOfPartner(partnerId)
                .doOnSubscribe(disposable -> {
                    loading = true;
                    lastRequest = RequestType.PARTNER_SHARES;
                    lastRequest.partnerId = partnerId;
                    getView().showLoading();
                })
                .onErrorResumeNext(error -> {
                    getView().hideLoading();
                    handleError(error);
                    return sharesUseCase.getSharesOfPartnerFallback(partnerId);
                })
                .subscribe(listShares -> {
                    getView().showSharesData(listShares.getItems(), listShares.getCount());
                    getView().hideLoading();
                    loading = false;
                }, this::handleError));
    }


    @Override
    public void retryLastRequest() {
        switch (lastRequest) {
            case FILTERED_SHARES:
                getShares(false);
                return;
            case PARTNER_SHARES:
                getSharesOfPartner(lastRequest.partnerId);
        }
    }

    @Override
    public Paginate.Callbacks getCallbacks() {
        return callbacks;
    }

    @Override
    public ShareFilterWrapper getCurrentFilter() {
        return filter;
    }

    private void getMoreShares() {
        if (!isViewAttached()) {
            return;
        }

        getCompositeDisposable().add(
                sharesUseCase.getShares(page, filter)
                        .doOnSubscribe(disposable -> {
                            loading = true;
                            lastRequest = RequestType.FILTERED_SHARES;
                        })
                        .onErrorResumeNext(error -> sharesUseCase.getSharesFallback(page, 30))
                        .subscribe(listPair -> {
                            getView().showMoreSharesData(listPair.getItems());

                            currentItemsAmount += listPair.getItems().size();
                            if (!listPair.getItems().isEmpty()) {
                                page++;
                            }
                            loading = false;
                        }, throwable -> {
                            loading = false;
                            Timber.w(throwable, throwable.getMessage());
                        })
        );
    }

    private enum RequestType {
        FILTERED_SHARES,
        PARTNER_SHARES;

        long partnerId;
    }
}
