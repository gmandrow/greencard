package com.aspirity.greencard.presentation.contract;


import com.aspirity.greencard.domain.model.VirtualCard;
import com.aspirity.greencard.presentation.base.MvpPresenter;
import com.aspirity.greencard.presentation.base.MvpView;
import com.aspirity.greencard.presentation.utils.RequestType;
import com.aspirity.greencard.presentation.utils.error.RequestError;

public interface VirtualCardContract {

    interface View extends MvpView {

        void showError(String message, RequestError requestError);

        void showVirtualCard(VirtualCard virtualCard);
    }

    interface Presenter extends MvpPresenter<VirtualCardContract.View> {

        void getVirtualCard();

        RequestType getLastRequest();
    }
}
