package com.aspirity.greencard.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "operation", indices = @Index(value = "unique_id", unique = true))
public class OperationEntity {
    public static final String PRIMARY_KEY = "uniqueId";

    @ColumnInfo(name = "id")
    @SerializedName("id")
    private Long id;

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "unique_id")
    @SerializedName("unique_id")
    private String uniqueId;

    @ColumnInfo(name = "accrued_bonuses")
    @SerializedName("accrued_bonuses")
    private String accruedBonuses;

    @ColumnInfo(name = "address")
    @SerializedName("address")
    private String address;

    @ColumnInfo(name = "bonuses")
    @SerializedName("bonuses")
    private String bonuses;

    @ColumnInfo(name = "date_of")
    @SerializedName("date_of")
    private Long dateOf;

    @ColumnInfo(name = "debited_bonuses")
    @SerializedName("debited_bonuses")
    private String debitedBonuses;

    @ColumnInfo(name = "partner_id")
    @SerializedName("partner_id")
    private Long partnerId;

    @ColumnInfo(name = "partner_color")
    @SerializedName("partner_color")
    private String partnerColor;

    @ColumnInfo(name = "partner_logo_src")
    @SerializedName("partner_logo_src")
    private String partnerLogoSrc;

    @ColumnInfo(name = "partner_logo_black_bg_src")
    @SerializedName("partner_logo_black_bg_src")
    private String partnerLogoBlackBgSrc;

    @ColumnInfo(name = "partner_name")
    @SerializedName("partner_name")
    private String partnerName;

    @Ignore
    @SerializedName("products")
    private List<ProductEntity> productEntities;

    @ColumnInfo(name = "total_price")
    @SerializedName("total_price")
    private String totalPrice;

    @ColumnInfo(name = "type")
    @SerializedName("type")
    private String type;

    @ColumnInfo(name = "vendor_name")
    @SerializedName("vendor_name")
    private String vendorName;

    public OperationEntity() {
    }

    public String getAccruedBonuses() {
        return accruedBonuses;
    }

    public void setAccruedBonuses(String accruedBonuses) {
        this.accruedBonuses = accruedBonuses;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBonuses() {
        return bonuses;
    }

    public void setBonuses(String bonuses) {
        this.bonuses = bonuses;
    }

    public Long getDateOf() {
        return dateOf;
    }

    public void setDateOf(Long dateOf) {
        this.dateOf = dateOf;
    }

    public String getDebitedBonuses() {
        return debitedBonuses;
    }

    public void setDebitedBonuses(String debitedBonuses) {
        this.debitedBonuses = debitedBonuses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerColor() {
        return partnerColor;
    }

    public void setPartnerColor(String partnerColor) {
        this.partnerColor = partnerColor;
    }

    public String getPartnerLogoSrc() {
        return partnerLogoSrc;
    }

    public void setPartnerLogoSrc(String partnerLogoSrc) {
        this.partnerLogoSrc = partnerLogoSrc;
    }

    public String getPartnerLogoBlackBgSrc() {
        return partnerLogoBlackBgSrc;
    }

    public void setPartnerLogoBlackBgSrc(String partnerLogoBlackBgSrc) {
        this.partnerLogoBlackBgSrc = partnerLogoBlackBgSrc;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public List<ProductEntity> getProductEntities() {
        return productEntities;
    }

    public void setProductEntities(List<ProductEntity> productEntities) {
        this.productEntities = productEntities;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }
}
