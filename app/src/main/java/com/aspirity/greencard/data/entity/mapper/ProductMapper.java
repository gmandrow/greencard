package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.ProductEntity;
import com.aspirity.greencard.domain.model.Product;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class ProductMapper implements Function<ProductEntity, Product> {

    @Override
    public Product apply(@NonNull ProductEntity productEntity) {
        return new Product(
                productEntity.getName(),
                productEntity.getCount(),
                productEntity.getPrice(),
                productEntity.getTotalPrice(),
                productEntity.getAccruedBonuses(),
                productEntity.getDebitedBonuses());
    }
}
