package com.aspirity.greencard.data.entity.response;


import com.google.gson.annotations.SerializedName;

import androidx.annotation.Nullable;

public class ChangedCardResponse {

    @SerializedName("is_changed")
    private Boolean isChanged;

    @Nullable
    @SerializedName("non_field_errors")
    private String errorMessage;

    public Boolean isChanged() {
        return isChanged;
    }

    public void setIsChanged(Boolean isChanged) {
        this.isChanged = isChanged;
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(@Nullable String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
