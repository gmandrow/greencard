package com.aspirity.greencard.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

/**
 * Created by namtarr on 21.12.2017.
 */

@Entity(tableName = "count")
public class CountEntity {

    @PrimaryKey
    private long id;

    @ColumnInfo(name = "count")
    private int count;

    public CountEntity() {
    }

    @Ignore
    public CountEntity(long id, int count) {
        this.id = id;
        this.count = count;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
