package com.aspirity.greencard.data.entity;

import com.google.gson.annotations.SerializedName;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "bonus_percent",
        foreignKeys = @ForeignKey(
                entity = PartnerEntity.class,
                parentColumns = "id",
                childColumns = "partner_id",
                onDelete = ForeignKey.CASCADE)
)
public class BonusPercentEntity {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;

    @ColumnInfo(name = "partner_id", index = true)
    private long partnerId;

    @ColumnInfo(name = "percents")
    @SerializedName("percents")
    private Long percents;

    @ColumnInfo(name = "status")
    @SerializedName("status")
    private String status;

    public BonusPercentEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(long partnerId) {
        this.partnerId = partnerId;
    }

    public Long getPercents() {
        return percents;
    }

    public void setPercents(Long percents) {
        this.percents = percents;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
