package com.aspirity.greencard.data.db.dao;

import com.aspirity.greencard.data.entity.OperationEntity;
import com.aspirity.greencard.data.entity.ProductEntity;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Single;

@Dao
public interface OperationDao {
    @Query("SELECT * FROM operation ORDER BY date_of DESC LIMIT :limit OFFSET :offset")
    Single<List<OperationEntity>> getOperations(int offset, int limit);

    @Query("SELECT count(*) FROM operation")
    Single<Integer> getOperationCount();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertOperations(List<OperationEntity> operations);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateOperation(OperationEntity operation);

    @Query("DELETE FROM operation")
    void deleteOperations();

    @Query("DELETE FROM product")
    void deleteProducts();

    @Query("SELECT * FROM operation WHERE unique_id = :uniqueId")
    Single<OperationEntity> getOperation(String uniqueId);

    @Query("SELECT * FROM product WHERE operation_id = :operationId")
    Single<List<ProductEntity>> getProducts(String operationId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProducts(List<ProductEntity> products);
}