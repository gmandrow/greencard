package com.aspirity.greencard.data.db.dao;

import com.aspirity.greencard.data.entity.ShareEntity;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Single;

@Dao
public interface ShareDao {
    @Query("SELECT min(update_date) FROM share WHERE is_archived = :archived AND update_date = " +
            "(SELECT update_date FROM share WHERE is_archived = :archived ORDER BY rowid LIMIT :limit OFFSET :offset)")
    Single<Long> getListOldestDate(int offset, int limit, boolean archived);

    @Query("SELECT * FROM share WHERE is_archived = :archived ORDER BY rowid LIMIT :limit OFFSET :offset")
    Single<List<ShareEntity>> getItems(int offset, int limit, boolean archived);

    @Query("SELECT * FROM share WHERE id = :id")
    Single<ShareEntity> getItem(long id);

    @Query("DELETE FROM share WHERE is_archived = :archived")
    void deleteShares(boolean archived);

    // SHARES OF PARTNER

    @Query("SELECT min(update_date) FROM share WHERE update_date = (SELECT update_date FROM share WHERE partner_id = :partnerId AND is_archived = 0 ORDER BY date_start DESC)")
    Single<Long> getPartnerListOldestDate(long partnerId);

    @Query("SELECT count(is_archived) FROM share WHERE is_archived = (SELECT is_archived FROM share WHERE is_archived = 0)")
    Single<Integer> getShareCount();

    @Query("SELECT * FROM share WHERE is_archived = 0 AND partner_id = :partnerId ORDER BY date_start DESC")
    Single<List<ShareEntity>> getItemsOfPartner(long partnerId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertShares(List<ShareEntity> shares);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateShare(ShareEntity share);
}
