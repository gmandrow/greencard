package com.aspirity.greencard.data.network;


import com.aspirity.greencard.data.entity.PartnerEntity;
import com.aspirity.greencard.data.entity.response.ListResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PartnerService {

    /**
     * Get partners
     *
     * @param page     - page of partners - it may is null
     * @param pageSize - count of partners on one page - it may is null
     * @return list of partners
     */
    @GET("partners/")
    Single<ListResponse<PartnerEntity>> partners(
            @Query("page") Integer page,
            @Query("page_size") Integer pageSize);

    /**
     * Get one partner
     *
     * @param partnerId - id of partner
     * @return partner
     */
    @GET("partners/{partner_id}")
    Single<PartnerEntity> partner(
            @Path("partner_id") Long partnerId);
}
