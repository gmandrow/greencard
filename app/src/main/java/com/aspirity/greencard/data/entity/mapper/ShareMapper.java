package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.ShareEntity;
import com.aspirity.greencard.domain.model.Share;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class ShareMapper implements Function<ShareEntity, com.aspirity.greencard.domain.model.Share> {

    @Override
    public com.aspirity.greencard.domain.model.Share apply(@NonNull ShareEntity shareEntity) {
        return new Share(
                shareEntity.getId(),
                shareEntity.isSpecial(),
                shareEntity.getContent(),
                shareEntity.getDateEnd(),
                shareEntity.getImageSrc(),
                shareEntity.getImageHorizontalSrc(),
                shareEntity.getImageVerticalSrc(),
                shareEntity.getPartnerColor(),
                shareEntity.getPartnerId(),
                shareEntity.getPartnerLogoSrc(),
                shareEntity.getTitle(),
                shareEntity.getVendors());
    }

}
