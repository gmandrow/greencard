package com.aspirity.greencard.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

/**
 * Created by namtarr on 25.12.2017.
 */

@Entity(tableName = "photo",
        foreignKeys = @ForeignKey(
                entity = PartnerEntity.class,
                parentColumns = "id",
                childColumns = "partner_id",
                onDelete = ForeignKey.CASCADE)
)
public class PhotoEntity {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;

    @ColumnInfo(name = "partner_id", index = true)
    private long partnerId;

    @ColumnInfo(name = "url")
    private String url;

    public PhotoEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(long partnerId) {
        this.partnerId = partnerId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
