package com.aspirity.greencard.data.repository;

import com.aspirity.greencard.data.entity.mapper.NotificationsMapper;
import com.aspirity.greencard.data.entity.response.ListResponse;
import com.aspirity.greencard.data.network.NotificationsService;
import com.aspirity.greencard.data.network.RetrofitService;
import com.aspirity.greencard.domain.model.Chunk;
import com.aspirity.greencard.domain.model.Notification;
import com.aspirity.greencard.domain.repository.NotificationsRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class NotificationsDataRepository implements NotificationsRepository {
    private NotificationsService notificationsService = RetrofitService.createRetrofitClient(NotificationsService.class);

    @Override
    public Single<Chunk<Notification>> notifications(String token) {
        Single<ListResponse<Notification>> response = notificationsService
                .notifications(token)
                .cache();

        Single<Integer> count = response.map(ListResponse::getTotalCount);
        Single<List<Notification>> notifications = response
                .map(ListResponse::getResults)
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new NotificationsMapper())
                .toList();

        return Single.zip(notifications, count, Chunk::new);
    }

    @Override
    public Single<Void> notificationsStatus(Long notification_id, String token, String status) {
        return notificationsService.notificationsStatus(notification_id, token, status);
    }
}
