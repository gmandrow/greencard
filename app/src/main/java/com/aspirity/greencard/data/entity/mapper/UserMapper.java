package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.UserEntity;
import com.aspirity.greencard.domain.model.User;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class UserMapper implements Function<UserEntity, com.aspirity.greencard.domain.model.User> {
    @Override
    public User apply(@NonNull UserEntity userEntity) {
        return new User(
                userEntity.getId(),
                userEntity.getName(),
                userEntity.getFullName(),
                userEntity.getPhone(),
                userEntity.getBonuses(),
                userEntity.getStatus(),
                userEntity.getBonusesForNextStatus(),
                userEntity.getCardNumber(),
                userEntity.isSubscribedToPushOperations(),
                userEntity.isSubscribedToPush(),
                userEntity.isSubscribedToEmail(),
                userEntity.isSubscribedToSms(),
                userEntity.isActive());
    }
}
