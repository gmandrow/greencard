package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.VendorEntity;
import com.aspirity.greencard.domain.model.Vendor;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class VendorMapper implements Function<VendorEntity, Vendor> {

    @Override
    public Vendor apply(@NonNull VendorEntity vendorEntity) {
        return new Vendor(
                vendorEntity.getId(),
                vendorEntity.getAddress(),
                vendorEntity.getEmail(),
                vendorEntity.getLatitude(),
                vendorEntity.getLongitude(),
                vendorEntity.getPartnerColor(),
                vendorEntity.getPartnerId(),
                vendorEntity.getPartnerLogoSrc(),
                vendorEntity.getName(),
                vendorEntity.getPartnerName(),
                vendorEntity.getPhone(),
                vendorEntity.getStoreId());
    }
}
