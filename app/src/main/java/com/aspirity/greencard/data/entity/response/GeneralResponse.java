package com.aspirity.greencard.data.entity.response;


import com.google.gson.annotations.SerializedName;

import androidx.annotation.Nullable;

public class GeneralResponse<T> {

    @SerializedName("results")
    private T results;

    @SerializedName("next")
    private String nextPageUrl;

    @SerializedName("count")
    private Integer totalCount;

    @Nullable
    @SerializedName("non_field_errors")
    private String errorMessage;

    public T getResults() {
        return results;
    }

    public void setResults(T results) {
        this.results = results;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(@Nullable String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
