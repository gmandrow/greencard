package com.aspirity.greencard.data.network;


import com.aspirity.greencard.data.entity.OperationEntity;
import com.aspirity.greencard.data.entity.response.ListResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OperationService {

    /**
     * Get operations
     *
     * @param page - page of operations - it may is null
     *             //@param pageSize - count of operations on one page - it may is null
     * @return list of operations
     */
    @GET("operations/")
    Single<ListResponse<OperationEntity>> operations(
            @Query("page") Integer page,
            @Query("page_size") Integer pageSize);

    @GET("operations/")
    Single<ListResponse<OperationEntity>> operations(
            @Query("page") Integer page,
            @Query("page_size") Integer pageSize,
            @Query("date_of_0") String beginDate,
            @Query("date_of_1") String endDate,
            @Query("o") String sort,
            @Query("vendor_id") List<Long> vendors);

    /**
     * Get one operation
     *
     * @param operationId - id of operation
     * @return operation
     */
    @GET("operations/{operation_id}")
    Single<OperationEntity> operation(
            @Path("operation_id") String operationId);
}
