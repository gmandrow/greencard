package com.aspirity.greencard.data.repository;

import com.aspirity.greencard.data.db.DatabaseFactory;
import com.aspirity.greencard.data.db.dao.CountDao;
import com.aspirity.greencard.data.db.dao.VendorDao;
import com.aspirity.greencard.data.entity.CountEntity;
import com.aspirity.greencard.data.entity.mapper.VendorMapper;
import com.aspirity.greencard.data.entity.response.ListResponse;
import com.aspirity.greencard.data.network.RetrofitService;
import com.aspirity.greencard.data.network.VendorService;
import com.aspirity.greencard.domain.model.Vendor;
import com.aspirity.greencard.domain.repository.VendorsRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class VendorsDataRepository implements VendorsRepository {
    private final VendorService vendorService = RetrofitService.createRetrofitClient(VendorService.class);
    private final VendorDao vendorDao = DatabaseFactory.getInstance().vendorDao();
    private final CountDao countDao = DatabaseFactory.getInstance().countDao();

    // Проверяем, если в базе есть запись о количестве вендоров, значит, мы уже загружали всех разом
    // например, для фильтра, поэтому далее можно использовать их
    private Single<Boolean> didLoadAllItems() {
        return countDao
                .getCount(CountDao.VENDOR_COUNT_ID)
                .map(count -> true)
                .onErrorResumeNext(error -> Single.just(false));
    }

    @Override
    public Single<List<Vendor>> vendors() {
        return vendorsForceRefresh();
    }

    @Override
    public Single<List<Vendor>> vendorsForceRefresh() {
        return vendorService
                .vendors(null, null)
                .doOnSuccess(vendors -> {
                    vendorDao.deleteVendors();
                    vendorDao.insertVendors(vendors.getResults());
                    countDao.insertCount(new CountEntity(CountDao.VENDOR_COUNT_ID, vendors.getTotalCount()));
                })
                .map(ListResponse::getResults)
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new VendorMapper())
                .toList();
    }

    @Override
    public Single<List<Vendor>> vendorsFromDb() {
        return vendorDao
                .getVendors()
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new VendorMapper())
                .toList();
    }

    @Override
    public Single<List<Vendor>> vendorsOfPartner(long partnerId) {
        return vendorsOfPartnerForceRefresh(partnerId);
    }

    // Если обновляется список вендоров партнера, возможно, что весь список более невалиден
    // Поэтому здесь удаляется общее количество вендоров, чтобы в следующий раз при запросе всех
    // они были загружены заново
    @Override
    public Single<List<Vendor>> vendorsOfPartnerForceRefresh(long partnerId) {
        return vendorService
                .vendorsOfPartner(partnerId, null, null)
                .map(ListResponse::getResults)
                .doOnSuccess(list -> {
                    countDao.deleteCount(CountDao.VENDOR_COUNT_ID);
                    vendorDao.insertVendors(list);
                })
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new VendorMapper())
                .toList();
    }

    @Override
    public Single<List<Vendor>> vendorsOfPartnerFromDb(long partnerId) {
        return vendorDao
                .getVendorsOfPartner(partnerId)
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new VendorMapper())
                .toList();
    }
}
