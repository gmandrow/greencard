package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.PhotoEntity;

import io.reactivex.functions.Function;

/**
 * Created by namtarr on 26.12.2017.
 */

public class PhotoMapper implements Function<PhotoEntity, String> {
    @Override
    public String apply(PhotoEntity photoEntity) {
        return photoEntity.getUrl();
    }
}
