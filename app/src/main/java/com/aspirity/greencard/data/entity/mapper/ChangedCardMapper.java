package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.response.ChangedCardResponse;
import com.aspirity.greencard.domain.model.ChangedCard;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class ChangedCardMapper implements Function<ChangedCardResponse, ChangedCard> {
    @Override
    public ChangedCard apply(@NonNull ChangedCardResponse changedCardResponse) {
        return new ChangedCard(changedCardResponse.isChanged());
    }
}
