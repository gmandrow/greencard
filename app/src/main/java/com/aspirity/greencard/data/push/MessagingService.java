package com.aspirity.greencard.data.push;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.PowerManager;

import com.aspirity.greencard.R;
import com.aspirity.greencard.presentation.activities.SplashActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import androidx.core.app.NotificationCompat;

public class MessagingService extends FirebaseMessagingService {
    private int notify_id = 0;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        boolean result = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH && powerManager.isInteractive() || Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT_WATCH && powerManager.isScreenOn();

        if (!result) {
            PowerManager.WakeLock wl = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MH24_SCREENLOCK");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MH24_SCREENLOCK");
            wl_cpu.acquire(10000);
        }

        String title = remoteMessage.getData().get("title");
        String text = remoteMessage.getData().get("text");
        String image_url = remoteMessage.getData().get("image_url");
        String miniature_image_url = remoteMessage.getData().get("miniature_image_url");
        Bitmap image = null;
        Bitmap miniature_image = null;
        try {
            image = Picasso.with(this).load(image_url).get();
            miniature_image = Picasso.with(this).load(miniature_image_url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent notifyIntent = new Intent(this, SplashActivity.class);
        notifyIntent.putExtra("menuFragment", "notifications");
        notifyIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent notifyPendingIntent = PendingIntent.getActivity(this, 0, notifyIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "greencard")
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(image)
                        .bigLargeIcon(null))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(text))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setSmallIcon(R.drawable.ic_notifications)
                .setContentIntent(notifyPendingIntent)
                .setLargeIcon(miniature_image)
                .setTicker(title)
                .setContentTitle(title)
                .setContentText(text)
                .setColor(0xFF8CC800)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel("greencard", "greencard", NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.setLightColor(0xFF8CC800);
                notificationChannel.enableVibration(true);
                notificationManager.createNotificationChannel(notificationChannel);
            }

            notificationManager.notify(notify_id++, notificationBuilder.build());
        }
    }
}
