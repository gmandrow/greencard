package com.aspirity.greencard.data.network;

import com.aspirity.greencard.data.prefs.PreferenceDataManager;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitService {
    private static final String BASE_URL = "http://api.green-bonus.ru/";
    private static Retrofit client = null;
    private final OkHttpClient httpClient;
    private final PreferenceDataManager preferenceDataManager;

    public RetrofitService(PreferenceDataManager preferenceDataManager) {
        this.preferenceDataManager = preferenceDataManager;
        httpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    Request.Builder ongoing = chain.request().newBuilder();
                    if (!getToken().isEmpty() && request.headers("@").isEmpty()) {
                        ongoing.addHeader("Authorization", "Token " + getToken());
                    }
                    return chain.proceed(ongoing.build());
                })
                .build();


        client = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)
                .build();
    }

    public static <T> T createRetrofitClient(final Class<T> service) {
        return client.create(service);
    }

    private String getToken() {
        return preferenceDataManager.getToken();
    }
}
