package com.aspirity.greencard.data.db;

import com.aspirity.greencard.BuildConfig;
import com.aspirity.greencard.data.db.dao.CardDao;
import com.aspirity.greencard.data.db.dao.CountDao;
import com.aspirity.greencard.data.db.dao.FaqDao;
import com.aspirity.greencard.data.db.dao.OperationDao;
import com.aspirity.greencard.data.db.dao.PartnerDao;
import com.aspirity.greencard.data.db.dao.ShareDao;
import com.aspirity.greencard.data.db.dao.UserDao;
import com.aspirity.greencard.data.db.dao.VendorDao;
import com.aspirity.greencard.data.entity.BonusPercentEntity;
import com.aspirity.greencard.data.entity.CardEntity;
import com.aspirity.greencard.data.entity.CountEntity;
import com.aspirity.greencard.data.entity.FaqEntity;
import com.aspirity.greencard.data.entity.OperationEntity;
import com.aspirity.greencard.data.entity.PartnerEntity;
import com.aspirity.greencard.data.entity.PhotoEntity;
import com.aspirity.greencard.data.entity.ProductEntity;
import com.aspirity.greencard.data.entity.ShareEntity;
import com.aspirity.greencard.data.entity.UserEntity;
import com.aspirity.greencard.data.entity.VendorEntity;

import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {
        ShareEntity.class,
        CountEntity.class,
        UserEntity.class,
        PartnerEntity.class,
        BonusPercentEntity.class,
        PhotoEntity.class,
        VendorEntity.class,
        OperationEntity.class,
        ProductEntity.class,
        FaqEntity.class,
        CardEntity.class
}, version = BuildConfig.VERSION_CODE, exportSchema = false)
public abstract class Database extends RoomDatabase {

    public abstract ShareDao shareDao();

    public abstract UserDao userDao();

    public abstract PartnerDao partnerDao();

    public abstract VendorDao vendorDao();

    public abstract CountDao countDao();

    public abstract OperationDao operationDao();

    public abstract FaqDao faqDao();

    public abstract CardDao cardDao();

    public void deleteAll() {
        countDao().deleteCounts();
        shareDao().deleteShares(false);
        shareDao().deleteShares(true);
        userDao().deleteUser();
        partnerDao().deletePartners();
        vendorDao().deleteVendors();
        operationDao().deleteOperations();
        operationDao().deleteProducts();
        faqDao().deleteFaq();
        cardDao().deleteCards();
    }
}
