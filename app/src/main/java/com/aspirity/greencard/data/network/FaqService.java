package com.aspirity.greencard.data.network;


import com.aspirity.greencard.data.entity.FaqEntity;
import com.aspirity.greencard.data.entity.response.ListResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface FaqService {

    @GET("faq/")
    Single<ListResponse<FaqEntity>> faq();

    @GET("faq/{faq_id}")
    Single<FaqEntity> faq(
            @Path("faq_id") Long faqId);
}
