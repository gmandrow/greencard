package com.aspirity.greencard.data.db.dao;

import com.aspirity.greencard.data.entity.CountEntity;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Single;

@Dao
public interface CountDao {
    long SHARE_COUNT_ID = 0L;
    long VENDOR_COUNT_ID = 1L;
    long SHARE_ARCHIVE_COUNT_ID = 2L;
    long PARTNER_COUNT_ID = 3L;

    @Query("SELECT * FROM count WHERE id = :id")
    Single<CountEntity> getCount(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCount(CountEntity count);

    @Query("DELETE FROM count WHERE id = :id")
    void deleteCount(long id);

    @Query("DELETE FROM count")
    void deleteCounts();
}
