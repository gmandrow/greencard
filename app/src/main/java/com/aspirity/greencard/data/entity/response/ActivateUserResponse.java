package com.aspirity.greencard.data.entity.response;


import com.google.gson.annotations.SerializedName;

public class ActivateUserResponse {

    @SerializedName("phone")
    private String phone;

    @SerializedName("card_num")
    private String cardNumber;

    @SerializedName("is_active")
    private Boolean isActive;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Boolean isActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
