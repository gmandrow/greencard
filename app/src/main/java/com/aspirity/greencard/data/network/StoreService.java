package com.aspirity.greencard.data.network;


import com.aspirity.greencard.data.entity.StoreEntity;
import com.aspirity.greencard.data.entity.response.ListResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface StoreService {

    /**
     * Get stores
     *
     * @param page     - page of stores - it may is null
     * @param pageSize - count of stores on one page - it may is null
     * @return list of stores
     */
    @GET("stores/")
    Single<ListResponse<StoreEntity>> stores(
            @Query("page") Integer page,
            @Query("page_size") Integer pageSize);

    /**
     * Get one store
     *
     * @param storeId - id of store
     * @return store
     */
    @GET("stores/{store_id}")
    Single<StoreEntity> store(
            @Path("store_id") Long storeId);
}
