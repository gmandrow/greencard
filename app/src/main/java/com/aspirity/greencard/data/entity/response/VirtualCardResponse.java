package com.aspirity.greencard.data.entity.response;


import com.google.gson.annotations.SerializedName;

public class VirtualCardResponse {

    @SerializedName("number")
    private String number;

    @SerializedName("hash")
    private String hash;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
