package com.aspirity.greencard.data.repository;

import com.aspirity.greencard.data.db.DatabaseFactory;
import com.aspirity.greencard.data.db.dao.UserDao;
import com.aspirity.greencard.data.entity.UserEntity;
import com.aspirity.greencard.data.entity.mapper.OfertaMapper;
import com.aspirity.greencard.data.entity.mapper.UserMapper;
import com.aspirity.greencard.data.entity.response.ActivateUserResponse;
import com.aspirity.greencard.data.entity.response.AuthResponse;
import com.aspirity.greencard.data.network.RetrofitService;
import com.aspirity.greencard.data.network.UserService;
import com.aspirity.greencard.domain.model.Oferta;
import com.aspirity.greencard.domain.model.User;
import com.aspirity.greencard.domain.repository.UserRepository;

import io.reactivex.Single;

public class UserDataRepository implements UserRepository {
    private final UserService userService = RetrofitService.createRetrofitClient(UserService.class);
    private final UserDao userDao = DatabaseFactory.getInstance().userDao();

    @Override
    public Single<User> registration(
            String cardNumber,
            String cardCode,
            String firstName,
            String gender,
            String birthDate,
            String phone,
            String email,
            Boolean agreement) {
        return userService
                .registration(
                        cardNumber,
                        cardCode,
                        firstName,
                        gender,
                        birthDate,
                        phone,
                        email,
                        agreement)
                .map(new UserMapper());
    }

    @Override
    public Single<String> auth(String phone, String password) {
        return userService.auth(phone, password)
                .map(AuthResponse::getToken);
    }

    @Override
    public Single<Boolean> restorePassword(String phone) {
        return userService.restorePassword(phone)
                .map(jsonObject -> jsonObject.has("status")
                        && jsonObject.get("status").getAsString()
                        .equalsIgnoreCase("OK"));
    }

    @Override
    public Single<Boolean> validateSmsCode(String smsCode, String phone) {
        return userService.validateSmsCode(smsCode, phone)
                .map(jsonObject -> jsonObject.has("status")
                        && jsonObject.get("status").getAsString()
                        .equalsIgnoreCase("OK"));
    }

    @Override
    public Single<Boolean> setNewPassword(String phone, String smsCode, String password) {
        return userService.setNewPassword(phone, smsCode, password)
                .map(jsonObject -> jsonObject.has("status")
                        && jsonObject.get("status").getAsString()
                        .equalsIgnoreCase("OK"));
    }

    @Override
    public Single<User> changePassword(String password) {
        return userService.changePassword(password)
                .map(new UserMapper());
    }

    @Override
    public Single<User> updateUserSubscribed(
            Boolean isSubscribedPushOperations,
            Boolean isSubscribedPush,
            Boolean isSubscribedEmail,
            Boolean isSubscribedSms) {
        Single<UserEntity> userEntitySingle = userService
                .updateUserSubscribed(
                        isSubscribedPushOperations,
                        isSubscribedPush,
                        isSubscribedEmail,
                        isSubscribedSms)
                .cache();

        Single<User> userSingle = userEntitySingle
                .map(new UserMapper());

        Single<UserEntity> saveUserSingle = userEntitySingle
                .flatMap(userEntity -> userDao.getUser())
                .doOnSuccess(userEntity -> {
                    userEntity.setSubscribedToPushOperations(isSubscribedPushOperations);
                    userEntity.setSubscribedToPush(isSubscribedPush);
                    userEntity.setSubscribedToEmail(isSubscribedEmail);
                    userEntity.setSubscribedToSms(isSubscribedSms);
                    userDao.updateUser(userEntity);
                });

        return userSingle;
    }

    @Override
    public Single<User> searchUserByCardNumber(String cardNumber, String cardCode) {
        return userService.searchByCardNumber(cardNumber, cardCode)
                .map(new UserMapper());
    }

    @Override
    public Single<User> getUser() {
        return getUserForceRefresh();
    }

    @Override
    public Single<User> getUserForceRefresh() {
        return userService
                .getUser()
                .doOnSuccess(userEntity -> {
                    if (userEntity.isActive()) {
                        userDao.insert(userEntity);
                    }
                })
                .map(new UserMapper());
    }

    @Override
    public Single<User> getUserFromDb() {
        return userDao.getUser()
                .map(new UserMapper());
    }

    @Override
    public Single<Oferta> getOferta() {
        return userService.getOferta()
                .map(new OfertaMapper());
    }

    @Override
    public Single<Oferta> getFamilyAccountRules() {
        return userService.getFamilyAccountRules()
                .map(new OfertaMapper());
    }

    @Override
    public Single<Boolean> activateUser() {
        return userService.activate()
                .map(ActivateUserResponse::isActive);
    }

    @Override
    public Single<Boolean> logOut() {
        return Single.just(true).doOnSubscribe(t -> DatabaseFactory.getInstance().deleteAll());
    }
}
