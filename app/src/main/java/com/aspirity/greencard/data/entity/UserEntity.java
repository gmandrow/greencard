package com.aspirity.greencard.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "user")
public class UserEntity {
    public static final String PRIMARY_KEY = "id";

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private Long id;

    @Nullable
    @ColumnInfo(name = "first_name")
    @SerializedName("first_name")
    private String name;

    @Nullable
    @ColumnInfo(name = "full_name")
    @SerializedName("full_name")
    private String fullName;

    @Nullable
    @ColumnInfo(name = "phone")
    @SerializedName("phone")
    private String phone;

    @Nullable
    @ColumnInfo(name = "card_num")
    @SerializedName("card_num")
    private String cardNumber;

    @Nullable
    @ColumnInfo(name = "bns_balance")
    @SerializedName("bns_balance")
    private String bonuses;

    @Nullable
    @ColumnInfo(name = "bns_to_next_status")
    @SerializedName("bns_to_next_status")
    private String bonusesForNextStatus;

    @Nullable
    @ColumnInfo(name = "current_status")
    @SerializedName("current_status")
    private String status;

    @Nullable
    @SerializedName("password")
    private String errorMessageOfPassword;

    @ColumnInfo(name = "subscribed_to_push_operations")
    @SerializedName("subscribed_to_push_operations")
    private boolean subscribedToPushOperations;

    @ColumnInfo(name = "subscribed_to_push")
    @SerializedName("subscribed_to_push")
    private boolean subscribedToPush;

    @ColumnInfo(name = "subscribed_to_email")
    @SerializedName("subscribed_to_email")
    private boolean subscribedToEmail;

    @ColumnInfo(name = "subscribed_to_sms")
    @SerializedName("subscribed_to_sms")
    private boolean subscribedToSms;

    @ColumnInfo(name = "is_active")
    @SerializedName("is_active")
    private boolean isActive;

    @ColumnInfo(name = "update_date")
    private long updateDate;

    public UserEntity() {
        updateDate = new Date().getTime();
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getFullName() {
        return fullName;
    }

    public void setFullName(@Nullable String fullName) {
        this.fullName = fullName;
    }

    @Nullable
    public String getPhone() {
        return phone;
    }

    public void setPhone(@Nullable String phone) {
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Nullable
    public String getBonusesForNextStatus() {
        return bonusesForNextStatus;
    }

    public void setBonusesForNextStatus(@Nullable String bonusesForNextStatus) {
        this.bonusesForNextStatus = bonusesForNextStatus;
    }

    @Nullable
    public String getStatus() {
        return status;
    }

    public void setStatus(@Nullable String status) {
        this.status = status;
    }

    @Nullable
    public String getBonuses() {
        return bonuses;
    }

    public void setBonuses(@Nullable String bonuses) {
        this.bonuses = bonuses;
    }

    @Nullable
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(@Nullable String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Nullable
    public String getErrorMessageOfPassword() {
        return errorMessageOfPassword;
    }

    public void setErrorMessageOfPassword(@Nullable String errorMessageOfPassword) {
        this.errorMessageOfPassword = errorMessageOfPassword;
    }

    public boolean isSubscribedToPushOperations() {
        return subscribedToPushOperations;
    }

    public void setSubscribedToPushOperations(boolean subscribedToPushOperations) {
        this.subscribedToPushOperations = subscribedToPushOperations;
    }

    public boolean isSubscribedToPush() {
        return subscribedToPush;
    }

    public void setSubscribedToPush(boolean subscribedToPush) {
        this.subscribedToPush = subscribedToPush;
    }

    public boolean isSubscribedToEmail() {
        return subscribedToEmail;
    }

    public void setSubscribedToEmail(boolean subscribedToEmail) {
        this.subscribedToEmail = subscribedToEmail;
    }

    public boolean isSubscribedToSms() {
        return subscribedToSms;
    }

    public void setSubscribedToSms(boolean subscribedToSms) {
        this.subscribedToSms = subscribedToSms;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }
}
