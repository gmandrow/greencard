package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.response.OfertaResponse;
import com.aspirity.greencard.domain.model.Oferta;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class OfertaMapper implements Function<OfertaResponse, Oferta> {
    @Override
    public Oferta apply(@NonNull OfertaResponse ofertaResponse) {
        return new Oferta(
                ofertaResponse.getTitle(),
                ofertaResponse.getSubHeader(),
                ofertaResponse.getContent());
    }
}
