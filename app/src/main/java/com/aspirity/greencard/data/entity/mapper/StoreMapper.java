package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.StoreEntity;
import com.aspirity.greencard.domain.model.Store;
import com.aspirity.greencard.presentation.utils.rx.AppSchedulerProvider;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class StoreMapper implements Function<StoreEntity, Store> {

    @Override
    public Store apply(@NonNull StoreEntity storeEntity) {
        return Observable.just(storeEntity.getVendors())
                .observeOn(new AppSchedulerProvider().computation())
                .flatMap(Observable::fromIterable)
                .map(new VendorMapper())
                .toList()
                .map(vendors -> new Store(
                        storeEntity.getId(),
                        storeEntity.getAddress(),
                        storeEntity.getPartnerId(),
                        storeEntity.getPartnerName(),
                        vendors,
                        false)).blockingGet();
    }
}
