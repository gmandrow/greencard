package com.aspirity.greencard.data.entity;

import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "product",
        primaryKeys = {"position_num", "operation_id"}
//        foreignKeys = @ForeignKey(
//                entity = OperationEntity.class,
//                parentColumns = "unique_id",
//                childColumns = "operation_id",
//                onDelete = ForeignKey.CASCADE
//        )
)
public class ProductEntity {

    @NonNull
    @ColumnInfo(name = "operation_id", index = true)
    private String operationId;

    @ColumnInfo(name = "position_num")
    @SerializedName("position_num")
    private int positionNumber;

    @ColumnInfo(name = "accrued_bonuses")
    @SerializedName("accrued_bonuses")
    private String accruedBonuses;

    @ColumnInfo(name = "count")
    @SerializedName("count")
    private Double count;

    @ColumnInfo(name = "debited_bonuses")
    @SerializedName("debited_bonuses")
    private String debitedBonuses;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    private String name;

    @ColumnInfo(name = "price")
    @SerializedName("price")
    private String price;

    @ColumnInfo(name = "total_price")
    @SerializedName("total_price")
    private String totalPrice;

    public ProductEntity() {
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public int getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(int positionNumber) {
        this.positionNumber = positionNumber;
    }

    public String getAccruedBonuses() {
        return accruedBonuses;
    }

    public void setAccruedBonuses(String accruedBonuses) {
        this.accruedBonuses = accruedBonuses;
    }

    public Double getCount() {
        return count;
    }

    public void setCount(Double count) {
        this.count = count;
    }

    public String getDebitedBonuses() {
        return debitedBonuses;
    }

    public void setDebitedBonuses(String debitedBonuses) {
        this.debitedBonuses = debitedBonuses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }
}
