package com.aspirity.greencard.data.prefs;

import android.content.SharedPreferences;

import com.aspirity.greencard.BuildConfig;
import com.aspirity.greencard.presentation.utils.Constants;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DefaultPreferenceDataManager implements PreferenceDataManager {

    private static final String PREF_TOKEN = "token";
    private static final String PREF_FIREBASE_TOKEN = "firebase_token";
    private static final String PREF_ACTIVE_USER = "is_active_user";
    private static final String PREF_CHECK_UPDATE = "check_update";
    private final SharedPreferences sharedPreferences;

    public DefaultPreferenceDataManager(SharedPreferences preferences) {
        sharedPreferences = preferences;
    }

    @NonNull
    @Override
    public String getToken() {
        return sharedPreferences.getString(PREF_TOKEN, Constants.EMPTY_ANSWER);
    }

    @Override
    public void setToken(@Nullable String token) {
        if (token == null) {
            sharedPreferences.edit().remove(PREF_TOKEN).apply();
        } else {
            sharedPreferences.edit().putString(PREF_TOKEN, token).apply();
        }
    }

    @NonNull
    @Override
    public String getFirebaseToken() {
        return sharedPreferences.getString(PREF_FIREBASE_TOKEN, Constants.EMPTY_ANSWER);
    }

    @Override
    public void setFirebaseToken(@NonNull String token) {
        sharedPreferences.edit().putString(PREF_FIREBASE_TOKEN, token).apply();
    }

    @Override
    public boolean isActiveUser() {
        return sharedPreferences.getBoolean(PREF_ACTIVE_USER, false);
    }

    @Override
    public void setActiveUser(boolean isActiveUser) {
        sharedPreferences.edit().putBoolean(PREF_ACTIVE_USER, isActiveUser).apply();
    }

    @Override
    public String getCheckUpdate() {
        return sharedPreferences.getString(PREF_CHECK_UPDATE, "1.0.0");
    }

    @Override
    public void setCheckUpdate(String checkUpdate) {
        sharedPreferences.edit().putString(PREF_CHECK_UPDATE, checkUpdate).apply();
    }

    @Override
    public void clearAll() {
        try {
            FirebaseInstanceId.getInstance().deleteInstanceId();
        } catch (IOException e) {
            e.printStackTrace();
        }

        sharedPreferences.edit().clear().apply();
    }

    @Override
    public boolean getClear() {
        return sharedPreferences.getBoolean("clear_" + BuildConfig.VERSION_NAME, true);
    }

    @Override
    public void setClear(boolean clear) {
        sharedPreferences.edit().putBoolean("clear_" + BuildConfig.VERSION_NAME, clear).apply();
    }

    @Override
    public String getVirtualCardNumber() {
        return sharedPreferences.getString("virtual_card_number", "");
    }

    @Override
    public void setVirtualCardNumber(String number) {
        sharedPreferences.edit().putString("virtual_card_number", number).apply();
    }

    @Override
    public String getVirtualCardHash() {
        return sharedPreferences.getString("virtual_card_hash", "");
    }

    @Override
    public void setVirtualCardHash(String hash) {
        sharedPreferences.edit().putString("virtual_card_hash", hash).apply();
    }

    @Override
    public float getLastBrightness() {
        return sharedPreferences.getFloat("last_brightness", 0.0f);
    }

    @Override
    public void setLastBrightness(float last_brightness) {
        sharedPreferences.edit().putFloat("last_brightness", last_brightness).apply();
    }
}
