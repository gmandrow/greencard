package com.aspirity.greencard.data.network;


import com.aspirity.greencard.data.entity.ShareEntity;
import com.aspirity.greencard.data.entity.response.ListResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ShareService {

    @GET("shares/")
    Single<ListResponse<ShareEntity>> shares(
            @Query("page") Integer page,
            @Query("page_size") Integer pageSize,
            @Query("date_end_0") String beginDate,
            @Query("date_end_1") String endDate,
            @Query("o") String sort,
            @Query("vendor_id") List<Long> vendors);

    @GET("shares/")
    Single<ListResponse<ShareEntity>> sharesOfPartner(
            @Query("partner_id") Long partnerId,
            @Query("page") Integer page,
            @Query("page_size") Integer pageSize);


    @GET("shares/{share_id}")
    Single<ShareEntity> share(
            @Path("share_id") Long shareId);

    @GET("shares/archive/{share_id}")
    Single<ShareEntity> archivedShare(
            @Path("share_id") Long shareId);

    @GET("shares/archive/")
    Single<ListResponse<ShareEntity>> archive(
            @Query("page") Integer page,
            @Query("page_size") Integer pageSize);
}
