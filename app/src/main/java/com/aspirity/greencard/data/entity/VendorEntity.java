package com.aspirity.greencard.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "vendor",
        foreignKeys = @ForeignKey(
                entity = PartnerEntity.class,
                parentColumns = "id",
                childColumns = "partner_id",
                onDelete = ForeignKey.CASCADE))
public class VendorEntity {

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private Long id;

    @ColumnInfo(name = "address")
    @SerializedName("address")
    private String address;

    @ColumnInfo(name = "email")
    @SerializedName("email")
    private String email;

    @ColumnInfo(name = "latitude")
    @SerializedName("latitude")
    private String latitude;

    @ColumnInfo(name = "longitude")
    @SerializedName("longitude")
    private String longitude;

    @ColumnInfo(name = "partner_color")
    @SerializedName("partner_color")
    private String partnerColor;

    @ColumnInfo(name = "partner_id", index = true)
    @SerializedName("partner_id")
    private Long partnerId;

    @ColumnInfo(name = "partner_logo_src")
    @SerializedName("partner_logo_src")
    private String partnerLogoSrc;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    private String name;

    @ColumnInfo(name = "partner_name")
    @SerializedName("partner_name")
    private String partnerName;

    @ColumnInfo(name = "phone")
    @SerializedName("phone")
    private String phone;

    @ColumnInfo(name = "store_id")
    @SerializedName("store_id")
    private Long storeId;

    @ColumnInfo(name = "update_date")
    private long updateDate;

    public VendorEntity() {
        updateDate = new Date().getTime();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPartnerColor() {
        return partnerColor;
    }

    public void setPartnerColor(String partnerColor) {
        this.partnerColor = partnerColor;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerLogoSrc() {
        return partnerLogoSrc;
    }

    public void setPartnerLogoSrc(String partnerLogoSrc) {
        this.partnerLogoSrc = partnerLogoSrc;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }
}
