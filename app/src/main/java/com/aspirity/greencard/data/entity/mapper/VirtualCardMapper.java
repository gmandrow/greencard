package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.response.VirtualCardResponse;
import com.aspirity.greencard.domain.model.VirtualCard;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class VirtualCardMapper implements Function<VirtualCardResponse, VirtualCard> {
    @Override
    public VirtualCard apply(@NonNull VirtualCardResponse virtualCardResponse) {
        return new VirtualCard(virtualCardResponse.getNumber(), virtualCardResponse.getHash());
    }
}
