package com.aspirity.greencard.data.db.dao;

import com.aspirity.greencard.data.entity.UserEntity;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Single;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user")
    Single<UserEntity> getUser();

    @Query("SELECT update_date FROM user")
    Single<Long> getDate();

    @Query("DELETE FROM user")
    void deleteUser();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(UserEntity user);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateUser(UserEntity userEntity);
}
