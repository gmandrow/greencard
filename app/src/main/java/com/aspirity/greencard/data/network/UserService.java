package com.aspirity.greencard.data.network;


import com.aspirity.greencard.data.entity.UserEntity;
import com.aspirity.greencard.data.entity.response.ActivateUserResponse;
import com.aspirity.greencard.data.entity.response.AuthResponse;
import com.aspirity.greencard.data.entity.response.OfertaResponse;
import com.google.gson.JsonObject;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface UserService {

    @Headers("@: NoAuth")
    @FormUrlEncoded
    @POST("user/auth/")
    Single<AuthResponse> auth(
            @Field("username") String username,
            @Field("password") String password);

    /**
     * This method we use after registration.
     *
     * @return "phone": "79135121121",
     * "card_num": "3000012510005005",
     * "is_active": true
     */
    @POST("/user/activate/")
    Single<ActivateUserResponse> activate();

    /**
     * This is method needed for registration on Registration screen, uses after cards/validate/
     *
     * @param cardNumber it your card number
     * @param cardCode   it your card code
     * @param firstName  it your name
     * @param gender     it your sex, if you Male then use 'M' or if you Female use 'F'
     * @param birthDate  it your birthday, format 1993-02-21
     * @param phone      it your phone 7923.......
     * @param email      it your email not Requirement
     * @param agreement  if you agreed with oferta
     * @return
     */
    @Headers("@: NoAuth")
    @FormUrlEncoded
    @POST("user/registration/")
    Single<UserEntity> registration(
            @Field("card_num") String cardNumber,
            @Field("card_code") String cardCode,
            @Field("first_name") String firstName,
            @Field("gender") String gender,
            @Field("birth_date") String birthDate,
            @Field("phone") String phone,
            @Field("email") String email,
            @Field("agreement") Boolean agreement);


    @Headers("@: NoAuth")
    @FormUrlEncoded
    @POST("user/restore_password/")
    Single<JsonObject> restorePassword(@Field("phone") String phone);

    @Headers("@: NoAuth")
    @FormUrlEncoded
    @POST("user/validate_sms_code/")
    Single<JsonObject> validateSmsCode(@Field("sms_code") String sms_code, @Field("phone") String phone);

    @Headers("@: NoAuth")
    @FormUrlEncoded
    @POST("/user/set_new_password/")
    Single<JsonObject> setNewPassword(
            @Field("phone") String phone,
            @Field("sms_code") String smsCode,
            @Field("password") String password);

    /**
     * This method we use when want attached card for family account.
     * We automatic call this method when field cardNumber and cardCode are filled.
     * Then we call request POST /cards/ with id of User
     *
     * @param cardNumber - number of card which we want attach with family account
     * @param cardCode   - code of card which we want attach with family account
     * @return part of UserEntity (id, phone, fullName)
     */
    @FormUrlEncoded
    @POST("/user/search_by_card_number/")
    Single<UserEntity> searchByCardNumber(
            @Field("card_num") String cardNumber,
            @Field("card_code") String cardCode);

    /**
     * Use this method, when we want change password on settings screen and activate screen
     *
     * @param password - new password
     * @return current UserEntity
     */
    @FormUrlEncoded
    @POST("user/change_password/")
    Single<UserEntity> changePassword(
            @Field("password") String password);

    @FormUrlEncoded
    @POST("user/")
    Single<UserEntity> updateUserSubscribed(
            @Field("subscribed_to_push_operations") Boolean isSubscribedPushOperations,
            @Field("subscribed_to_push") Boolean isSubscribedPush,
            @Field("subscribed_to_email") Boolean isSubscribedEmail,
            @Field("subscribed_to_sms") Boolean isSubscribedSms);

    @GET("user/")
    Single<UserEntity> getUser();

    /**
     * This is method needed for registration
     *
     * @return "title": "Оферта",
     * "subheader": "Договор участия в программе лояльности",
     * "content": "html text"
     */
    @Headers("@: NoAuth")
    @GET("help/pages/oferta/")
    Single<OfertaResponse> getOferta();

    @GET("/faq/pages/family_account/")
    Single<OfertaResponse> getFamilyAccountRules();
}
