package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.OperationEntity;
import com.aspirity.greencard.domain.model.Operation;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class OperationMapper implements Function<OperationEntity, Operation> {

    // TODO: 05.09.2017 проверить этот маппер
    @Override
    public Operation apply(@NonNull OperationEntity operationEntity) {
        return new Operation(
                operationEntity.getId(),
                operationEntity.getUniqueId(),
                operationEntity.getAddress(),
                operationEntity.getBonuses(),
                operationEntity.getType(),
                operationEntity.getDateOf(),
                operationEntity.getAccruedBonuses(),
                operationEntity.getDebitedBonuses(),
                operationEntity.getPartnerLogoSrc(),
                operationEntity.getPartnerLogoBlackBgSrc(),
                operationEntity.getPartnerName(),
                operationEntity.getTotalPrice(),
                operationEntity.getVendorName(),
                operationEntity.getPartnerId());
    }
}
