package com.aspirity.greencard.data.repository;

import com.aspirity.greencard.data.db.DatabaseFactory;
import com.aspirity.greencard.data.db.dao.CountDao;
import com.aspirity.greencard.data.db.dao.PartnerDao;
import com.aspirity.greencard.data.entity.CountEntity;
import com.aspirity.greencard.data.entity.PartnerEntity;
import com.aspirity.greencard.data.entity.PhotoEntity;
import com.aspirity.greencard.data.entity.mapper.BonusMapper;
import com.aspirity.greencard.data.entity.mapper.PartnerMapper;
import com.aspirity.greencard.data.entity.mapper.PhotoMapper;
import com.aspirity.greencard.data.entity.mapper.ShortPartnerMapper;
import com.aspirity.greencard.data.entity.response.ListResponse;
import com.aspirity.greencard.data.network.PartnerService;
import com.aspirity.greencard.data.network.RetrofitService;
import com.aspirity.greencard.domain.model.BonusPercent;
import com.aspirity.greencard.domain.model.Partner;
import com.aspirity.greencard.domain.repository.PartnerRepository;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

// TODO: 28.09.2017 необходимо в будущем сделать прокидывание SchedulersProvider в repositories
public class PartnersDataRepository implements PartnerRepository {
    private PartnerService partnerService = RetrofitService.createRetrofitClient(PartnerService.class);
    private PartnerDao partnerDao = DatabaseFactory.getInstance().partnerDao();
    private CountDao countDao = DatabaseFactory.getInstance().countDao();
    private SchedulerProvider schedulerProvider;

    public PartnersDataRepository(SchedulerProvider schedulerProvider) {
        this.schedulerProvider = schedulerProvider;
    }

    private Single<Boolean> isListForceRefreshNeeded() {
        Single<Integer> currentCountSingle = partnerDao.getPartnersCount().map(Long::intValue);
        Single<Integer> totalCountSingle = countDao.getCount(CountDao.PARTNER_COUNT_ID)
                .map(CountEntity::getCount)
                .onErrorResumeNext(error -> Single.just(0));
        return Single.zip(currentCountSingle, totalCountSingle,
                (current, total) -> !total.equals(current));
    }

    @Override
    public Single<List<Partner>> partners() {
        return partnersForceRefresh();
    }

    @Override
    public Single<List<Partner>> partnersForceRefresh() {
        return partnerService
                .partners(null, null)
                .toObservable()
                .map(ListResponse::getResults)
                .doOnNext(items -> {
                    partnerDao.deletePartners();
                    partnerDao.insertItems(items);
                    countDao.insertCount(new CountEntity(CountDao.PARTNER_COUNT_ID, items.size()));
                })
                .flatMap(Observable::fromIterable)
                .map(new ShortPartnerMapper())
                .toList();
    }

    @Override
    public Single<List<Partner>> partnersFromDb() {
        return partnerDao
                .getPartners()
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new ShortPartnerMapper())
                .toList();
    }

    @Override
    public Single<Partner> partner(long partnerId) {
        return partnerForceRefresh(partnerId);
    }

    @Override
    public Single<Partner> partnerForceRefresh(long partnerId) {
        Single<PartnerEntity> partnerEntitySingle = partnerService.partner(partnerId)
                .doOnSuccess(partnerEntity -> {
                    partnerEntity.setVendorsCount(partnerEntity.getVendors().size());
                    if (partnerDao.updatePartner(partnerEntity) == 0)
                        partnerDao.insertPartner(partnerEntity);
                })
                .cache();

        Single<List<PhotoEntity>> photos = partnerEntitySingle
                .map(PartnerEntity::getPhotos)
                .onErrorResumeNext(error -> Single.just(new ArrayList<>()))
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(url -> {
                    PhotoEntity entity = new PhotoEntity();
                    entity.setPartnerId(partnerId);
                    entity.setUrl(url);
                    return entity;
                })
                .toList()
                .doOnSuccess(list -> {
                    partnerDao.deletePhotos(partnerId);
                    partnerDao.insertPhotos(list);
                });

        Single<List<BonusPercent>> bonus = partnerEntitySingle
                .map(PartnerEntity::getBonusPercentEntities)
                .toObservable()
                .flatMap(Observable::fromIterable)
                .doOnNext(bonusPercentEntity -> bonusPercentEntity.setPartnerId(partnerId))
                .toList()
                .doOnSuccess(bonusPercentEntities -> {
                    partnerDao.deleteBonuses(partnerId);
                    partnerDao.insertBonuses(bonusPercentEntities);
                })
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new BonusMapper())
                .toList();

        Single<Partner> partner = partnerEntitySingle
                .map(new PartnerMapper());

        return Single.zip(partner, bonus, photos, (partner1, bonusPercents, photoList) -> {
            partner1.setBonusPercents(bonusPercents);
            return partner1;
        });
    }

    private Single<PartnerEntity> tryPartnerFromDb(long partnerId) {
        return partnerDao.getPartner(partnerId)
                .onErrorResumeNext(error -> {
                    PartnerEntity entity = new PartnerEntity();
                    entity.setId(partnerId);
                    entity.setUpdateDate(0L);
                    return Single.just(entity);
                });
    }

    @Override
    public Single<Partner> partnerFromDb(long partnerId) {
        Single<List<BonusPercent>> bonus = partnerDao
                .getBonusesForPartner(partnerId)
                .onErrorResumeNext(error -> Single.just(new ArrayList<>()))
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new BonusMapper())
                .toList();

        Single<Partner> partner = partnerDao.getPartner(partnerId)
                .map(new PartnerMapper());

        Single<List<String>> photoList = partnerDao
                .getPhotosForPartner(partnerId)
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new PhotoMapper())
                .toList()
                .onErrorResumeNext(error -> Single.just(new ArrayList<>()));

        return Single.zip(partner, bonus, photoList, (partner1, bonusPercents, photo) -> {
            partner1.setBonusPercents(bonusPercents);
            partner1.setPhotos(photo);
            return partner1;
        });
    }
}
