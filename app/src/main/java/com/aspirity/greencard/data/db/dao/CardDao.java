package com.aspirity.greencard.data.db.dao;

import com.aspirity.greencard.data.entity.CardEntity;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Single;

@Dao
public interface CardDao {
    @Query("SELECT min(update_date) FROM card")
    Single<Long> getListOldestDate();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCards(List<CardEntity> cards);

    @Query("SELECT * FROM card")
    Single<List<CardEntity>> getCards();

    @Query("DELETE FROM card")
    void deleteCards();
}
