package com.aspirity.greencard.data.network;

import com.aspirity.greencard.data.entity.response.ListResponse;
import com.aspirity.greencard.domain.model.Notification;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface NotificationsService {
    @GET("notifications/history/")
    Single<ListResponse<Notification>> notifications(@Query("token") String token);

    @FormUrlEncoded
    @PATCH("notifications/history/{notification_id}/")
    Single<Void> notificationsStatus(
            @Path("notification_id") Long notification_id,
            @Field("token") String token,
            @Field("status") String status);
}