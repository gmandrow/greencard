package com.aspirity.greencard.data.entity.response;


import com.google.gson.annotations.SerializedName;

import androidx.annotation.Nullable;

public class AttachedCardResponse {

    @Nullable
    @SerializedName("is_attached")
    private Boolean isAttached;

    @Nullable
    @SerializedName("sms_sent_to")
    private String smsSentTo;

    @Nullable
    @SerializedName("non_field_errors")
    private String errorMessage;

    @Nullable
    public Boolean isAttached() {
        return isAttached;
    }

    public void setIsAttached(@Nullable Boolean isAttached) {
        this.isAttached = isAttached;
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(@Nullable String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Nullable
    public String getSmsSentTo() {
        return smsSentTo;
    }

    public void setSmsSentTo(@Nullable String smsSentTo) {
        this.smsSentTo = smsSentTo;
    }
}
