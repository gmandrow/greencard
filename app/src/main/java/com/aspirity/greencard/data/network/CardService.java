package com.aspirity.greencard.data.network;


import com.aspirity.greencard.data.entity.CardEntity;
import com.aspirity.greencard.data.entity.response.AttachedCardResponse;
import com.aspirity.greencard.data.entity.response.ChangedCardResponse;
import com.aspirity.greencard.data.entity.response.ListResponse;
import com.aspirity.greencard.data.entity.response.ValidCardResponse;
import com.aspirity.greencard.data.entity.response.VirtualCardResponse;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CardService {

    /**
     * We use this method when we want attach new card for family account
     *
     * @param cardNumber - userId which we get when call request POST /user/search_by_card_number/
     * @param cardCode   - userId which we get when call request POST /user/search_by_card_number/
     * @return string with phone number 'sms_sent_to'
     */
    @Headers("@: NoAuth")
    @FormUrlEncoded
    @POST("cards/validate/")
    Single<ValidCardResponse> validate(
            @Field("card_num") String cardNumber,
            @Field("card_code") String cardCode);

    /**
     * We use this method when we want finish attach your card
     *
     * @param userId  - userId which we get when call request POST /user/search_by_card_number/
     * @param smsCode - smsCode which we get on phone after call POST cards/
     * @return boolean 'is_attached'
     */
    @FormUrlEncoded
    @POST("cards/verify_attached_card/")
    Single<AttachedCardResponse> verifyAttachedCard(
            @Field("user_id") Long userId,
            @Field("sms_code") String smsCode);

    /**
     * We use this method when we want attach new card for family account
     *
     * @param userId - userId which we get when call request POST /user/search_by_card_number/
     * @return string with phone number 'sms_sent_to'
     */
    @FormUrlEncoded
    @POST("cards/")
    Single<AttachedCardResponse> cards(
            @Field("user_id") Long userId);

    /**
     * We use this method when we want block or unlock card
     *
     * @param cardId   - it is your card id from CardEntity
     * @param password - it is your password
     * @return success
     */
    @FormUrlEncoded
    @POST("cards/{card_id}/change_state/")
    Single<ChangedCardResponse> cardChangeState(
            @Path("card_id") Long cardId,
            @Field("password") String password);

    @GET("cards/")
    Single<ListResponse<CardEntity>> getCards();

    @GET("cards/get-virtual-token")
    Single<VirtualCardResponse> getVirtualCard();
}
