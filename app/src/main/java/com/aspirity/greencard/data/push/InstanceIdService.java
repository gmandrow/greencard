package com.aspirity.greencard.data.push;

import android.util.Log;

import com.aspirity.greencard.GreenCardApplication;
import com.aspirity.greencard.data.prefs.PreferenceDataManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class InstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.i("mlg", token);
        if (token == null) return;
        GreenCardApplication app = (GreenCardApplication) getApplicationContext();
        PreferenceDataManager manager = app.getPreferenceDataManager();
        manager.setFirebaseToken(token);
    }
}
