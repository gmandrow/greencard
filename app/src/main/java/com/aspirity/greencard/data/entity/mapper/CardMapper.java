package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.CardEntity;
import com.aspirity.greencard.domain.model.Card;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class CardMapper implements Function<CardEntity, Card> {

    @Override
    public Card apply(@NonNull CardEntity cardEntity) {
        return new Card(
                cardEntity.getId(),
                cardEntity.getOwnerStatus(),
                cardEntity.getOwnerName(),
                cardEntity.getOwnerPhone(),
                cardEntity.getNumber(),
                cardEntity.isMain(),
                cardEntity.isLocked());
    }
}
