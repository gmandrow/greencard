package com.aspirity.greencard.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StoreEntity {

    @SerializedName("id")
    private Long id;

    @SerializedName("address")
    private String address;

    @SerializedName("partner_id")
    private Long partnerId;

    @SerializedName("partner_name")
    private String partnerName;

    @SerializedName("vendors")
    private List<VendorEntity> vendors;

    public StoreEntity() {
    }

    public StoreEntity(
            Long id,
            String address,
            Long partnerId,
            String partnerName,
            List<VendorEntity> vendors) {
        this.id = id;
        this.address = address;
        this.partnerId = partnerId;
        this.partnerName = partnerName;
        this.vendors = vendors;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public List<VendorEntity> getVendors() {
        return vendors;
    }

    public void setVendors(List<VendorEntity> vendors) {
        this.vendors = vendors;
    }

}
