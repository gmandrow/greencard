package com.aspirity.greencard.data.entity.response;


import com.google.gson.annotations.SerializedName;

import androidx.annotation.Nullable;

public class AuthResponse {

    @SerializedName("token")
    private String token;

    @Nullable
    @SerializedName("non_field_errors")
    private String errorMessage;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(@Nullable String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
