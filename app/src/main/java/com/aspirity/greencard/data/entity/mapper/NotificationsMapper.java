package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.domain.model.Notification;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class NotificationsMapper implements Function<Notification, Notification> {

    @Override
    public Notification apply(@NonNull Notification notification) {
        return new Notification(
                notification.getId(),
                notification.getCategory(),
                notification.getStatus(),
                notification.getDateCreated(),
                notification.getData(),
                notification.getRenderedText());
    }

}
