package com.aspirity.greencard.data.db;

import android.content.Context;

import androidx.room.Room;

public class DatabaseFactory {
    private static Database instance;

    public static Database getInstance() {
        return instance;
    }

    public static void initDatabase(Context context) {
        instance = Room
                .databaseBuilder(context, Database.class, "greencard.db")
                .fallbackToDestructiveMigration()
                .build();
    }
}
