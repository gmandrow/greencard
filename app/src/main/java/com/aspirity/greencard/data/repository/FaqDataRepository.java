package com.aspirity.greencard.data.repository;

import com.aspirity.greencard.data.db.DatabaseFactory;
import com.aspirity.greencard.data.db.dao.FaqDao;
import com.aspirity.greencard.data.entity.mapper.FaqMapper;
import com.aspirity.greencard.data.entity.response.ListResponse;
import com.aspirity.greencard.data.network.FaqService;
import com.aspirity.greencard.data.network.RetrofitService;
import com.aspirity.greencard.domain.model.Faq;
import com.aspirity.greencard.domain.repository.FaqRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;


public class FaqDataRepository implements FaqRepository {
    private final FaqService faqService = RetrofitService.createRetrofitClient(FaqService.class);
    private final FaqDao faqDao = DatabaseFactory.getInstance().faqDao();

    @Override
    public Single<List<Faq>> faq() {
        return faqForceRefresh();
    }

    //@Override
    public Single<List<Faq>> faqFromDb() {
        return faqDao
                .getFaqList()
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new FaqMapper())
                .toList();
    }

    @Override
    public Single<List<Faq>> faqForceRefresh() {
        return faqService
                .faq()
                .map(ListResponse::getResults)
                .doOnSuccess(faqDao::insertFaq)
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new FaqMapper())
                .toList();
    }
}
