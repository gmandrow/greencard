package com.aspirity.greencard.data.repository;

import com.aspirity.greencard.data.network.AppsService;
import com.aspirity.greencard.data.network.RetrofitService;
import com.aspirity.greencard.domain.model.Apps;
import com.aspirity.greencard.domain.repository.AppsRepository;

import io.reactivex.Single;

public class AppsDataRepository implements AppsRepository {
    private AppsService appsService = RetrofitService.createRetrofitClient(AppsService.class);

    @Override
    public Single<Apps> apps() {
        return appsService.apps();
    }

    @Override
    public Single<Void> registrationApp(String app_id, String type) {
        return appsService.registrationApp(app_id, type);
    }
}