package com.aspirity.greencard.data.network;

import com.aspirity.greencard.domain.model.Apps;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AppsService {
    @GET("apps/versions")
    Single<Apps> apps();

    @FormUrlEncoded
    @POST("apps/")
    Single<Void> registrationApp(
            @Field("app_id") String appId,
            @Field("type") String type);
}
