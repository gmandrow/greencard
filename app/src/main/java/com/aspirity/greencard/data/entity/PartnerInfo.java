package com.aspirity.greencard.data.entity;

public class PartnerInfo {

    String pathInfo;
    boolean isTableOfBonuses;

    public PartnerInfo(String pathInfo, boolean isTableOfBonuses) {
        this.pathInfo = pathInfo;
        this.isTableOfBonuses = isTableOfBonuses;
    }

    public String getPathInfo() {
        return pathInfo;
    }

    public boolean isTableOfBonuses() {
        return isTableOfBonuses;
    }
}
