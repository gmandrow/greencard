package com.aspirity.greencard.data.repository;

import com.aspirity.greencard.data.db.DatabaseFactory;
import com.aspirity.greencard.data.db.dao.OperationDao;
import com.aspirity.greencard.data.entity.OperationEntity;
import com.aspirity.greencard.data.entity.ProductEntity;
import com.aspirity.greencard.data.entity.mapper.OperationMapper;
import com.aspirity.greencard.data.entity.mapper.ProductMapper;
import com.aspirity.greencard.data.entity.mapper.ShortOperationMapper;
import com.aspirity.greencard.data.entity.response.ListResponse;
import com.aspirity.greencard.data.network.OperationService;
import com.aspirity.greencard.data.network.RetrofitService;
import com.aspirity.greencard.domain.model.Chunk;
import com.aspirity.greencard.domain.model.Operation;
import com.aspirity.greencard.domain.model.Product;
import com.aspirity.greencard.domain.repository.OperationsRepository;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;


public class OperationsDataRepository implements OperationsRepository {

    private OperationService operationService = RetrofitService.createRetrofitClient(OperationService.class);
    private OperationDao operationDao = DatabaseFactory.getInstance().operationDao();

    @Override
    public Single<Chunk<Operation>> operations(Integer page, String beginDate, String endDate, String sort, List<Long> vendors) {
        Single<ListResponse<OperationEntity>> response = operationService
                .operations(page, 30, beginDate, endDate, sort, vendors)
                .cache();

        Single<Integer> count = response.map(ListResponse::getTotalCount);
        Single<List<Operation>> list = response
                .map(ListResponse::getResults)
                .doOnSuccess(results -> operationDao.insertOperations(results))
                .toFlowable()
                .flatMap(Flowable::fromIterable)
                .map(new ShortOperationMapper())
                .toList();

        return Single.zip(list, count, Chunk::new);
    }

    @Override
    public Single<Chunk<Operation>> operationsFromDb(int page, int pageSize) {
        Single<List<Operation>> list = operationDao
                .getOperations(page, pageSize)
                .toFlowable()
                .flatMap(Flowable::fromIterable)
                .map(new ShortOperationMapper())
                .toList();

        Single<Integer> count = operationDao
                .getOperationCount();

        return Single.zip(list, count, Chunk::new);
    }

    @Override
    public Single<Operation> operation(String uniqueId) {
        return operationFromDb(uniqueId)
                .flatMap(operation -> {
                    if (operation.getUniqueId() == null || operation.getUniqueId().isEmpty() || operation.getProducts().isEmpty())
                        return operationForceRefresh(uniqueId);
                    return Single.just(operation);
                });
    }

    @Override
    public Single<Operation> operationForceRefresh(String uniqueId) {
        Single<OperationEntity> operationEntitySingle = operationService
                .operation(uniqueId)
                .doOnSuccess(operationEntity -> {
                    operationDao.updateOperation(operationEntity);
                })
                .cache();

        Single<Operation> operationSingle = operationEntitySingle
                .map(new OperationMapper());

        Flowable<ProductEntity> productEntitiesFlowable = operationEntitySingle
                .map(OperationEntity::getProductEntities)
                .toFlowable()
                .flatMap(Flowable::fromIterable)
                .doOnNext(productEntity -> productEntity.setOperationId(uniqueId))
                .share();

        Single<List<Product>> productsSingle = productEntitiesFlowable
                .map(new ProductMapper())
                .toList();

        Single<List<ProductEntity>> saveProductsSingle = productEntitiesFlowable
                .toList()
                .doOnSuccess(list -> {
                    operationDao.insertProducts(list);
                });

        return Single.zip(operationSingle, productsSingle, saveProductsSingle, (operation, productList, save) -> {
            operation.setProducts(productList);
            return operation;
        });
    }

    //@Override
    public Single<Operation> operationFromDb(String uniqueId) {
        Single<Operation> operationSingle = operationDao
                .getOperation(uniqueId)
                .map(new OperationMapper());

        Single<List<Product>> productsSingle = operationDao
                .getProducts(uniqueId)
                .toFlowable()
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new ProductMapper())
                .toList();

        return Single
                .zip(operationSingle, productsSingle, (operation, productList) -> {
                    operation.setProducts(productList);
                    return operation;
                })
                .onErrorResumeNext(error -> Single.just(new Operation()));
    }
}