package com.aspirity.greencard.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;

@Entity(tableName = "share", primaryKeys = {"id", "title"})
public class ShareEntity {
    public static final String PRIMARY_KEY = "id";
    public static final String PARTNER_KEY = "partnerId";

    @NonNull
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private Long id;

    @ColumnInfo(name = "is_special")
    @SerializedName("is_special")
    private Boolean isSpecial;

    @ColumnInfo(name = "content")
    @SerializedName("content")
    private String content;

    @ColumnInfo(name = "date_start")
    @SerializedName("date_start")
    private Long dateStart;

    @ColumnInfo(name = "date_end")
    @SerializedName("date_end")
    private Long dateEnd;

    @ColumnInfo(name = "image_src")
    @SerializedName("image_src")
    private String imageSrc;

    @ColumnInfo(name = "image_horizontal_src")
    @SerializedName("image_horizontal_src")
    private String imageHorizontalSrc;

    @ColumnInfo(name = "image_vertical_src")
    @SerializedName("image_vertical_src")
    private String imageVerticalSrc;

    @ColumnInfo(name = "partner_color")
    @SerializedName("partner_color")
    private String partnerColor;

    @ColumnInfo(name = "partner_id")
    @SerializedName("partner_id")
    private Long partnerId;

    @ColumnInfo(name = "partner_logo_src")
    @SerializedName("partner_logo_src")
    private String partnerLogoSrc;

    @ColumnInfo(name = "partner_logo_white_bg_src")
    @SerializedName("partner_logo_white_bg_src")
    private String partnerLogoWhiteBgSrc;

    @ColumnInfo(name = "partner_logo_black_bg_src")
    @SerializedName("partner_logo_black_bg_src")
    private String partnerLogoBlackBgSrc;

    @ColumnInfo(name = "partner_logo_map_src")
    @SerializedName("partner_logo_map_src")
    private String partnerLogoMapSrc;

    @NonNull
    @ColumnInfo(name = "title")
    @SerializedName("title")
    private String title;

    @Ignore
    @SerializedName("vendors")
    private long[] vendors;

    @Nullable
    @Ignore
    @SerializedName("non_field_errors")
    private String errorMessage;

    @ColumnInfo(name = "update_date")
    private Long updateDate;

    @ColumnInfo(name = "is_archived")
    private boolean isArchived;

    public ShareEntity() {
        updateDate = new Date().getTime();
    }

    public Boolean isSpecial() {
        return isSpecial;
    }

    public void setSpecial(Boolean special) {
        isSpecial = special;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getDateStart() {
        return dateStart;
    }

    public void setDateStart(Long dateStart) {
        this.dateStart = dateStart;
    }

    public Long getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Long dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    public String getImageHorizontalSrc() {
        return imageHorizontalSrc;
    }

    public void setImageHorizontalSrc(String imageHorizontalSrc) {
        this.imageHorizontalSrc = imageHorizontalSrc;
    }

    public String getImageVerticalSrc() {
        return imageVerticalSrc;
    }

    public void setImageVerticalSrc(String imageVerticalSrc) {
        this.imageVerticalSrc = imageVerticalSrc;
    }

    public String getPartnerColor() {
        return partnerColor;
    }

    public void setPartnerColor(String partnerColor) {
        this.partnerColor = partnerColor;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerLogoSrc() {
        return partnerLogoSrc;
    }

    public void setPartnerLogoSrc(String partnerLogoSrc) {
        this.partnerLogoSrc = partnerLogoSrc;
    }

    public String getPartnerLogoWhiteBgSrc() {
        return partnerLogoWhiteBgSrc;
    }

    public void setPartnerLogoWhiteBgSrc(String partnerLogoWhiteBgSrc) {
        this.partnerLogoWhiteBgSrc = partnerLogoWhiteBgSrc;
    }

    public String getPartnerLogoBlackBgSrc() {
        return partnerLogoBlackBgSrc;
    }

    public void setPartnerLogoBlackBgSrc(String partnerLogoBlackBgSrc) {
        this.partnerLogoBlackBgSrc = partnerLogoBlackBgSrc;
    }

    public String getPartnerLogoMapSrc() {
        return partnerLogoMapSrc;
    }

    public void setPartnerLogoMapSrc(String partnerLogoMapSrc) {
        this.partnerLogoMapSrc = partnerLogoMapSrc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long[] getVendors() {
        return vendors;
    }

    public void setVendors(long[] vendors) {
        this.vendors = vendors;
    }

    public Long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Long updateDate) {
        this.updateDate = updateDate;
    }

    public boolean getArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }
}
