package com.aspirity.greencard.data.prefs;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface PreferenceDataManager {

    @NonNull
    String getToken();

    void setToken(@Nullable String token);

    @NonNull
    String getFirebaseToken();

    void setFirebaseToken(@NonNull String token);

    boolean isActiveUser();

    void setActiveUser(boolean isActiveUser);

    String getCheckUpdate();

    void setCheckUpdate(String checkUpdate);

    void clearAll();

    boolean getClear();

    void setClear(boolean clear);

    String getVirtualCardNumber();

    void setVirtualCardNumber(String number);

    String getVirtualCardHash();

    void setVirtualCardHash(String hash);

    float getLastBrightness();

    void setLastBrightness(float last_brightness);
}
