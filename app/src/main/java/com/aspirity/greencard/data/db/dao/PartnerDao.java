package com.aspirity.greencard.data.db.dao;

import com.aspirity.greencard.data.entity.BonusPercentEntity;
import com.aspirity.greencard.data.entity.PartnerEntity;
import com.aspirity.greencard.data.entity.PhotoEntity;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Single;

@Dao
public interface PartnerDao {
    @Query("SELECT min(update_date) FROM partner")
    Single<Long> getListOldestDate();

    @Query("SELECT count(*) FROM partner")
    Single<Long> getPartnersCount();

    @Query("SELECT * FROM partner ORDER BY id ASC")
    Single<List<PartnerEntity>> getPartners();

    @Query("SELECT * FROM bonus_percent WHERE partner_id = :partnerId")
    Single<List<BonusPercentEntity>> getBonusesForPartner(long partnerId);

    @Query("SELECT * FROM photo WHERE partner_id = :partnerId")
    Single<List<PhotoEntity>> getPhotosForPartner(long partnerId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertItems(List<PartnerEntity> items);

    @Query("DELETE FROM partner")
    void deletePartners();

    @Query("SELECT * FROM partner WHERE id = :partnerId")
    Single<PartnerEntity> getPartner(long partnerId);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updatePartner(PartnerEntity partnerEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPartner(PartnerEntity partnerEntity);

    // BONUSES

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertBonuses(List<BonusPercentEntity> bonuses);

    @Query("DELETE FROM bonus_percent WHERE partner_id = :partnerId")
    void deleteBonuses(long partnerId);

    // PHOTOS

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPhotos(List<PhotoEntity> photos);

    @Query("DELETE FROM photo WHERE partner_id = :partnerId")
    void deletePhotos(long partnerId);
}
