package com.aspirity.greencard.data.db.dao;

import com.aspirity.greencard.data.entity.VendorEntity;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Single;

@Dao
public interface VendorDao {
    @Query("SELECT min(update_date) FROM vendor")
    Single<Long> getListOldestDate();

    @Query("SELECT * FROM vendor")
    Single<List<VendorEntity>> getVendors();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertVendors(List<VendorEntity> vendors);

    @Query("SELECT min(update_date) FROM vendor WHERE partner_id = :partnerId")
    Single<Long> getListOldestDate(long partnerId);

    @Query("SELECT * FROM vendor WHERE partner_id = :partnerId")
    Single<List<VendorEntity>> getVendorsOfPartner(long partnerId);

    @Query("SELECT * FROM vendor WHERE id = :vendorId")
    Single<VendorEntity> getVendor(long vendorId);

    @Query("DELETE FROM vendor")
    void deleteVendors();
}
