package com.aspirity.greencard.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "card")
public class CardEntity {

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private Long id;

    @ColumnInfo(name = "owner_status")
    @SerializedName("owner_status")
    private String ownerStatus;

    @ColumnInfo(name = "owner_first_name")
    @SerializedName("owner_first_name")
    private String ownerName;

    @ColumnInfo(name = "owner_phone")
    @SerializedName("owner_phone")
    private String ownerPhone;

    @ColumnInfo(name = "card_num")
    @SerializedName("card_num")
    private String number;

    @ColumnInfo(name = "is_main")
    @SerializedName("is_main")
    private Boolean isMain;

    @ColumnInfo(name = "is_locked")
    @SerializedName("is_locked")
    private Boolean isLocked;

    @ColumnInfo(name = "update_date")
    private Long updateDate;

    public CardEntity() {
        updateDate = new Date().getTime();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwnerStatus() {
        return ownerStatus;
    }

    public void setOwnerStatus(String ownerStatus) {
        this.ownerStatus = ownerStatus;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Boolean isMain() {
        return isMain;
    }

    public void setMain(Boolean main) {
        isMain = main;
    }

    public Boolean isLocked() {
        return isLocked;
    }

    public void setLocked(Boolean locked) {
        isLocked = locked;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }

    public Long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Long updateDate) {
        this.updateDate = updateDate;
    }
}
