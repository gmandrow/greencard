package com.aspirity.greencard.data.entity.response;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.annotation.Nullable;

public class ListResponse<T> {

    @SerializedName("results")
    private List<T> results;

    @SerializedName("next")
    private String nextPageUrl;

    @SerializedName("count")
    private Integer totalCount;

    @Nullable
    @SerializedName("non_field_errors")
    private String errorMessage;

    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(@Nullable String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
