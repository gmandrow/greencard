package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.BonusPercentEntity;
import com.aspirity.greencard.domain.model.BonusPercent;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class BonusMapper implements Function<BonusPercentEntity, BonusPercent> {

    @Override
    public BonusPercent apply(@NonNull BonusPercentEntity bonusPercentEntity) {
        return new BonusPercent(bonusPercentEntity.getPercents(), bonusPercentEntity.getStatus());
    }
}
