package com.aspirity.greencard.data.entity.response;


import com.google.gson.annotations.SerializedName;

public class OfertaResponse {

    @SerializedName("title")
    private String title;

    @SerializedName("subheader")
    private String subHeader;

    @SerializedName("content")
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
