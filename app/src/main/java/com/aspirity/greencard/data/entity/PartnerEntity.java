package com.aspirity.greencard.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;


@Entity(tableName = "partner", primaryKeys = {"id", "name"},
        indices = @Index(value = "id", name = "id", unique = true))
public class PartnerEntity {
    public static final String PRIMARY_KEY = "id";

    @ColumnInfo(name = "id")
    @SerializedName("id")
    @NonNull
    private long id;

    @ColumnInfo(name = "color")
    @SerializedName("color")
    private String color;

    @ColumnInfo(name = "logo_src")
    @SerializedName("logo_src")
    private String logoSrc;

    @ColumnInfo(name = "logo_map_src")
    @SerializedName("logo_map_src")
    private String logoMapSrc;

    @ColumnInfo(name = "logo_shares_white_bg_src")
    @SerializedName("logo_shares_white_bg_src")
    private String logoWhiteBgSrc;

    @ColumnInfo(name = "logo_shares_black_bg_src")
    @SerializedName("logo_shares_black_bg_src")
    private String logoBlackBgSrc;

    @ColumnInfo(name = "logo_detail_page_src")
    @SerializedName("logo_detail_page_src")
    private String logoDetailSrc;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    @NonNull
    private String name;

    @ColumnInfo(name = "bns_description")
    @SerializedName("bns_description")
    private String bonusDescription;

    @ColumnInfo(name = "features")
    @SerializedName("features")
    private String features;

    @Ignore
    @SerializedName("bns_percents")
    private List<BonusPercentEntity> bonusPercentEntities;

    @ColumnInfo(name = "description")
    @SerializedName("description")
    private String description;

    @Ignore
    @SerializedName("photos")
    private List<String> photos;

    @ColumnInfo(name = "video_code")
    @SerializedName("video_code")
    private String videoCode;

    @Nullable
    @SerializedName("non_field_errors")
    private String errorMessage;

    @ColumnInfo(name = "update_date")
    private long updateDate;

    @Ignore
    @SerializedName("vendors")
    private List<Long> vendors;

    @ColumnInfo(name = "vendors_count")
    private int vendorsCount;

    public PartnerEntity() {
        updateDate = new Date().getTime();
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogoSrc() {
        return logoSrc;
    }

    public void setLogoSrc(String logoSrc) {
        this.logoSrc = logoSrc;
    }

    public String getLogoMapSrc() {
        return logoMapSrc;
    }

    public void setLogoMapSrc(String logoMapSrc) {
        this.logoMapSrc = logoMapSrc;
    }

    public String getLogoWhiteBgSrc() {
        return logoWhiteBgSrc;
    }

    public void setLogoWhiteBgSrc(String logoWhiteBgSrc) {
        this.logoWhiteBgSrc = logoWhiteBgSrc;
    }

    public String getLogoBlackBgSrc() {
        return logoBlackBgSrc;
    }

    public void setLogoBlackBgSrc(String logoBlackBgSrc) {
        this.logoBlackBgSrc = logoBlackBgSrc;
    }

    public String getLogoDetailSrc() {
        return logoDetailSrc;
    }

    public void setLogoDetailSrc(String logoDetailSrc) {
        this.logoDetailSrc = logoDetailSrc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBonusDescription() {
        return bonusDescription;
    }

    public void setBonusDescription(String bonusDescription) {
        this.bonusDescription = bonusDescription;
    }

    public List<BonusPercentEntity> getBonusPercentEntities() {
        return bonusPercentEntities;
    }

    public void setBonusPercentEntities(List<BonusPercentEntity> bonusPercentEntities) {
        this.bonusPercentEntities = bonusPercentEntities;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public String getVideoCode() {
        return videoCode;
    }

    public void setVideoCode(String videoCode) {
        this.videoCode = videoCode;
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(@Nullable String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public List<Long> getVendors() {
        return vendors;
    }

    public void setVendors(List<Long> vendors) {
        this.vendors = vendors;
        if (vendors != null) {
            setVendorsCount(vendors.size());
        } else {
            setVendorsCount(0);
        }
    }

    public Integer getVendorsCount() {
        return vendorsCount;
    }

    public void setVendorsCount(Integer vendorsCount) {
        this.vendorsCount = vendorsCount;
    }
}
