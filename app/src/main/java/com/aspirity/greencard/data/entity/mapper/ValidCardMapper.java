package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.response.ValidCardResponse;
import com.aspirity.greencard.domain.model.ValidCard;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class ValidCardMapper implements Function<ValidCardResponse, ValidCard> {
    @Override
    public ValidCard apply(@NonNull ValidCardResponse validCardResponse) {
        return new ValidCard(validCardResponse.isValid(), validCardResponse.isRegistered());
    }
}
