package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.PartnerEntity;
import com.aspirity.greencard.domain.model.Partner;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class ShortPartnerMapper implements Function<PartnerEntity, Partner> {

    @Override
    public Partner apply(@NonNull PartnerEntity partnerEntity) {
        return new Partner(
                partnerEntity.getId(),
                partnerEntity.getColor(),
                partnerEntity.getLogoSrc(),
                partnerEntity.getLogoMapSrc(),
                partnerEntity.getLogoWhiteBgSrc(),
                partnerEntity.getLogoBlackBgSrc(),
                partnerEntity.getLogoDetailSrc(),
                partnerEntity.getName()
        );
    }
}
