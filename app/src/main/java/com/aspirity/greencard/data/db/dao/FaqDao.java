package com.aspirity.greencard.data.db.dao;

import com.aspirity.greencard.data.entity.FaqEntity;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Single;

@Dao
public interface FaqDao {
    @Query("SELECT min(update_date) FROM faq")
    Single<Long> getListOldestDate();

    @Query("SELECT * FROM faq ORDER BY id DESC")
    Single<List<FaqEntity>> getFaqList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFaq(List<FaqEntity> faq);

    @Query("DELETE FROM faq")
    void deleteFaq();
}
