package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.FaqEntity;
import com.aspirity.greencard.domain.model.Faq;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class FaqMapper implements Function<FaqEntity, Faq> {

    @Override
    public Faq apply(@NonNull FaqEntity faqEntity) {
        return new Faq(faqEntity.getId(), faqEntity.getQuestion(), faqEntity.getAnswer());
    }
}
