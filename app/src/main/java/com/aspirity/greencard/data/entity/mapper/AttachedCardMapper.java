package com.aspirity.greencard.data.entity.mapper;

import com.aspirity.greencard.data.entity.response.AttachedCardResponse;
import com.aspirity.greencard.domain.model.AttachedCard;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class AttachedCardMapper implements Function<AttachedCardResponse, AttachedCard> {
    @Override
    public AttachedCard apply(@NonNull AttachedCardResponse attachedCardResponse) {
        return new AttachedCard(attachedCardResponse.isAttached(), attachedCardResponse.getSmsSentTo());
    }
}
