package com.aspirity.greencard.data.repository;


import com.aspirity.greencard.domain.repository.AppsRepository;
import com.aspirity.greencard.domain.repository.CardsRepository;
import com.aspirity.greencard.domain.repository.FaqRepository;
import com.aspirity.greencard.domain.repository.NotificationsRepository;
import com.aspirity.greencard.domain.repository.OperationsRepository;
import com.aspirity.greencard.domain.repository.PartnerRepository;
import com.aspirity.greencard.domain.repository.SharesRepository;
import com.aspirity.greencard.domain.repository.UserRepository;
import com.aspirity.greencard.domain.repository.VendorsRepository;
import com.aspirity.greencard.presentation.utils.rx.SchedulerProvider;

public class RepositoryProvider {
    // TODO: 30.09.2017 необходимо прокидывать сюда preference, realmService и retrofitService

    private static AppsRepository appsRepository;

    private static SharesRepository sharesRepository;

    private static UserRepository userRepository;

    private static VendorsRepository vendorsRepository;

    private static FaqRepository faqRepository;

    private static PartnerRepository partnerRepository;

    private static OperationsRepository operationsRepository;

    private static NotificationsRepository notificationsRepository;

    private static CardsRepository cardsRepository;

    public static AppsRepository getAppsRepository() {
        if (appsRepository == null) {
            appsRepository = new AppsDataRepository();
        }
        return appsRepository;
    }

    public static SharesRepository getSharesRepository() {
        if (sharesRepository == null) {
            sharesRepository = new SharesDataRepository();
        }
        return sharesRepository;
    }

    public static void setSharesRepository(SharesRepository repository) {
        sharesRepository = repository;
    }

    public static UserRepository getUserRepository() {
        if (userRepository == null) {
            userRepository = new UserDataRepository();
        }
        return userRepository;
    }

    public static void setUserRepository(UserRepository repository) {
        userRepository = repository;
    }

    public static VendorsRepository getVendorsRepository() {
        if (vendorsRepository == null) {
            vendorsRepository = new VendorsDataRepository();
        }
        return vendorsRepository;
    }

    public static void setVendorsRepository(VendorsRepository repository) {
        vendorsRepository = repository;
    }

    public static FaqRepository getFaqRepository() {
        if (faqRepository == null) {
            faqRepository = new FaqDataRepository();
        }
        return faqRepository;
    }

    public static void setFaqRepository(FaqRepository repository) {
        faqRepository = repository;
    }

    public static PartnerRepository getPartnerRepository(SchedulerProvider schedulerProvider) {
        if (partnerRepository == null) {
            partnerRepository = new PartnersDataRepository(schedulerProvider);
        }
        return partnerRepository;
    }

    public static void setPartnerRepository(PartnerRepository repository) {
        partnerRepository = repository;
    }

    public static OperationsRepository getOperationsRepository(SchedulerProvider schedulerProvider) {
        if (operationsRepository == null) {
            operationsRepository = new OperationsDataRepository();
        }
        return operationsRepository;
    }

    public static void setOperationsRepository(OperationsRepository repository) {
        operationsRepository = repository;
    }

    public static NotificationsRepository getnotificationsRepository(SchedulerProvider schedulerProvider) {
        if (notificationsRepository == null) {
            notificationsRepository = new NotificationsDataRepository();
        }
        return notificationsRepository;
    }

    public static CardsRepository getCardsRepository() {
        if (cardsRepository == null) {
            cardsRepository = new CardsDataRepository();
        }
        return cardsRepository;
    }

    public static void setCardsRepository(CardsRepository repository) {
        cardsRepository = repository;
    }
}
