package com.aspirity.greencard.data.repository;

import com.aspirity.greencard.data.db.DatabaseFactory;
import com.aspirity.greencard.data.db.dao.CountDao;
import com.aspirity.greencard.data.db.dao.ShareDao;
import com.aspirity.greencard.data.entity.CountEntity;
import com.aspirity.greencard.data.entity.ShareEntity;
import com.aspirity.greencard.data.entity.mapper.ShareMapper;
import com.aspirity.greencard.data.entity.mapper.ShortShareMapper;
import com.aspirity.greencard.data.entity.response.ListResponse;
import com.aspirity.greencard.data.network.RetrofitService;
import com.aspirity.greencard.data.network.ShareService;
import com.aspirity.greencard.domain.model.Chunk;
import com.aspirity.greencard.domain.model.Share;
import com.aspirity.greencard.domain.repository.SharesRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;


public class SharesDataRepository implements SharesRepository {
    private ShareService shareService = RetrofitService.createRetrofitClient(ShareService.class);
    private ShareDao shareDao = DatabaseFactory.getInstance().shareDao();
    private CountDao countDao = DatabaseFactory.getInstance().countDao();

    // MAIN SCREEN SHARES

    @Override
    public Single<Chunk<Share>> shares(int page, int pageSize) {
        return sharesForceRefresh(page);
    }

    @Override
    public Single<Chunk<Share>> sharesForceRefresh(int page) {
        Single<ListResponse<ShareEntity>> response = shareService
                .shares(page, 30, null, null, null, null)
                .cache();

        Single<List<ShareEntity>> entitiesSingle = response
                .map(ListResponse::getResults)
                .toFlowable()
                .toObservable()
                .flatMap(Observable::fromIterable)
                .doOnNext(entity -> entity.setArchived(false))
                .toList()
                .doOnSuccess(shareEntities -> {
                    if (page == 1)
                        shareDao.deleteShares(false);

                    shareDao.insertShares(shareEntities);
                });
        Single<Integer> countSingle = response
                .map(ListResponse::getTotalCount)
                .map(c -> new CountEntity(CountDao.SHARE_COUNT_ID, c))
                .doOnSuccess(countEntity -> {
                    if (page == 1)
                        countDao.insertCount(countEntity);
                })
                .map(CountEntity::getCount);

        Single<List<Share>> shares = entitiesSingle
                .toFlowable()
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new ShortShareMapper())
                .toList();

        return Single.zip(shares, countSingle, Chunk::new);
    }

    @Override
    public Single<Chunk<Share>> sharesFromDb(int page, int pageSize) {
        Single<List<Share>> shares = shareDao
                .getItems((page - 1) * pageSize, pageSize, false)
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new ShortShareMapper())
                .toList();

        Single<Integer> count = countDao
                .getCount(CountDao.SHARE_COUNT_ID)
                .map(CountEntity::getCount);

        return Single.zip(shares, count, Chunk::new);
    }

    // FILTERED SHARES

    @Override
    public Single<Chunk<Share>> shares(int page, String beginDate, String endDate, String sort, List<Long> vendors) {
        Single<ListResponse<ShareEntity>> response = shareService
                .shares(page, 30, beginDate, endDate, sort, vendors)
                .cache();

        Single<Integer> count = response.map(ListResponse::getTotalCount);
        Single<List<Share>> shares = response
                .map(ListResponse::getResults)
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new ShortShareMapper())
                .toList();

        return Single.zip(shares, count, Chunk::new);
    }

    // SHARES OF PARTNER

    @Override
    public Single<Chunk<Share>> sharesOfPartner(Long partnerId) {
        return sharesOfPartnerForceRefresh(partnerId);
    }

    //@Override
    public Single<Chunk<Share>> sharesOfPartnerFromDb(Long partnerId) {
        return shareDao
                .getItemsOfPartner(partnerId)
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new ShortShareMapper())
                .toList()
                .map(list -> new Chunk<>(list, list.size()));
    }

    private Single<Boolean> didLoadAllItems() {
        return Single.zip(
                countDao.getCount(CountDao.SHARE_COUNT_ID),
                shareDao.getShareCount(),
                (countEntity, currentAmount) -> countEntity.getCount() == currentAmount
        );
    }

    @Override
    public Single<Chunk<Share>> sharesOfPartnerForceRefresh(long partnerId) {
        Single<ListResponse<ShareEntity>> response = shareService
                .sharesOfPartner(partnerId, null, null)
                .cache();

        Single<Integer> count = response.map(ListResponse::getTotalCount);
        Single<List<Share>> shares = response
                .map(ListResponse::getResults)
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new ShortShareMapper())
                .toList();

        return Single.zip(shares, count, Chunk::new);
    }

    @Override
    public Single<Chunk<Share>> archivedShares(int page) {
        return archivedSharesForceRefresh(page);
    }

    @Override
    public Single<Chunk<Share>> archivedSharesFromDb(int page, int pageSize) {
        Single<List<Share>> shares = shareDao
                .getItems(page, pageSize, true)
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new ShortShareMapper())
                .toList();

        Single<Integer> count = countDao
                .getCount(CountDao.SHARE_ARCHIVE_COUNT_ID)
                .map(CountEntity::getCount);

        return Single.zip(shares, count, Chunk::new);
    }

    @Override
    public Single<Chunk<Share>> archivedSharesForceRefresh(int page) {
        Single<ListResponse<ShareEntity>> response = shareService
                .archive(1, 30)
                .cache();

        Single<List<ShareEntity>> entitiesSingle = response
                .map(ListResponse::getResults)
                .toFlowable()
                .toObservable()
                .flatMap(Observable::fromIterable)
                .doOnNext(entity -> entity.setArchived(true))
                .toList()
                .doOnSuccess(shareEntities -> {
                    /*if (page == 1) {
                        shareDao.deleteShares(true);
                    }
                    shareDao.insertShares(shareEntities);*/
                });
        Single<Integer> countSingle = response
                .map(ListResponse::getTotalCount)
                .map(c -> new CountEntity(CountDao.SHARE_ARCHIVE_COUNT_ID, c))
                .doOnSuccess(countEntity -> {
                    /*if (page == 1) {
                        countDao.insertCount(countEntity);
                    }*/
                })
                .map(CountEntity::getCount);

        Single<List<Share>> shares = entitiesSingle
                .toFlowable()
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new ShortShareMapper())
                .toList();

        return Single.zip(shares, countSingle, Chunk::new);
    }

    // SINGLE SHARE


    @Override
    public Single<Share> archivedShare(long shareId) {
        return archivedShareForceRefresh(shareId);
    }

    private Single<Share> archivedShareForceRefresh(long shareId) {
        return shareService
                .archivedShare(shareId)
                .doOnSuccess(apiShare -> {
                    apiShare.setArchived(true);
                    shareDao.updateShare(apiShare);
                })
                .map(new ShareMapper());
    }

    @Override
    public Single<Share> share(long shareId) {
        return shareForceRefresh(shareId);
    }

    @Override
    public Single<Share> shareForceRefresh(long shareId) {
        return shareService
                .share(shareId)
                .doOnSuccess(apiShare -> {
                    apiShare.setArchived(false);
                    shareDao.updateShare(apiShare);
                })
                .map(new ShareMapper());
    }

    //@Override
    public Single<Share> shareFromDb(long shareId) {
        return shareDao
                .getItem(shareId)
                .map(new ShareMapper());
    }

    private Single<ShareEntity> tryShareFromDb(long shareId) {
        return shareDao
                .getItem(shareId)
                .onErrorResumeNext(error -> {
                    ShareEntity entity = new ShareEntity();
                    entity.setId(shareId);
                    entity.setUpdateDate(0L);
                    return Single.just(entity);
                });
    }
}
