package com.aspirity.greencard.data.entity.response;


import com.google.gson.annotations.SerializedName;

import androidx.annotation.Nullable;

public class ValidCardResponse {

    @SerializedName("is_valid")
    private Boolean isValid;

    @SerializedName("is_registered")
    private Boolean isRegistered;

    @Nullable
    @SerializedName("non_field_errors")
    private String errorMessage;

    public Boolean isValid() {
        return isValid;
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public Boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(Boolean registered) {
        isRegistered = registered;
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(@Nullable String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
