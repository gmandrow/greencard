package com.aspirity.greencard.data.repository;

import com.aspirity.greencard.data.db.DatabaseFactory;
import com.aspirity.greencard.data.db.dao.CardDao;
import com.aspirity.greencard.data.entity.mapper.CardMapper;
import com.aspirity.greencard.data.entity.mapper.ValidCardMapper;
import com.aspirity.greencard.data.entity.mapper.VirtualCardMapper;
import com.aspirity.greencard.data.entity.response.AttachedCardResponse;
import com.aspirity.greencard.data.entity.response.ChangedCardResponse;
import com.aspirity.greencard.data.entity.response.ListResponse;
import com.aspirity.greencard.data.network.CardService;
import com.aspirity.greencard.data.network.RetrofitService;
import com.aspirity.greencard.domain.model.Card;
import com.aspirity.greencard.domain.model.ValidCard;
import com.aspirity.greencard.domain.model.VirtualCard;
import com.aspirity.greencard.domain.repository.CardsRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class CardsDataRepository implements CardsRepository {
    private final CardService cardService = RetrofitService.createRetrofitClient(CardService.class);
    private final CardDao cardDao = DatabaseFactory.getInstance().cardDao();

    @Override
    public Single<ValidCard> validateCard(String cardNumber, String cardCode) {
        return cardService.validate(cardNumber, cardCode)
                .map(new ValidCardMapper());
    }

    @Override
    public Single<Boolean> verifyAttachedCard(Long userId, String smsCode) {
        return cardService.verifyAttachedCard(userId, smsCode)
                .map(AttachedCardResponse::isAttached);
    }

    @Override
    public Single<String> attachCard(Long userId) {
        return cardService.cards(userId)
                .map(AttachedCardResponse::getSmsSentTo);
    }

    @Override
    public Single<Boolean> cardChangeState(Long cardId, String password) {
        return cardService.cardChangeState(cardId, password)
                .map(ChangedCardResponse::isChanged);
    }

    @Override
    public Single<List<Card>> getCards() {
        return getCardsForceRefresh();
    }

    //@Override
    public Single<List<Card>> getCardsFromDb() {
        return cardDao
                .getCards()
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new CardMapper())
                .toList();
    }

    @Override
    public Single<List<Card>> getCardsForceRefresh() {
        return cardService.getCards()
                .map(ListResponse::getResults)
                .doOnSuccess(cardDao::insertCards)
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(new CardMapper())
                .toList();
    }

    @Override
    public Single<VirtualCard> getVirtualCard() {
        return cardService.getVirtualCard()
                .map(new VirtualCardMapper());
    }
}
