package com.aspirity.greencard.data.network;


import com.aspirity.greencard.data.entity.VendorEntity;
import com.aspirity.greencard.data.entity.response.ListResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface VendorService {

    /**
     * Get vendors
     *
     * @param page     - page of vendors - it may is null
     * @param pageSize - count of vendors on one page - it may is null
     * @return list of vendors
     */
    @GET("vendors/")
    Single<ListResponse<VendorEntity>> vendors(
            @Query("page") Integer page,
            @Query("page_size") Integer pageSize);

    /**
     * Get vendors
     *
     * @param page     - page of vendors - it may is null
     * @param pageSize - count of vendors on one page - it may is null
     * @return list of vendors
     */
    @GET("vendors/")
    Single<ListResponse<VendorEntity>> vendorsOfPartner(
            @Query("partner_id") Long partnerId,
            @Query("page") Integer page,
            @Query("page_size") Integer pageSize);

    /**
     * Get one vendor
     *
     * @param vendorId - id of vendor
     * @return vendor
     */
    @GET("vendors/{vendor_id}")
    Single<VendorEntity> vendor(
            @Path("vendor_id") Long vendorId);
}
